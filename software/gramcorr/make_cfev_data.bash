#!/bin/sh
#
# Create error data and ibases from 4 data sets.
# 1.00 2013-11-06 Initial version
#
TRAINFILE="utexas.10e6.dt.cf05.100000"   #with errors
ORIGFILE="utexas.10e6.dt.100000"         #w/o errors
ORIGTESTFILE="utexas.t1000.dt"
TESTFILE="utexas.t1000.dt.cf05" 
#
if test $# -lt 3
then
  echo "Supply ERRORTRAINFILE, ORIGTRAINFILE, PARAMS and ID as arguments."
  exit
fi
#
TRAINFILE=$1 #with errors
ORIGFILE=$2  #w/o errors
ID=$3
#
WOPR="wopr"
SCRIPT="make_cfev_data.wopr_script"
KVS="kvs.${ID}"
OUT="output.${ID}"
#
TIMBL="-a1 +D"
LC=2
RC=2
T="it:1"
#
if [ ! -e "${TRAINFILE}" ]
then
    echo "${TRAINFILE} nor found, aborting."
    exit 1
fi
if [ ! -e "${ORIGFILE}" ]
then
    echo "${ORIGFILE} nor found, aborting."
    exit 1
fi
#
echo "Running with these files:"
echo " ORIGFILE  ${ORIGFILE}"
echo " ERRORFILE ${TRAINFILE}"

BLOB=`${WOPR} -V | cut -c14-`
echo "${BLOB}" > ${OUT}

# Run all
#echo "Running script ${SCRIPT}"
#${WOPR} -s ${SCRIPT} -p trainset:${TRAINFILE},rc:${RC},lc:${LC},id:${ID},timbl:"${TIMBL}",origtrain:${ORIGFILE},${T},kvsfile:${KVS} >>${OUT}
#
SCRIPT="lexicon,window_lr,make_ibase,kvs"
${WOPR} -l -r ${SCRIPT} -p filename:${TRAINFILE},rc:${RC},lc:${LC},id:${ID},timbl:"${TIMBL}",targetfile:${ORIGFILE},${T},kvsfile:${KVS} >>${OUT}