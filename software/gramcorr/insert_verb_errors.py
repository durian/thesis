#!/usr/bin/python
# -*- coding: utf-8 -*-
#
import getopt
import sys
import time
import os
import datetime
import random
import re
import platform
from operator import itemgetter
import operator
import codecs
try:
    from collections import Counter
except:
    pass
from pattern.en import *

"""
Takes a plain text file, and insert errors.

Disadvantage: won't change "gave" into "gived".

We need an extra list with irregular verbs, and create regular past
tenses. (Confusibles)

1.22 2013-11-06 Less verbose NOALT, len(vf.split) instead of no_spc(vf)
1.21 2013-11-06 Added len() check.
1.20 2013-11-06 Added has_spc() fix
1.11 2013-11-06 Some tweaks, checking of lengths
1.10 2013-11-06 Added codecs/utf8
1.00 2013-11-06 First version.
"""

idx       = "00"
afile     =  None
chance    =  0.5 #chance we change a verb (if random < change )

try:
    opts, args = getopt.getopt(sys.argv[1:], "c:f:i:", ["file="])
except getopt.GetoptError, err:
    print str(err)
    sys.exit(1)
for o, a in opts:
    if o in ("-f", "--file="):
        afile = a 
    elif o in ("-c"): 
        chance = float(a)
    elif o in ("-i"): 
        idx = a
    else:
        assert False, "unhandled option"

"""
Determine is we are dealing with don't, won't, &c.
"""
def neg(form):
    if len(form) > 3 and form[-3:] == "n't":
        return True
    elif len(form) > 3 and form[-4:] == " not":
        return True
    return False

def has_spc(form):
    """
    We need to filter out these:
    CHANGE 32 VBZ isn't wasn't [u'am not', u"aren't", u"wasn't", u"weren't"]
    otherwise we can change one word to two!
    """
    if ' ' in form:
        return True
    return False

f_path, f_name = os.path.split(afile)
afile_outs = f_name+".cf"+idx # with errors
afile_logs = f_name+".cf"+idx+".log" # with errors
print "Error file:", afile_outs
print "Log file:", afile_logs
r = random.Random()
made_changes = 0
poss_changes = 0
no_alts      = 0
changed = {}

with codecs.open(afile, "r", encoding="utf-8") as f:
    with codecs.open(afile_outs, "w", encoding="utf-8") as fo: #file out
        with codecs.open(afile_logs, "w",encoding="utf-8") as lo: #log out
            for line in f:
                lo.write( "O "+line )
                line = line[:-1]
                if line == "":
                    print "EMPTY LINE"
                    continue
                #print line
                wordtags = tag(line,tokenize=False)
                newline = []
                i = 0
                last_pos = -2
                for w,t in wordtags:
                    if t[0] == 'V':
                        if w[-2:] == "'s" or w[0].isupper():
                            """
                            Ignore forms like "that's", and capitalised forms.
                            """
                            newline.append(w)
                            continue
                        #print w,t, lexeme(w) #or conjugate
                        poss_changes += 1
                        if random.uniform(0, 1) > chance:
                            """
                            If random <= chance, we change, otherwise
                            add the original verb, unchanged.
                            """
                            newline.append(w)
                            lo.write("POSSIBLE "+str(i)+" "+t+" "+w+"\n")
                            continue
                        if last_pos == (i - 1):
                            """
                            Don't do two in a row. Append the original verb,
                            and continue.
                            """
                            lo.write("SKIPCONS "+str(i)+" "+t+" "+w+"\n")
                            newline.append(w)
                            continue
                        isneg = neg(w)
                        vforms = lexeme(w) 
                        alts = [vf for vf in vforms if neg(vf) == isneg and vf != w and len(vf.split()) == 1 ] #same negativity and not word itself
                        if alts:
                            alt = random.choice(alts)
                            #print w, t, isneg, alt, alts
                            #print "CHANGE",w,alt
                            lo.write("CHANGE "+str(i)+" "+t+" "+w+" "+alt+" "+repr(alts)+"\n")
                            made_changes += 1
                            newline.append(alt)
                            last_pos = i
                        else:
                            no_alts += 1
                            lo.write("NOALT "+str(i)+" "+t+" "+w+"\n")
                            newline.append(w)
                    else:
                        newline.append(w)
                    i += 1
                if len(line.split()) != len( (" ".join(newline)).split() ):
                    print "ERROR: unequal line/newline", len(line.split()),  len( (" ".join(newline)).split() )
                    print line
                    sys.exit(1)
                fo.write( " ".join(newline)+"\n" )
                lo.write( "N "+" ".join(newline)+"\n" )

print "Made", made_changes, "changes out of", poss_changes, "possible changes,", no_alts, "no alternatives."
print "Chance", chance
