#!/usr/bin/python
# -*- coding: utf-8 -*-
#
import getopt
import sys
import time
import os
import datetime
import random
import re
import platform
from operator import itemgetter
import operator
try:
    from collections import Counter
except:
    pass

"""
2014-06-11 First version

EXAMPLE:
Takes a plain text file, and inserts confusibles/errors,
generates an m2 scoring file on the side.

python insert_confusibles2_m2.py -f utexas.10e6.dt3.ucto.t1000 -c preps_for_icm2.txt

cat testzin.txt testcats.txt testzin.txt.cc000 testzin.txt.cc000.m2

I am on the chair under the roof .

Prep Prep INS:2 of on under above   <--- Do 2 INSERTIONS IN THE TEXT


I of am on the chair under the roof above .

(The m2 file:)
S I of am on the chair under the roof above .   <--- Source, from test set, with errors
(0 1  2  3  4   5     6     7   8    9     10)  <--- positions
A 2 3|||Prep||||||REQUIRED|||-NONE-|||0         <--- delete "of", eh "am"
A 9 10|||Prep||||||REQUIRED|||-NONE-|||0        <--- delete "above"

python examine_m2.py -f testzin.txt.cc000.m2
Prep 2 REP   0 0.00 DEL   2 100.00 RPL   0 0.00 INS   0 0.00   <--- The m2 contains two DEL actions

SO THESE NEED TO BE INVERSED!
To get two deletes in the m2 file, specify two inserts!

The "-i" option inverts the stats if you take them from another m2 file.
"""

print "VERSION 1.00"

afile      = None
cfile      = None
skip       = 0   # errors every Nth line, 0 & 1 is in all lines
classname  = None
n          = 0
invert     = False # swaps DEL-INS, INS-DEL, REP-REP

try:
    opts, args = getopt.getopt(sys.argv[1:], "c:C:f:in:s:", ["file="])
except getopt.GetoptError, err:
    print str(err)
    sys.exit(1)
for o, a in opts:
    if o in ("-f", "--file="):
        afile = a 
    elif o in ("-c"): 
        cfile = a
    elif o in ("-i"): 
        invert = True
    elif o in ("-C"): 
        classname = a #ArtOrDet, ...
    elif o in ("-s"): 
        skip = int(a)
    elif o in ("-n"): 
        n = int(a)
    else:
        assert False, "unhandled option"

# ArtOrDet INS:1,REP:5,DEL:4 a all an another any both each either ...

f_path, f_name = os.path.split(afile)
afile_outs = f_name+".cc"+'{0:03n}'.format( n ) # with errors # id_str = '{0:03n}'.format( idx )
m2file = afile_outs+".m2"

inverter = {"DEL":"INS", "REP":"REP", "INS":"DEL"}
m2classes = {}
classes   = []
class_members = {}
class_ops = {}
class_totals = {} #absolute number of requested changes
# ArtOrDet ArtOrDet INS:1,REP:5,DEL:4 a all an another any both each either e
with open(cfile, "r") as f:
    for line in f:
        if len(line) < 2 or line == "" or line[0] == "#":
            continue
        line = line[:-1]
        words = line.split()
        classname   = words[0] #so we an have subclasses
        class_members[classname] = []
        class_ops[classname] = []
        m2classname = words[1] #with different ratios, still have same m2 edit class
        classes.append(classname)
        m2classes[classname] = m2classname #mapping to m2class
        opsf = words[2].split(",") # REP:10,INS:4
        ops = []
        ops_count = 0
        for opf in opsf:
            (op,f) = opf.split(":") #REP:10 -> (REP 10)
            if invert:
                op = inverter[op]
            ops += [op] * int(f)
            ops_count += int(f)
        class_totals[classname] = ops_count
        for c in words[3:]: #go over the list
            class_members[classname].append(c)
            class_ops[classname] = ops
print "classes", repr(classes)
print "m2classes", repr(m2classes)
print "class_members", class_members
print "class_ops", class_ops
print "class_totals", class_totals

r = random.Random()
made_changes = 0
poss_changes = 0
skipped_poss_changes = 0
changed = {}
skip_cnt = skip
"""
INS insert one, creates an unnecessary/redundant word
DEL delete one, missing
REP replace one
"""

class_member_counts = {}
possible_inserts = {}
possible_deletes = {}
possible_replaces = {}
class_stats = {} #count total number of class members in the text
m2class_stats = {} 
#pass one, examine the whole file, count class members.
ln = 0
with open(afile, "r") as f:
    for line in f:
        print line[:-1]
        line = line[:-1]
        words = line.split()
        indexes = [] #list of index->word which we can errorify
        idx = 0
        cmlist = [] #list with True/False if class member
        for word in words:
            # check each class if it is a member
            was_class_member = False
            for c in classes:
                if word in class_members[c]:
                    was_class_member = True
                    try:
                        class_stats[c] += 1
                    except KeyError:
                        class_stats[c] = 1
                    try:
                        m2class_stats[m2classes[c]] += 1
                    except KeyError:
                        m2class_stats[m2classes[c]] = 1
                    h = (ln, idx, word, c)
                    print h
                    # possible edits does not include INS....
                    try:
                        class_member_counts[c] += 1
                        possible_deletes[c].append(h)
                        possible_replaces[c].append(h)
                    except KeyError:
                        class_member_counts[c] = 1
                        possible_deletes[c] = [h]
                        possible_replaces[c] = [h]
            cmlist.append(was_class_member)
            idx += 1
        print cmlist
        # here we do the inserts. Not per class, we don't want to do them
        # double, all inserts positions are valid for all classes.
        for pos in xrange(1, len(cmlist)):
            if cmlist[pos-1:pos+1] == [False, False]:
                c = "INS"
                h = (ln, pos, words[pos], c) #insert BEFORE the pos
                print h
                try:
                    possible_inserts[c].append(h)
                except KeyError:
                    possible_inserts[c] = [h]
        ln += 1
print "class_member_counts", repr(class_member_counts)
print "class_stats", class_stats
print "m2class_stats", m2class_stats

# now choose Right number of edits from all possible, and do them in pass 2
# e.g PrepU we want REP:1 only, this is stored in class_ops

todo_edits = []

'''
c = "PrepL"
print "possible_inserts",  possible_inserts["INS"]
print "possible_deletes",  possible_deletes[c]
print "possible_replaces", possible_replaces[c]
print "class_ops", c, class_ops[c]
for op in class_ops[c]:
    print c, op
    if op == "INS":
        print random.choice( possible_inserts["INS"] )
    elif op == "DEL":
        print random.choice( possible_deletes[c] )
    else: #REP
        print random.choice( possible_replaces[c] )
'''

for c in classes:
    for op in class_ops[c]:
        print c, op
        if op == "INS":
            try:
                rc = random.choice( possible_inserts["INS"] )
                possible_inserts["INS"].remove( rc )
                rcnew = ( rc[0], rc[1], "INS", c)  #now we put class here!!
                todo_edits.append( rcnew )
                print possible_inserts["INS"]
            except KeyError:
                rc = None
            except IndexError:
                rc = None
        elif op == "DEL":
            try:
                rc = random.choice( possible_deletes[c] )
                possible_deletes[c].remove( rc )
                possible_replaces[c].remove( rc ) #remove others also, otherwise double edits on same pos
                rcnew = ( rc[0], rc[1], "DEL", c)
                todo_edits.append( rcnew )

                print possible_deletes[c]
            except KeyError:
                rc = None
            except IndexError:
                rc = None
        else: #REP
            try:
                rc = random.choice( possible_replaces[c] )
                possible_replaces[c].remove( rc )
                possible_deletes[c].remove( rc ) #remove others also, otherwise double edits on same pos
                rcnew = ( rc[0], rc[1], "REP", c)
                todo_edits.append( rcnew )
                print possible_replaces[c]
            except KeyError:
                rc = None
            except IndexError:
                rc = None
        print rc

#print todo_edits
todo_edits = sorted( todo_edits, key=lambda tup:(tup[0], tup[1]), reverse=True )
print "number of edits:", len(todo_edits)
for e in todo_edits:
    print e

# Now go through text, apply the chosen edits.

changed_class_stats = {}
changed_m2class_stats = {}
ln = 0
e = todo_edits.pop()
with open(afile, "r") as f:
    with open(afile_outs, "w") as fo:
        with open(m2file, "w") as fo_m2:
            for line in f:
                print line[:-1]
                line = line[:-1]
                words = line.split()
                new_words = []
                idx = 0
                annotations = []
                for word in words:
                    to_add = word #add this if no edit action
                    if e and ln == e[0] and idx == e[1]:
                        print e
                        edt = e[2] #edit actions, INS, DEL,...
                        cls = e[3] #the class (not the m2class!)
                        try:
                            changed_class_stats[cls] += 1 #inserts are counted here too
                        except KeyError:
                            changed_class_stats[cls] = 1
                        try:
                            changed_m2class_stats[m2classes[cls]] += 1 #inserts are counted here too
                        except KeyError:
                            changed_m2class_stats[m2classes[cls]] = 1
                        #
                        if edt == "INS": #INS here is a redundant in the text
                            to_add = random.choice( class_members[cls] ) + " " + word
                            m2cls = m2classes[cls]
                            # A 24 25|||Wci|||drawing near|||REQUIRED|||-NONE-|||0
                            astr = "A "+str(idx)+" "+str(idx+1)+"|||"+m2cls+"||||||REQUIRED|||-NONE-|||0"
                            annotations.append( astr )
                        elif edt == "DEL":
                            to_add = ""
                            #A 15 16|||ArtOrDet||||||REQUIRED|||-NONE-|||0
                            m2cls = m2classes[cls]
                            # the m2 bit is an insert of the deleted word
                            # two ways, with or without the original
                            #astr = "A "+str(idx-1)+" "+str(idx)+"|||"+m2cls+"|||"+words[idx-1]+" "+words[idx]+"|||REQUIRED|||-NONE-|||0"
                            astr = "A "+str(idx)+" "+str(idx)+"|||"+m2cls+"|||"+words[idx]+"|||REQUIRED|||-NONE-|||0"
                            print astr
                            annotations.append( astr )
                        else: #REP
                            others = [x for x in class_members[cls] if x != word]
                            new_w = random.choice( others )
                            to_add = new_w
                            m2cls = m2classes[cls]
                            #A 4 5|||ArtOrDet|||the|||REQUIRED|||-NONE-|||0
                            astr = "A "+str(idx)+" "+str(idx+1)+"|||"+m2cls+"|||"+words[idx]+"|||REQUIRED|||-NONE-|||0"
                            annotations.append( astr )
                        # get next edit seq
                        try:
                            e = todo_edits.pop()
                        except IndexError:
                            e = None
                    new_words.append(to_add)
                    idx += 1
                # The test file sentence
                new_words_str = " ".join(new_words)
                new_words = new_words_str.split() #get rid of double spaces etc.
                fo.write( " ".join(new_words)+"\n" )
                # The m2 file
                fo_m2.write( "S "+" ".join(new_words)+"\n" ) #the fline with errors
                for astr in annotations:
                    fo_m2.write( astr+"\n" )
                fo_m2.write( "\n" )
                ln += 1

print "changed_class_stats / total class count"
for c in changed_class_stats:
    print c, changed_class_stats[c], "/", class_stats[c], '{0:.2f}'.format(changed_class_stats[c] / float(class_stats[c]) * 100.0)
print "changed_m2class_stats / total m2class count"
for c in changed_m2class_stats:
    print c, changed_m2class_stats[c], "/", m2class_stats[c], '{0:.2f}'.format(changed_m2class_stats[c] / float(m2class_stats[c]) * 100.0)
print "Output in", afile_outs, "and", m2file
