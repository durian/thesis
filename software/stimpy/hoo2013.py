#!/usr/bin/python
# -*- coding: utf-8 -*-
#
from stimpy import *
import getopt
import sys
import time
import datetime
import random
import platform
from operator import itemgetter
import operator
#
import logging
import logging.handlers
#
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)-8s %(message)s')
#
rtfile  = logging.handlers.RotatingFileHandler("./hoo2013.log",maxBytes=1024*1024*10, backupCount=5)
console = logging.StreamHandler()
#
glog = logging.getLogger('HOO')
rtformat='%(asctime)s %(levelname)-8s %(message)s'
rtformatter = logging.Formatter(fmt=rtformat)
rtfile.setFormatter(rtformatter)
glog.addHandler(rtfile)
glog.propagate = False #stop console log for this one
# Set up console
cformat='%(asctime)s %(message)s'
cformatter = logging.Formatter(fmt=cformat, datefmt="%Y%m%d %H:%M:%S")
console.setFormatter(cformatter)
console.setLevel(logging.INFO)
glog.addHandler(console)
'''
durian:m2scorer pberck$ python scripts/m2scorer.py  system_pb example/source_gold 

in /Users/pberck/uvt/hoo2013/release2.2/data_5types:
gold training data: conll13st-preprocessed.5types.m2

'''
#
glog.info("HOO2013 starting.")
glog.info("See hoo2013.log for debug info.")
#
have_nltk = False
have_inflect = False
have_en = False
try:
    glog.info("Loading nltk library.")
    import nltk
    from nltk import WordNetLemmatizer as wnl
    have_nltk = True
except:
    glog.error("No NLTK toolkit available.")
have_inflect = False
try:
    glog.info("Loading inflect library.")
    import inflect
    ie = inflect.engine()
    have_inflect = True
except:
    glog.error("No inflect available.")
try:
    glog.info("Loading notebox.net linguistic library.")
    import en
    have_en = True
except:
    glog.error("No notebox.net available.")

# ----
# https://pypi.python.org/pypi/inflect
'''
durian:software pberck$ pip install inflect
Downloading/unpacking inflect
  Downloading inflect-0.2.3.zip (97Kb): 97Kb downloaded
  Running setup.py egg_info for package inflect
Installing collected packages: inflect
  Running setup.py install for inflect
Successfully installed inflect
>>> p = inflect.engine()
>>> print p.plural("idea")
ideas
>>> print p.plural("ideas")
idea
>>> print p.plural("books")
book
>>> print p.plural("book")
books
>>> print p.singular_noun("ideas")
idea
>>> print p.singular_noun("idea")
False
>>> print p.a("thing"), " or ", p.a("idea")
a thing  or  an idea
>>> print p.an("thing"), " or ", p.a("idea")
a thing  or  an idea

scootaloo:
pberck@scootaloo:/scratch/pberck/hoo2013$ pip install --user inflect
'''

'''
PennTreeBank tags:
VB - Verb, base form
VBD - Verb, past tense
VBG - Verb, gerund or present participle
VBN - Verb, past participle
VBP - Verb, non-3rd person singular present
VBZ - Verb, 3rd person singular present

NN - Noun, singular or mass
NNS - Noun, plural
NNP - Proper noun, singular
NNPS - Proper noun, plural

['he', 'walks', 'in', 'the', 'park', '.']
[('he', 'PRP'), ('walks', 'VBZ'), ('in', 'IN'), ('the', 'DT'), ('park', 'NN'), ('.', '.')]
['he', 'walk', 'in', 'the', 'park', '.']

['ball', 'balls', 'park', 'parks']
[('ball', 'NN'), ('balls', 'NNS'), ('park', 'VBP'), ('parks', 'NNS')]
['ball', 'ball', 'park', 'park']

['a', 'nice', 'ideas', '.']
[('a', 'DT'), ('nice', 'JJ'), ('ideas', 'NNS'), ('.', '.')]
['a', 'nice', 'idea', '.']
'''
    
# ----

# "one" =>[ (one 0 2) ]
def split_span(s):
    for match in re.finditer(r"\S+", s):
        span = match.span()
        yield match.group(0), span[0], span[1] - 1 # remove -1 to get pointer on char after

# specialised subroutines to find errors

#                     word
#                     |  index
#                     |  |    instances
def check_missingprep(w, idx, wl):
    glog.debug( "check_missingprep("+w+", "+str(idx)+")" )
    # don't try to predict a PREP if we have a PREP
    if w in PREPS:
        glog.debug( "Already a PREP." )
        return None 
    c = s1.get_classifier_by_name("preps_np")
    if c:
        glog.debug( "Classifier: "+c.name+" START" )
        res = c.classify_i(wl[c.ctx][idx])
        if not res:
            glog.error("Classifier "+c.name+" returned None.")
            sys.exit(8)
        # check distr size
        if len(c.distr) > PARAMS['MP_DS']:
            glog.debug("Distribution larger dan MP_DS")
            return None
        ans = ans_to_dict( res )
        glog.debug( str(ans) )
        posneg_r = ans['posneg_r'] 
        if ans['classification'] == "+" and posneg_r > PARAMS["MP_PNR"]:
            glog.debug( "posneg_r > "+str(PARAMS["MP_PNR"])+": "+str(ans['posneg_r']) )
            c1 = s1.get_classifier_by_name("preps")
            if c1:
                glog.debug( "Classifier: "+c1.name+" START" )
                res = c1.classify_i(wl[c1.ctx][idx])
                if not res:
                    glog.error("Classifier "+c1.name+" returned None.")
                    sys.exit(8)
                ans = ans_to_dict( res )
                glog.debug( str(ans) )
                return (ans["classification"], idx, idx + 1)
        glog.debug( "Classifier: "+c.name+" END" )
    else:
        glog.error( "Classifier: preps_np NOT FOUND" )
    return None

#                     word
#                     |  index
#                     |  |    instances
def check_replaceprep(w, idx, wl):
    glog.debug( "check_replaceprep("+w+", "+str(idx)+")" )
    # only if PREP
    if not w in PREPS:
        glog.debug( "Not a PREP." )
        return None 
    c = s1.get_classifier_by_name("preps")
    if c:
        glog.debug( "Classifier: "+c.name+" START" )
        res = c.classify_i(wl[c.ctx][idx])
        if not res:
            glog.error("Classifier "+c.name+" returned None.")
            sys.exit(8)
        if len(c.distr) > PARAMS['RP_DS']:
            glog.debug("Distribution larger dan RP_DS")
            return None
        ans = ans_to_dict( res )
        glog.debug( str(ans) )
        glog.debug("Best alternative: "+str(ans['topn'][0]))
        best_alt_freq = float(ans['topn'][0][1])
        if not best_alt_freq > PARAMS["RP_F"]:
            glog.debug("Frequency of top classification <= RP_F")
            return None
        # check if the original prep is in the distr
        w_in_distr = c.in_distr(w)
        w_freq = 0.001
        if w_in_distr:
            glog.debug("Found in distribution: "+str(w_in_distr))
            w_freq = float(w_in_distr[1])
        glog.debug("Ratio: "+str(best_alt_freq / w_freq))
        if (best_alt_freq / w_freq) <= PARAMS["RP_R"]:
            glog.debug( "Ratio between classification and original <= RP_R" )
            return None
        return (ans['topn'][0][0], idx, idx + 1) 
    else:
        glog.error( "Classifier: preps NOT FOUND" )
    return None

#                         word
#                         |  index
#                         |  |    instances
def check_unnecessaryprep(w, idx, wl):
    glog.debug( "check_unnecessaryprep("+w+", "+str(idx)+")" )
    # only if PREP
    if not w in PREPS:
        glog.debug( "Not a PREP." )
        return None 
    c = s1.get_classifier_by_name("preps_np")
    if c:
        glog.debug( "Classifier: "+c.name+" START" )
        res = c.classify_i(wl[c.ctx][idx])
        if not res:
            glog.error("Classifier "+c.name+" returned None.")
            sys.exit(8)
        ans = ans_to_dict( res )
        glog.debug( str(ans) )
        posneg_r = ans['posneg_r']
        if ans['classification'] == "-" and posneg_r > PARAMS["UP_NPR"]:
            glog.debug( "posneg_r > "+str(PARAMS["UP_NPR"])+": "+str(ans['posneg_r']) )
            return ("_", idx, idx + 1) #how to return a "remove" ?
    else:
        glog.error( "Classifier: dets_np NOT FOUND" )
    return None


# ----------


#RP_DS = 20 #distribution size must be less than this
#RP_F  = 20 #frequency of top classification must be larger than this
#RP_R  = 20 #ratio between classification and original
#                    word
#                    |  index
#                    |  |    instances
def check_missingdet(w, idx, wl):
    glog.debug( "check_missingdet("+w+", "+str(idx)+")" )
    # don't try to predict a DET if we have a DET
    if w in DETS:
        glog.debug( "Already a DET." )
        return None #(w, idx, idx + 1)
    c = s1.get_classifier_by_name("dets_np")
    if c:
        glog.debug( "Classifier: "+c.name+" START" )
        res = c.classify_i(wl[c.ctx][idx])
        if not res:
            glog.error("Classifier "+c.name+" returned None.")
            sys.exit(8)
        # check distr size
        if len(c.distr) > PARAMS['MD_DS']:
            glog.debug("Distribution larger dan MD_DS")
            return None
        ans = ans_to_dict( res )
        glog.debug( str(ans) )
        posneg_r = ans['posneg_r']
        if ans['classification'] == "+" and posneg_r > PARAMS["MD_PNR"]:
            glog.debug( "posneg_r > "+str(PARAMS["MD_PNR"])+": "+str(ans['posneg_r']) )
            c1 = s1.get_classifier_by_name("dets")
            if c1:
                glog.debug( "Classifier: "+c1.name+" START" )
                res = c1.classify_i(wl[c1.ctx][idx])
                if not res:
                    glog.error("Classifier "+c1.name+" returned None.")
                    sys.exit(8)
                ans = ans_to_dict( res )
                glog.debug( str(ans) )
                return (ans["classification"], idx, idx + 1)
        glog.debug( "Classifier: "+c.name+" END" )
    else:
        glog.error( "Classifier: preps_np NOT FOUND" )
    return None #(w, idx, idx + 1)

#RD_DS = 20   # RD distribution size must be less than this
#RD_F  = 20 # RD frequency of top classification must be larger than this
#RD_R  = 20  # RD ratio between classification and original
#                    word
#                    |  index
#                    |  |    instances
def check_replacedet(w, idx, wl):
    glog.debug( "check_replacedet("+w+", "+str(idx)+")" )
    # only if DET
    if not w in DETS:
        glog.debug( "Not a DET." )
        return None 
    c = s1.get_classifier_by_name("dets")
    if c:
        glog.debug( "Classifier: "+c.name+" START" )
        res = c.classify_i(wl[c.ctx][idx])
        if not res:
            glog.error("Classifier "+c.name+" returned None.")
            sys.exit(8)
        if len(c.distr) > PARAMS['RD_DS']:
            glog.debug("Distribution larger dan RD_DS")
            return None
        ans = ans_to_dict( res )
        glog.debug( str(ans) )
        glog.debug("Best alternative: "+str(ans['topn'][0]))
        best_alt_freq = float(ans['topn'][0][1])
        if not best_alt_freq > PARAMS["RD_F"]:
            glog.debug("Frequency of top classification <= RD_F")
            return None
        # check if the original prep is in the distr
        w_in_distr = c.in_distr(w)
        w_freq = 0.001
        if w_in_distr:
            glog.debug("Found in distribution: "+str(w_in_distr))
            w_freq = float(w_in_distr[1])
        glog.debug("Ratio: "+str(best_alt_freq / w_freq))
        if (best_alt_freq / w_freq) <= PARAMS["RD_R"]:
            glog.debug( "Ratio between classification and original <= RD_R" )
            return None
        return (ans['topn'][0][0], idx, idx + 1) 
    else:
        glog.error( "Classifier: preps NOT FOUND" )
    return None

#                        word
#                        |  index
#                        |  |    instances
def check_unnecessarydet(w, idx, wl):
    glog.debug( "check_unnecessarydet("+w+", "+str(idx)+")" )
    # only if DET
    if not w in DETS:
        glog.debug( "Not a DET." )
        return None 
    c = s1.get_classifier_by_name("dets_np")
    if c:
        glog.debug( "Classifier: "+c.name+" START" )
        res = c.classify_i(wl[c.ctx][idx])
        if not res:
            glog.error("Classifier "+c.name+" returned None.")
            sys.exit(8)
        ans = ans_to_dict( res )
        glog.debug( str(ans) )
        posneg_r = ans['posneg_r']
        if ans['classification'] == "-" and posneg_r > PARAMS["UD_NPR"]:
            glog.debug( "posneg_r > "+str(PARAMS["UD_NPR"])+": "+str(ans['posneg_r']) )
            return ("_", idx, idx + 1) #how to return a "remove" ?
    else:
        glog.error( "Classifier: preps_np NOT FOUND" )
    return None

# Return a tuple (w/newword, pos_start, pos_end)
#
#                word
#                |  index
#                |  |    instances
def check_nform(w, idx, wl, postag):
    glog.debug( "check_nform("+w+", "+str(idx)+")" )
    if postag[0] != "N":
        glog.debug( "Not an N." )
        return None # (w, idx, idx+1)
    # Bug, words with hyphens crash ie.singular_noun/plural_noun
    if '-' in w:
        glog.debug("Contains hyphens")
        return None
    #ie.singular_noun(w) returns False is w is singular, else singular form
    w_alternative = ie.singular_noun(w)
    if w_alternative == False:
        w_alternative = ie.plural_noun(w)
    glog.debug( "check_nform, "+w+", look for: "+str(w_alternative) )
    c = s1.get_classifier_by_name("wpred")
    if c:
        glog.debug( "Classifier: "+c.name+" START" )
        res = c.classify_i(wl[c.ctx][idx])
        if not res:
            glog.error("Classifier "+c.name+" returned None.")
            sys.exit(8)
        #glog.info( c.distr_w )
        # check distr size
        glog.debug("ds="+str(len(c.distr)))
        if len(c.distr) > PARAMS["NF_DS"]:
            glog.debug("Distribution larger dan NF_DS")
            return None
        w_in_distr = c.in_distr(w)
        if w_alternative in c.distr_w:
            glog.debug("Found: "+str(c.in_distr(w_alternative)))
        else:
            glog.debug("Not found.")
        if w_in_distr == None:
            if w_alternative in c.distr_w:
                glog.debug(c.in_distr(w_alternative))
                # If we finf w_alternative, but not w in the distr,
                # we have a possible correction sing-plural. If we find both
                # it could be a correct use of the word. Need good data, better
                # take ratio,...
                glog.debug(w+" => "+w_alternative)
                return (w_alternative, idx, idx+1)
        else:
            glog.debug("Word is found in distr: "+str(w_in_distr))
    return None # (w, idx, idx+1)

# Return a tuple (w/newword, pos_start, pos_end)
#
#                word
#                |  index
#                |  |    instances
def check_vform(w, idx, wl, postag):
    glog.debug( "check_vform("+w+", "+str(idx)+")" )
    if postag[0] != "V":
        glog.debug( "Not a V." )
        return None # (w, idx, idx + 1) 
    v_lemma = wnl().lemmatize(w, "v")
    glog.debug("v_lemma="+str(v_lemma))
    v_alt = []
    if have_en:
        try:
            v_tense = en.verb.tense(w)
        except:
            glog.error("en.verb.tense("+w+") gave error")
            v_tense = "unknown"
        glog.debug("v_tense="+str(v_tense))
        # generate the other forms? check in distr?
        # 3rd singular present
        #
        vs_present = []
        vs_past = []
        vs_presp = []
        if "present" in v_tense or v_tense == "unknown" or v_tense == "infinitive":
            try:
                vs_present = [en.verb.present(v_lemma, person=p) for p in range(1,7)]
            except:
                glog.error("en.verb.present() gave an error.")
        if "past" in v_tense or v_tense == "unknown":
            try:
                vs_past = [en.verb.past(v_lemma, person=p) for p in range(1,7)]
            except:
                glog.error("en.verb.past() gave an error.")
        try:
            vs_presp = [en.verb.present_participle(v_lemma)]
        except:
            glog.error("en.verb.present_participle() gave an error.")
        ##glog.debug(str(vs_present))
        #glog.debug(str(vs_past))
        #glog.debug(str(vs_presp))
        # Should check the tense (present, no need for past)
        for v in vs_present:
            if not v in v_alt and v != w:
                v_alt.append(v)
        for v in vs_past:
            if not v in v_alt and v != w:
                v_alt.append(v)
        for v in vs_presp:
            if not v in v_alt and v != w:
                v_alt.append(v)
        glog.debug("v_alt="+str(v_alt))
    c = s1.get_classifier_by_name("wpred")
    if c:
        glog.debug( "Classifier: "+c.name+" START" )
        res = c.classify_i(wl[c.ctx][idx])
        #glog.debug(ans_str(res))
        if not res:
            glog.error("Classifier "+c.name+" returned None.")
            sys.exit(8)
        # check distr size
        glog.debug("ds="+str(len(c.distr)))
        if len(c.distr) > PARAMS["VF_DS"]:
            glog.debug("Distribution larger dan VF_DS")
            return None
        w_in_distr = c.in_distr(w) #the word in the text, to be corrected
        glog.debug("w_in_distr="+str(w_in_distr))
        if True or not w_in_distr: #ignore this check, en check frequency ratios with best alternative?
            poss = [ c.in_distr(v) for v in v_alt if c.in_distr(v) != None ]
            poss = sorted(poss, key=itemgetter(1), reverse=True)
            #poss = sorted(poss.iteritems(), key=operator.itemgetter(1), reverse=True)
            glog.debug("Alternatives: "+str(poss))
            if poss:
                glog.debug("Best alternative: "+str(poss[0]))
                best_alt_freq = float(poss[0][1])
                w_freq = 0.001
                if w_in_distr:
                    w_freq = float(w_in_distr[1])
                if (best_alt_freq / w_freq) > 10:
                    glog.debug("Ratio > 10")
                    return (poss[0][0], idx, idx + 1) 
    return None # (w, idx, idx + 1) 

'''
    if s1.classification == "+" and len(s1.distr) > 1:
        # maybe we had a DET, then we are OK, otherwise...
        if pos.word not in DETS:
            # Ratio between yes:no
            posneg_r = float(s1.distr[0][1]) / float(s1.distr[1][1])
            # Ratio is > ...
            if posneg_r > MD_PNR:
                # We want a determiner at this point
                # Call s3 to determine which DET we want
                s3.classify( pos.instance + " ?" )
                print "INSERT DET:", posneg_r, s3.classification, repr(s1.distr), repr(s3.distr[0])
                error_type = "MD"
                dbg( repr(s3.distr) )
'''


# handle per word, we first create all the instances
# we might need in window_all. returns dict[ctx] =>['i1', 'i2']
# We need to pos_tag the whole sentence here, because of:
#  nltk.pos_tag(['flower'])
#    [('flower', 'JJR')]
#  nltk.pos_tag(['one','flower'])
#    [('one', 'CD'), ('flower', 'NN')]
def handle_line(l):
    r = random.Random()
    result = ""
    lt = nltk.word_tokenize(l)
    wl = s1.window_all(lt)
    glog.info("S: "+l)
    glog.debug( "wl:  "+str(wl) )
    wtags = nltk.pos_tag( lt )
    glog.debug( "tags: "+str(wtags) )
    #print wl["l2r2"][0] #instances for l2r2, word 0 ("one")
    for idx,w in enumerate( lt ):
        postag = wtags[idx][1]
        glog.info( "  Processing word: "+w+"/"+postag )
        correction = False
        fired = []
        # Check if we want a DET _before_ the current word
        # The check_missingdet() returns a possible det on this
        # position, which is technically before the current word
        # if it is missing.
        # We should check if _this_ word is an N
        # we should insert, and continue with current word, or abort?
        if w not in DETS: #True or postag in ["NN"]:
            cMD = check_missingdet(w, idx, wl)
            if cMD:
                #glog.info("cMD="+cMD[0])
                result += cMD[0]+' '
                fired.append("MD")
        if fired == [] and w not in PREPS: #True or postag in ["PREP"]:
            cMP = check_missingprep(w, idx, wl)
            if cMP:
                #glog.info("cMD="+cMP[0])
                result += cMP[0]+' '
                fired.append("MP")
        if fired == [] and w in PREPS:
            cRP = check_replaceprep(w, idx, wl)
            if cRP:
                #glog.info("cRD="+cRP[0])
                result += cRP[0]+' '
                correction = True
                fired.append("RP")
        if fired == [] and w in DETS:
            cRD = check_replacedet(w, idx, wl)
            if cRD:
                #glog.info("cRD="+cRP[0])
                result += cRD[0]+' '
                correction = True
                fired.append("RD")

        if fired == [] and w in PREPS:
            cUP = check_unnecessaryprep(w, idx, wl)
            if cUP:
                if cUP[0] == '_':
                    result += ' '
                    correction = True
                    fired.append("UP")
        if fired == [] and w in DETS:
            cUD = check_unnecessarydet(w, idx, wl)
            if cUD:
                if cUD[0] == '-':
                    result += ' '
                    correction = True
                    fired.append("UD")

        #new_w = check_missingprep(w, idx, wl)
        #glog.info("new_w="+new_w[0])
        #result += new_w+' '
        #new_w = check_missingdet(w, idx, wl)
        #result += new_w+' '
        if True or fired == []:
            new_tuple = check_nform(w, idx, wl, postag)
            if new_tuple:
                new_w = new_tuple[0]
                if new_w != w:
                    result += new_w+' '
                    correction = True
                    fired.append("NF")
        if True or fired == []:
            new_tuple = check_vform(w, idx, wl, postag)
            if new_tuple:
                new_v = new_tuple[0]
                if new_v != w:
                    result += new_v+' '
                    correction = True
                    fired.append("VF")
        if not correction:
            result += w+' '
        #
        # handle all
        for c in s1.classifiers: #[1:2]:
            if c.name == "_nountag0":
                glog.debug( "Classifier: "+c.name )
                # a c.apply?(w,idx,l) function?
                res = c.classify_i(wl[c.ctx][idx])
                glog.info( c.name+": "+ans_shstr(res) )
            if c.name == "_nountag1":
                glog.debug( "Classifier: "+c.name )
                # a c.apply?(w,idx,l) function?
                res = c.classify_i(wl[c.ctx][idx])
                glog.info( c.name+": "+ans_shstr(res) )
            if c.name == "_verbtag0":
                glog.debug( "Classifier: "+c.name )
                # a c.apply?(w,idx,l) function?
                res = c.classify_i(wl[c.ctx][idx])
                glog.info( c.name+": "+ans_shstr(res) )
        glog.debug("fired="+str(fired))
    glog.info("R: "+result)
    return (result, len(lt))
    
def handle(l):
    r = random.Random()
    #print r.random(), r.uniform(0, 10), r.randrange(0, 10)
    words = l.split()
    wordspan = split_span(l) # [('one', 0, 2), ('two', 4, 6), ('three', 9, 13)]
    print list(wordspan)
    all_ctx_res = dict()
    for ctx in s1.get_contexts():
        m = ctx_re.match(ctx)
        lc = int(m.group(1))
        rc = int(m.group(2))
        #print lc,rc
        il = window_lr(l, lc, rc) # [ instances ]
        il = [ i.split() for i in il ]
        classifiers = s1.get_classifiers(ctx)
        all_res = dict()
        for c in classifiers:
            res = c.classify_il(il)
            all_res[c.name] = res
        all_ctx_res[ctx] = all_res
    #print all_ctx_res
    #create results per word
    word_results = dict()
    for idx, w in enumerate(words):
        ctx_keys = all_ctx_res.keys()
        #print ctx_keys
        for ctx_key in ctx_keys: #loop over contexts
            class_keys = (all_ctx_res[ctx_key]).keys()
            #print class_keys
            for class_key in class_keys: # loop over classes
                #print class_key
                #print w, all_ctx_res[ctx_key][class_key][idx]
                if idx in word_results:
                    word_results[idx].append( (class_key, all_ctx_res[ctx_key][class_key][idx]) )
                else:
                    word_results[idx] = [ (class_key, all_ctx_res[ctx_key][class_key][idx]) ]
    for idx, w in enumerate(words):
        print w
        # send results for analysis to another function.
        for r in word_results[idx]:
            #print r[0],ans_str(r[1]) # print result of each classifier for first word, tuple (classname, answer)
            print r[0],r[1][1],r[1][5] #r[1][1] is the target
    #print word_results #dict with index as key, value is list with dicts with classifier name as key, ans as value
            
def create_mistake(nid, pid, sid, st, et, ty, c):
    res  = "<MISTAKE nid=\""+str(nid)+"\" pid=\""+str(pid)+"\" sid=\""+str(sid)+"\" start_token=\""+str(st)+"\" end_token=\""+str(et)+"\">\n"
    res += "<TYPE>"+ty+"</TYPE>\n"
    res += "<CORRECTION>"+c+"</CORRECTION>\n"
    res += "</MISTAKE>\n"
    return res

def make_data_file(infile, lc, rc):
    with open('infile', 'r') as f:
        line = f.readline()
        print make_data(line, 2, 0)

def make_data(l, lc, rc):
    tokens = l.split()
    wtags = nltk.pos_tag(tokens)
    res = window_lrt( tokens, lc, rc, [ w[1] for w in wtags] )
    return res
# --

# HOO2012:
# First try the yes/no-det classifier (s1)
# Call s3 to determine which DET we want
# Try the PREP yes/no classifier (s2)
# Which PREP do we want? (s4)
#
# s1 timblserver -i google-5gms.dets-negpos.0.igtree -a1 +D +vdb+di -S 2012
# s2 timblserver -i google-5gms.preps-negpos.0.igtree -a1 +D +vdb+di -S 2013
# s3 timblserver -i google-5gms.dets.0.igtree -a1 +D +vdb+di -S 2014
# s4 timblserver -i google-5gms.preps.0.igtree -a1 +D +vdb+di -S 2015

#wpred="-i /scratch/pberck/data/nyt.1e7.l2r0_-a1+D.ibase -a1 +D +vdb+di"
#wpred1="-i /scratch/pberck/data/nyt.1e7.l2r2_-a1+D.ibase -a1 +D +vdb+di"
#wpred2="-i /scratch/pberck/data/nyt.1e7.l3r0_-a4+D.ibase -a4 +D +vdb+di"

#Run timblserver on port 2000
s1=TServers("localhost",2000)

# For scootaloo:
if platform.node() == "scootaloo":
    dflt0=Classifier("dets_np", 2, 2, s1)
    s1.add_classifier(dflt0)
    dflt1=Classifier("preps_np", 2, 2, s1)
    s1.add_classifier(dflt1)
    dflt2=Classifier("dets", 2, 2, s1)
    s1.add_classifier(dflt2)
    dflt3=Classifier("preps", 2, 2, s1)
    s1.add_classifier(dflt3)
    wpred0 = Classifier("wpred", 2, 0, s1) # we use "wpred" in subroutines
    s1.add_classifier(wpred0)
    wpred1 = Classifier("wpred1", 2, 2, s1)
    s1.add_classifier(wpred0)
    wpred2 = Classifier("wpred2", 3, 0, s1)
    s1.add_classifier(wpred2)
    ptpred0 = Classifier("ptpred0", 2, 0, s1)
    s1.add_classifier(ptpred0)
    ptpred1 = Classifier("ptpred1", 2, 2, s1)
    s1.add_classifier(ptpred1)
    nountag0 = Classifier("nountag0", 2, 2, s1)
    s1.add_classifier(nountag0)
    verbtag0 = Classifier("verbtag0", 2, 2, s1)
    s1.add_classifier(verbtag0)
else: #"durian"
    #ts0300=Classifier("ts0300", 2, 2, s1)
    #s1.add_classifier(ts0300)
    #dflt0=Classifier("dflt0", 2, 2, s1)
    #s1.add_classifier(dflt0)
    #dflt0.read_lex("austen.train.lex")
    wpred0 = Classifier("wpred0", 2, 0, s1)
    s1.add_classifier(wpred0)
    wpred1 = Classifier("wpred1", 2, 3, s1)
    s1.add_classifier(wpred1)
    
#print create_mistake(0,1,2, 5, 6, "Wci", "or and when")
#durian:software pberck$ bash ~/uvt/wopr/stimpy/dir_to_tspy.bash
#print list(split_span("one two  three"))
#print list(split_span("a b c"))

#print s1.get_contexts()
#print s1.get_classifiers("l2r0")
#print s1.get_classifiers("l1r0")
#list = window(sentence)
#for all ctx classifiers, c.classify_il() (l=list with instances)

# from /exp2/antalb/hoo2012/preps|dets
#
DETS = [ "a", "an", "another", "any", "every", "he", "her", "his", "its", "my", "no", "one", "other", "our", "own", "that", "the", "their", "these", "this", "those", "what", "which", "your"]

PREPS = [ "about", "after", "against", "along", "among", "around", "as", "at", "before", "behind", "below", "beside", "besides", "between", "by", "concerning", "down", "during", "for", "from", "in", "into", "near", "of", "off", "on", "onto", "out", "outside", "over", "referring", "regarding", "regards", "since", "than", "through", "throughout", "till", "to", "toward", "towards", "under", "until", "via", "with", "within", "without" ]

#conll: ArtOrDet, Prep, Nn, Vform, and SVA.

'''
MD_PNR = 20 # MD posneg ratio must be larger than this
MT_PNR = 20 # MT posneg ratio must be larger than this

RD_DS = 20   # RD distribution size must be less than this
RD_F  = 20 # RD frequency of top classification must be larger than this
RD_R  = 20  # RD ratio between classification and original

RT_DS = 50   # RT distribution size must be less than this
RT_F  = 5 # RT frequency of top classification must be larger than this
RT_R  = 20  # RT ratio between classification and original

UD_NPR = 2 # UD negpos ratio larger than this

UT_NPR = 4 # UT negpos ratio larger than this
'''
PARAMS = { "MD_PNR":20, #Missing DET min ratio
           "MD_DS":20, #Missing DET max distr size

           "MP_PNR":20, #Missing PREP min ratio
           "MP_DS":20, #Missing PREP max distr size

           "UD_NPR":2, # Unnecessary det negpos ratio larger than this

           "UP_NPR":4, # Unnecessary prep negpos ratio larger than this
           
           "RD_DS":20, #Replace ET max distr size
           "RD_F":20,  #RD frequency of top classification must be larger than this
           "RD_R":20,  #RD ratio between classification and original

           "RP_DS":50, #Replace PREP max distr size
           "RP_F":5,  #RP frequency of top classification must be larger than this
           "RP_R":20,  #RP ratio between classification and original
           
           "NF_DS":10000, #Noun form DET max distr size

           "VF_DS":10000 #Verb form max distr size
        }
glog.debug(repr(PARAMS))

# ----

ctx_re = re.compile("l(\d)r(\d)")
host = "localhost"
port = 2000
lexfile = None
afile = None
instance = None
sentence = None
topn = 3
debug = False
datafile = None
tokenizefile = None

try:
    opts, args = getopt.getopt(sys.argv[1:], "d:f:i:l:L:h:n:p:r:s:t:", ["lc="])
except getopt.GetoptError, err:
    # print help information and exit:
    print str(err) # will print something like "option -a not recognized"
    #usage()
    sys.exit(2)
for o, a in opts:
    if o in ("-f"):
        afile = a
    elif o in ("-d"):
        datafile = a
    elif o in ("-i"):
        instance = a
    elif o in ("-l", "--lc"):
        lc = a
    elif o in ("-L"):
        lexfile = a
    elif o in ("-h"):
        host = a
    elif o in ("-n"):
        topn = int(a)
    elif o in ("-p"):
        port = a
    elif o in ("-r"):
        rc = a
    elif o in ("-s"):
        sentence = a
    elif o in ("-t"):
        tokenizefile = a
    else:
        assert False, "unhandled option"
"""
all_files = []
if dirmode:
    # Find all
    #
    test     = re.compile(test_str, re.IGNORECASE)
    files = os.listdir( the_dir )
    files = filter(test.search, files)    
    for a_file in files:
        all_files.append( the_dir + a_file)
else:
    all_files.append( a_file )
"""

if afile != None:
    f = open(afile, "r")
    afile_out = afile+".out"
    fo = open( afile_out, "w" )
    cnt = 0
    wcnt = 0
    td0 = int(time.time())
    for line in f:
        l = line[:-1]
        (res, ws) = handle_line(l)
        fo.write(res+"\n");
        cnt += 1
        wcnt += ws
    fo.close()
    f.close()
    td1 = int(time.time())
    glog.info("Processed "+str(cnt)+" lines, "+str(wcnt)+" words.")
    td = td1-td0
    glog.info("Time taken: %s", str(datetime.timedelta(seconds=td)))
    tdw = float(float(td)/float(wcnt))
    glog.info("Time per word: %s", str(tdw))
    
if datafile != None:
    f = open(datafile, "r")
    for line in f:
        l = line[:-1]
        instances = make_data(l, 2, 2)
        for i in instances:
            print i

if tokenizefile != None:
    f = open(tokenizefile, "r")
    for line in f:
        l = line[:-1]
        t = nltk.word_tokenize(l)
        print l

if instance != None:
    c = s1.get_trigger(instance)
    if c:
        c.classify_i(instance)
        if not c.error:
            print c.name,":",ans_str(c.ans)
    print "----"
    for c in s1.classifiers:
        #print c
        c.classify_i(instance)
        if not c.error:
            print c.name,":",ans_str(c.ans)

if sentence != None:
    glog.info(sentence)
    #handle(sentence)
    handle_line(sentence)
    #t = nltk.word_tokenize(sentence)
    #print t
    #wtags = nltk.pos_tag(t)
    #print wtags
    #should combine t and wtags (map from NNS => 'n', etc)
    #print [ wnl().lemmatize(w) for w in t]
    #print  wnl().lemmatize("went", "v")
    
    '''
    r0 = all_res["dflt0"]
    r1 = all_res["dflt1"]
    dl = [ (ans_str(a[0]),ans_str(a[1])) for a in zip(r0,r1) ]
    for d in dl:
        print d[0]
        print d[1]
        print
    '''

