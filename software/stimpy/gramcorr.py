#!/usr/bin/python
# -*- coding: utf-8 -*-
#
import getopt
import sys
import time
import datetime
import random
import platform
from operator import itemgetter
import operator
import threading
from threading import Thread, Lock
from Queue import Queue
from Queue import PriorityQueue
from Queue import Empty
import multiprocessing
from stimpy import *
#
import logging
import logging.handlers
#
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)-8s %(message)s')
#
rtfile  = logging.handlers.RotatingFileHandler("./gramcorr.log",maxBytes=1024*1024*10, backupCount=5)
console = logging.StreamHandler()
#
glog = logging.getLogger('HOO')
rtformat='%(asctime)s %(levelname)-8s %(message)s'
rtformatter = logging.Formatter(fmt=rtformat)
rtfile.setFormatter(rtformatter)
glog.addHandler(rtfile)
glog.propagate = False #stop console log for this one
# Set up console
cformat='%(asctime)s %(message)s'
cformatter = logging.Formatter(fmt=cformat, datefmt="%Y%m%d %H:%M:%S")
console.setFormatter(cformatter)
console.setLevel(logging.INFO)
glog.addHandler(console)
#
glog.info("gramcorr starting.")
glog.info("See gramcorr.log for debug info.")
#

# Thread worker class thingy.
#
class myThread (threading.Thread):
    def __init__(self, threadID, server_id):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.res = []
        self.sid = server_id
        self.cont = True
        self.cnt = 0
    def debug(self, s):
        glog.debug(self.threadID+": "+s)
    def info(self, s):
        glog.info(self.threadID+": "+s)
    def error(self, s):
        glog.error(self.threadID+": "+s)
    def process(self, idx, line, cmd):
        c = servers[self.sid].get_classifier_by_name("wpred")
        if not c:
            self.error("classifier wpred not found.")
            return
        words = line.split()
        #window_lrt( tokens, lc, rc, targets ):
        self.res = []
        self.debug("words "+str(words))
        wl = s1.window_all(words)
        self.debug("wl "+str(wl))
        for i,w in enumerate(words):
            self.debug(str((wl[c.ctx][i])))
            #lock.acquire() #no lock, each worker has own socket
            res = c.classify_i(wl[c.ctx][i])
            #self.res.append( (i, res[1]) )
            self.res.append( (i, ans_str(res)) ) #we create a list with tuples
            self.debug("res: "+str(res))
            self.cnt += 1
        r.put( ( idx, self.res ) ) # result on queue
        if cmd == "_TOK":
            self.res = line.split()
            self.cnt += 1
            #r.put(( idx, self.res )) # result on queue
            lock.acquire()
            try:
                print self.res
            finally:
                lock.release()
    def run(self):
        self.info( "Starting" )
        # First wait 4 seconds, than loop full-speed
        try:
            (idx, line, cmd) = q.get(True, 4)
            self.info(str(idx)+","+line[:40]+"...")
            self.process(idx, line, cmd)
            q.task_done()
        except Empty:
            self.info( "No data?" )
            self.info( "Exit." )
            return 1
        while self.cont:
            try:
                (idx, line, cmd) = q.get_nowait()
                self.info(str(idx)+","+line[:40]+"...")
                self.process(idx, line, cmd)
                q.task_done()
            except Empty:
                self.info( "Queue empty." )
                self.cont = False 
        self.info( "Exit." )
        return 0

# ----


# ----


#
afile = None
datafile = 0
q = Queue()
r = PriorityQueue()
workers = []
lock = Lock()
procs = int(multiprocessing.cpu_count() / 2)+1 
lexfile = None

try:
    opts, args = getopt.getopt(sys.argv[1:], "d:f:l:p:", ["file="])
except getopt.GetoptError, err:
    print str(err)
    sys.exit(1)
for o, a in opts:
    if o in ("-f", "--file="):
        afile = a
    elif o in ("-d"):
        datafile = a
    elif o in ("-l"):
        lexfile = a
    elif o in ("-p"):
        procs = int(a)
    else:
        assert False, "unhandled option"

# Connect to a timblserver on port 2000
servers = []
if platform.node() == "scootaloo":
    try:
        for i in range(procs):
            s1 = TServers("localhost", 2000)
            s1.add_classifier( Classifier("wpred", 2, 2, s1) )
            servers.append( s1 )
    except:
        glog.error("Help")
        sys.exit(1)

else:
    try:
        for i in range(procs):
            s1=TServers("localhost", 2000)
            cl = Classifier("wpred", 2, 0, s1)
            lx = None
            if lexfile:
                lx = Lexicon("lex")
                lx.read_lex( lexfile )
                cl.add_lex( lx )
            s1.add_classifier( cl )
            servers.append( s1 )
    except:
        glog.error("Help")
        sys.exit(1)

for i in range(procs):
    t = myThread("Thread-{:02n}".format(i), i ) #i points to servers[i]
    workers.append(t)
    t.daemon = True
    t.start()

if afile != None:
    f = open(afile, "r")
    i = 0
    # double loop, add and output to file in blocks of procs lines?
    for line in f:
        l = line[:-1]
        q.put( (i, l, "pplx") )
        i += 1
    f.close()
    glog.info("Queue filled with "+str(i)+" items.")

    td0 = int(time.time())
    
    # Fill result q till procs items before we start reading
    # from the result queue.
    if i > procs: #otherwise we hang here
        while r.qsize() < procs:
            time.sleep(.1)

    # Open outfile
    afile_out = afile+".out"
    fo = open( afile_out, "w" )

    # Keep reading, because of order issues, we keep
    # procs "distance" from the end of the queue, the
    # item[0] from the tuples is the print index, which
    # should be equal to the print_idx to get the order
    # right. Put item back on queue if out of order.
    #
    print_idx = 0
    gap = 4
    while any( [ t.isAlive() for t in workers ] ):
        if r.qsize() > gap:
            res = r.get() #check for empty? not if we use gap
            if print_idx != res[0]: #res[0] is the index of the line
                #glog.error( "Print_idx out of order, "+str(print_idx)+" vs "+str(res[0])+" ("+str(r.qsize())+")" )
                r.put( res ) #put back and wait
                time.sleep(2) #maybe longer?
            else:
                res_line = res[1] #res[1] is list of tuples (i,str)
                for l in res_line:
                    fo.write( str(l[1])+"\n" )
                fo.flush()
                td1 = int(time.time())
                td = td1-td0
                print_idx += 1
                #calculate time left from q.qsize, not r.qsize !
                persec = float(float(td)/float(print_idx))
                left = int( persec * (q.qsize()+procs) )
                glog.info(  "Wrote line "+str(print_idx-1)+", time taken: %s (%s left)", str(datetime.timedelta(seconds=td)),  str(datetime.timedelta(seconds=left)) )
        else:
            time.sleep(1) #wait for r.queue to be filled
            
    # the rest, order is OK now
    while r.qsize() > 0:
        res = r.get()
        res_line = res[1] #res[1] is list of tuples (i,str)
        for l in res_line:
            fo.write( str(l[1])+"\n" )
        fo.flush()
        td1 = int(time.time())
        td = td1-td0
        print_idx += 1
        #calculate time left from q.qsize, not r.qsize !
        persec = float(float(td)/float(print_idx))
        left = int( persec * (q.qsize()+procs) )
        glog.info(  "Wrote line "+str(print_idx-1)+", time taken: %s (%s left)", str(datetime.timedelta(seconds=td)),  str(datetime.timedelta(seconds=left)) )

    fo.close()

    td1 = int(time.time())
    td = td1-td0
    glog.info("Time taken: %s", str(datetime.timedelta(seconds=td)))
    glog.debug( "Result Queue contains: "+str(r.qsize()) )
    total_cnt = 0
    for w in workers:
        glog.debug( str(w.name)+" processed "+str(w.cnt)+ " instances." )
        total_cnt += w.cnt
    glog.info( "Total processed "+str(total_cnt)+ " instances." )
    tdw = float(float(td)/float(total_cnt))
    #scootaloo, -p24: Time per word: 0.0596267637688 (16 p/s)
    glog.info("Time per word: %s", str(tdw))
    sys.exit(0)
    
