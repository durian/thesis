#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Simple TIMbleserver PYthon Interface
#
import os
import re
import sys
from xml.dom import minidom
import datetime
import time
from time import strptime
from time import asctime
from math import sqrt,sin,cos,asin,pi
import getopt
import math
import codecs
import socket
from time import sleep
from itertools import islice, izip
from operator import itemgetter
import random
import copy
import math;
from itertools import izip
import cProfile, pstats, io

# ---

debug = False
def dbg(*strs):
    if debug:
        print >> sys.stderr, "DBG:", "".join(str(strs))

def find(f, seq):
  """Return first item in sequence where f(item) == True."""
  for item in seq:
    if f(item): 
      return item

# ----

class Cache():
    def __init__(self, name):
        self.name = name
        self.cache = {}
        self.keys = []
        #print "New cache:",name
    def store(self, k, v):
        if k in self.keys:
            return
        #print "Store in", self.name, k #, repr(v)
        self.cache[k] = v
        self.keys.append(k)
        #print self.name, "has now", len(self.keys), "items"
    def retrieve(self, k):
        try:
            #print "Retrieved from",self.name, k
            return self.cache[k]
        except:
            return None
    def has_key(self, k):
        if k in self.keys:
            return True
        return False
    def strhash(self, str):
        return hash(str)
    
'''
[ _ _ _ the quick brown fox _ _ _ ]
        ^start
        [ pos-LC : pos+LC ]
'''
def window_lr( str_ar, lc, rc ):
    if type( str_ar ) == "str":
        str_ar = str_ar.split()
    # words = ["_" for x in range(lc-1)] + ["<S>"] + str_ar + ["</S>"] + ["_" for x in range(rc-1)]
    words = ["_" for x in range(lc)] + str_ar + ["_" for x in range(rc)]
    result = []
    for i in range(len(str_ar)):
        #              shift to right index by added lc
        res = words[i:i+lc] + words[i+1+lc:i+rc+1+lc] + [str_ar[i]]
        #print i, words[i:i+lc],words[i+1+lc:i+rc+1+lc],"=",str_ar[i]
        #print " ".join(res)
        result.append( " ".join(res) )
    return result

# As above, but supply a list with targets (e.g pos tags)
# Other difference, str is a tokenized list here.
def window_lrt( tokens, lc, rc, targets ):
    words = ["_" for x in range(lc)] + tokens + ["_" for x in range(rc)]
    result = []
    for i in range(len(tokens)):
        #              shift to right index by added lc
        res = words[i:i+lc] + words[i+1+lc:i+rc+1+lc] + [targets[i]]
        result.append( " ".join(res) )
    return result

def window_to( str, lc, o ): #offset
    str_ar = str.split()
    # words = ["_" for x in range(lc-1)] + ["<S>"] + str_ar + ["</S>"] + ["_" for x in range(rc-1)]
    words = ["_" for x in range(lc)] + str_ar
    result = []
    for i in range(len(str_ar)-o):
        #              shift to right index by added lc
        res = words[i:i+lc] + [str_ar[i+o]]
        #print i, words[i:i+lc],words[i+1+lc:i+rc+1+lc],"=",str_ar[i]
        #print " ".join(res)
        result.append( " ".join(res) )
    return result
        
class TServers():
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.ibs = []
        self.classifiers = []
        self.s = None
        self.file = None
        self.lex = {}
        self.topn = 3
        self.triggers = {}
        self.contexts = {}
        self.caches = {}
        try:
            self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.s.connect((self.host, self.port))
            self.file = self.s.makefile("rb") # buffered
            #self.s.settimeout(1)
            #print self.s.gettimeout()
        except:
            #print "Error, no timblserver"
            raise Exception('TServer', 'Cannot contact timblserver.')
        self.readline() #available bases
                        #print "1",self.data
        self.readline() #available bases
                        #print "2",self.data
        #self.readline() #available bases
        #print "3",self.data

    def readline(self):
        "Read a line from the server.  Strip trailing CR and/or LF."
        CRLF = "\r\n"
        if not self.file:
            return
        s = self.file.readline()
        if not s:
            raise EOFError
        if s[-2:] == CRLF:
            s = s[:-2]
        elif s[-1:] in CRLF:
            s = s[:-1]
        self.data = s

    def sendline(self,l):
        try:
            self.s.sendall(l)
        except socket.error:
            print "Socket kapot."
        
    # classify one instance, classifier c
    def classify( self, c, i ):
        if not self.s:
            return
        if type(i) is list:
            self.i = ' '.join(i)
            tokens = i
        else:
            self.i = i
            tokens = self.i.split()
        self.target = tokens[-1]
        #what do we return? Own class for ib?
        c.error = False
        #try:
        self.sendline("b "+c.name+"\n")
        self.readline()
        # check result!
        if self.data[0:5] == "ERROR":
            c.ans = []
            c.error = True
            return
        self.sendline("c "+self.i+"\n")
        self.readline()
        if self.data[0:5] == "ERROR":
            c.ans = []
            c.error = True
            return
        self.grok( c, self.data )
        c.ans = self.ans
        
    # classify with triggers.
    def classify_tr(self, i):
        if not self.s:
            return
        self.i = i
        tokens = self.i.split()
        self.target = tokens[-1]
        if self.target in self.triggers:
            c = self.triggers[self.target]
            return self.classify(c,i)
        return []

    def get_trigger(self, i):
        tokens = i.split()
        target = tokens[-1]
        if target in self.triggers:
            c = self.triggers[target]
            return c
        return None
    
    def grok( self, c, tso ):
        '''
        Parse the TimblServer output
        '''
        #CATEGORY {concerned} DISTRIBUTION { concerned 2.00000, volatile 1.00000 } DISTANCE {0}
        dbg(tso)
        # error checking
        if tso[0:5] == "ERROR":
            print tso
            print self.i
            self.data = ""
            self.ans = []
            self.distr = []
            return
        m = re.search( r'^CATEGORY {(.*)} DISTRIBUTION { (.*) } DISTANCE {(.*)}$', tso )
        #print m.group(1) #concerned
        if not m:
            c.ans = []
            c.error = True
            return
        # check cache
        if c.name in self.caches:
            strh = self.caches[c.name].strhash(tso)
            if self.caches[c.name].has_key(strh):
                (self.ans, c.distr, c.distr_w) = self.caches[c.name].retrieve(strh)
                return
        self.classification = m.group(1)
        #print m.group(2) #concerned 2.00000, volatile 1.00000
        #print m.group(3) #0 distance
        self.distance = float(m.group(3))
        #maybe set a cap on large distributions - we don't use them anyway
        bits = m.group(2).split()
        pairs = zip(bits[::2], bits[1::2])
        #remove silly commas from frequencies
        self.distr = {}
        c.distr = {}
        self.sum_freqs = 0
        self.distr_count = 0
        c.distr_w = [] #work directly in Classifier
        for pair in pairs:
            #print pair[0], pair[1].rstrip(",")
            self.sum_freqs += float(pair[1].rstrip(","))
            c.distr_w.append(pair[0])
            self.distr_count += 1
            c.distr[ pair[0] ] = float(pair[1].rstrip(","))
            #CHECK IN THIS LOOP FOR CD ! Order is wrong for RR?
        #print sorted( distr.iteritems(), key=itemgetter(1), reverse=True )[0:5]
        #print "dc,sf",distr_count, sum_freqs
        #self.distr = copy.deepcopy(\
        #            sorted( distr.iteritems(), key=itemgetter(1), reverse=True ) )
        c.distr = sorted( c.distr.iteritems(), key=itemgetter(1), reverse=True )
        #self.distr = sorted( self.distr.iteritems(), key=operator.itemgetter(1), reverse=True)
        c.distr = [(w,f,f/self.sum_freqs) for (w,f) in c.distr]
        #c.distr = copy.deepcopy(self.distr) #have a full version
        # Most of the following are 0/[] for not used in hoo2013
        self.logs = [p * math.log(p,2) for (w,f,p) in c.distr]
        self.entropy =-sum(d for d in self.logs)
        self.pplx = math.pow(2, self.entropy)
        self.p = 0
        self.indicator = "??"
        self.ku = "u"
        self.rr = 0
        self.log2p = 0
        self.wlpx = 0

        lexf = c.in_lex(self.target) * 1.0 
        if lexf:
            self.ku = "k"
        if self.classification == self.target:
            self.p = (c.distr[0])[2]
            self.indicator = "cg"
            self.rr = 1
        else:
            #print "ID",self.in_distr(self.target) #faster than next line?
            tmp = [(w,f,p) for (w,f,p) in c.distr if w == self.target]
            if tmp != []:
                tmp = tmp
                self.p = tmp[0][2]
                self.indicator = "cd"
                self.rr = 1.0/(c.distr.index(tmp[0])+1)
            else:
                self.indicator = "ic"
                if lexf:
                    self.p = lexf / c.get_lex_sumfreq()
        #if self.p == 0: self.p = 1.8907e-06 #p(0) should be calculated
        try:
            self.log2p = math.log(self.p,2) #word logprob
            self.wlpx  = math.pow(2,-self.log2p)
        except ValueError:
            self.log2p = -99.0
            self.wlpx  = 0 #pow(2,32) #0 #should be inf, ok for logscale y plot

        topn = c.distr[0:self.topn]
        self.ans = (self.i, self.classification, self.log2p, self.entropy, self.wlpx,\
                    self.indicator, self.ku, self.distr_count, self.sum_freqs, self.rr, topn)
        # check cache
        if c.name in self.caches:
            strh = self.caches[c.name].strhash(tso)
            if self.caches[c.name].has_key(strh):
                return
            self.caches[c.name].store(strh, (self.ans, c.distr, c.distr_w))
  
            

    def in_distr(self, w):
        getword = itemgetter(0)
        res = find( lambda ding: ding[0] == w, self.distr )
        return res

    def read_lex(self, lexfile):
        self.lex = dict( ( line.split()[0], int(line.split()[1])) for line in open(lexfile))
        self.lex_sumfreq = sum(self.lex.itervalues())
        self.lex_count = len(self.lex)

    def in_lex(self, w):
        try:
            return self.lex[w]
        except KeyError:
            return 0
        
    def add_triggers(self, c, t):
        for trigger in t:
            self.triggers[trigger] = c

    def add_classifier(self, c):
        self.classifiers.append(c)
        #should know context size
        ctx = "l"+str(c.lc)+"r"+str(c.rc)
        if ctx in self.contexts:
            self.contexts[ctx].append(c)
        else:
            self.contexts[ctx] = [c]
        # Add cache
        if c.name not in self.caches:
            self.caches[c.name] = Cache(c.name)
    def get_classifiers(self, ctx):
        if ctx in self.contexts:
            return self.contexts[ctx]
        return []

    def get_classifier_by_name(self, name):
        for c in self.classifiers:
            if c.name == name:
                return c
        return None

    def get_contexts(self):
        return self.contexts.keys()

    def window_all(self, l):
        #window the line for all the context sizes this server handles.
        ctx_re = re.compile("l(\d)r(\d)")
        res = dict()
        for ctx in self.get_contexts():
            m = ctx_re.match(ctx)
            lc = int(m.group(1))
            rc = int(m.group(2))
            il = window_lr(l, lc, rc)
            res[ctx] = [i.split() for i in il] # = il
        return res

# Classifier, uses TimblServer
class Classifier():
    def __init__(self, name, lc, rc, ts):
        self.name = name
        self.lc = lc
        self.rc = rc
        self.ctx = "l"+str(lc)+"r"+str(rc)
        self.to = 0
        self.ts = ts #Timblserver
        self.ans = None  #one instance answer
        self.error = False
        self.triggers = []
        self.lex = None
        self.cache = Cache(name)
    def window_lr(self, s):
        if self.to == 0:
            res = window_lr(s, self.lc, self.rc)
        else:
            res = window_to(s, self.lc, self.to)
        return res
    def set_triggers(self, t):
        self.triggers = t
        self.ts.add_triggers(self, t)
    def classify_i(self, i):
        #self.ts.lex = self.lex
        #self.ts.lex_sumfreq = self.lex_sumfreq
        #self.ts.lex_count = self.lex_count
        i_str = repr(i) #PJB CRUDE!!
        if self.cache.has_key(i_str):
            # from cache ?
            #  self.ans = []
            #  self.distr = []
            tmp = self.cache.retrieve(i_str)
            if tmp:
                self.ans = tmp[0]
                self.distr = tmp[1]
                self.distr_w = tmp[2]
                return self.ans
            return None
        self.ts.classify(self, i)
        if self.ans:
            self.cache.store(i_str, (self.ans, self.distr, self.distr_w))
        return self.ans
    def classify_s(self, s):
        return [ self.classify_i(i) for i in self.window_lr(s) ]
    def classify_il(self, l):
        #cProfile.run('return [ self.classify_i(i) for i in l ]')
        return [ self.classify_i(i) for i in l ]
    def in_distr(self, w):
        getword = itemgetter(0)
        res = find( lambda ding: ding[0] == w, self.distr )
        return res
    def in_distr_w(self, w):
        return w in self.distr_w
    def add_lex(self, lexicon):
        self.lex = lexicon
    def in_lex(self, w):
        if self.lex:
            return self.lex.in_lex(w)
        return 0 #this fun returns frequency
    def get_lex(self, w):
        if self.lex:
            return self.lex.get_lex(w)
        return None
    def hapax(self, line, h):
        if self.lex:
            return self.lex.hapax(line, h)
        return None
    def get_lex_sumfreq(self):
        if self.lex:
            return self.lex.lex_sumfreq
        return 0
    def get_lex_count(self):
        if self.lex:
            return self.lex.count
        return 0

# Classifier, uses TimblServer
class Lexicon():
    def __init__(self, name):
        self.name = name
        self.lex = {}
        self.lex_sumfreq = 0
        self.lex_count = 0
        self.lex_file = None
    def read_lex(self, lexfile):
        try:
            self.lex = dict( ( line.split()[0], int(line.split()[1])) for line in open(lexfile))
            self.lex_sumfreq = sum(self.lex.itervalues())
            self.lex_count = len(self.lex)
            self.lex_file = lexfile
        except (IOError):
            pass
    def in_lex(self, w):
        try:
            return self.lex[w]
        except KeyError:
            return 0 #we return frequency here
    def get_lex(self, w):
        if w in self.lex:
            return self.lex[w]
        return None
    def hapax(self, line, h):
        words = line.split()
        res = []
        for w in words:
            wl = self.get_lex(w)
            if wl > h:
                res.append(w)
            else:
                res.append('HPX')
        return ' '.join(res)


# ----

# Output from wopr:
# instance+target classification log2prob entropy word_lp guess k/u md mal dist.cnt dist.sum RR ([topn])
# stympy:
# (i, classification, log2p, entropy, wlpx, indicator, ku, distr_count, sum_freqs, rr, topn)

def ans_str(ans):
    if ans and len(ans) == 11:
        return '{0} {1} {2:.4f} {3:.4f} {4:.4f} {5} {6} _ _ {7:d} {8:.0f} {9:.4f}'.format(*ans) + topn_str(ans[10])
    return None

# Short string
def ans_shstr(ans):
    if ans and len(ans) == 11:
        #  classification distr
        return '{1}'.format(*ans) + topn_str(ans[10])
    return None

def ans_to_dict(ans):
    if ans and len(ans) == 11:
        nms = [ "i", "classification", "log2p", "entropy", "wlpx", "ind", "ku", "dcount", "sumf", "rr", "topn" ]
        b = dict(zip(nms, ans))
        posneg_r = 0
        if len(ans[10]) > 1:
            posneg_r = float(ans[10][0][1]) / float(ans[10][1][1]) #independent from topn
        b["posneg_r"] = posneg_r
        return b
    else:
        return None
    
def topn_str(tns):
    if len(tns) == 0:
        return ""
    res = " [ "
    for tn in tns:
        s = '{0} {1:.0f} '.format(*tn)
        res += s
    res += "]"
    return res

#file with ans_str(results):
#plot [][] "plt" using 6:xticlabels(3) with lp

# -----------------------------------------------------------------------------
# ----- MAIN starts here
# -----------------------------------------------------------------------------

if __name__ == "__main__":
    print "Python interface for timblsercer."

    c = Cache()
    print c.has_key("t")
    c.store("t", ("a", "b"))
    print c.has_key("t")
    print c.retrieve("t")
