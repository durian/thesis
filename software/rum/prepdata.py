#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#
import os
import re
import sys
from xml.dom import minidom
import datetime
import time
from time import strptime
from time import asctime
import getopt
import math
import codecs
import socket
from time import sleep
from itertools import islice, izip
from operator import itemgetter
import random
import copy

#import unicodedata
#unicodedata.lookup('ZERO WIDTH NO-BREAK SPACE')
#u'\ufeff'

# ---


'''
Contains a list with suffix.
'''
class LIST():
    def __init__(self, l, s, t):
        self.l = l      #the list
        self.s = s      #file suffix
        self.f = None   #output file
        self.t = t      #type, 0=output_word, 1=pos/neg
        self.info = {}
        self.wn = 0     #wanted nr of negatives after a positive
        if self.t == 0:
            for e in l: #stats for the words
                self.info[e] = 0
        elif self.t == 1: #stats for the pos/neg
            for e in ['+','-']:
                self.info[e] = 0

    def open( self, filename ):
        self.f = open( filename+"."+self.s, "wb" )

    def close( self ):
        self.f.close()

    def check( self, w, line ):
        if w.lower() in self.l:
            if self.t == 0:
                self.f.write( " ".join(line)+"\n" )
                self.info[w.lower()] += 1
            elif self.t == 1:
                self.f.write( " ".join(line[:-1])+" +\n" )
                self.info['+'] += 1
                self.wn = self.wn + 1 #we want a corresponding negative.
        else: #we did not match
            if self.t == 1: #but type is one, print a negative
                if self.wn > 0 and random.random() > 0.5: #not always next line
                    self.f.write( " ".join(line[:-1])+" -\n" )
                    self.info['-'] += 1
                    self.wn = self.wn - 1

    def stats( self ):
        if self.t == 0:
            for e in self.l:
                print e, self.info[ e.lower() ]
        elif self.t == 1:
            for e in ['+','-']:
                print e, self.info[ e ]
        print "wn=",self.wn

# from /exp2/antalb/hoo2012/preps|dets
#
DETS = [ "a", "an", "another", "any", "every", "he", "her", "his", "its", "my", "no", "one", "other", "our", "own", "that", "the", "their", "these", "this", "those", "what", "which", "your"]

PREPS = [ "about", "after", "against", "along", "among", "around", "as", "at", "before", "behind", "below", "beside", "besides", "between", "by", "concerning", "down", "during", "for", "from", "in", "into", "near", "of", "off", "on", "onto", "out", "outside", "over", "referring", "regarding", "regards", "since", "than", "through", "throughout", "till", "to", "toward", "towards", "under", "until", "via", "with", "within", "without" ]

OTHER= [ "woman", "women" ]

lists = []
lists.append( LIST( DETS, "d", 0 ) )
lists.append( LIST( DETS, "D", 1 ) )
lists.append( LIST( PREPS, "p", 0 ) )
lists.append( LIST( PREPS, "P", 1 ) )
lists.append( LIST( OTHER, "o", 0 ) )
lists.append( LIST( OTHER, "O", 1 ) )

# ----

def dbg(*strs):
    if verbose > 0:
        print >> sys.stderr, "DBG:", "".join(strs)

def find(f, seq):
  """Return first item in sequence where f(item) == True."""
  for item in seq:
    if f(item): 
      return item
  
            
# -----------------------------------------------------------------------------
# ----- MAIN starts here
# -----------------------------------------------------------------------------


if __name__ == "__main__":

    debug      = False
    verbose    = 0

    try:
        opts, args = getopt.getopt(sys.argv[1:], "f:tv", ["file="])
    except getopt.GetoptError, err:
        # print help information and exit:
        print str(err) # will print something like "option -a not recognized"
    #usage()
        sys.exit(2)
    for o, a in opts:
        if o in ("-f", "--file"):
            a_file = a
        elif o in ("-v"):
            verbose = 1
        elif o in ("-t"):
            tweak = True
        else:
            assert False, "unhandled option"

    #implement stats in LIST, plus file pointer.
    #info = {}
    #for prep in PREPS:
    #    info[prep] = 0

    dbg( a_file )
    f = open( a_file, "r" )

    for li in lists:
        li.open( a_file )

    for l in f:
        words = l.split()
        dbg( repr(words) )
        target = words[-1]
        dbg( target )
        for li in lists:
            #print li
            li.check( target.lower(), words )

    for li in lists:
        li.close()
        li.stats()
