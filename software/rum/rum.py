#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#
import os
import re
import sys
import datetime
import time
from time import strptime
from time import asctime
import getopt
import math
import codecs
import socket
from time import sleep
from itertools import islice, izip
from operator import itemgetter
import random
import copy

#import unicodedata
#unicodedata.lookup('ZERO WIDTH NO-BREAK SPACE')
#u'\ufeff'

# ---

# from /exp2/antalb/hoo2012/preps|dets
#
DETS = [ "a", "an", "another", "any", "every", "he", "her", "his", "its", "my", "no", "one", "other", "our", "own", "that", "the", "their", "these", "this", "those", "what", "which", "your"]
#
PREPS = [ "about", "after", "against", "along", "among", "around", "as", "at", "before", "behind", "below", "beside", "besides", "between", "by", "concerning", "down", "during", "for", "from", "in", "into", "near", "of", "off", "on", "onto", "out", "outside", "over", "referring", "regarding", "regards", "since", "than", "through", "throughout", "till", "to", "toward", "towards", "under", "until", "via", "with", "within", "without" ]
#
P = [ '+' ]
N = [ '-' ]

# ----

def dbg(*strs):
    if verbose > 0:
        print >> sys.stderr, "DBG:", "".join(strs)

def find(f, seq):
  """Return first item in sequence where f(item) == True."""
  for item in seq:
    if f(item): 
      return item
  
'''
[ _ _ _ the quick brown fox _ _ _ ]
        ^start
        [ pos-LC : pos+LC ]
'''
def window_lr( str, lc, rc ):
    str_ar = str.split()
    words = ["_" for x in range(lc-1)] + ["<S>"] + str_ar + ["</S>"] + ["_" for x in range(rc-1)]
    #print words
    for i in range(len(str_ar)):
        #              shift to right index by added lc
        res = words[i:i+lc] + words[i+1+lc:i+rc+1+lc] + [str_ar[i]]
        #print i, words[i:i+lc],words[i+1+lc:i+rc+1+lc],"=",str_ar[i]
        print " ".join(res)

# window 2-0-2-T
# Does "tokenisation", removes and expands the punctuation attached to words,
# changing the instances. Same for punctuation on target.
# returns a list with [instance][target]
# The #"!?.," allow for punctuation removal
#
def window( str, lc, rc ):
    str_ar = str.split()
    words = ["_" for x in range(lc)] + str_ar + ["_" for x in range(rc)]
    #words = ["_" for x in range(lc-1)] + ["<S>"] + str_ar + ["</S>"] + ["_" for x in range(rc-1)]
    #print words
    result = []
    for i in range(len(str_ar)):
        #              shift to right index by added lc
        res = copy.deepcopy(words[i:i+lc] + words[i+1+lc:i+rc+1+lc])
        idx = 0
        tmp_res = copy.deepcopy(res)
        for rb in tmp_res:
            if len(rb) > 1 and rb[-1:] in "": #"!?.,":
                lp = rb[0:-1] #left part
                rp = rb[-1:]  #right part
                #replacement depends on 0,1 or 2,3 positions
                if idx == 3:
                    res[3] = lp
                if idx == 2:
                    res[2] = lp
                    res[3] = rp
                if idx == 1:
                    res[1] = rp
                    res[0] = lp
                if idx == 0:
                    res[0] = rp
                #res[idx:idx+2] = [lp, rp]
                #print "PUNC", idx, lp, rp, res
            idx = idx + 1
        tar  = str_ar[i]
        ptar = ''
        if tar[-1:] in "": #"!?.,":
            ptar = tar[-1:] #punctuation part
            tar = tar[0:-1] #left part
            #in this case, move ptar in instance also
            res[3] = res[2]
            res[2] = ptar
        result.append( [" ".join(res), tar] )
    #print result
    return result

#window( "0 1 2 3", 2, 1)
#window( "0 1 2 3", 2, 0)
#window( "0 1 2 3", 0, 2)
#sys.exit(8)

# Also put edit info in here, then loop over INSTANCE list to
# create a new <P> string with edit structure, which can be parsed
# to XML again.
'''
Contains a list with words, instance, start/endpos, etc
'''
class INSTANCE():
    def __init__(self, i, w):
        self.instance = i  #instance around word
        self.word = w      #word/target
        self.etype = None  #error type, MT, RT, etc
        
    def output_all(self):
        print self.instance, self.word
        
class TServer():
    def __init__(self, host, port, name):
        self.host = host
        self.port = port
        self.name = name
        self.s = None
        self.file = None
        try:
            self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.s.connect((self.host, self.port))
            self.file = self.s.makefile("rb") # buffered
            #self.s.settimeout(1)
            #print self.s.gettimeout()
        except:
            print "Error initialising", self.name
            return
        #self.recv_data()
        #self.s.sendall("base default\n")
        #self.recv_data()
        self.readline()
        print name+": ("+self.data+")"

    def read(self, maxbytes = None):
        "Read data from server."
        if maxbytes is None:
            return self.file.read()
        else:
            return self.file.read(maxbytes)

    def readline(self):
        "Read a line from the server.  Strip trailing CR and/or LF."
        CRLF = "\r\n"
        s = self.file.readline()
        if not s:
            raise EOFError
        if s[-2:] == CRLF:
            s = s[:-2]
        elif s[-1:] in CRLF:
            s = s[:-1]
        self.data = s
    
    def recv_data(self):
        self.data = ""
        recv_data = ""
        while 0:
            try:
                recv_data = self.s.recv(4096)
                print "["+recv_data+"]"
                self.data = self.data+recv_data
            except:
                #print "Received data: ", self.data
                break
                pass
            if not recv_data:
                #print "Received data: ", self.data
                break
                pass
        
    def classify( self, i ):
        if not self.s:
            return
        #print "--SERVER--> classify "+i
        self.i = i
        try:
            self.s.sendall("classify "+i+"\n")
        except:
            # UTF-8 problems? save as iso-8859-1
            #print "classify "+i
            tmp0 = "classify "+i
            tmp1 = tmp0.encode("iso-8859-1", "replace")
            self.s.sendall(tmp1+"\n")
            #sys.exit(8)
        self.readline()
        self.grok( self.data )
        #print self.distr
        
    def grok( self, tso ):
        '''
        Parse the TimblServer output
        '''
        #m = re.findall(r'CATEGORY {(.*)} ', tso)
        #for url in m:
        #print url
        #CATEGORY {concerned} DISTRIBUTION { concerned 2.00000, volatile 1.00000 } DISTANCE {0}
        #print "["+tso+"]"
        # error checking
        m = re.search( r'ERROR', tso )
        if m:
            print "ERROR"
            print tso
            print self.i
            sys.exit(8)
        m = re.search( r'CATEGORY {(.*)} DISTRIBUTION { (.*) } DISTANCE {(.*)}', tso )
        #print m.group(1) #concerned
        self.classification = m.group(1)
        #print m.group(2) #concerned 2.00000, volatile 1.00000
        #print m.group(3) #0
        bits = m.group(2).split()
        pairs = zip(bits[::2], bits[1::2])
        #remove silly commas from frequencies
        distr = {}
        sum_freqs = 0
        distr_count = 0
        for pair in pairs:
            #print pair[0], pair[1].rstrip(",")
            sum_freqs += float(pair[1].rstrip(","))
            distr_count += 1
            distr[ pair[0] ] = float(pair[1].rstrip(","))
        #print sorted( distr.iteritems(), key=itemgetter(1), reverse=True )[0:5]
        #print distr_count, sum_freqs
        self.distr = copy.deepcopy(\
                    sorted( distr.iteritems(), key=itemgetter(1), reverse=True ) )
        self.distr_count = distr_count
        self.sum_freqs = sum_freqs

    def in_distr(self, w):
        getword = itemgetter(0)
        res = find( lambda ding: ding[0] == w, self.distr )
        return res
        #return w in map(getword, self.distr)


# first/last woord in <P> indicator meesturen?
# This calls all the classifiers, and decides if we should edit.
'''
s1: timblserver -i google-5gms.dets-negpos.0.igtree -a1 +D +vdb+di -S 2012
s2: timblserver -i google-5gms.preps-negpos.0.igtree -a1 +D +vdb+di -S 2013
s3: timblserver -i google-5gms.dets.0.igtree -a1 +D +vdb+di -S 2014
s4: timblserver -i google-5gms.preps.0.igtree -a1 +D +vdb+di -S 2015

setting MD_PNR MT_PNR RD_DS RD_F RD_R RT_DS RT_F RT_R UD_NPR UT_NPR
-------------------------------------------------------------------
     F	    20	   20	 20   20   20	 50    5   20	   2	  4 << high F-score (13/9/5)
     G	    10	   10	 50    5   20	 50    5   20	   2	  4 << high recall (21/14/9)
     D	    20     20     5   50   10    50    5   20     10     20 << high precision (13/10/6)
-------------------------------------------------------------------
'''

# instance, classifier, list-with-allowed-classes, error_label, max_distr_length
# firstsecond_r (like pos-neg), toprest_r
def apply( i, c, el, kvs ): #l, max_dl, fs_r, tr_r ):
    dbg( "apply: " )
    result = {}
    result['res'] = False
    result['n'] = el
    c.classify( i.instance + " ?" )
    result['c'] = c.classification
    # bunch of if/thens? if max_dl and dis > max_dl ... etc
    # lisp like eval? nah
    #
    # First test, the classification must be in the allowed list
    if 'l' in kvs:
        dbg( el, " class:", c.classification )
        if not c.classification in kvs['l']:
            return result
        #else append "inlist" or something?
    # Second test, the distribution must have max_dl items.
    if 'max_dl' in kvs:
        result['max_dl'] = len(c.distr)
        dbg( el, repr(len(c.distr)), " ", str(kvs['max_dl']) )
        if not len(c.distr) <= kvs['max_dl']:
            return result
    #Third test, first.second must be larger or equal than fs_r.
    if 'fs_r' in kvs:
        firstsecond_r = -1 #cannot compute
        if len(c.distr) > 1:
            firstsecond_r = float(c.distr[0][1]) / float(c.distr[1][1])
        result['fs_r'] = firstsecond_r
        dbg( el, " firstsecond_r = ", str(firstsecond_r) )
        if firstsecond_r == -1 or not firstsecond_r >= kvs['fs_r']:
            return result
    #Fourth test, ratio top classification:rest of distribution
    if 'tr_r' in kvs:
        toprest_r = -1 #cannot compute
        if len(c.distr) > 1:
            toprest_r = float(c.distr[0][1]) / float(c.sum_freqs-c.distr[0][1])
        result['tr_r'] = toprest_r
        dbg( el, " toprest_r = ", str(toprest_r) )
        if toprest_r == -1 or not toprest_r > kvs['tr_r']:
            return result
    #Fifth test, classification:original ratio from distribution
    if 'co_r' in kvs:
        classorig_r = -1
        dbg( i.word )
        in_distr = c.in_distr( i.word )
        dbg( repr(in_distr) )
        if in_distr:
            classorig_r = float(c.distr[0][1]) / float(in_distr[1])
        result['co_r'] = classorig_r
        dbg( el, " classorig_r = ", str(classorig_r) )
        if classorig_r == 1 or not classorig_r > kvs['co_r']:
            return result
    # All relevant checks passed, return true
    result['res'] = True
    return result

            
# -----------------------------------------------------------------------------
# ----- MAIN starts here
# -----------------------------------------------------------------------------


if __name__ == "__main__":

#timblserver -i google-5gms.dets-negpos.0.igtree -a1 +D +vdb+di -S 2012
#timblserver -i google-5gms.preps-negpos.0.igtree -a1 +D +vdb+di -S 2013
#timblserver -i google-5gms.dets.0.igtree -a1 +D +vdb+di -S 2014
#timblserver -i google-5gms.preps.0.igtree -a1 +D +vdb+di -S 2015


    s1 = TServer("localhost", 2012, "s1")
    s2 = TServer("localhost", 2013, "s2")
    s3 = TServer("localhost", 2014, "s3")
    s4 = TServer("localhost", 2015, "s4")

    #s1.classify( "it lies the bed _" )
    #print s1.distr
    #s2.classify( "it lies the bed _" )
    #print s2.distr
    #s3.classify( "it lies the bed _" )
    #print s3.distr
    #s4.classify( "it lies the bed _" )
    #print s4.distr

    debug      = False
    verbose    = 0
    dirmode    = False
    run        = "0"
    tweak      = False

    try:
        opts, args = getopt.getopt(sys.argv[1:], "df:r:tv", ["file="])
    except getopt.GetoptError, err:
        # print help information and exit:
        print str(err) # will print something like "option -a not recognized"
    #usage()
        sys.exit(2)
    for o, a in opts:
        if o in ("-f", "--file"):
            a_file = a
        elif o in ("-d"):
            dirmode = True
        elif o in ("-v"):
            verbose = 1
        elif o in ("-t"):
            tweak = True
        elif o in ("-r"):
            run = a
        else:
            assert False, "unhandled option"

    #window_lr( "een korte string.", 2, 2 )
    #print "--"
    #res = window( "een korte string.", 2, 2 )
    #for r in res:
    #print r

# --

    '''
    RT	Replace preposition	When I arrived at London
    MT	Missing preposition	I gave it John
    UT	Unnecessary preposition	I told to John that ...
    RD	Replace determiner	Have the nice day
    MD	Missing determiner	I have car
    UD	Unnecessary determiner	There was a lot of the traffic
    '''

    '''
    Parameters (INPUT):
    l      : list with allowed classifications
    max_dl : max length of distribution
    fs_r   : ratio between top-1 freq and top-2 freq
    tr_r   : ratio between top-1 freq and sum of rest
    co_r   : ratio between classification and original word in text, if that is
             found in the distribution

    OUTPUT:
    res    : True/False depending of parameter tests
    c      : classification
    n      : name of classifier
    '''
    #col1 = { 'l':['+'], 'max_dl':5, 'fs_r':1, 'tr_r':1, 'co_r':1 }
    col1 = { 'l':P, 'fs_r':1 }
    col2 = { 'l':PREPS, 'fs_r':1.5 }
    col3 = { 'l':DETS, 'fs_r':1.5 }
    dbg( a_file )
    f = open( a_file, "r" )
    for l in f:
        instances = window( l, 2, 2 )
        for i in instances:
            instance = INSTANCE( " ".join(i[:-1]), i[-1] )
            instance.output_all()

            s1_res2 = apply( instance, s1, "S1.2", col1 )
            #print "S1_RES", s1_res2
            if s1_res2['res']:
                s2_res2 = apply( instance, s2, "S2.2", col2 )
                if s2_res2['res']:
                    print "INSERT A PREP", s2_res2
            #print apply( instance, s1, ['-'], "S1/R", 1, 1, 2 )
            s3_res2 = apply( instance, s3, "S3.2", col1 )
            #print "S1_RES", s1_res2
            if s3_res2['res']:
                s4_res2 = apply( instance, s2, "S4.2", col3 )
                if s4_res2['res']:
                    print "INSERT A DET", s4_res2

            #UD
            res_ud = apply( instance, s1, "S1", { 'l':N, 'fs_r':1 } )
            print res_ud

#result_type,edit_txt = classifiers( pos, edit_count, a_file, part_Loop )

# loop over testfile, generate a new data file with the 4 classifiers
# output, after checking parameters, for next classifier...
# eg:
# it lies the chair (on) + of - _

