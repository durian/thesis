#!/usr/bin/python
# -*- coding: utf-8 -*-
#
import os
import re
import sys
from xml.dom import minidom
import datetime
import time
from time import strptime
from time import asctime
from math import sqrt,sin,cos,asin,pi
import getopt
import math
import codecs

#import unicodedata
#unicodedata.lookup('ZERO WIDTH NO-BREAK SPACE')
#u'\ufeff'

from xml.etree.ElementTree import ElementTree as xml
from xml.etree.ElementTree import Element, SubElement, Comment, fromstring

# ---

#http://code.activestate.com/recipes/498286-elementtree-text-helper/
def _textlist(self, _addtail=False):
    '''Returns a list of text strings contained within an element and its sub-elements.
       We concatenate only P and "original" from the edit XML, giving us the original text."
    '''
    result = []
    tag =  self.tag
    #print tag
    if self.text is not None:
        if self.tag == "P" or tag == "original":
            result.append(self.text)
    for elem in self:
        result.extend(elem.textlist(True))
    if _addtail and self.tail is not None:
        result.append(self.tail)
    return result

from xml.etree.ElementTree import _Element
_Element.textlist = _textlist

# ---

def getText(nodelist):
    rc = ""
    try:
        for node in nodelist:
            if node.nodeType == node.TEXT_NODE:
                rc = rc + node.data
    except:
        rc = "ERROR"
    return rc

def dbg(*strs):
    if verbose > 0:
        print >> sys.stderr, "DBG:", "".join(strs)

'''
[ _ _ _ the quick brown fox _ _ _ ]
        ^start
        [ pos-LC : pos+LC ]
'''

def window( str, lc, rc ):
    str_ar = str.split()
    words = ["_" for x in range(lc)] + str_ar +  ["_" for x in range(rc)]
    #print words
    for i in range(len(str_ar)):
        #              shift to right index by added lc
        res = words[i:i+lc] + words[i+1+lc:i+rc+1+lc] + [str_ar[i]]
        #print i, words[i:i+lc],words[i+1+lc:i+rc+1+lc],"=",str_ar[i]
        print " ".join(res)

#window( "0 1 2 3", 2, 1)
#window( "0 1 2 3", 2, 0)
#window( "0 1 2 3", 0, 2)
#sys.exit(8)

def extract( str, start_i, end_i, type, ctxt, lc, rc ):
    lstr = "_ _ _ _ " + str + " _ _ _ _ _"
    lpos = start_i+len("_ _ _ _ ")
    #print "-->", lpos
    words_l = 0
    while lpos > 0 and words_l <= lc:
        if lstr[lpos] == " ":
            words_l += 1
        lpos -= 1
    lpos += 2 #adjust back, we are at last letter prev word
    #
    rpos = end_i+len("_ _ _ _ ")+1 #end_i was start_i
    target = str[start_i:end_i]
    dbg( target )
    #print "-->", rpos
    words_r = 0
    while rpos < len(lstr) and words_r < rc:
        if lstr[rpos] == " ":
            words_r += 1
        rpos += 1

    # Bij R een woord opschuiven naar rechts? om het target heen dus?
        
    #print "-->", lstr[lpos : start_i+len("_ _ _ _ ")]
    #print "-->", lstr[lpos : start_i+len("_ _ _ _ ")], "/",repr(ctxt)+type,"/", lstr[ start_i+len("_ _ _ _ ") : rpos]
    if type[0:1] == "M":
        ctxt = ctxt.strip()
        print lstr[lpos : start_i+len("_ _ _ _ ")-1], lstr[ start_i+len("_ _ _ _ ") : rpos-1], "("+ctxt+type+")" #+type
        output[type].write( lstr[lpos : start_i+len("_ _ _ _ ")-1]+" "+lstr[ end_i+len("_ _ _ _ ") : rpos-1]+" "+ctxt+"\n" )
    if type[0:1] == "U":
        if ctxt == None:
            ctxt = "DEL"
        ctxt = ctxt.strip()
        print lstr[lpos : start_i+len("_ _ _ _ ")-1], lstr[ start_i+len("_ _ _ _ ") : rpos-1], "("+ctxt+type+")" #+type
        output[type].write( lstr[lpos : start_i+len("_ _ _ _ ")-1]+" "+lstr[ end_i+len("_ _ _ _ ") : rpos-1]+" "+ctxt+"\n" )
    if type[0:1] == "R":
        if ctxt == None:
            ctxt = "DEL"
        ctxt = ctxt.strip()
        print lstr[lpos : start_i+len("_ _ _ _ ")-1].encode('latin-1', 'ignore'), lstr[ end_i+len("_ _ _ _ ") : rpos-1].encode('latin-1', 'ignore'), "("+ctxt+type+")" #+type
        output[type].write( lstr[lpos : start_i+len("_ _ _ _ ")-1]+lstr[ end_i+len("_ _ _ _ ") : rpos-1]+" "+ctxt+"\n" )

# ------------------------

debug = False
verbose = 0
dirmode = False

try:
    opts, args = getopt.getopt(sys.argv[1:], "df:v", ["file="])
except getopt.GetoptError, err:
    # print help information and exit:
    print str(err) # will print something like "option -a not recognized"
    #usage()
    sys.exit(2)
for o, a in opts:
    if o in ("-f", "--file"):
        a_file = a
    elif o in ("-d"):
        dirmode = True
    elif o in ("-v"):
        verbose = 1
    else:
        assert False, "unhandled option"

all_files = []
if dirmode:
    # Find all
    #
    test_str = "^.*\.xml$"
    test     = re.compile(test_str, re.IGNORECASE)
    files = os.listdir( "data/web/" )
    files = filter(test.search, files)
    
    for a_file in files:
            all_files.append("data/web/"+a_file)
else:
  all_files.append( a_file )

# --

'''
RT	Replace preposition	When I arrived at London
MT	Missing preposition	I gave it John
UT	Unnecessary preposition	I told to John that ...
RD	Replace determiner	Have the nice day
MD	Missing determiner	I have car
UD	Unnecessary determiner	There was a lot of the traffic
'''
counts = {}
for et in ["RT", "MT", "UT", "RD", "MD", "UD"]:
    counts[et] = 0
langs = {}

output = {}
for et in ["RT", "MT", "UT", "RD", "MD", "UD"]:
    output[et] = codecs.open("out_"+et+".txt", "w", "utf-8-sig")

#temp = codecs.open("tmp", "w", "utf-8-sig")
#temp.write("hi mom\n")
#temp.write(u"This has ♭")
#temp.close()

for a_file in all_files:
      
    dbg( a_file )
    print a_file
    
    tree = xml()
    tree.parse( a_file )
    root = tree.getroot()
    parts_list = root.findall("BODY/PART")
    #print parts_list
    for part in parts_list:
        part_id = part.attrib["id"]
        dbg( "\nPART", part_id, "\n" )
        p_list = part.findall("P")
        part_txt = ""
        for p in p_list:
            this_txt = "<P>"+"".join(p.textlist())+"</P>"
            dbg( this_txt )
            try:
                result_xml = fromstring( this_txt )
                part.remove( p ) #erase old XML
                part.append( result_xml ) #replace with our XML
            except:
                "Error in parsing XML."
    #http://www.doughellmann.com/PyMOTW/xml/etree/ElementTree/create.html
    new_file = a_file[:-6] + "NE.xml"
    formatted_file = a_file[:-6] + "NE0.xml"
    print "Writing", new_file
    tree.write( new_file )
    # xmllint --format data/HOO2012TestData20120406/2001__.xml > ...
    print "Writing", formatted_file
    os.system( "/usr/bin/xmllint --format " + new_file + " > " + formatted_file )


