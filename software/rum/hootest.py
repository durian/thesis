#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Test a HOO2012 XML file, add <edit /> structure from Timbl output.
#
import os
import re
import sys
from xml.dom import minidom
import datetime
import time
from time import strptime
from time import asctime
from math import sqrt,sin,cos,asin,pi
import getopt
import math
import codecs
import socket
from time import sleep
from itertools import islice, izip
from operator import itemgetter
import random
import copy

#import unicodedata
#unicodedata.lookup('ZERO WIDTH NO-BREAK SPACE')
#u'\ufeff'

from xml.etree.ElementTree import ElementTree as xml
from xml.etree.ElementTree import Element, SubElement, Comment, fromstring

# ---

#http://code.activestate.com/recipes/498286-elementtree-text-helper/
def _textlist(self, _addtail=False):
    '''Returns a list of text strings contained within an element and its sub-elements.
       We concatenate only P and "original" from the edit XML, giving us the original text."
    '''
    result = []
    tag =  self.tag
    #print tag
    if self.text is not None:
        if self.tag == "P" or tag == "original":
            result.append(self.text)
    for elem in self:
        result.extend(elem.textlist(True))
    if _addtail and self.tail is not None:
        result.append(self.tail)
    return result

# Add above function to the Python XML functions.
from xml.etree.ElementTree import _Element
_Element.textlist = _textlist

def indent(elem, level=0):
    i = "\n" + level*"  "
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + "  "
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for elem in elem:
            indent(elem, level+1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i
            
# ---

# from /exp2/antalb/hoo2012/preps|dets
#
DETS = [ "a", "an", "another", "any", "every", "he", "her", "his", "its", "my", "no", "one", "other", "our", "own", "that", "the", "their", "these", "this", "those", "what", "which", "your"]
PREPS = [ "about", "after", "against", "along", "among", "around", "as", "at", "before", "behind", "below", "beside", "besides", "between", "by", "concerning", "down", "during", "for", "from", "in", "into", "near", "of", "off", "on", "onto", "out", "outside", "over", "referring", "regarding", "regards", "since", "than", "through", "throughout", "till", "to", "toward", "towards", "under", "until", "via", "with", "within", "without" ]

def getText(nodelist):
    rc = ""
    try:
        for node in nodelist:
            if node.nodeType == node.TEXT_NODE:
                rc = rc + node.data
    except:
        rc = "ERROR"
    return rc

def dbg(*strs):
    if verbose > 0:
        print >> sys.stderr, "DBG:", "".join(strs)

def find(f, seq):
  """Return first item in sequence where f(item) == True."""
  for item in seq:
    if f(item): 
      return item
  
'''
[ _ _ _ the quick brown fox _ _ _ ]
        ^start
        [ pos-LC : pos+LC ]
'''
def window_lr( str, lc, rc ):
    str_ar = str.split()
    words = ["_" for x in range(lc-1)] + ["<S>"] + str_ar + ["</S>"] + ["_" for x in range(rc-1)]
    #print words
    for i in range(len(str_ar)):
        #              shift to right index by added lc
        res = words[i:i+lc] + words[i+1+lc:i+rc+1+lc] + [str_ar[i]]
        #print i, words[i:i+lc],words[i+1+lc:i+rc+1+lc],"=",str_ar[i]
        print " ".join(res)

# window 2-0-2-T
# Does "tokenisation", removes and expands the punctuation attached to words,
# changing the instances. Same for punctuation on target.
# returns a list with [instance][target][taregtpunc]
def window( str, lc, rc ):
    str_ar = str.split()
    words = ["_" for x in range(lc)] + str_ar + ["_" for x in range(rc)]
    #words = ["_" for x in range(lc-1)] + ["<S>"] + str_ar + ["</S>"] + ["_" for x in range(rc-1)]
    #print words
    result = []
    for i in range(len(str_ar)):
        #              shift to right index by added lc
        res = copy.deepcopy(words[i:i+lc] + words[i+1+lc:i+rc+1+lc])
        idx = 0
        tmp_res = copy.deepcopy(res)
        for rb in tmp_res:
            if len(rb) > 1 and rb[-1:] in "!?.,":
                lp = rb[0:-1] #left part
                rp = rb[-1:]  #right part
                #replacement depends on 0,1 or 2,3 positions
                if idx == 3:
                    res[3] = lp
                if idx == 2:
                    res[2] = lp
                    res[3] = rp
                if idx == 1:
                    res[1] = rp
                    res[0] = lp
                if idx == 0:
                    res[0] = rp
                #res[idx:idx+2] = [lp, rp]
                #print "PUNC", idx, lp, rp, res
            idx = idx + 1
        tar  = str_ar[i]
        ptar = ''
        if tar[-1:] in "!?.,":
            ptar = tar[-1:] #punctuation part
            tar = tar[0:-1] #left part
            #in this case, move ptar in instance also
            res[3] = res[2]
            res[2] = ptar
        result.append( [" ".join(res), tar, ptar] )
    #print result
    return result

#window( "0 1 2 3", 2, 1)
#window( "0 1 2 3", 2, 0)
#window( "0 1 2 3", 0, 2)
#sys.exit(8)

# Also put edit info in here, then loop over POS list to
# create a new <P> string with edit structure, which can be parsed
# to XML again.
'''
Contains a list with words, instance, start/endpos, etc
'''
class POS():
    def __init__(self, w, s, e, i):
        self.word = w      #the word (also target)
        self.start = s     #startpos according to counting rules
        self.end = e       #endpos according to counting rules, no space!
        self.instance = i  #instance around word
        self.etype = None  #error type, MT, RT, etc
        
    def output_pos(self):
        print self.word, self.start, self.end

    def output_all(self):
        print self.word, self.start, self.end, self.instance
        
class TServer():
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.s = None
        self.file = None
        try:
            self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.s.connect((self.host, self.port))
            self.file = self.s.makefile("rb") # buffered
            #self.s.settimeout(1)
            #print self.s.gettimeout()
        except:
            print "error"
            return
        #self.recv_data()
        #self.s.sendall("base default\n")
        #self.recv_data()
        self.readline()
        print "("+self.data+")"

    def read(self, maxbytes = None):
        "Read data from server."
        if maxbytes is None:
            return self.file.read()
        else:
            return self.file.read(maxbytes)

    def readline(self):
        "Read a line from the server.  Strip trailing CR and/or LF."
        CRLF = "\r\n"
        s = self.file.readline()
        if not s:
            raise EOFError
        if s[-2:] == CRLF:
            s = s[:-2]
        elif s[-1:] in CRLF:
            s = s[:-1]
        self.data = s
    
    def recv_data(self):
        self.data = ""
        recv_data = ""
        while 0:
            try:
                recv_data = self.s.recv(4096)
                print "["+recv_data+"]"
                self.data = self.data+recv_data
            except:
                #print "Received data: ", self.data
                break
                pass
            if not recv_data:
                #print "Received data: ", self.data
                break
                pass
        
    def classify( self, i ):
        if not self.s:
            return
        #print "--SERVER--> classify "+i
        self.i = i
        try:
            self.s.sendall("classify "+i+"\n")
        except:
            # UTF-8 problems? save as iso-8859-1
            #print "classify "+i
            tmp0 = "classify "+i
            tmp1 = tmp0.encode("iso-8859-1", "replace")
            self.s.sendall(tmp1+"\n")
            #sys.exit(8)
        self.readline()
        self.grok( self.data )
        #print self.distr
        
    def grok( self, tso ):
        '''
        Parse the TimblServer output
        '''
        #m = re.findall(r'CATEGORY {(.*)} ', tso)
        #for url in m:
        #print url
        #CATEGORY {concerned} DISTRIBUTION { concerned 2.00000, volatile 1.00000 } DISTANCE {0}
        #print "["+tso+"]"
        # error checking
        m = re.search( r'ERROR', tso )
        if m:
            print "ERROR"
            print tso
            print self.i
            sys.exit(8)
        m = re.search( r'CATEGORY {(.*)} DISTRIBUTION { (.*) } DISTANCE {(.*)}', tso )
        #print m.group(1) #concerned
        self.classification = m.group(1)
        #print m.group(2) #concerned 2.00000, volatile 1.00000
        #print m.group(3) #0
        bits = m.group(2).split()
        pairs = zip(bits[::2], bits[1::2])
        #remove silly commas from frequencies
        distr = {}
        sum_freqs = 0
        distr_count = 0
        for pair in pairs:
            #print pair[0], pair[1].rstrip(",")
            sum_freqs += float(pair[1].rstrip(","))
            distr_count += 1
            distr[ pair[0] ] = float(pair[1].rstrip(","))
        #print sorted( distr.iteritems(), key=itemgetter(1), reverse=True )[0:5]
        #print distr_count, sum_freqs
        self.distr = copy.deepcopy(\
                    sorted( distr.iteritems(), key=itemgetter(1), reverse=True ) )
        self.distr_count = distr_count
        self.sum_freqs = sum_freqs

    def in_distr(self, w):
        getword = itemgetter(0)
        res = find( lambda ding: ding[0] == w, self.distr )
        return res
        #return w in map(getword, self.distr)

def make_edit(s, e, i, t, o, c, n, p):
    '''
    Create an XML string with the edit structure. Will be parsed to XML
    at the end again.
    s start-pos
    e end-pos
    i index
    t type (MD, UT,...)
    o original (XML str)
    c correction (XML str)
    n file_number
    p part

    <edit type="RT" file="0098" part="1" index="0006" start="771" end="772">
    '''
    edit_txt = "<edit"
    edit_txt = edit_txt + " type=\""+t
    edit_txt = edit_txt + "\" file=\""+n
    edit_txt = edit_txt + "\" part=\""+p
    edit_txt = edit_txt + "\" index=\""+repr(i).zfill(4)
    edit_txt = edit_txt + "\" start=\""+repr(s)
    edit_txt = edit_txt + "\" end=\""+repr(e)
    edit_txt = edit_txt + "\">"
    edit_txt = edit_txt + "<original>"+o+"</original>"
    edit_txt = edit_txt + "<corrections><correction>"+c+"</correction></corrections>"
    edit_txt = edit_txt + "</edit>"
    return edit_txt

# first/last woord in <P> indicator meesturen?
# This calls all the classifiers, and decides if we should edit.
'''
s1: timblserver -i google-5gms.dets-negpos.0.igtree -a1 +D +vdb+di -S 2012
s2: timblserver -i google-5gms.preps-negpos.0.igtree -a1 +D +vdb+di -S 2013
s3: timblserver -i google-5gms.dets.0.igtree -a1 +D +vdb+di -S 2014
s4: timblserver -i google-5gms.preps.0.igtree -a1 +D +vdb+di -S 2015

setting MD_PNR MT_PNR RD_DS RD_F RD_R RT_DS RT_F RT_R UD_NPR UT_NPR
-------------------------------------------------------------------
     F	    20	   20	 20   20   20	 50    5   20	   2	  4 << high F-score (13/9/5)
     G	    10	   10	 50    5   20	 50    5   20	   2	  4 << high recall (21/14/9)
     D	    20     20     5   50   10    50    5   20     10     20 << high precision (13/10/6)
-------------------------------------------------------------------
'''

# RUN 0
MD_PNR = 20 # MD posneg ratio must be larger than this
MT_PNR = 20 # MT posneg ratio must be larger than this

RD_DS = 20   # RD distribution size must be less than this
RD_F  = 20 # RD frequency of top classification must be larger than this
RD_R  = 20  # RD ratio between classification and original

RT_DS = 50   # RT distribution size must be less than this
RT_F  = 5 # RT frequency of top classification must be larger than this
RT_R  = 20  # RT ratio between classification and original

UD_NPR = 2 # UD negpos ratio larger than this

UT_NPR = 4 # UT negpos ratio larger than this

#----

#RUN1
if False:
    MD_PNR = 10     # MD posneg ratio must be larger than this
    MT_PNR = 10     # MT posneg ratio must be larger than this
    RD_DS = 50 # RD distribution size less than this
    RD_F  = 5      # RD frequency of top classification larger than this
    RD_R  = 20      # RD ratio classification:original
    RT_DS = 50 # RT distribution size less than this
    RT_F  = 5      # RT frequency of top classification larger than this
    RT_R  = 20      # RT ratio classification:original
    UD_NPR = 2     # UD negpos ratio larger than this
    UT_NPR = 4     # UT negpos ratio larger than this

#----

#RUN2
if False:
    MD_PNR = 20     # MD posneg ratio must be larger than this
    MT_PNR = 20     # MT posneg ratio must be larger than this
    RD_DS = 5 # RD distribution size less than this
    RD_F  = 50      # RD frequency of top classification larger than this
    RD_R  = 10      # RD ratio classification:original
    RT_DS = 50 # RT distribution size less than this
    RT_F  = 5      # RT frequency of top classification larger than this
    RT_R  = 20      # RT ratio classification:original
    UD_NPR = 10     # UD negpos ratio larger than this
    UT_NPR = 20    # UT negpos ratio larger than this


#---- The following settings switch off the tweaking, if enabled:
#
if False:
    MD_PNR = 1     # MD posneg ratio must be larger than this
    MT_PNR = 1     # MT posneg ratio must be larger than this
    RD_DS = 500000 # RD distribution size less than this
    RD_F  = 1      # RD frequency of top classification larger than this
    RD_R  = 1      # RD ratio classification:original
    RT_DS = 500000 # RT distribution size less than this
    RT_F  = 1      # RT frequency of top classification larger than this
    RT_R  = 1      # RT ratio classification:original
    UD_NPR = 1     # UD negpos ratio larger than this
    UT_NPR = 1     # UT negpos ratio larger than this
#----

def classifiers( pos, edit_count, filename, part_id ):
    #pos.output_all()
    error_type = None
    et = ""
    # First try the yes/no-det classifier (s1).
    s1.classify( pos.instance + " ?" )
    #print s1.distr[0], s1.distr[1]
    # We get a yes
    if s1.classification == "+" and len(s1.distr) > 1:
        # maybe we had a DET, then we are OK, otherwise...
        if pos.word not in DETS:
            # Ratio between yes:no
            posneg_r = float(s1.distr[0][1]) / float(s1.distr[1][1])
            # Ratio is > ...
            if posneg_r > MD_PNR:
                # We want a determiner at this point
                # Call s3 to determine which DET we want
                s3.classify( pos.instance + " ?" )
                print "INSERT DET:", posneg_r, s3.classification, repr(s1.distr), repr(s3.distr[0])
                error_type = "MD"
                dbg( repr(s3.distr) )
    #
    # Try the PREP yes/no classifier
    s2.classify( pos.instance + " ?" )
    #print s2.classification
    # We got a yes
    if s2.classification == "+" and len(s2.distr) > 1:
        # maybe we had a PREP, then we are OK, otherwise...
        if pos.word not in PREPS:
            # Ratio between yes:no
            posneg_r = float(s2.distr[0][1]) / float(s2.distr[1][1])
            # If > ..
            if posneg_r > MT_PNR:
                # Which PREP do we want?
                s4.classify( pos.instance + " ?" )
                print "INSERT PREP:", posneg_r, s4.classification, repr(s2.distr), repr(s4.distr[0])
                error_type = "MT"
                dbg( repr(s4.distr) )
    #
    # Replace a DET
    # Check if target is in det list
    # only replace if pos.word is not in distribution?
    if pos.word.lower() in DETS:
        # OK, we have a DET in the text, is it what the classifier wants?
        s3.classify( pos.instance + " ?" )
        # The classifier thinks it should be another DET
        if pos.word != s3.classification:
            # Is the target (pos.word) in the classifiers distribution?
            # If yes, set the classification:orig ratio.
            co_r = 1000000
            ding = s3.in_distr( pos.word )
            if ding:
                co_r = float(s3.distr[0][1]) / float(ding[1])
            # Only if the distribution is < ... elements
            if len(s3.distr) < RD_DS:
                # and frequency > ...
                if s3.distr[0][1] > RD_F and co_r > RD_R:
                    #print s3.in_distr( pos.word )
                    print "REPLACE DET:", ding, pos.word, s3.classification, s3.distr[0]
                    error_type = "RD"
                    dbg( repr(s3.distr) )
    #
    # Replace a PREP
    # Check if target is in preps list
    if pos.word.lower() in PREPS:
        # OK, we have a PREP in the text, is it what the classifier wants?
        s4.classify( pos.instance + " ?" )
        # The classifier thinks it should be another PREP
        if pos.word != s4.classification:
            # Is the target (pos.word) in the classifiers distribution?
            # If yes, set the classification:orig ratio.
            co_r = 1000000
            ding = s4.in_distr( pos.word )
            if ding:
                co_r = float(s4.distr[0][1]) / float(ding[1])
            # but only if distribution < ... elements
            if len(s4.distr) < RT_DS:
                # and frequency > ...
                if s4.distr[0][1] > RT_F and co_r > RT_R:
                    print "REPLACE PREP:", ding, pos.word, s4.classification, s4.distr[0]
                    error_type = "RT"
                    dbg( repr(s4.distr) )
    #
    # Unnecessary DET
    if pos.word.lower() in DETS:
        s1.classify( pos.instance + " ?" )
        #print s1.distr[0], s1.distr[1]
        # We get a no
        if s1.classification == "-" and len(s1.distr) > 1:
            # Ratio between no:yes
            posneg_r = float(s1.distr[0][1]) / float(s1.distr[1][1])
            # Ratio is > ...
            if posneg_r > UD_NPR:
                # We do not want a determiner at this point
                print "UNNECESSARY DET.", posneg_r
                # Create edit structure
                error_type = "UD"
    #
    # Try the PREP yes/no classifier to determine unnecessary PREP
    if pos.word.lower() in PREPS:
        s2.classify( pos.instance + " ?" )
        # We got a no
        if s2.classification == "-" and len(s2.distr) > 1:
            # Ratio between no:yes
            posneg_r = float(s2.distr[0][1]) / float(s2.distr[1][1])
            # If > ...
            if posneg_r > UT_NPR:
                print "UNNECESSARY PREP.", posneg_r
                error_type = "UT"
    #
    file_number =  a_file[-8:-4]
    #
    if error_type == "MD":
        et =  make_edit(pos.start, pos.start, edit_count, "MD", "<empty/>", s3.classification+" ", file_number, part_id)
    if error_type == "MT":
        et =  make_edit(pos.start, pos.start, edit_count, "MT", "<empty/>", s4.classification+" ", file_number, part_id)
    #
    if error_type == "RD":
        et =  make_edit(pos.start, pos.end, edit_count, "RD", pos.word,\
                        s3.classification, file_number, part_id)
        et = et + " "
    if error_type == "RT":
        et =  make_edit(pos.start, pos.end, edit_count, "RT", pos.word,\
                        s4.classification, file_number, part_id)
        et = et + " "
    #
    if error_type == "UD":
        et =  make_edit(pos.start, pos.end+1, edit_count, "UD", pos.word+" ", "<empty/>", file_number, part_id)
    if error_type == "UT":
        et =  make_edit(pos.start, pos.end+1, edit_count, "UT", pos.word+" ", "<empty/>", file_number, part_id)
    #
    return [ error_type, et ]

            
# -----------------------------------------------------------------------------
# ----- MAIN starts here
# -----------------------------------------------------------------------------



#timblserver -i google-5gms.dets-negpos.0.igtree -a1 +D +vdb+di -S 2012
#timblserver -i google-5gms.preps-negpos.0.igtree -a1 +D +vdb+di -S 2013
#timblserver -i google-5gms.dets.0.igtree -a1 +D +vdb+di -S 2014
#timblserver -i google-5gms.preps.0.igtree -a1 +D +vdb+di -S 2015


s1 = TServer("localhost", 2012)
s2 = TServer("localhost", 2013)
s3 = TServer("localhost", 2014)
s4 = TServer("localhost", 2015)

debug      = False
verbose    = 0
dirmode    = False
run        = "0"
tweak      = False
edits_only = True

try:
    opts, args = getopt.getopt(sys.argv[1:], "df:r:tv", ["file="])
except getopt.GetoptError, err:
    # print help information and exit:
    print str(err) # will print something like "option -a not recognized"
    #usage()
    sys.exit(2)
for o, a in opts:
    if o in ("-f", "--file"):
        a_file = a
    elif o in ("-d"):
        dirmode = True
    elif o in ("-v"):
        verbose = 1
    elif o in ("-t"):
        tweak = True
    elif o in ("-r"):
        run = a
    else:
        assert False, "unhandled option"

the_dir = "data/HOO2012TestData20120406/"
test_str = "^....\.xml$"
if tweak:
    the_dir = "data/web/"
    test_str = "^....NE0\.xml$"
    
all_files = []
if dirmode:
    # Find all
    #
    test     = re.compile(test_str, re.IGNORECASE)
    files = os.listdir( the_dir )
    files = filter(test.search, files)
    
    for a_file in files:
        all_files.append( the_dir + a_file)
else:
  all_files.append( a_file )

# --

'''
RT	Replace preposition	When I arrived at London
MT	Missing preposition	I gave it John
UT	Unnecessary preposition	I told to John that ...
RD	Replace determiner	Have the nice day
MD	Missing determiner	I have car
UD	Unnecessary determiner	There was a lot of the traffic
'''
counts = {}
for et in ["RT", "MT", "UT", "RD", "MD", "UD"]:
    counts[et] = 0

for a_file in all_files:
      
    dbg( a_file )
    print a_file
    
    tree = xml()
    tree.parse( a_file )
    root = tree.getroot()
    parts_list = root.findall("BODY/PART")
    #print parts_list
    edits_xml = Element( "edits" )
    edits_txt = "<edits>\n"
    text_pos = 0
    edit_count = 0
    for part in parts_list:
        part_id = part.attrib["id"]
        dbg( "\nPART", part_id, "\n" )
        p_list = part.findall("P")
        part_txt = ""
        # Loop over individual <P> elements.
        start_pos = 0
        end_pos = 0
        word_count = 0
        p_count = 0
        #should we add one for each new <P> ?
        for p in p_list:
            # Grab whole text from the element.
            this_txt = "".join(p.textlist())
            dbg( this_txt,"\n" )
            words = this_txt.split()
            if words and words[-1] == '': #fix for last line 2097.xml
                words = words[0:-1]
            instances = window( this_txt, 2, 2)
            #print instances
            i_count = 0
            pos_list = []
            for word in words:
                end_pos = end_pos + len(word)
                #adjust for no-space after last word
                if word == words[-1]:
                    end_pos = end_pos - 1
                #print word, start_pos+word_count+p_count, end_pos+word_count+p_count
                ding = POS(word, start_pos+word_count+p_count, end_pos+word_count+p_count, instances[i_count][0])
                #ding.output_pos()
                pos_list.append( ding )
                start_pos = start_pos + len(word)
                #adjust for no-space after last word
                if word == words[-1]:
                    start_pos = start_pos - 1
                word_count = word_count + 1
                i_count = i_count + 1
            #print instances [0][0],"=", instances[0][1]
            #now loop over all POS and call TimbleServer to see what we do.
            #save edit info in pos, then output again afterwards.
            #
            # result_txt contains the next text plus <edit/>s. It will be
            # parsed in the end, and it will replace the original, plain
            # XML <P> element.
            result_txt = "<P>"
            for pos in pos_list:
                #pos.output_pos()
                # call classifiers, determine errors.
                # pos.word is inside the edit structure as <original> !
                # <edit> XML comes before the edit, after "prevword ", AFAICS
                result_type,edit_txt = classifiers( pos, edit_count, a_file, part_id )
                if result_type:
                    #print result_type, edit_txt
                    result_txt = result_txt + edit_txt #note no space
                    edit_count = edit_count + 1
                    counts[result_type] += 1
                    edit_xml = fromstring( edit_txt )
                    edits_txt = edits_txt + edit_txt + "\n"
                    #print edit_xml
                    edits_xml.append( edit_xml )
                else:
                    result_txt = result_txt + pos.word + " "
            if result_txt[-1] != ">":
                result_txt = result_txt[:-1] + "</P>" #if not ends with "..edit>"
            dbg( result_txt )
            # parse and replace original p
            try:
                result_xml = fromstring( result_txt )
                #p.clear() #erase old XML
                part.remove( p ) #erase old XML
                #p.append( p_new ) #replace with our XML
                part.append( result_xml ) #replace with our XML
            except:
                "Error in parsing XML."
            #
            p_count = p_count + 0 #or 1 to count <P>
            
    #http://www.doughellmann.com/PyMOTW/xml/etree/ElementTree/create.html
    new_file = a_file[:-4] + "VA.xml"
    formatted_file = a_file[:-4] + "VA"+run+".xml"
    edits_txt = edits_txt + "</edits>\n"
    if edits_only:
        try:
            print edits_xml
            edits_tree = xml( edits_xml )
            print "Writing", new_file
            #f = open( formatted_file, 'wb')
            #f.write( edits_txt )
            #f.close()
            edits_tree.write( new_file )
            print "Writing", formatted_file
            os.system( "/usr/bin/xmllint --format " + new_file + " > " + formatted_file )
            # and remove unformatted file
            os.unlink( new_file )
        except:
            "Error in saving XML."
    else:
        tree.write( new_file )
        # xmllint --format data/HOO2012TestData20120406/2001__.xml > ...
        print "Writing", formatted_file
        os.system( "/usr/bin/xmllint --format " + new_file + " > " + formatted_file )
        # and remove unformatted file
        os.unlink( new_file )

print len(all_files),"files."
for et in ["RT", "MT", "UT", "RD", "MD", "UD"]:
    print et, counts[et]

print "RUN",run, MD_PNR, MT_PNR, RD_DS, RD_F, RD_R, RT_DS, RT_F, RT_R, UD_NPR, UT_NPR
