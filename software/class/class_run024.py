#!/usr/bin/python
# -*- coding: utf-8 -*-
#
import getopt
import sys
import time
import os
import datetime
import random
import re
import platform
from operator import itemgetter
import operator
import cProfile
import threading
from threading import Thread, Lock
from Queue import Queue
from Queue import PriorityQueue
from Queue import Empty
import multiprocessing
from stimpy import *
#
import logging
import logging.handlers
#
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)-8s %(message)s')
#
rtfile  = logging.handlers.RotatingFileHandler("./class.log",maxBytes=1024*1024*10, backupCount=1)
console = logging.StreamHandler()
#
glog = logging.getLogger('CLASS')
rtformat='%(asctime)s %(levelname)-8s %(message)s'
rtformatter = logging.Formatter(fmt=rtformat)
rtfile.setFormatter(rtformatter)
glog.addHandler(rtfile)
glog.propagate = False #stop console log for this one
# Set up console
cformat='%(asctime)s %(message)s'
cformatter = logging.Formatter(fmt=cformat, datefmt="%Y%m%d %H:%M:%S")
console.setFormatter(cformatter)
console.setLevel(logging.INFO)
glog.addHandler(console)
#
glog.info("class starting.")
glog.info("See class.log for debug info.")
#

# Regexen
#
#NUM   = re.compile( r'''(\d+) | ((\d+[\.,:\/]?)+(\d+[:\/]?))''', re.DOTALL | re.VERBOSE)
#PUNC  = re.compile( r'''([][(){}<>=,\.\?\!;\'&:*+-/]+)''', re.DOTALL )
#CLEAN = re.compile( r'''([-A-Za-z]{3,})''', re.DOTALL | re.VERBOSE) # NOT FOR UTF8 texts!!

# Thread worker class thingy.
#
class myThread (threading.Thread):
    def __init__(self, threadID, server_id):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.res = []
        self.sid = server_id
        self.cont = True
        self.cnt = 0
    def debug(self, s):
        glog.debug(self.threadID+": "+s)
    def info(self, s):
        if not type(s) == "str":
            s = repr(s)
        glog.info(self.threadID+": "+s)
    def error(self, s):
        glog.error(self.threadID+": "+s)
    def process(self, idx, line, cmd):
        
        if cmd == "corr":
            if not sentences:
                #          handle instances.
                #print idx, line
                words = line.split()
                self.res = []
                self.debug("words "+str(words))
                target = words[-1:][0]
                x = (line, target, 0.0, 0.0, 0.0, '--', '-', 0, 1, 1, [])
                res = ans_sc(x) + " [ ]"
                c = servers[self.sid].get_classifier_by_trigger(target)
                if c:
                    #print c.name
                    res = c.classify_i( line )
                    res = ans_sc(res)
                    #print "classsifier", res #ans_str, wopr output? #add classifier used?
                r.put( (idx, [(0, res)]) )
            else:
                '''
                Operate per sentence is easier b/c missing/redundant classifiers
                Need to manually check different servers.
                  1) mising/reduntant prep/det
                  2) which prep/det
                '''
                words = line.split()
                self.res = []
                self.stats = { 'DETS_REP':0, 'DETS_RED':0, 'DETS_MIS':0, 'NOP':0, 'PREPS_REP':0, 'PREPS_RED':0, 'PREPS_MIS':0 }
                """
                Can't do a missing BEFORE the first word?
                """
                #self.debug("words "+str(words))
                #window the sentence
                #wl = servers[self.sid].window_all(words) #wl is index per context.
                #self.debug(repr(wl))
                #for w in wl:
                #    self.debug(w+" "+repr(wl[w]))

                """
                local_changes format: (type, w_orig, [w_repl], priority, instance_str)
                REP replace
                RED redundant
                MIS missing

                Classifiers are CLASS_PN_MIS, CLASS_PN_RED, CLASS_MIS and CLASS_REP
                """
                changes = []
                new_s = []
                """
                ------------------------------------ LOOP ----------------------------------------------
                """
                prev_was_class = None #mark if the previous word was a classmember, don't do MIS if it was.
                for i,w in enumerate(words):
                    local_changes = []
                    pos = len(new_s)
                    stat = "NOP"
                    self.debug(str(i)+"-"+str(pos)+" "+w+":"+repr(new_s)+"/"+repr(words[i:]))
                    """
                    Prepare possible instances around current position in wla. We take the modified
                    words before our current position in the instances.
                    """
                    wla = servers[self.sid].window_around(new_s + words[i:], pos)
                    if i == 0:
                        self.debug("wla="+repr(wla))
                    """
                    We need priorities on the changes, take highest priority.
                    """
                    if w in PREPS:
                        prev_was_class = 'P'
                        """
                        If a PREP, we can check if it is redundant or wrong.
                        If we have done the highest priority one, it is no use
                        doing the other one ("fired").

                        These normal classifiers could be error variants (?).
                        """
                        fired = 0
                        """
                        Check if we want a PREP here first (may be reduntant).
                        Error variant (EV) of the redundant classifier.
                        """
                        c = servers[self.sid].get_classifier_by_id("PREPS_PN_RED")
                        if not c:
                            self.error("PREPS_PN_RED not found.")
                            sys.exit(1)
                        if c:
                            """
                            The REDUNDANCY classifier. Have focus word, normal instance. Classification
                            is '-' for redundant, '+' for "it appears here".
                            """
                            instance = wla[c.ctx] #wl[c.ctx][i]
                            instance_str = ' '.join(instance)
                            res = c.classify_i( wla[c.ctx] )
                            if c.error:
                                self.error("PREPS_PN_RED returned error.")
                                self.error( repr(wla[c.ctx]) )
                                sys.exit(1)
                            instance_str += " ("+res[1]+")"
                            self.debug( "PREPS_PN_RED/"+w+"/"+c.ctx+"/"+instance_str )
                            #self.debug( instance_str )
                            # If res == '-', it is considered redundant.
                            if res[1] == "-":
                                ans = ans_to_dict( res )
                                self.debug( str(ans) )
                                # No more parameters
                                posneg_r = float(ans['posneg_r'] )
                                if posneg_r == 0 or posneg_r > PARAMS["UP_PNR"]: #unnecessary/redundant
                                    local_changes.append( ("RED", w, [], 1, instance_str) )
                                    fired = 1 
                                    stat = "PREPS_RED"
                        """
                        If it wasn't redundant, it might be wrong.
                        """
                        c = servers[self.sid].get_classifier_by_id("PREPS_REP")
                        if not c:
                            self.error("PREPS_REP not found.")
                            sys.exit(1)
                        if fired < 1 and c:
                            instance = wla[c.ctx] #wl[c.ctx][i]
                            instance_str = ' '.join(instance)
                            res = c.classify_i( wla[c.ctx] )
                            if c.error:
                                self.error("PREPS_REP returned error.")
                                self.error( repr(wla[c.ctx]) )
                                sys.exit(1)
                            instance_str += " ("+res[1]+")"
                            self.debug( "PREPS_REP/"+w+"/"+c.ctx+"/"+instance_str )
                            if res[1] != w:
                                ans = ans_to_dict( res )
                                #self.debug( str(ans) )
                                # we blindly accept answer
                                local_changes.append( ("REP", w, [res[1]], 2, instance_str) )
                                fired = 2
                                stat = "PREPS_REP"
                                    
                    if w in DETS:
                        prev_was_class = 'D'
                        """
                        If a DET, we can check if it is redundant or wrong.
                        Error variant.
                        """
                        fired = 0
                        c = servers[self.sid].get_classifier_by_id("DETS_PN_RED")
                        if not c:
                            self.error("DETS_PN_RED not found.")
                            sys.exit(1)
                        if c:
                            instance = wla[c.ctx]
                            instance_str = ' '.join(instance)
                            res = c.classify_i( wla[c.ctx] )
                            if c.error:
                                self.error("DETS_PN_RED returned error.")
                                self.error( repr(wla[c.ctx]) )
                                sys.exit(1)
                            instance_str += " ("+res[1]+")"
                            self.debug( "DETS_PN_RED/"+w+"/"+c.ctx+"/"+instance_str )
                            #self.debug( instance_str )
                            if res[1] == "-":
                                ans = ans_to_dict( res )
                                self.debug( str(ans) )
                                # No more parametrs
                                posneg_r = ans['posneg_r'] 
                                if posneg_r == 0 or posneg_r > PARAMS["UD_PNR"]: #unnecessary/redundant
                                    local_changes.append( ("RED", w, [], 1, instance_str) )
                                    fired = 1
                                    stat = "DETS_RED"
                        """
                        If not redundant, check if correct.
                        """
                        c = servers[self.sid].get_classifier_by_id("DETS_REP")
                        if not c:
                            self.error("DETS_REP not found.")
                            sys.exit(1)
                        if fired < 1 and c:
                            instance = wla[c.ctx]
                            instance_str = ' '.join(instance)
                            #self.debug( instance_str )
                            res = c.classify_i( wla[c.ctx] )
                            if c.error:
                                self.error("DETS_REP returned error.")
                                self.error( repr(wla[c.ctx]) )
                                sys.exit(1)
                            instance_str += " ("+res[1]+")"
                            self.debug( "DETS_REP/"+w+"/"+c.ctx+"/"+instance_str )
                            if res[1] != w:
                                ans = ans_to_dict( res )
                                #self.debug( str(ans) )
                                #accept without params
                                local_changes.append( ("REP", w, [res[1]], 2, instance_str) )
                                fired = 2
                                stat = "DETS_REP"
                        
                    if w not in DETS and w not in PREPS:
                        """
                        Check for a missing prep/det. Look at timbl dist to get best one.
                        The MISSING classifier. Without focus/target word. Classification
                        is '+' for missing ("it appears here"), '-' for not missing.
                        """
                        c = servers[self.sid].get_classifier_by_id("PREPS_PN_MIS")
                        if not c:
                            self.error("PREPS_PN_MIS not found.")
                            sys.exit(1)
                        if c and prev_was_class != 'P':
                            instance = wla[c.ctx]
                            instance_str = ' '.join(instance)
                            #self.debug( c.ctx+"/"+instance_str )
                            res = c.classify_i( wla[c.ctx] )
                            if c.error:
                                self.error("PREPS_PN_MIS returned error.")
                                self.error( repr(wla[c.ctx]) )
                                sys.exit(1)
                            instance_str += " ("+res[1]+")"
                            self.debug( "PREPS_PN_MIS/"+w+"/"+c.ctx+"/"+instance_str )
                            #self.debug( instance_str )
                            # If res == '+', it is considered mising.
                            if res[1] == "+":
                                ans = ans_to_dict( res )
                                self.debug( str(ans) )
                                posneg_r = ans['posneg_r'] 
                                if posneg_r == 0 or posneg_r > PARAMS["MP_PNR"]:#missing prep PN ratio
                                    c = servers[self.sid].get_classifier_by_id("PREPS_MIS")
                                    if not c:
                                        self.error("PREPS_MIS not found.")
                                        sys.exit(1)
                                    if c:
                                        instance = wla[c.ctx]
                                        instance_str = ' '.join(instance)
                                        #self.debug( instance_str )
                                        res = c.classify_i( wla[c.ctx] )
                                        if c.error:
                                            self.error("PREPS_MIS returned error.")
                                            self.error( repr(wla[c.ctx]) )
                                            sys.exit(1)
                                        instance_str += " ("+res[1]+")"
                                        self.debug( "PREPS_MIS/"+w+"/"+c.ctx+"/"+instance_str )
                                        """
                                        If we find a missing prep/det, it goes BEFORE the current pos.
                                        """
                                        if len(c.distr) > PARAMS['MP_DS'] or len(c.distr) == 0:
                                            local_changes.append( ("MIS", w, [res[1], w], 1, instance_str) )
                                            stat = "PREPS_MIS"

                        c = servers[self.sid].get_classifier_by_id("DETS_PN_MIS")
                        if not c:
                            self.error("DETS_PN_MIS not found.")
                            sys.exit(1)
                        if c and prev_was_class != 'D':
                            instance = wla[c.ctx]
                            instance_str = ' '.join(instance)
                            #self.debug( c.ctx+"/"+instance_str )
                            res = c.classify_i( wla[c.ctx] )
                            if c.error:
                                self.error("DETS_PN_MIS returned error.")
                                self.error( repr(wla[c.ctx]) )
                                sys.exit(1)
                            instance_str += " ("+res[1]+")"
                            self.debug( "DETS_PN_MIS/"+w+"/"+c.ctx+"/"+instance_str )
                            #self.debug( instance_str )
                            if res[1] == "+":
                                ans = ans_to_dict( res )
                                self.debug( str(ans) )
                                posneg_r = ans['posneg_r'] 
                                if posneg_r == 0 or posneg_r > PARAMS["MD_PNR"]: #missing DET PN ratio:
                                    c = servers[self.sid].get_classifier_by_id("DETS_MIS")
                                    if not c:
                                        self.error("DETS_MIS not found.")
                                        sys.exit(1)
                                    if c:
                                        instance = wla[c.ctx]
                                        instance_str = ' '.join(instance)
                                        #self.debug( instance_str )
                                        res = c.classify_i( wla[c.ctx] )
                                        if c.error:
                                            self.error("DETS_MIS returned error.")
                                        instance_str += " ("+res[1]+")"
                                        self.debug( "DETS_MIS/"+w+"/"+c.ctx+"/"+instance_str )
                                        """
                                        If we find a missing prep/det, it goes BEFORE the current pos.
                                        """
                                        if len(c.distr) > PARAMS['MD_DS'] or len(c.distr) == 0:
                                            local_changes.append( ("MIS", w, [res[1], w], 1, instance_str) )
                                            stat = "DETS_MIS"
                    prev_was_class = None

                    """
                    Take the change, and apply it. Take highest priority.
                    """
                    self.stats[stat] += 1
                    if len(local_changes) > 0:
                        self.debug("lc="+repr(local_changes))
                        ordered_changes = sorted(local_changes, key=itemgetter(3), reverse=True) #hm, heapq ?
                        (typ, wrd, lst, pri, ist) = ordered_changes[0]
                        self.debug("oc="+repr((typ, wrd, lst, pri, ist) ))
                        changes.append(typ+"/"+wrd)
                        if typ == "MIS":
                            new_s += lst
                        elif typ == "REP":
                            new_s += lst 
                        elif typ == "RED":
                            pass
                    else:
                        new_s += [w]
                        changes.append("---/"+w)
                    tmp = str(" ".join(new_s))
                    self.debug(str(i)+",new_s="+tmp )

                self.debug( repr(changes) )
                tmp = str(" ".join(new_s))
                self.cnt += len(words)
                r.put( (idx, [(0, tmp)], self.stats) )


    def run(self):
        self.info( "Starting" )
        while self.cont:
            try:
                (idx, line, cmd) = q.get_nowait()
                if idx % info_gap == 0:
                    self.info(str(idx)+","+line[:40]+"...")
                self.process(idx, line, cmd)
                q.task_done()
            except Empty:
                self.info( "Queue empty." )
                self.cont = False 
        self.info( "Exit." )
        return 0

# ----

q = Queue()
r = PriorityQueue()
lock = Lock()

afile      = None
dirmode    = False 
dirpattern = "*"
workers    = []
procs      = int(multiprocessing.cpu_count() / 2)+1 
lexfile    = None
cmd        = "corr"
outfile    = None
info_gap   = None
test       = False
test_words = ["One", "two", "three"]
test_pos   = 1
test_c_id  = None #means all classifiers
force      = False
sentences  = True #False #expect instances in test file
pyconfig   = None #"pyserver_SET00.py" #generated by split_data_confusibles.py
id_str     = None

#PARAMS = { "MAX_DSIZE":100, "MINFREQ":0, "MINLD":0, "MAXLD":20, "MAXALT":100 }
CLEAN  = re.compile( r'''([-A-Za-z]{3,})''', re.DOTALL | re.VERBOSE) # NOT FOR UTF8 texts!!
# max distribution size
# in top-n
# minimum frequency of alternative
# minimum levenshtein distance
# maximum levenshtein distance
# maximum number of alternatives 
# test parameters:
#PARAMS = { "MAX_DSIZE":2000000, "INTOP":2000000, "MINFREQ":0, "MINLD":0, "MAXLD":3, "MAXALT":2000 }
#CLEAN  = re.compile( r'''([-A-Za-z]{1,})''', re.DOTALL | re.VERBOSE) # NOT FOR UTF8 texts!!

try:
    opts, args = getopt.getopt(sys.argv[1:], "C:c:d:f:Fg:i:l:o:p:qstP:T:I:", ["file="])
except getopt.GetoptError, err:
    print str(err)
    sys.exit(1)
for o, a in opts:
    if o in ("-f", "--file="):
        afile = a #if dirmode, is dirname
    elif o in ("-F"):
        force = True
    elif o in ("-c"):
        cmd = a
    elif o in ("-C"):
        pyconfig = a
    elif o in ("-d"):
        dirmode = True
    elif o in ("-g"):
        info_gap = int(a)
    elif o in ("-i"):
        id_str = a
    elif o in ("-l"):
        lexfile = a
    elif o in ("-o"):
        outfile = a
    elif o in ("-p"):
        procs = int(a)
    elif o in ("-s"):
        sentences = True
    elif o in ("-t"):
        test = True
    elif o in ("-T"):
        test_words = a.split()
    elif o in ("-P"):
        test_pos = int(a)
    elif o in ("-I"):
        test_c_id = a
    elif o in ("-q"): # "quiet"
        glog.setLevel(logging.WARNING)
    else:
        assert False, "unhandled option"

if pyconfig:
    """
    Read the config generated by split_data_confusibles.py (pyconfig..). It Expects
    the corresponding tserver_..ini to have been started.
    (timblserver --config=tserver_EXP01.ini --daemonize=no) (killall -HUP timblserver)

    DANGEROUS

    Expects: 
    s1=TServers("localhost",2000)
    EXP01000=Classifier("EXP01000", 2, 0, 2, s1)
    ...
    EXP01001.set_triggers(["toward","towards"])
    s1.add_classifier(EXP01001)
    ...

    pberck@scootaloo:/scratch/pberck/2013$ more pyserver_UT1e6_l4r4_DETSPN.py
    s1=TServers("localhost",2000)
    UT1e6_l4r4_DETSPN000=Classifier("UT1e6_l4r4_DETSPN000", 4, 4, 1, s1)
    UT1e6_l4r4_DETSPN000.set_triggers(["a","all",...,"Who"])
    s1.add_classifier(UT1e6_l4r4_DETSPN000)
    pberck@scootaloo:/scratch/pberck/2013$ more pyserver_UT1e6_l4r4_PREPSPN.py
    s1=TServers("localhost",2000)
    UT1e6_l4r4_PREPSPN000=Classifier("UT1e6_l4r4_PREPSPN000", 4, 4, 1, s1)
    UT1e6_l4r4_PREPSPN000.set_triggers(["about","above",...,"Without"]
        )
    s1.add_classifier(UT1e6_l4r4_PREPSPN000)
    """
    with open(pyconfig, "r") as f:
        lines = f.readlines()
        lines = "\n".join(lines)
        co = compile( lines, '<string>', 'exec')
        print co
        exec(co)

# Connect to a timblserver on port 2000, uvt/hoo2013/software
# timblserver --config=ts.out --daemonize=no
#
# The servers[] has an entry for each processor to a TServer.
# We run only one TServer, so they are the same.
servers = []

DETS=["a","all","an","another","any","both","each","either","every","many","neither","no","some","that","the","these","this","those","what","whatever","which","whichever","who","A","All","An","Another","Any","Both","Each","Either","Every","Many","Neither","No","Some","That","The","These","This","Those","What","Whatever","Which","Whichever","Who"]
PREPS=["about","above","across","after","against","along","alongside","amid","among","amongst","apart","around","as","aside","at","atop","before","behind","below","beneath","beside","besides","between","beyond","by","down","due","for","from","in","inside","into","near","next","of","off","on","onto","out","outside","over","past","per","since","though","through","throughout","till","to","toward","towards","under","unlike","until","up","upon","versus","via","vs.","whether","while","with","within","without","About","Above","Across","After","Against","Along","Alongside","Amid","Among","Amongst","Apart","Around","As","Aside","At","Atop","Before","Behind","Below","Beneath","Beside","Besides","Between","Beyond","By","Down","Due","For","From","In","Inside","Into","Near","Next","Of","Off","On","Onto","Out","Outside","Over","Past","Per","Since","Though","Through","Throughout","Till","To","Toward","Towards","Under","Unlike","Until","Up","Upon","Versus","Via","Vs.","Whether","While","With","Within","Without"]
PARAMS = { "MD_PNR":20, #Missing DET min ratio
           "MD_DS":20, #Missing DET max distr size

           "MP_PNR":20, #Missing PREP min ratio
           "MP_DS":20, #Missing PREP max distr size

           "UD_PNR":2, # Unnecessary det negpos ratio larger than this

           "UP_PNR":4, # Unnecessary prep negpos ratio larger than this
           
           "RD_DS":20, #Replace ET max distr size
           "RD_F":20,  #RD frequency of top classification must be larger than this
           "RD_R":20,  #RD ratio between classification and original

           "RP_DS":50, #Replace PREP max distr size
           "RP_F":5,  #RP frequency of top classification must be larger than this
           "RP_R":20,  #RP ratio between classification and original
           
           "NF_DS":100000, #Noun form DET max distr size

           "VF_DS":100000 #Verb form max distr size
        }

if platform.node() == "scootaloo" or platform.node() == "rarity":
    try:
        for i in range(procs): 
            s1=TServers("localhost",2000)

            """
            The PN classifiers are added twice, once with an empty focus position,
            and once without. 
            RED, Redundancy version, has focus, we check if the word in the text
            is redundant. Error variant
            MIS, Missing version, no focus pos, because we might be missing it.

            port=2000
            maxconn=64

            DETS_PN_MIS="-i utexas.10e6.dt.1e6.l4r4_UT1e6_l4r4_DETSPN.cs0_-a1+D.ibase -a1 +D +vdb+di"
            PREPS_PN_MIS="-i utexas.10e6.dt.1e6.l4r4_UT1e6_l4r4_PREPSPN.cs0_-a1+D.ibase -a1 +D +vdb+di"
            
            DETS_MIS="-i utexas.10e6.dt.1e6.l4r4_DETS.cs0_-a1+D.ibase -a1 +D +vdb+di"
            PREPS_MIS="-i utexas.10e6.dt.1e6.l4r4_PREPS.cs0_-a1+D.ibase -a1 +D +vdb+di"

            DETS_REP="-i utexas.10e6.dt.cf05.1e6.l4t1r4_DETSEXEV_1e6_a1.cs0_-a1+D.ibase -a1 +D +vdb+di"
            PREPS_REP="-i utexas.10e6.dt.cf05.1e6.l4t1r4_PREPSEXEV_1e6_a1.cs0_-a1+D.ibase -a1 +D +vdb+di"

            DETS_PN_RED="-i utexas.10e6.dt.1e6.l4r4_UT1e6_l4r4_DETSPN.cs0_-a1+D.ibase -a1 +D +vdb+di"
            #"-i utexas.10e6.dt.1e6.l4t1r4.pnr_dets_s1_-a1+D.ibase -a1 +D +vdb+di" this we had in class_run012
            PREPS_PN_RED="-i utexas.10e6.dt.1e6.l4r4_UT1e6_l4r4_PREPSPN.cs0_-a1+D.ibase -a1 +D +vdb+di"
            #"-i utexas.10e6.dt.1e6.l4t1r4.pnr_preps_s1_-a1+D.ibase -a1 +D +vdb+di" this we had in class_run012
            """
            
            # Determiner PosNeg classifer for REDUNDANT DET
            #DETSPNEV000RED=Classifier("DETS_PN_MIS", 4, 4, 0, s1) #redundant has focus pos, normal instance
            DETSPNEV000RED=Classifier("DETS_PN_RED", 4, 4, 2, s1) 
            DETSPNEV000RED.set_triggers(DETS) #not used, really
            DETSPNEV000RED.id="DETS_PN_RED"
            s1.add_classifier(DETSPNEV000RED)

            # Determiner PosNeg classifier for MISSING DET
            DETSPN000MIS=Classifier("DETS_PN_MIS", 4, 4, 1, s1) #missing has no focus, type 1
            DETSPN000MIS.set_triggers(DETS)
            DETSPN000MIS.id="DETS_PN_MIS"
            s1.add_classifier(DETSPN000MIS)

            # Determiner Normal prediction DET
            DETSEXEV000=Classifier("DETS_REP", 4, 4, 2, s1) #type 2, EV
            DETSEXEV000.set_triggers(DETS)
            DETSEXEV000.id="DETS_REP"
            s1.add_classifier(DETSEXEV000)

            # Determiner Missing prediction DET
            DETS000MIS=Classifier("DETS_MIS", 4, 4, 1, s1) #type 1, no focus pos
            DETS000MIS.set_triggers(DETS)
            DETS000MIS.id="DETS_MIS"
            s1.add_classifier(DETS000MIS)

            #---
            
            # Preposition PosNeg classifier for REDUNDANT PREP
            #PREPSPNEV000RED=Classifier("PREPS_PN_MIS", 4, 4, 0, s1)  #normal instance
            PREPSPNEV000RED=Classifier("PREPS_PN_RED", 4, 4, 2, s1)
            PREPSPNEV000RED.set_triggers(PREPS)
            PREPSPNEV000RED.id="PREPS_PN_RED"  #ID used in heuristic
            s1.add_classifier(PREPSPNEV000RED)

            # Preposition PosNeg classifier for MISSING PREP
            PREPSPN000MIS=Classifier("PREPS_PN_MIS", 4, 4, 1, s1) #type 1 for missing
            PREPSPN000MIS.set_triggers(PREPS)
            PREPSPN000MIS.id="PREPS_PN_MIS"
            s1.add_classifier(PREPSPN000MIS)

            # Preposition Normal prediction PREP
            PREPSEXEV000=Classifier("PREPS_REP", 4, 4, 2, s1) #EV
            PREPSEXEV000.set_triggers(PREPS)
            PREPSEXEV000.id="PREPS_REP" #replace action
            s1.add_classifier(PREPSEXEV000)

            # Preposition Missing prediction PREP
            PREPS000MIS=Classifier("PREPS_MIS", 4, 4, 1, s1) #type 1
            PREPS000MIS.set_triggers(PREPS)
            PREPS000MIS.id="PREPS_MIS"
            s1.add_classifier(PREPS000MIS)
            
            servers.append( s1 )
    except:
        glog.error("Help")
        raise
        sys.exit(1)
else:
    try:
        for i in range(procs):
            servers.append( s1 )
    except KeyError:
        glog.error("Help")
        sys.exit(1)
glog.info( "Timblservers initialized.")

if test:    
    glog.error(repr(test_words))
    for ts in servers[0:1]:
        glog.error("t="+repr(ts))
        wa = ts.window_around(test_words,test_pos)
        for c in ts.classifiers:
            if test_c_id == None or test_c_id == c.id:
                glog.error("        "+c.id+"/"+c.name+"/"+str(c.type))
                glog.error(repr(wa[c.ctx])+" :"+str(len((wa[c.ctx]))))
                res = c.classify_i(wa[c.ctx])
                if c.error:
                    glog.error("RETURNED ERROR")
                    #glog.error(repr(res))
                ans = ans_to_dict( res )
                glog.error(repr(ans))
    sys.exit(2)

# Open outfile
if outfile:
  afile_out = outfile
else:
  # local file
  f_path, f_name = os.path.split(afile)
  if cmd == "corr":
    if not id_str:
      afile_out = f_name+".sc"
    else:
      afile_out = f_name+"_"+id_str+".sc"
  else:
    afile_out = f_name+".out"
    #afile_out = afile+".out"
if not force and os.path.exists(afile_out):
  glog.info("File exists: "+afile_out)
  sys.exit(1)
glog.info("Output in "+afile_out)

all_files = []
if dirmode: #not implemented yet
    # Find all
    test     = re.compile("txt$", re.IGNORECASE)
    files = os.listdir( the_dir )
    files = filter(test.search, files)    
    for a_file in files:
        all_files.append( the_dir + a_file)
else:
    all_files.append( afile )

"""
Read input file and put content on queues for processing. 
The queue entry is a tuple: (line-nr, line, cmd)
Process per line or per instance? Parameter? Depend on cmd?
"""
if os.path.exists(afile):
    input_lines = 0
    if afile != None:
        f = open(afile, "r")
        input_lines = 0
        for line in f:
            l = line[:-1]
            if len(l) > 2:
                q.put( (input_lines, l, cmd) )
                input_lines += 1
        f.close()

# Filled by file or from test
glog.info("Queue filled with "+str(input_lines)+" lines.")

if not info_gap:
    info_gap = input_lines // 10 # 10*procs for write info?
if info_gap < 1:
    info_gap = 1 

"""
Start the Threads. They will all point to the same timblserver.
"""
for i in range(procs):
    t = myThread("T{:02n}".format(i), i ) #i points to servers[i]
    workers.append(t)
    t.daemon = True
    t.start()

td0 = int(time.time())

# Open the output file
fo = open( afile_out, "w" )

"""

HERE we proces the results from the queue.

"""
# Keep reading, because of order issues, we keep
# procs "distance" from the end of the queue, the
# item[0] from the tuples is the print index, which
# should be equal to the print_idx to get the order
# right. Put item back on queue if out of order.
print_idx   = 0
gap         = 1 #Gap lets the result queue fill before taking items from it.
last_r_size = 0
stuck_cnt   = 0
stats = { 'DETS_REP':0, 'DETS_RED':0, 'DETS_MIS':0, 'NOP':0, 'PREPS_REP':0, 'PREPS_RED':0, 'PREPS_MIS':0 }
while any( [ t.isAlive() for t in workers ] ):
    if r.qsize() > gap:
        """
        r.put( (idx, [(0, tmp)], self.stats) )
        The result is a tuple (line-nr, [(result_str)], stats)
        The second element is a list with tuples.
        Third is stats.
        """
        res = r.get() #check for empty? not if we use gap
        if print_idx != res[0]: #res[0] is the index of the line
                                #glog.error( "Print_idx out of order, "+str(print_idx)+" vs "+str(res[0])+" ("+str(r.qsize())+")" )
            r.put( res ) #put back and wait
            time.sleep(1) #maybe longer?
        else:
            res_list = res[1] #res[1] is list of tuples (i,str)
            res_stat = res[2] #dict with stats
            for s in res_stat:
                stats[s] += res_stat[s]
            #print res_list[0] 
            fo.write( str(res_list[0][1])+"\n" )
            fo.flush()
            td1 = int(time.time())
            td = td1-td0
            print_idx += 1
            #calculate time left from q.qsize, not r.qsize !
            if print_idx % info_gap == 0:
                persec = float(float(td)/float(print_idx+r.qsize())) #printed + left on queue but ready
                left = int( persec * (q.qsize()+len([ 1 for t in workers if t.isAlive() ])) )
                glog.info(  "Wrote line "+str(print_idx)+", time taken: %s (%s left)", str(datetime.timedelta(seconds=td)),  str(datetime.timedelta(seconds=left)) )
    else:
        time.sleep(1) #wait for r.queue to be filled
    if r.qsize() == last_r_size:
        #list [ for t.cnt for t in thread ] - [the same last time] => any 0?
        #glog.error( "Queue not changing? ("+str(last_r_size)+")" )
        stuck_cnt += 1
        if stuck_cnt >20:
            glog.error( "Result queue not changing? ("+str(last_r_size)+" in queue)" )
            stuck_cnt = 0
    else:
        stuck_cnt = 0
    last_r_size = r.qsize()

glog.info( "All threads exited." )

# the rest of the result, order is OK now 
while r.qsize() > 0:
    res = r.get() # (idx, [list-with-tuples])
    res_list = res[1] #res[1] is list of tuples (i,str)
    res_stat = res[2] #dict with stats
    for s in res_stat:
        stats[s] += res_stat[s]
    #print res_list[0] 
    fo.write( str(res_list[0][1])+"\n" )
    fo.flush()
    td1 = int(time.time())
    td = td1-td0
    print_idx += 1
    #calculate time left from q.qsize, not r.qsize !
    if print_idx % info_gap == 0:
        persec = float(float(td)/float(print_idx+r.qsize())) #printed + left on queue but ready
        left = 0
        glog.info( "Wrote line "+str(print_idx)+", time taken: %s (%s left)", str(datetime.timedelta(seconds=td)),  str(datetime.timedelta(seconds=left)) )

fo.close()

glog.setLevel(logging.DEBUG)
td1 = int(time.time())
td = td1-td0
glog.debug( "Result Queue contains: "+str(r.qsize()) )
total_cnt = 0
for w in workers:
    glog.debug( str(w.name)+" processed "+str(w.cnt)+ " instances." )
    total_cnt += w.cnt
glog.info("Time taken: %s", str(datetime.timedelta(seconds=td)))
glog.info( "Processed "+str(total_cnt)+ " instances, in "+str(procs)+" threads." )
if total_cnt == 0:
  sys.exit(1)
tdw = float(float(td)/float(total_cnt))
#scootaloo, -p24: Time per word: 0.0596267637688 (16 p/s)
glog.info("Time per instance: %s", str(tdw))
glog.info( "Output in "+afile_out )
glog.info( repr(stats) )
sys.exit(0)
