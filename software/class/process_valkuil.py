
#!/usr/bin/python
# -*- coding: utf-8 -*-
#
import getopt
import sys
import time
import os
import datetime
import random
import re
import platform
from operator import itemgetter
import operator

"""
Takes an annotated error file, and creates two datasets for use with wopr -r correct.

Input is like this:
00003C6E2967480ED5C2AF101488F42C|Verschillende geruchten deden ~meermaal|meermaals~ de ronde dat de intussen overleden ex-rijkswachter Madani Bouhouche betrokken zou zijn geweest bij de overvallen. De politie heeft echter nooit bewijzen gevonden tegen de tot crimineel omgevormde rijkswachter.

000895F090D3C80C84436AD94F9FEF3E|Woensdag 28 februari is het precies tien jaar geleden dat de Regie voor Maritieme Transport (RMT) de boeken dicht deed. ~BIj|Bij~ de RMT werkten 1.700 mensen.

000D05A3CEBF028AC78FE0C3D211B9F8|Sinterklaas ~word|wordt~ geintervieuwd voor tv.

Data needs to be split into two datasets, and tokenized (in the same way).
"""

afile = None

try:
    opts, args = getopt.getopt(sys.argv[1:], "f:v", ["file="])
except getopt.GetoptError, err:
    print str(err)
    sys.exit(1)
for o, a in opts:
    if o in ("-f", "--file="):
        afile = a #if dirmode, is dirname
    elif o in ("-v"): # "quiet"
        print "Version 0.1"
    else:
        assert False, "unhandled option"

f_path, f_name = os.path.split(afile)
afile_outg = f_name+".gld" # Gold
afile_outs = f_name+".spc" # with spelling errors

with open(afile, "r") as f:
	with open(afile_outg, "w") as fg:
		with open(afile_outs, "w") as fs:
			for line in f:
				line = line[:-1]

				# Skip lines with result in extra words
				if re.search('~((.*?[\ ].*?)\|(.*?))~', line):
					continue
				if re.search('~((.*?)\|(.*?[\ ].*?))~', line):
					continue

				words = line.split()
				#print words
				for w in words:
					m = re.match( "[A-Z0-9]{32}\|(.*)", w)
					if m:
						w = m.group(1)
					# problem: ~inplaats|in plaats~ 
					# problem: ~suez kanaal|Suezkanaal~
					m = re.match('~(.*?)\|(.*?)~', w)
					if m:
						#print m.group(1), m.group(2)
						fs.write( m.group(1) )
						fg.write( m.group(2) )
					else:
						fs.write( w )
						fg.write( w )
					fs.write( " " )
					fg.write( " " )
				fs.write( "\n" )
				fg.write( "\n" )

