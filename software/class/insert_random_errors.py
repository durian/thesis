
#!/usr/bin/python
# -*- coding: utf-8 -*-
#
import getopt
import sys
import time
import os
import datetime
import random
import re
import platform
from operator import itemgetter
import operator

"""
Takes a plain text file, and inserts errors/
"""

def levenshtein_wp(s1, s2):
    if len(s1) < len(s2):
        return levenshtein_wp(s2, s1)
    if not s1:
        return len(s2)
 
    previous_row = xrange(len(s2) + 1)
    for i, c1 in enumerate(s1):
        current_row = [i + 1]
        for j, c2 in enumerate(s2):
            insertions = previous_row[j + 1] + 1 # j+1 instead of j since previous_row and current_row are one character longer
            deletions = current_row[j] + 1       # than s2
            substitutions = previous_row[j] + (c1 != c2)
            current_row.append(min(insertions, deletions, substitutions))

        previous_row = current_row
 
    return previous_row[-1]

def damerau(seq1, seq2):
    oneago = None
    thisrow = range(1, len(seq2) + 1) + [0]
    for x in xrange(len(seq1)):
        # Python lists wrap around for negative indices, so put the
        # leftmost column at the *end* of the list. This matches with
        # the zero-indexed strings and saves extra calculation.
        twoago, oneago, thisrow = oneago, thisrow, [0] * len(seq2) + [x + 1]
        for y in xrange(len(seq2)):
            delcost = oneago[y] + 1
            addcost = thisrow[y - 1] + 1
            subcost = oneago[y - 1] + (seq1[x] != seq2[y])
            thisrow[y] = min(delcost, addcost, subcost)
            # This block deals with transpositions
            if (x > 0 and y > 0 and seq1[x] == seq2[y - 1]
                and seq1[x-1] == seq2[y] and seq1[x] != seq2[y]):
                thisrow[y] = min(thisrow[y], twoago[y - 2] + 1)
    return thisrow[len(seq2) - 1]

ALPHABET = "abcdefghijklmnopqrstuvwxyz"

def insert(w):
    lw = len(w)
    if lw < 3:
        return w
    pos = int( r.uniform(0, lw+1) )
    l = ALPHABET[int( r.uniform(0, len(ALPHABET)) )]
    new_w = w[0:pos]+l+w[pos:]
    return new_w

def replace(w):
    lw = len(w)
    if lw < 3:
        return w
    pos = int( r.uniform(0, lw) )
    l = ALPHABET[int( r.uniform(0, len(ALPHABET)) )]
    new_w = w[0:pos]+l+w[pos+1:]
    return new_w

def delete(w):
    lw = len(w)
    if lw < 3:
        return w
    pos = int( r.uniform(0, lw) )
    new_w = w[0:pos]+w[pos+1:]
    return new_w

def swap(w):
    lw = len(w)
    if lw < 3:
        return w
    pos = int( r.uniform(0, lw-1) )
    bit = w[pos:pos+2]
    new_w = w[0:pos]+bit[1]+bit[0]+w[pos+2:]
    return new_w

def change(w):
    p = r.uniform(0, 1) 
    if p < 0.25:
        return (swap(w), 'SWP')
    if p < 0.5:
        return (delete(w), 'DEL')
    if p < 0.75:
        return (replace(w), 'RPL')
    return (insert(w), 'INS')

afile  = None
prob   = 2
lex    = None
min_l  = 3 #minimum length of word >= min_l
max_ld = 1 #maximum LD <= max_ld

try:
    opts, args = getopt.getopt(sys.argv[1:], "d:f:l:m:p:", ["file="])
except getopt.GetoptError, err:
    print str(err)
    sys.exit(1)
for o, a in opts:
    if o in ("-f", "--file="):
        afile = a 
    elif o in ("-l"): 
        lex = a
    elif o in ("-m"): 
        min_l = int(a)
    elif o in ("-d"): 
        max_ld = int(a)
    elif o in ("-p"): 
        prob = int(a)
    else:
        assert False, "unhandled option"

lexicon  = {}
lex_errs = {}
if lex:
    lexicon = dict( ( line.split()[0], int(line.split()[1])) for line in open(lex))

f_path, f_name = os.path.split(afile)
afile_outs = f_name+".rerr" # with spelling errors

r = random.Random()
made_changes = 0
changed = {}
lds = {}
subs = {}
with open(afile, "r") as f:
    with open(afile_outs, "w") as fo:
        for line in f:
            line = line[:-1]
            print line
            words = line.split()
            indexes = []
            idx = 0
            numerr = 1
            if len(words) > 10:
                numerr = 2
            posses = random.sample(set(range(0,len(words))), numerr)
            for pos in posses:
                w = words[pos]
                (e, s) = change(w)
                q = r.uniform(0, 1)
                if q > 0.80:
                    (e, s1) = change(e)
                    s = s + "." + s1
                ld = damerau(w, e)
                if ld > 0 and ld <= max_ld:
                    print "CHANGE", w, e, ld
                    try:
                        changed[ w ] += 1
                    except:
                        changed[ w ] = 1
                    try:
                        lds[ld] += 1
                    except:
                        lds[ld] = 1
                    try:
                        subs[s] += 1
                    except:
                        subs[s] = 1
                    made_changes += 1
                    words[pos] = e
            new_line = ' '.join(words)
            fo.write(new_line+"\n")
print "Made", made_changes, "changes."

for change in changed:
    print "FREQ", change, changed[change]
for ld in lds:
    print "LD", ld, lds[ld]
for s in subs:
    print "SUB", s, subs[s]