#!/usr/bin/python
# -*- coding: utf-8 -*-
#
from stimpy import *
import getopt
import sys
import random

have_nltk = False
try:
    import nltk
    have_nltk = True
except:
    print "No NLTK toolkit available."

host = "localhost"
port = 2000
lexfile = None
afile = None
instance = None
sentence = None
topn = 3
debug = False

try:
    opts, args = getopt.getopt(sys.argv[1:], "f:i:l:L:h:n:p:r:s:", ["lc="])
except getopt.GetoptError, err:
    # print help information and exit:
    print str(err) # will print something like "option -a not recognized"
    #usage()
    sys.exit(2)
for o, a in opts:
    if o in ("-f"):
        afile = a
    elif o in ("-i"):
        instance = a
    elif o in ("-l", "--lc"):
        lc = a
    elif o in ("-L"):
        lexfile = a
    elif o in ("-h"):
        host = a
    elif o in ("-n"):
        topn = int(a)
    elif o in ("-p"):
        port = a
    elif o in ("-r"):
        rc = a
    elif o in ("-s"):
        sentence = a
    else:
        assert False, "unhandled option"
"""
all_files = []
if dirmode:
    # Find all
    #
    test     = re.compile(test_str, re.IGNORECASE)
    files = os.listdir( the_dir )
    files = filter(test.search, files)    
    for a_file in files:
        all_files.append( the_dir + a_file)
else:
    all_files.append( a_file )
"""

# "one" =>[ (one 0 2) ]
def split_span(s):
    for match in re.finditer(r"\S+", s):
        span = match.span()
        yield match.group(0), span[0], span[1] - 1 # remove -1 to get pointer on char after

# specialised subroutines to find errors

#                     word
#                     |  index
#                     |  |    instances
def check_missingprep(w, idx, wl):
    c = s1.get_classifier_by_name("dflt0")
    if c:
        res = c.classify_i(wl[c.ctx][idx])
        print ans_str( res )
        ans = ans_to_dict( res )
        if ans['rr'] > 0.3:
            print "yay"
        posneg_r = ans['posneg_r'] 
        if posneg_r > 10:
            print "hurra", posneg_r
        print c.distr_w[0:2] # not in order
        if w in c.distr_w:
            print "iets leuks"

'''
    if s1.classification == "+" and len(s1.distr) > 1:
        # maybe we had a DET, then we are OK, otherwise...
        if pos.word not in DETS:
            # Ratio between yes:no
            posneg_r = float(s1.distr[0][1]) / float(s1.distr[1][1])
            # Ratio is > ...
            if posneg_r > MD_PNR:
                # We want a determiner at this point
                # Call s3 to determine which DET we want
                s3.classify( pos.instance + " ?" )
                print "INSERT DET:", posneg_r, s3.classification, repr(s1.distr), repr(s3.distr[0])
                error_type = "MD"
                dbg( repr(s3.distr) )
'''

# handle per word, we first create all the instances
# we might need in window_all. returns dict[ctx] =>['i1', 'i2']
def handle_line(l):
    r = random.Random()
    wl = s1.window_all(l)
    #print wl["l2r2"][0] #instances for l2r2, word 0 ("one")
    for idx,w in enumerate(l.split()):
        print w
        # determine here if we want to check it.
        check_missingprep(w, idx, wl)
        # handle all
        for c in s1.classifiers: #[1:2]:
            # a c.apply?(w,idx,l) function?
            if w in c.triggers:
                res = c.classify_i(wl[c.ctx][idx])
                print ans_str(res)

# A prepare function which windows a line in the necessary
# sizes (as a dict? l2r0 => [...])
# Process in order of context size so we only need to
# window each sentence once for each context size.
def handle_o(l):
    for ctx in s1.get_contexts():
        m = ctx_re.match(ctx)
        lc = int(m.group(1))
        rc = int(m.group(2))
        #print lc,rc
        il = window_lr(l, lc, rc) # [ instances ]
        il = [ i.split() for i in il ]
        classifiers = s1.get_classifiers(ctx)
        all_res = dict()
        for c in classifiers:
            res = c.classify_il(il)
            all_res[c.name] = res
            print c.name,":"
            for r in res:
                print ans_str(r)

def handle(l):
    r = random.Random()
    #print r.random(), r.uniform(0, 10), r.randrange(0, 10)
    words = l.split()
    wordspan = split_span(l) # [('one', 0, 2), ('two', 4, 6), ('three', 9, 13)]
    print list(wordspan)
    all_ctx_res = dict()
    for ctx in s1.get_contexts():
        m = ctx_re.match(ctx)
        lc = int(m.group(1))
        rc = int(m.group(2))
        #print lc,rc
        il = window_lr(l, lc, rc) # [ instances ]
        il = [ i.split() for i in il ]
        classifiers = s1.get_classifiers(ctx)
        all_res = dict()
        for c in classifiers:
            res = c.classify_il(il)
            all_res[c.name] = res
        all_ctx_res[ctx] = all_res
    #print all_ctx_res
    #create results per word
    word_results = dict()
    for idx, w in enumerate(words):
        ctx_keys = all_ctx_res.keys()
        #print ctx_keys
        for ctx_key in ctx_keys: #loop over contexts
            class_keys = (all_ctx_res[ctx_key]).keys()
            #print class_keys
            for class_key in class_keys: # loop over classes
                #print class_key
                #print w, all_ctx_res[ctx_key][class_key][idx]
                if idx in word_results:
                    word_results[idx].append( (class_key, all_ctx_res[ctx_key][class_key][idx]) )
                else:
                    word_results[idx] = [ (class_key, all_ctx_res[ctx_key][class_key][idx]) ]
    for idx, w in enumerate(words):
        print w
        # send results for analysis to another function.
        for r in word_results[idx]:
            #print r[0],ans_str(r[1]) # print result of each classifier for first word, tuple (classname, answer)
            print r[0],r[1][1],r[1][5] #r[1][1] is the target
    #print word_results #dict with index as key, value is list with dicts with classifier name as key, ans as value
            
def create_mistake(nid, pid, sid, st, et, ty, c):
    res  = "<MISTAKE nid=\""+str(nid)+"\" pid=\""+str(pid)+"\" sid=\""+str(sid)+"\" start_token=\""+str(st)+"\" end_token=\""+str(et)+"\">\n"
    res += "<TYPE>"+ty+"</TYPE>\n"
    res += "<CORRECTION>"+c+"</CORRECTION>\n"
    res += "</MISTAKE>\n"
    return res

# --

#print create_mistake(0,1,2, 5, 6, "Wci", "or and when")

#durian:software pberck$ bash ~/uvt/wopr/stimpy/dir_to_tspy.bash

#print list(split_span("one two  three"))
#print list(split_span("a b c"))

s1=TServers("localhost",2000)
s1.topn=5

ts0300=Classifier("ts0300", 2, 2, s1)
ts0300.set_triggers(["about","along","among","around","as","at","beside","besides","between","by","down","during","except","for","from","in","inside","into","of","off","on","outside","over","through","to","toward","towards","under","underneath","until","up","upon","with","within","without" ]);
s1.add_classifier(ts0300)
dflt0=Classifier("dflt0", 2, 2, s1)
s1.add_classifier(dflt0)
dflt0.read_lex("austen.train.lex")


ctx_re = re.compile("l(\d)r(\d)")

#print s1.get_contexts()
#print s1.get_classifiers("l2r0")
#print s1.get_classifiers("l1r0")
#list = window(sentence)
#for all ctx classifiers, c.classify_il() (l=list with instances)

# from /exp2/antalb/hoo2012/preps|dets
#
DETS = [ "a", "an", "another", "any", "every", "he", "her", "his", "its", "my", "no", "one", "other", "our", "own", "that", "the", "their", "these", "this", "those", "what", "which", "your"]

PREPS = [ "about", "after", "against", "along", "among", "around", "as", "at", "before", "behind", "below", "beside", "besides", "between", "by", "concerning", "down", "during", "for", "from", "in", "into", "near", "of", "off", "on", "onto", "out", "outside", "over", "referring", "regarding", "regards", "since", "than", "through", "throughout", "till", "to", "toward", "towards", "under", "until", "via", "with", "within", "without" ]

# ----

if afile != None:
    f = open(afile, "r")
    for line in f:
        l = line[:-1]
        handle_line(l)
        
if instance != None:
    c = s1.get_trigger(instance)
    if c:
        c.classify_i(instance)
        if not c.error:
            print c.name,":",ans_str(c.ans)
    print "----"
    for c in s1.classifiers:
        #print c
        c.classify_i(instance)
        if not c.error:
            print c.name,":",ans_str(c.ans)

if sentence != None:
    #handle(sentence)
    handle_line(sentence)
    '''
    r0 = all_res["dflt0"]
    r1 = all_res["dflt1"]
    dl = [ (ans_str(a[0]),ans_str(a[1])) for a in zip(r0,r1) ]
    for d in dl:
        print d[0]
        print d[1]
        print
    '''

