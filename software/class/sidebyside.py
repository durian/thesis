#!/usr/bin/python
# -*- coding: utf-8 -*-
#
import getopt
import sys
import time
import os
import datetime
import random
import re
import platform
import shutil
from operator import itemgetter
import operator
from subprocess import call
"""
2014-09-23 1.00
"""

# Hold the info for a confusible
class Matrix():
    def __init__(self, word):
        self.confusible = word
        self.GS = 0
        self.WS = 0
        self.NS = 0
        self.BS = 0
        self.count= 0
        self.corrected_from = []
        self.corrected_as = []
        self.outputformat = 1 #0=gs/ws, 1=acc/rec
    def __repr__(self):
        return self.confusible+":c="+str(self.count)+",GS="+str(self.GS)+",WS="+str(self.WS)+",NS="+str(self.NS)+",BS="+str(self.BS)
    def add_count(self):
        self.count += 1
    def add_GS(self, error):
        # GOODSUGG being -> begin
        # add the original error to list
        self.GS += 1
        self.corrected_from.append(error)
    def add_NS(self):
        self.NS += 1
    def add_WS(self, error):
        # WRONGSUGG sight -> cite (should be: site)
        # should that be here?
        self.WS += 1
        self.corrected_as.append(error)
    def add_BS(self, correction):
        # BADSUGG led -> lead (should be: led)
        self.BS += 1
    def total(self):
        return self.GS+self.WS+self.NS
    def get_stats(self, m2mode):
        """
        my $TP = $good_sugg;
        my $FN = $wrong_sugg + $no_sugg;
        my $FP = $bad_sugg;
        my $TN = $clines - $TP - $FN - $FP; #Niks gedaan als niks moest
        my $RCL = 0;
        if ( $TP+$FN > 0 ) {
          $RCL = ($TP*100)/($TP+$FN);           #TPR
        }
        my $ACC = (100*($TP+$TN))/($clines);

        my $PRC = 0;
        if ( $TP+$FP > 0 ) {
          $PRC = (100*$TP)/($TP+$FP);           #PPV
        }
        my $FPR = 0;
        if ( $FP+$TN > 0 ) {
          $FPR = (100*$FP)/($FP+$TN);
        }
        my $F1S = (100*2*$TP)/((2*$TP)+$FP+$FN);
        """
        '''
        TP  = self.GS #corrected errors
        FN  = self.WS + self.NS #corrected wrongly, or not corrected (misses)
        FP  = self.BS  #non-errors unnecessarily "corrected"
        PE  = self.GS + self.WS # m2scorer's "PROPOSED EDITS", no NS (no gold standard)
        '''
        GOLD = self.GS + self.WS + self.NS
        TP  = self.GS #corrected errors
        FN  = GOLD - self.GS 
        FP  = self.WS + self.BS

        PE  = self.GS + self.WS
        print "GS=",self.GS,"WS=",self.WS,"NS=",self.NS,"BS=",self.BS
        ALL = self.count #number of this confusible
        TN  = ALL - TP - FN - FP #true negatives, untouched ones
        RCL = 0 #proportion of actual positives correctly identified (corrected)
        if TP + FN > 0:
            RCL = (TP*100.0)/(TP+FN)
        ACC = 0
        if ALL > 0:
            ACC = (100.0*(TP+TN))/(ALL)
        PRC = 0 #fraction of relevant corrections
        if m2mode:
            if PE > 0:
                PRC = (100.0*TP)/PE #m2scorer's variant
        else:
            if TP+FP > 0:
                PRC = (100.0*TP)/(TP+FP)
        try:
            beta = 1.0
            F1S = (1 + (beta*beta))*(PRC*RCL) / ((beta*beta*PRC)+RCL)
            #F1S1 = (100*(1+(beta*beta))*TP)/(((1+(beta*beta))*TP)+FP+FN) #old
            beta = 0.5
            F5S = (1 + (beta*beta))*(PRC*RCL) / ((beta*beta*PRC)+RCL) #m2scorer
        except ZeroDivisionError:
            F1S = 0
            F5S = 0
        #m2scorer gives PRC, RCL, F0.5
        return (self.total(), int(TP), int(FN), int(FP), int(TN), round(ACC,2), round(PRC,2), round(RCL,2), round(F1S, 2),  round(F5S, 2))

of_name    = None #original file
tf_name    = None #test file
sc_name    = None #wopr/timblserver output
cs_name    = None #confusible sets, TEST1.compile.txt
m2mode     = False #if set, m2scorer

try:
    opts, args = getopt.getopt(sys.argv[1:], "mo:t:s:c:", ["file="])
except getopt.GetoptError, err:
    print str(err)
    sys.exit(1)
for o, a in opts:
    if o in ("-o", "--file="):
        of_name = a 
    elif o in ("-t"): 
        tf_name = a
    elif o in ("-s"): 
        sc_name = a
    elif o in ("-c"): 
        cs_name = a
    elif o in ("-m"): 
        m2mode = True
    else:
        assert False, "unhandled option"

cs = []
if cs_name:
    with open(cs_name, "r") as f:
        lines = f.readlines()
        lines = "\n".join(lines)
        #print "compile:", lines
        co = compile( lines, '<string>', 'exec')
        exec(co)  #cs=[ ["their","there"], ["two","too","to"], ]

score = []
g_score = Matrix("global")

for c in cs: #for set in confusible set
    lst = {} #sets have unique words, we have a dict
    for cw in c: #for word in the set   **WHAT ABT WORDS IN TWO SETS?**
        lst[cw] = Matrix(cw) #word => word, TP, FP, TN, etc counts
    score.append( lst )
#print score

word_cnt = 0
line_cnt = 0
conf_cnt = 0
with open(of_name, "r") as of, open(tf_name, "r") as tf, open(sc_name, "r") as sf:
    for ol in of: #original
        tl = tf.readline() #test
        sl = sf.readline() #spell corrected
        #print ol
        #split
        olb = ol.split()
        tlb = tl.split()
        slb = sl.split()
        #assert equal length
        for i in xrange(0, len(olb)):
            # Only check the known confusibles.
            for c,m in zip(cs, score): #cs=[ ["their","there"], ["two","too","to"], ] and matrices
                if olb[i] in c: #in the/a confusible set
                    conf_cnt += 1 #NB WE CAN COUNT DOUBLE
                    cm = m[olb[i]] #the confusible matrix ("two:GS=0,WS=0,NS=0,BS=0")
                    cm.add_count()
                    g_score.add_count()
                    #
                    if olb[i] == tlb[i]: #original == test (no error in test file)
                        # There was no test error, but
                        if slb[i] != olb[i]: # but spell-corrected != original
                            #print c, "FP", olb[i], slb[i]
                            cm.add_BS(slb[i])
                            g_score.add_BS(slb[i])
                    else: #there was an error in the test file to be corrected (count errs?)
                        #print "Text error:", olb[i], tlb[i]
                        if slb[i] == olb[i]: #correct correction!
                            #print c, "correct", olb[i], slb[i] #TP
                            cm.add_GS(slb[i])
                            g_score.add_GS(slb[i])
                        else: #what abt NS? slb == tlb (corrected version is test version)
                            if slb[i] == tlb[i]: #NS
                                #print c, "nothing", olb[i], slb[i] #NS
                                cm.add_NS()
                                g_score.add_NS()
                            else:
                                #print c, "incorrect", olb[i], slb[i] #FN
                                cm.add_WS(slb[i])
                                g_score.add_WS(slb[i])
        word_cnt += len(olb)
        line_cnt += 1

print line_cnt, word_cnt, conf_cnt
# self.total(),ACC, PRC, RCL, F1S, F5S 
print "confusible  err TP  FN  FP  TN    ACC    PRC    RCL     F1   F0.5"
for c,m in zip(cs, score):
    print
    for cw in c:
        #print cw, m[cw], m[cw].get_stats(m2mode)
        print '{0:<10}'.format(cw), '{0:3n} {1:3n} {2:3n} {3:3n} {4:3n} {5:6.2f} {6:6.2f} {7:6.2f} {8:6.2f} {9:6.2f}'.format( *m[cw].get_stats(m2mode) )
        #print '|{0:<10}'.format(cw), '|{0:3n}|{1:3n}|{2:3n}|{3:3n}|{4:3n}|{5:6.2f}|{6:6.2f}|{7:6.2f}|{8:6.2f}|{9:6.2f}'.format( *m[cw].get_stats(m2mode) )
print
print '{0:<10}'.format("G"), '{0:3n} {1:3n} {2:3n} {3:3n} {4:3n} {5:6.2f} {6:6.2f} {7:6.2f} {8:6.2f} {9:6.2f}'.format( *g_score.get_stats(m2mode) )

# out += '{0:<10} {1:3n} {2:6.2f} {3:6.2f} {4:6.2f}   '.format( cf, 0, 0, 0, 0 )
