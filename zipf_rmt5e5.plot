# 2014-05-26 17:36:04 some tweaks.
#
set terminal post eps
set output "zipf_rmt5e5.ps"
#set title "Rank versus frequency, 9085910 tokens Reuters" #in caption
#set xtics font "Arial, 6"
set format y "%10.0f"
#set format x "%10.0f"
set xtics ( 1, 10, 100)
set xtics add ("1{\267}10^3" 1000,"10{\267}10^3" 10000,"100{\267}10^3" 100000,"1{\267}10^6" 1e6,"10{\267}10^6" 1e7)
set xtics add ("100{\267}10^6" 1e8, "1{\267}10^9" 1e9)
#
set ytics ( 1, 10, 100)
set ytics add ("1{\267}10^3" 1000,"10{\267}10^3" 10000,"100{\267}10^3" 100000,"1{\267}10^6" 1e6,"10{\267}10^6" 1e7)
set ytics add ("100{\267}10^6" 1e8, "1{\267}10^9" 1e9)
set border 3
set log x
set log y
set xtics out nomirror
set ytics out nomirror
set xlabel "rank"
set ylabel "frequency"
plot [0.9:1000000][0.9:] "rmt.5e5.lex.sorted" using 0:2 pt 7 ps 0.5 t "word", "rmt.5e5.lex.sorted" using 0:(1/column(0))*1000000 w l t "1/x"
#
# the 1000000 is from looking at the graph first.
# plot  "rmt.5e5.lex.sorted" using 0:2, "rmt.5e5.lex.sorted" using 0:(1/column(0))*1000000 w l
set terminal push
set terminal postscript eps enhanced dashed rounded lw 2 'Helvetica' 20
set out 'zipf_rmt5e5.ps'
replot
!epstopdf 'zipf_rmt5e5.ps'
set term pop
