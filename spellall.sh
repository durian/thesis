# added words with: https://gist.github.com/hassansin/bfe6cceaefc74ac5f6b6
#
   echo "====================================================="
for f in `cat chaps_en.txt`
do
   echo 
   echo "-----------------------------------------------------"
   ls $f
   echo "-----------------------------------------------------"
   cat $f | aspell --dont-tex-check-comments -d en_GB-ise -t list --encoding utf-8 | sort | uniq -c | sort -n
done;
