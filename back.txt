Allt mer text skrivs med hjälp av någon typ av ordbehandlingsprogram, på datorn, på surfplattan eller i telefonen. Det ställs ofta krav på att tekniken ska hjälpa till att korrigera fel som uppstår t ex pga skrivfel, stavfel, förväxling av ord eller bristande kunskap i språket som skrivs.

Denna typ av Text correction kan göras med olika tekniker. I denna bok förklaras hur Machine Learning och Memory based Language Modelling kan användas i detta syfte. Det text-correction system som byggts för detta ändamål kan förutse lämpliga ord i en mening baserat på omgivande text. Analysen har begränsats till två uppgifter, nämligen korrigering av confusibles resp determiners och prepositions

In this book we explore/This thesis aims to explain to what extent the memory-based models are suitable for text correction and what the limits are.

# ---

%In this day and age, natural language is more and more processed by computers

In this day and age, texts are written more and more on electronic devices, computers, tablets, smartphones.
We have come to expect these devices to provide some kind of aid in writing, like suggesting words or correcting errors. 

%It is also expected that these 
%be it on a computer or an a mobile phone or tablet, and it is expected that these devices provide some kind of %electronic help
%the purpose of these electronic aids is to correct errors
%from spelling mistakes and word completion to sentence construction.

%which stem from different causes. / have different underlying causes.

The source of these errors could be sloppy writing, causing spelling and confusible errors, or unfamiliarity with a language, resulting in chosing the wrong preposition or leaving out an article. The electronic aids/computer systems (could) make use of dictionaries, or be/are programmed with grammatical/linguistic rules to detect/correct errors.

In this thesis, the detection and correction of confusible errors and errors in articles and prepositions is treated/described/researched. The systems described/investigated are built around a classifier which learns to predict words from large amounts of plain text. These trained classifiers are used by the correction system to find errors, and suggest possible corrections.

The research described in the thesis centres around the question to what extent these classifiers are suitable to correct confusible and article/determiner errors in written text.

This thesis also provides an introduction to memory-based learning.

In this thesis, the detection and correction of confusible errors and errors in articles and prepositions is treated/described/researched.
The systems described/investigated are built around a classifier which learns to predict words from large amounts of plain text. These trained classifiers are used by the correction system to find errors, and suggest possible corrections.


The source of these errors could be sloppy writing, causing spelling and confusible errors, or unfamiliarity with a language, resulting in chosing the wrong word or leaving out words in certain situations.


# ---

In this day and age, texts are written more and more on electronic devices such as computers, tablets, or smartphones. We have come to expect these devices to provide some kind of aid in writing, like suggesting words or correcting errors caused by e g sloppy writing, dyslexia or writing in a foreign language.

In this thesis, we describe how machine learning and memory-based language modelling can be used to detect and correct such errors, focusing on confusible errors and errors in article and determiners. The research centres around the question to what extent these memory-based models are suitable for text-corrections, and where the limits of their ability lie.


