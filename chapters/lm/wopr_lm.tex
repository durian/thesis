%% NB: added to lmodeling.tex, this file is not used anymore
\section{Introduction}

%\Wopr{} is a \mb{} word predictor, built around the \Timbl{} \mb{}
%learner. It trains a classifier which predicts which word(s) possibly
%fit in a sequence of words.

The \wopr{} (\emph{wo}rd \emph{pr}edictor) \lm{} is based on the memory based
\knn{} classifier \Timbl{} \cite{Daelemans+09}. \Wopr{} uses \Timbl{} as its
machine learning engine, and implements a number of tools geared towards the
creation and evaluation of \lm{}s.

The \mb{} algorithms learn from fixed-length patterns of
feature-values plus a resulting class, called
\emph{instances}\footnote{Memory based learning is also called
  instance based learning}. The \mb{} \lm{} is implemented as a
\emph{word predictor}. The classifier is trained on millions of
fragments of text consisting of a couple of words. Typically between
two and ten. The classifier learns to predict the last word (or
another word in the sequence) of each fragment. In other words, the
classifier learns to predict a word given a number of preceding and
succeeding words. The number of preceding or succeeding words can be zero, but
not simultaneously. 

\Timbl{} implements several \mb{} machine learning algorithms,
including the archetypical \ibo{}. We'll be looking mostly at
successors to \ibo{}, namely \igtree{} and \triblt{}. \Ibo{} is the
simplest learning algorithm. It compares vectors of feature values by
calculating similarity using a simple similarity function. One of the
disadvantages of \ibo{} is that it is rather slow for very large
datasets, mainly due to its flat representation of the instances.

\Ibt{} is a variant of \ibo{} which stores misclassifications instead
of positive examples, starting with a \emph{seed} of correctly
classified examples. The algorithm is sensitive to noise, and the
influence of the seed on the total performance is unclear
\cite{Aha+91}. \Ibt{} is left unexplored in this article.

\Igtree{} produces a compressed pruned decision tree
instead of the \q{flat} format of the \ibo{} classifier. Performance
about equal to \ibo{}, but much, much faster. \Triblt{} is a hybrid
form which continues classification with \knn{} \ibo{} after a
mismatch. At this point, \ibo{} becomes useful again because it does
not assume an ordering of features and the number of instances
remaining is small enough to be workable.

The $k$ in \knn{} represents the number of nearest neighbours of the
instance being classified. The default value is $k=1$, that is, we
return the instances (one or more) that are nearest to the instance
itself. The nearest neighbours can be seen as lying in rings around
each other, with the exact match at the centre, and moving outwards
like ripples in water with increasing $k$'s. Each ring contains the
nearest neighbours at a specific distance. The closest ring is the
$k=1$ ring, the next one is $k=2$ \etc{}. There can be more than one
instance at a given distance, and all are returned by the
classifier. The instance with the highest frequency is in that case
the \q{winner}, that is, the classification which is returned.
% we take from all the rings, but can influence using -wIL to give
% importance to the rings.

In the \Timbl{} decision tree, instances
are represented as paths through nodes labeled with class
distributions. The connections between the nodes are labeled with the
feature values. The whole structure is anchored in a root node
containing a default class distribution, representing unsmoothed
unigram probabilities.

%The nodes
%are traversed from top to bottom, and are ordered by the
%\emph{gain-ratio} \cite{Quinlan93} of the features.

%here modus operandus?

The test instances are matched against the tree in \emph{gain-ratio}
\cite{Quinlan93} order until no more feature values match. The class
distribution on the node at that point is returned as the result. The
\triblt{} implementation will switch to \ibo{} at this point, and
continue matching the rest of the feature values to find the $k$
nearest neighbours given those remaining features. It is important
to realise that possibly more than one answer is returned in what will
be referred to as a \emph{distribution}.

Take the following data set consisting of four instances. The last word in each
instance is the target word, or the word we are trying to predict.

\needspace{4\baselineskip}%
\begin{verbatim}
    it   went   well
    that went   well
    he   went   home
    he   is     old
\end{verbatim}

Training an instance base with the \igtree{} algorithm gives the
following tree structure
(\figurename~\ref{fig:wopr_clin20_igtree.pdf}). 

\begin{figure}[h!]
  \centering
  \includegraphics[width=\pdfwidth]{figs/wopr_clin20_igtree.pdf}
 \caption{Pruned \igtree{}}
  \label{fig:wopr_clin20_igtree.pdf}
\end{figure}

\emph{Classification} is the process of taking a new instance, and determining
the resulting class.  The following will happen when a new test instance is
classified. The top node labeled \emph{default distribution} is returned when
none of the features in the test instance match. The answer could be any of the
words in the distribution; \emph{well}, \emph{home} or \emph{old}. When the word
before the target word is \emph{is}, we know the answer is \emph{old}. Likewise
for \emph{went}; when that is the word before the target word, the answer is
determined to be either \emph{well} or \emph{home}. This can be further
disambiguated by finding a match on the second word before the target word. In
the case this is \emph{he}, we give the answer \emph{home}. Note that the
\emph{that} and \emph{it} nodes are pruned. We don't need to match on those
values because the node above already has \emph{well} as the majority class,
which is the right answer.

\par
%show some corner cases with "no known words" wopr vs srilm maybe?
The memory based approach to language modeling has several advantages
over a classical \ngram{}-based approach. First of all, we are not
limited to \ngram{}s but instead are able to use any number and type
of features in the instances. The instances can for example contain
grammatical and non-local context information. In this article
however, we will limit ourselves to word features. It is also possible
to use a right-context together, or even without, the words on the
left.

Secondly, the tree-based approach has an inherent back-off mechanism. In
classical \ngram{}-models, a back-off to smaller sequences (from trigrams to
bigrams, to unigrams) is performed when an \ngram{} is not found in the training
data, typically when unseen words are encountered. The \igtree{} algorithm
returns the distribution stored at that point in the tree when an unknown word
is encountered. When even the first feature fails to match, the complete class
distribution is returned. The \triblt{} algorithm is more robust in that sense;
it continues to try to match the remaining feature values.

We mentioned that the features are tested in \gr{} order.  The
following table shows the \gr{} values for \tnum{10000} lines of data
which contains six word features (three to the left and three to the
right of the target word).

%scylla:/exp2/pberck/nyt.3e7.10000.l3r3
\begin{small}
\begin{verbatim}
Number of Classes : 20401
Feats   Vals    InfoGain     GainRatio
    1  19017    4.5231960    0.47404293
    2  19601    4.7708689    0.48238061
    3  20401    5.3249050    0.51710440
    4  19748    5.3507639    0.53296977
    5  19203    4.8424147    0.49843156
    6  18785    4.5744671    0.48731039
\end{verbatim}
\end{small}

%(good, because GR has bias when feature has more values)
The data is distributed very evenly; every feature has about
\tnum{20000} values, there are about \tnum{20000} classes, and the gain
ratio figures are all around \tnum{0.5}. In these cases it makes more sense to
use \triblt{} than \igtree{}. The relative importance of the features
lie so close together that it does not make sense to designate one of
them as \q{most important}. It is better to continue matching than to
return a default distribution after just that feature has mismatched.

\subsection{Feature Order}

The \mb{} classifier sorts the features in \gr{} order.

Figure~\ref{fig:featureorder} shows the order of importance of the features in a
context of four words before and three words after the target word. The features
nearest to the target (marked with a \cmp{T}) are deemed most important. The
first feature to the right is the most important one. This means that the system
is slightly better at predicting which word came \emph{before} a given word,
than which word came after. This feature is followed by the one directly to the
left of the target (marked \cmp{l1}). This pattern repeats itself, with the next
feature on the right being next in line (marked \cmp{r2}), followed by the next
one on the left (\cmp{l2}), \etc{}.

% rmt.1e5.l4r4
% < 5, 4, 6, 3, 7, 2, 8, 1 >
% l4 l3 l2 l1 T r1 r2 r3 r4 =>  1 l1 r2 l2 r3 l3 r4 l4
%
% rmt.1e5.l4r3
% < 5, 4, 6, 3, 7, 2, 1 > = r1 l1 r2 l2 r3 l3 l4

\noindent
\begin{figure}[h!]
 \centering
%\resizebox{\linewidth}{!}{
\resizebox{\halfline}{!}{
\begin{tikzpicture}[out=45,in=135,relative,line width=0.8pt]
\node [circle,draw, fill=gray!5] (l4) at (0,0) {\cmp{l4}};
\node [circle,draw, fill=gray!10] (l3) at (1,0) {\cmp{l3}};
\node [circle,draw, fill=gray!15] (l2) at (2,0) {\cmp{l2}};
\node [circle,draw, fill=gray!30] (l1) at (3,0) {\cmp{l1}};
\node [circle,draw, line width=1pt, fill=gray!50] (t)  at (4,0) {\cmp{T}};
\node [circle,draw, fill=gray!40] (r1) at (5,0) {\cmp{r1}};
\node [circle,draw, fill=gray!20] (r2) at (6,0) {\cmp{r2}};
\node [circle,draw, fill=gray!10] (r3) at (7,0) {\cmp{r3}};
\arrowsize=4pt;
\path[-latex] (r1) edge [above] (l1); 
\path[-latex] (l1) edge [above] (r2); 
\path[-latex] (r2) edge [above] (l2);
\path[-latex] (l2) edge [above] (r3);
\path[-latex] (r3) edge [above] (l3);
\path[-latex] (l3) edge [above] (l4);
%\tikzset{arrow data/.style 2 args={%
%      decoration={%
%         markings,
%         mark=at position #1 with \arrow{#2}},
%         postaction=decorate}
%      }%
%\draw [arrow data={0.3}{>}] (l2) to (t);
\end{tikzpicture}
}
 \caption{Feature order}
 \label{fig:featureorder}
\end{figure}


\begin{comment}
Statistical classification is the problem in statistics of identifying
the sub-population to which new observations belong, where the
identify of the sub-population is unknown, on the basis of a training
set of data containing observations whose sub-population is
known. Thus the requirement is that new individual items are placed
into groups based on quantitative information on one or more
measurements, traits or characteristics, etc) and based on the
training set in which previously decided groupings are already
established.
\end{comment}

\subsection{Modus Operandus}

%It takes a number of words around the prediction target as input (the
%context), and tries to predict the missing word. In the straight
%forward case, we look at a string of words from left to right and
%predict the word that comes next, but the \wopr{} approach is more
%flexible than that.

How does \wopr{} predict words? In the \mb{} paradigm we train a
classifier from data. In the case of word prediction, this data is
plain text. The text is converted to examples, or \emph{instances}, of
equal length. An instance is a sequence of words followed by a
\emph{target} word. The target word is the word to be predicted by the
algorithm. For example, the instance \texttt{one two three} teaches
the algorithm that \texttt{one two} is followed by \texttt{three}. The
words around the target word are also called the \emph{context} around
the word. We specify the size of the context with two parameters,
\cmp{l} and \cmp{r}, short for \cmp{left} and \cmp{right}. A context
consisting of two words to the left and one word to the right of the
target word is specified as \cmp{l2r1}. The aforementioned \texttt{one
  two three} instance has an \cmp{l2r0} context.

\tablename~\ref{fig:wopr_lc_contexts.pdf} show a
few examples of possible contexts. The word \textbf{fox} is in
all cases the word to be predicted, or as mentioned above, the target
feature.

\begin{figure}[h!]
  \centering
  \includegraphics[width=\pdfwidth]{figs/wopr_lc_contexts.pdf}
 \caption{Context around the word fox}
  \label{fig:wopr_lc_contexts.pdf}
\end{figure}

The test data is similarly converted to instances. The target feature
in this case is only used to check if \wopr{} predicted the correct
word. The complete test set is processed in one run, and the scores on
all the instances are accumulated and reported at the end of each
run. The same test set is used in all the experiments.

\Wopr{} predicts one or more possible words for each instance. The words
are sorted by frequency. In the case of more than one possible answer,
the answer with the highest frequency is taken. An answer will always
be returned. If none of the features match, the default distribution
on the top node will be returned. In the case of word prediction, it
will contain all possible targets.

The next example shows a real example of a classification. The context
used to train the classifier was two words on the left
(\cmp{l2r0}). The first three words represent the instance, followed
by the classification. In this case the correct word, \cmp{to}, was
predicted. The distribution returned by \wopr{} is shown between
square brackets. In this case, only word was predicted. The other
numbers are statistics and probabilities calculated from the \timbl{}
output.

\lf
%from nyt.tail1000.l2r0_ALG6000.px
%people close to to 0 0 1 cg k 1 1 1 12 1 [ to 12 ]
\cmp{people close to to 0 0 1 cg k 1 1 1 12 1 [ to 12 ]}
\lf
\begin{comment}
The next example shows an example where more than one answer was
returned. The highest frequency word was also in this case the correct
answer. 

%from nyt.tail1000.l2r0_ALG6000.px
%close to the the -0.97728 3.26688 1.96875 cg k 1 1 27 63 1 [ the 32
%my 3 a 2 his 2 100 2 ]
\begin{example}
close to the the -0.97728 3.26688 1.96875
   cg k 1 1 27 63 1 
   [ the 32 my 3 a 2 his 2 100 2 ]
%close to (the) \small{[ the 32 my 3 a 2 his 2 100 2 ]}
\end{example}

\item[instance] An instance is represented by a sequence of words with
  boxes around them. It is made up of context and a target. So a \cmp{l2r0}
instance contains three words in total, and would look like:
\boxed{one}~\boxed{two}~~\boxed{target}.
\end{comment}

As raw \wopr{} output is quite illegible, we introduce the following
notation to illustrate the \wopr{} output.  Output is represented by
an instance plus the target classification (on a grey background),
followed by the classification and distribution:
\\ \\
\resizebox{\halfline}{!}{\wlr{m}{cg}{crazy
    when}{the}{}{the}{\dist{the}{526}\dist{he}{464}\dist{they}{219}\dsum{24}{2824}}}
\\
The distribution is a list of tokens over their frequency
(\scalebox{0.8}{\dist{the}{526}}). The top figure after the $]$ shows
the total number of elements in the distribution. The bottom figure
the sum of all the frequencies of the distribution elements. In this
example, \wopr{} predicted the correct word \textsf{the} after
\textsf{crazy when}. It returned a total of \tnum{24} answers. The
distribution can contain thousands of elements, but we only show a
handful of the top elements in our examples. If the instance contains
a right hand side context, it is shown after the target. The next
example shows \wopr{} output where the instance contained right hand
side context, and the correct classification was somewhere deeper in
the distribution.
\\ \\
\resizebox{\halfline}{!}{\wlr{m}{cd}{crazy
    when}{the}{man}{a}{\dist{the}{488}\dist{a}{264}\dsum{8}{1244}}}
\\
A classification which is not correct, but instead found \q{deeper} in
the distribution is marked with a dotted line. An incorrect
classification would be crossed out with a solid line. We can
therefore distinguish three different scores. The first one measures
the number of correct classifications, the accuracy. The second score
is the \cd{} score. Finally, we count the number of incorrect
classifications.

