The following notation is used when talking about \wopr{} data en output. First
of all the instances used to train \wopr{}. An instance consists of a sequence
of feature values called \emph{context} followed by the \emph{target}. The
feature values are most often word tokens in the case of word prediction, but
other types are possible, for example part-of-speech tags. The instances are
generated from plain text by a process called \emph{windowing}. Windowing is the
operation of moving a pointer over the sentence, and generating an instance by
taking the left and right context of the word under the pointer. The word itself
becomes the target, in this case. The size of the context is specified with two
parameters, \cmp{l} and \cmp{r}, short for \cmp{left} and \cmp{right}. A context
consisting of two words to the left and one word to the right of the target word
is specified as \cmp{l2r1}. The instances in the annotated example
(\S\tnum{\ref{subsec:annoted_example}}) have an \cmp{l2r0} context. Note that
this is therefore also the notation for trigrams. A special token is used to
specify an \emph{empty} context, namely \cmp{\_}. This empty context symbol is
used to denote positions before the first word, and after the last word in the
sentence in case of a right context. The instances in the \mb{} system have a
fixed length, and to be able to create an instance which has the first word of a
sentence in the target position, the \q{emptyness} before the first word needs
to be represented somehow.

%The fact that the feature values can contain different types of information, be
%disjunct \etc{} makes \wopr{} combine cache/tree/... \lms{}.
The next example shows a real example of a classification. The context used to
train the classifier was two words on the left (\cmp{l2r0}). This first example
is a simple one, where the word predictor only returned one word.

%from nyt.tail1000.l2r0_ALG6000.px
%people close to to 0 0 1 cg k 1 1 1 12 1 [ to 12 ]
\needspace{2\baselineskip}%
\begin{wout}{}{}
people close to to 0 0 1 cg k 1 1 1 12 1 [ to 12 ]
\end{wout}

The first three words represent the instance \cmp{people close to}, followed by
the classification returned. The task was in this case to predict the word
following the two word sequence \cmp{people close}. In this case the correct
word, \cmp{to}, was predicted. Part of the distribution returned by \wopr{} is
shown between square brackets. In this case, only one word, \cmp{to}, was
predicted. The other numbers are statistics and probabilities calculated from
the \timbl{} output.

\begin{comment}
The next example shows an example where more than one answer was
returned. The highest frequency word was also in this case the correct
answer. 

%from nyt.tail1000.l2r0_ALG6000.px
%close to the the -0.97728 3.26688 1.96875 cg k 1 1 27 63 1 [ the 32
%my 3 a 2 his 2 100 2 ]
\begin{example}
close to the the -0.97728 3.26688 1.96875
   cg k 1 1 27 63 1 
   [ the 32 my 3 a 2 his 2 100 2 ]
%close to (the) \small{[ the 32 my 3 a 2 his 2 100 2 ]}
\end{example}

\item[instance] An instance is represented by a sequence of words with
  boxes around them. It is made up of context and a target. So a \cmp{l2r0}
instance contains three words in total, and would look like:
\boxed{one}~\boxed{two}~~\boxed{target}.
\end{comment}
As raw \wopr{} output is quite illegible, we introduce the following notation to
illustrate the \wopr{} output.  Output is represented by an instance plus the
target classification (on a grey background), followed by the classification and
distribution:
\\ \\
%wopr out: { W/M }{ CG/CD/IC }{ LHS }{ TARGET }{ RHS }{ CLASS }{ DISTR }%
%            1       2          3      4         5       6       7      %
\resizebox{\halfline}{!}{\wlr{m}{cg}{crazy
    when}{the}{}{the}{\dist{the}{526}\dist{he}{464}\dist{they}{219}\dsum{24}{2824}}}
\\
The distribution is a list of tokens over their frequency of occurence in the
specified context (\scalebox{0.8}{\dist{the}{526}}). The top figure after the
$]$ shows the total number of elements in the distribution. The bottom figure
the sum of all the frequencies of the distribution elements. In this example,
\wopr{} predicted the correct word \textsf{the} after \textsf{crazy when}. It
returned a total of \tnum{24} answers. The distribution can contain thousands of
elements, but we only show a handful of the top elements in our examples. If the
instance contains a right hand side context, it is shown after the target. The
next example shows \wopr{} output where the instance contained right hand side
context (\cmp{l2r1}), and the correct classification was somewhere deeper in the
distribution. Here \wopr{} predicted the word \cmp{a} between \cmp{crazy when}
and \cmp{man}. It should have predicted the word \cmp{the}.
\\ \\
\resizebox{\halfline}{!}{\wlr{m}{cd}{crazy
    when}{the}{man}{a}{\dist{a}{488}\dist{the}{264}\dsum{8}{1244}}}
\\
A classification which is not correct, but instead found \q{deeper} in the
distribution is marked with a dotted line. An incorrect classification would be
crossed out with a solid line. Therefore a distinction between three different
kind of scores can be made. The first one measures the number of correct
classifications, or the accuracy of our word predictor. The second score is the
\cd{} score. Finally, the number of incorrect classifications are counted.

\begin{comment}
We mentioned that the features are tested in \gr{} order.  The
following table shows the \gr{} values for \tnum{10,000} lines of data
which contains six word features (three to the left and three to the
right of the target word).

%scylla:/exp2/pberck/nyt.3e7.10000.l3r3
\begin{small}
\begin{verbatim}
Number of Classes : 20401
Feats   Vals    InfoGain     GainRatio
    1  19017    4.5231960    0.47404293
    2  19601    4.7708689    0.48238061
    3  20401    5.3249050    0.51710440
    4  19748    5.3507639    0.53296977
    5  19203    4.8424147    0.49843156
    6  18785    4.5744671    0.48731039
\end{verbatim}
\end{small}

%(good, because GR has bias when feature has more values)
The data is distributed very evenly; every feature has about
\tnum{20,000} values, there are about \tnum{20,000} classes, and the gain
ratio figures are all around \tnum{0.5}. In these cases it makes more sense to
use \triblt{} than \igtree{}. The relative importance of the features
lie so close together that it does not make sense to designate one of
them as \q{most important}. It is better to continue matching than to
return a default distribution after just that feature has mismatched.
\end{comment}
