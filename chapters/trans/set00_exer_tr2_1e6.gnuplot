# data is tab:set00exert in transition.tex
# 1e6, tribl2, conll testset
set style line 1 lc rgb '#1047a9' lt 1 lw 2 pt 7 pi -1 ps 1.5
set style line 2 lc rgb '#4577d4' lt 1 lw 2 pt 8 pi -1 ps 1.5
set style line 3 lc rgb '#6b90d4' lt 1 lw 2 pt 6 pi -1 ps 1.5
set title "F-scores for varying error percentages and confidence values"
#set size 1.3,1
set xlabel "Error percentage in training data"
set xrange []
set xtics out
set xtics rotate by 295 nomirror
set rmargin 5
set ytics out nomirror
set border 3
set yrange [0:20]
set style data histogram
set style histogram cluster gap 1
set style fill solid
set ylabel "F-score"
#plot 'set00_exer_tr2_1e6.data' using 4:xticlabels(1) ls 1 title "conf 0.0",'set00_exer_tr2_1e6.data' using 7 ls 2 title "conf 0.5", 'set00_exer_tr2_1e6.data' using 10 ls 3 title "conf 0.9"
plot 'set00_exer_tr2_1e6.data' using 4:xticlabels(1) w lp ls 1 title "conf 0.0",'set00_exer_tr2_1e6.data' using 7 w lp ls 2 title "conf 0.5", 'set00_exer_tr2_1e6.data' using 10 w lp ls 3 title "conf 0.9"
set terminal push
set terminal postscript eps enhanced rounded lw 2 'Helvetica' 20
set out 'set00_exer_tr2_1e6.ps'
replot
!epstopdf 'set00_exer_tr2_1e6.ps'

# 2.5 & 6.71 &  4.38 & 5.30 &  7.81 &  3.98 &  5.28 & 8.65 & 3.59 & 5.07 \\
# 5.0 & 6.94 &  7.97 & 7.42 &  7.59 &  6.77 &  7.16 & 8.42 & 6.37 & 7.26 \\
# 7.5 & 7.08 &  9.96 & 8.28 &  7.50 &  8.37 &  7.91 & 8.33 & 7.57 & 7.93 \\
#10.0 & 7.62 & 12.35 & 9.42 &  8.85 & 11.95 & 10.17 & 7.31 & 7.57 & 7.44 \\
#12.5 & 6.35 & 12.75 & 8.48 &  7.29 & 12.79 &  9.28 & 6.25 & 8.37 & 7.16 \\
#15.0 & 5.55 & 12.35 & 7.65 &  6.41 & 11.95 &  8.34 & 5.57 & 7.97 & 6.56 