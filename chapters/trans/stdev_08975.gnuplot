set terminal dumb
#
# Use directly in gnuplot
#
# cs0 2499  cf ave 0.87 stdv 0.18 min 0.25 max 1.00 cnt 2499
#
set style line 1 lt -1 lw 0.5 pi -1 pt 1 ps 0.9
set style line 2 lt -1 lw 0.5 pi -1 pt 2 ps 0.9
set style line 3 lt -1 lw 0.5 pi -1 pt 4 ps 0.9
set style line 4 lt -1 lw 0.5 pi -1 pt 5 ps 0.9
set style line 5 lt -1 lw 0.5 pi -1 pt 6 ps 0.9
set style line 6 lt -1 lw 0.5 pi -1 pt 7 ps 0.9
set style line 7 lt -1 lw 0.5 pi -1 pt 8 ps 0.9
set style line 10 linetype 0 pointtype 0 lw 0.1 linecolor rgb "gray"
#set title "Precision versus Recall, 1e6"
set size 0.8,1
set ylabel "Confidence"
set xlabel ""
set key off
set border 3
set xtics out nomirror
unset xtics
unset ytics
set ytics out nomirror
set mytics 2
#
plot [-1:10][0:1]\
"stdev_08975.data" using 1:5:9:11 with yerrorbars
#
set terminal push
#set terminal postscript eps enhanced color solid rounded lw 2 'Helvetica' 10
set terminal postscript eps enhanced dashed rounded lw 2 'Helvetica' 20
set out 'stdev_08975.ps'
replot
!epstopdf 'stdev_08975.ps'
set term pop
