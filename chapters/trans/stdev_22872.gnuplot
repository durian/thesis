set terminal dumb
#
# Use directly in gnuplot
#
# cs0 2499  cf ave 0.87 stdv 0.18 min 0.25 max 1.00 cnt 2499
#
set style line 1 lt -1 lw 0.5 pi -1 pt 1 ps 0.9
set style line 2 lt -1 lw 0.5 pi -1 pt 2 ps 0.9
set style line 3 lt -1 lw 0.5 pi -1 pt 4 ps 0.9
set style line 4 lt -1 lw 0.5 pi -1 pt 5 ps 0.9
set style line 5 lt -1 lw 0.5 pi -1 pt 6 ps 0.9
set style line 6 lt -1 lw 0.5 pi -1 pt 7 ps 0.9
set style line 7 lt -1 lw 0.5 pi -1 pt 8 ps 0.9
set style line 10 linetype 0 pointtype 0 lw 0.1 linecolor rgb "gray"
#set title "Precision versus Recall, 1e6"
set size 0.8,1
set ylabel ""
set y2label "Confidence"  rotate by 270
set xlabel ""
set key off
set border 9
set xtics out nomirror
unset xtics
unset ytics
set y2tics out nomirror
set my2tics 2
#
plot [-1:10][0:1]\
"stdev_22872.data" using 1:5:9:11 with yerrorbars
#
set terminal push
#set terminal postscript eps enhanced color solid rounded lw 2 'Helvetica' 10
set terminal postscript eps enhanced dashed rounded lw 2 'Helvetica' 20
set out 'stdev_22872.ps'
replot
!epstopdf 'stdev_22872.ps'
set term pop
