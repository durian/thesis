sed -i0 's#å#\\aa{}#g' $1
sed -i1 's#ä#\\"a{}#g' $1
sed -i2 's#ö#\\"o{}#g' $1
sed -i3 's#Å#\\AA{}#g' $1
sed -i4 's#Ä#\\"A{}#g' $1
sed -i5 's#Ö#\\"O{}#g' $1
sed -i6 "s#é#\\\\'e{}#g" $1
