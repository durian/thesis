\section{Introduction}

This chapter describes a text correction system based on several \mb{}
classifiers. The system detects and corrects (inserts or deletes) missing and
unnecessary prepositions and determiners, and replaces incorrect determiners and
prepositions. For each error category, two classifiers are trained. One
\q{pos-neg} classifier and one classifier trained specifically to predict words
from the relevant grammatical category. The system was entered in the \hoo{}
competition, ending somewhere in the middle.

%Our Valkuil.net team entry, known under the abbreviation 'VA' in the
%HOO-2012 competition \cite{Dale+12}, is a simplistic text correction
%system based on four memory-based classifiers. The goal of the system
%is to be lightweight: simple to set up and train, fast in
%execution. It requires a (preferably very large) corpus to train on,
%and a closed list of words which together form the category of
%interest---in the HOO-2012 context, the two categories of interest are
%prepositions and determiners.

\section{Description}

The text correction described in this chapter detects and corrects the following
errors. We'll refer to the errors by their two-letter abbreviations.

\begin{varlist}{mm}
\item[\textsc{md}] Insert missing determiner:\lf
\cmp{I gave him (a) book.}
\item[\textsc{ud}] Delete unnecessary determiner:\lf
\cmp{I gave him two (the) books.}
\item[\textsc{rd}] Replace incorrect determiner:\lf
\cmp{I gave him (a,the) books.}
\item[\textsc{mp}] Insert missing preposition:\lf
\cmp{I sit (on) the chair.}
\item[\textsc{up}] Delete unnecessary preposition:\lf
\cmp{Take your shoes off (of) the bed.}
\item[\textsc{rp}] Replace incorrect preposition:\lf
\cmp{I sit (at,on) the chair.}
\end{varlist}

As a corpus we used the Google 1TB 5-gram corpus \cite{Brants+06}, and
we used two lists, one consisting of 47 prepositions and one
consisting of 24 determiners, both extracted from the HOO-2012
training data. Using the Google corpus means that we restricted
ourselves to a simple 5-gram context, which obviously places a limit
on the context sensitivity of our system; yet, we were able to make
use of the entire Google corpus.

Memory-based classifiers have been used for confusible disambiguation
\cite{VandenBosch06b} and agreement error detection
\cite{Stehouwer+09}\footnote{A working context-sensitive spelling
  checker for Dutch based on these studies is released under the name
  Valkuil.net; see \url{http://valkuil.net} -- hence the team name.}. In both studies it is
argued that fast approximations of memory-based discriminative
classifiers are effective and efficient modules for spelling
correction because of their insensitivity to the number of classes to
be predicted; they can act as simple binary decision makers (e.g. for
confusible pairs: given this context, is {\em then}\/ or {\em than}\/
more likely?), and at the same time they can handle missing word
prediction with up to millions of possible outcomes, all in the same
model. \cite{VandenBosch06b} also showed consistent log-linear
performance gains in learning curve experiments, indicating that more
training data continues to be better for these models even at very
large amounts of training data. The interested reader is referred to
the two studies for more details.

\section{System}

\begin{figure*}[ht]
  \centering
  \includegraphics[width=0.7\textwidth]{figs/main-system.pdf}
 \caption{System architecture. Shaded rectangles are the four classifiers.}
  \label{main-system}
\end{figure*}
 
Our system centers around four classifiers that all take a windowed
input of two words to the left of the focus, and two words to the
right. The focus may either be a position between two words, or a
determiner or a preposition. In case of a position between two words,
the task is to predict whether the position should actually be filled
by a preposition or a determiner. When the focus is on a determiner or
preposition, the task may be to decide whether it should actually be
deleted, or whether it should be replaced.

The main system architecture is displayed in
Figure~\ref{main-system}. The classifiers are the shaded rectangular
boxes. They are all based on IGTree, an efficient decision tree
learner \cite{Daelemans+97}, a fast approximation of memory-based or
$k$-nearest neighbor classification, implemented within the
TiMBL\footnote{\url{http://ilk.uvt.nl/timbl}} software package
\cite{Daelemans+10}. 

The first two classifiers, {\bf preposition?} and
{\bf determiner?}, are binary classifiers that determine whether or
not there should be a preposition or a determiner, respectively,
between two words to the left and two words to the right:

\begin{itemize}
\item The {\bf preposition?} classifier is trained on all 118,105,582
  positive cases of contexts in the Google 1 TB 5-gram corpus in which
  one of the 47 known prepositions are found to occur in the middle
  position of a 5-gram. To enable the classifier to answer negatively
  to other contexts, roughly the same amount of negative cases of
  randomly selected contexts with no preposition in the middle are
  added to form a training set of 235,730,253 cases. In the
  participating system we take each n-gram as a single token, and
  ignore the Google corpus token counts. We performed a validation
  experiment on a single 90\%-10\% split of the training data; the
  classifier is able to make a correct decision on 89.1\% of the 10\%
  heldout cases.
\item Analogously, the {\bf determiner?} classifier takes all
  132,483,802 positive cases of 5-grams with a determiner in the
  middle position, and adds randomly selected negative cases to
  arrive at a training set of 252,634,322 cases. On a 90\%--10\%
  split, the classifier makes the correct decision in 88.4\% of the
  10\% heldout cases.
\end{itemize}

The second pair of classifiers perform the multi-label classification
task of predicting which preposition or determiner is most likely
given a context of two words to the left and to the right. Again,
these classifiers are trained on the entire Google 1TB 5-gram corpus:

\begin{itemize}
\item The {\bf which preposition?} classifier is trained on the
  aforementioned 118,105,582 cases of any of the 47 prepositions
  occurring in the middle of 5-grams. The task of the classifier is to
  generate a class distribution of likely prepositions given an input
  of the four words surrounding the preposition, with 47 possible
  outcomes.  In a 90\%-10\% split experiment on the complete training
  set, this classifier labels 59.6\% of the 10\% heldout
  cases correctly.
\item The {\bf which determiner?} classifier, by analogy, is trained
  on the 132,483,802 positive cases of 5-grams with a determiner in the
  middle position, and generates class distributions composed of the
  24 possible class labels (the possible determiners). On a 90\%-10\%
  split of the training set, the classifier predicts 63.1\% of all
  heldout cases correctly.
\end{itemize}

Using the four classifiers and the system architecture depicted in
Figure~\ref{main-system}, the system is capable of detecting missing
and unnecessary cases of prepositions and determiners, and of
replacing prepositions and determiners by other more likely
alternatives. Focusing on the preposition half of the system, we
illustrate how these three types of error detection and correction are
carried out.

First, Figure~\ref{missing-prep} illustrates how a missing preposition
is detected. Given an input text, a four-word window of two words to
the left and two words to the right is shifted over all words. At any
word, the binary {\bf preposition?} classifier is asked to determine
whether there should be a preposition in the middle. If the classifier
says no, the window is shifted to the next position and nothing
happens. If the classifier says yes beyond a certainty threshold (more
on this in Section~\ref{optimization}), the {\bf which preposition?}
classifier is invoked to make a best guess on which preposition should
be inserted.

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.4\textwidth]{figs/missing-prep.pdf}
 \caption{Workflow for detecting a missing preposition.}
  \label{missing-prep}
\end{figure}

Second, Figure~\ref{delete-prep} depicts the workflow of how a preposition
deletion is suggested. Given an input text, all cases of prepositions
are sought. Instances of two words to the left and right of each
preposition are created, and these context windows are presented to
the {\bf preposition?} classifier. If this classifier says no beyond a
certainty threshold, the system signals that the preposition currently
in focus should be deleted.

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.3\textwidth]{figs/delete-prep.pdf}
 \caption{Workflow for suggesting a preposition deletion.}
  \label{delete-prep}
\end{figure}

Third, Figure~\ref{replace-prep} illustrates how a replacement
suggestion is generated. Just as with the detection of deletions, an
input text is scanned for all occurrences of prepositions. Again,
contextual windows of two words to the left and right of each found
preposition are created. These contexts are presented to the {\bf
  which preposition?} classifier, which may produce a different most
likely preposition (beyond a certainty threshold) than the preposition
in the text. If so, the system signals that the original preposition
should be replaced by the new best guess.

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.35\textwidth]{figs/replace-prep.pdf}
 \caption{Workflow for suggesting a preposition replacement.}
  \label{replace-prep}
\end{figure}

Practically, the system is set up as a master process (implemented in
Python) that communicates with the four classifiers over socket connections. The
master process performs all necessary data conversion and writes its
edits to the designated XML format. First, missing prepositions and
determiners are traced according to the procedure sketched above;
second, the classifiers are employed to find replacement errors;
third, unnecessary determiners and prepositions are sought. The system
does not iterate over its own output.

\section{Optimizing the system}
\label{optimization}

When run unfiltered, the four classifiers tend to overpredict errors
massively. They are not very accurate (the binary classifiers operate
at a classification accuracy of about 88--89\%; the multi-valued
classifiers at 60--63\%). On the other hand, they produce class
distributions that have properties that could be exploited to filter
the classifications down to cases where the system is more
certain. This enables us to tune the precision and recall behavior of
the classifiers, and, for instance, optimize on F-Score. We introduce
five hyperparameter thresholds by which we can tune our four
classifiers.

First we introduce two thresholds for
the two binary classifiers {\bf preposition?} and {\bf determiner?}:

\begin{description}
\item[$M$] --- When the two binary {\bf preposition?} and {\bf
    determiner?}  classifiers are used for detecting missing
  prepositions or determiners, the positive class must be $M$ times
  more likely than the negative class.
\item[$U$] --- In the opposite case, when the two binary classifiers
  are used for signalling the deletion of an unnecessary preposition
  or determiner, the negative class must be $U$ times more likely than
  the positive class.
\end{description}

For the two multi-label classifiers {\bf which preposition?} and {\bf
  which determiner?} we introduce three thresholds (which again can be
set separately for determiners and prepositions):

\begin{description}
\item[$DS$] --- the distribution size (i.e. the number of labels that
  have a non-zero likelihood according to the classifier) must be
  smaller than $DS$. A large $DS$ signals a relatively large
  uncertainty.
\item[$F$] --- the frequency of occurrence of the most likely outcome
  in the training set must be larger than $F$. Outcomes with a smaller
  number of occurrences should be distrusted more.
\item[$R$] --- if the most likely outcome is different from the
  preposition or determiner currently in the text, the most likely
  outcome should be at least $R$ times more likely than the current
  preposition or determiner. Preferably the likelihood of the latter
  should be zero.
\end{description}

On the gold training data provided during the training phase of the
HOO-2012 challenge we found, through a semi-automatic optimization
procedure, three settings that optimized precision, recall, and
F-Score, respectively. Table~\ref{optimal-settings} displays the
optimal settings found. The results given in Section~\ref{results}
always refer to the system optimized on F-Score, listed in the
rightmost column of Table~\ref{optimal-settings}.

\begin{table}
\begin{center}
\begin{tabular}{|l|l|ccc|}\hline
 & & \multicolumn{3}{|c|}{Optimizing on} \\
Task & Thresh. & Precision & Recall & F-Score \\ \hline
Prep. & $M$ & 30 & 10 & 20 \\
 & $U$ & 30 & 4 & 4 \\
 & $DS$ & 5 & 50 & 50 \\
 & $F$ & 50 & 5 & 5 \\
 & $R$ & 10 & 20 & 20 \\ \hline
Det. & $M$ & 30 & 10 & 20 \\
 & $U$ & 30 & 2 & 2 \\
 & $DS$ & 5 & 50 & 20 \\
 & $F$ & 50 & 5 & 20 \\
 & $R$ & 10 & 20 & 20 \\ \hline
\hline
\end{tabular}
\end{center}
\label{optimal-settings}
\caption{Semi-automatically established thresholds that optimize
  precision, recall, and F-Score. Optimization was performed on the
  HOO-2012 training data.}
\end{table}

The table shows that most of the ratio thresholds found to optimize
F-Score are quite high; for example, the {\bf preposition?} classifier
needs to assign a likelihood to a positive classification that is at
least 20 times more likely than the negative classification in order
to trigger a missing preposition error. The threshold for marking
unnecessary prepositions is considerably lower at 4 (and even at 2 for
determiners).

\section{Results}
\label{results}

\begin{table*}[ht]
\begin{center}
\begin{tabular}{|l|l|ccc|ccc|}\hline
 & & \multicolumn{3}{|c|}{Before revisions} & \multicolumn{3}{|c|}{After
   revisions} \\
Task & Evaluation & Precision & Recall & F-Score & Precision & Recall & F-Score \\\hline
Overall & Detection & 12.5 & 15.23 & 13.73 & 13.22 & 15.43 & 14.24 \\
 & Recognition & 10.87 & 13.25 & 11.94 & 11.59 & 13.53 & 12.49 \\
 & Correction & 6.16 & 7.51 & 6.77 & 7.25 & 8.46 & 7.8  \\ \hline
Prepositions & Detection & 13.44 & 14.41 & 13.91 & 14.23 & 14.75 & 14.49  \\
 & Recognition & 11.46 & 12.29 & 11.86 & 12.65 & 13.11 & 12.88 \\
 & Correction & 7.51 & 8.05 & 7.77 & 8.7 & 9.02 & 8.85  \\ \hline
Determiners & Detection & 11.04 & 15.21 & 12.79 & 11.71 & 15.28 & 13.26  \\
 & Recognition & 10.37 & 14.29 & 12.02 & 10.7 & 13.97 & 12.12 \\
 & Correction & 5.02 & 6.91 & 5.81 & 6.02 & 7.86 & 6.82  \\
\hline
\end{tabular}
\end{center}
\label{overall}
\caption{Best scores of our system before (left) and after (right)
  revisions. Scores are reported at the overall level (top), on
  prepositions (middle), and determiners (bottom).}
\end{table*}

The output of our system on the data provided during the test phase of
HOO-2012 was processed through the evaluation software by the
organizers. The original test data was revised in a correction round
in which a subset of the participants could suggest corrections to the
gold standard. We did not contribute suggestions for revisions, but
our scores slightly improved after revisions. Table~\ref{overall}
summarizes the best scores of our system optimized on F-Score, before
and after revisions. Our best score is an overall F-Score of
14.24 on error detection, after revisions. Our system performs slightly better on
prepositions than on determiners, although the differences are
small. Optimizing on F-Score implies that a reasonable balance is
found between recall and precision, but overall our results are not
impressive, especially not in terms of correction.

\section{Discussion}

We presented a correction system for detecting errors on preposition
and determiners, the focus task of the HOO-2012 challenge. Our system
consists of four memory-based classifiers and a master process that
communicates with these classifiers in a simple workflow. It takes
several hours to train our system on the Google 1TB 5-gram corpus, and
it takes in the order of minutes to process the 1,000 training
documents. The system can be trained without needing linguistic
knowledge or the explicit computation of linguistic analysis levels
such as POS-tagging or syntactic analyses, and is to a large extent
language-independent (it does rely on tokenization being possible).

This simple generic approach leads to mediocre results, however. There
is room for improvement. We have experimented with incorporating the
n-gram counts in the Google corpus in our classifiers, leading to
improved recall (post-competition). It still remains to be seen if the
Google corpus is the best corpus for this task, or for the particular
English-as-a-second-language writer data used in the HOO-2012
competition. Another likely improvement would be to limit which words
get corrected by which other words based on confusion statistics in
the training data: for instance, the training data may tell that 'my'
should rarely, if ever, be corrected into 'your', but our system is
blind to such likelihoods.

\subsection*{Acknowledgements}

The authors thank Ko van der Sloot for his continued improvements on
the TiMBL software. This work is rooted in earlier joint work funded
through a grant from the Netherlands Organization for Scientific
Research (NWO) for the Vici project {\em Implicit Linguistics}.

