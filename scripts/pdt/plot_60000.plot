# Hand made for DATA.PRE.60000
#
# PRE60000 1000 136754 8056 5.89087 1 5 l2r0_-a1+D
# PRE60001 1000 136754 9344 6.83271 2 52 l2r0_-a1+D
# PRE60002 1000 136754 9565 6.99431 3 521 l2r0_-a1+D
# PRE60003 1000 136754 9570 6.99797 4 5211 l2r0_-a1+D
# PRE60004 1000 136754 9580 7.00528 5 52111 l2r0_-a1+D
#
set title "Different length and depth"
set xlabel "Lines of data"
set key bottom
set logscale x
set ylabel "Percentage Saved"
set grid
#
plot [1000:1000000][0:25] "< grep '1 5 l2r0_-a1+D' ../../data/pdt/DATA.PRE.60000.plot" using 2:5 w lp t "5",\
"< grep '2 11 l2r0_-a1+D' ../../data/pdt/DATA.PRE.50000.plot" using 2:5 w lp t "11",\
\
"< grep '2 55 l2r0_-a1+D' ../../data/pdt/DATA.PRE.50000.plot" using 2:5 w lp t "55",\
\
"< grep '3 511 l2r0_-a1+D' ../../data/pdt/DATA.PRE.10000.plot" using 2:5 w lp t "511",\
"< grep '3 555 l2r0_-a1+D' ../../data/pdt/DATA.PRE.10000.plot" using 2:5 w lp t "555"
#
#\
#"< grep '3 521 l2r0_-a1+D' DATA.PRE.60000.plot" using 2:5 w lp t "521",\
#"< grep '4 5211 l2r0_-a1+D' DATA.PRE.60000.plot" using 2:5 w lp t "5211",\
#"< grep '5 52111 l2r0_-a1+D' DATA.PRE.60000.plot" using 2:5 w lp t "52111",\
#"< grep '3 511 l2r0_-a1+D' DATA.PRE.10000.plot" using 2:5 w lp t "511",\
#"< grep '3 551 l2r0_-a1+D' DATA.PRE.10000.plot" using 2:5 w lp t "551",\
#"< grep '2 51 l2r0_-a1+D' DATA.PRE.50000.plot" using 2:5 w lp t "51",\
#
set terminal push
set terminal postscript eps enhanced monochrome rounded lw 2 'Helvetica' 20
set out 'DATA_PRE_60000.ps'
replot
!epstopdf 'DATA_PRE_60000.ps'
set term pop