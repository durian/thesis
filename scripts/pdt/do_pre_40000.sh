#!/bin/sh
#
TRAINBASE="../nyt.3e7"
TESTFILE="nyt.tail1000"
#
WOPR="/exp/pberck/wopr/wopr"
SCRIPT="do_pre.wopr_script"
#
CYCLE=40000
LOG="README.PRE.${CYCLE}.txt"
PLOT="DATA.PRE.${CYCLE}.plot"
#
# Exps
#
date "+#%Y-%m-%d %H:%M:%S" >> ${LOG}
#
#N=4 #Calculated
#DS=5111 #LOOPED
MO=1
#
for LINES in 1000 5000 10000 50000 100000 500000 1000000
do
    #Make the dataset here, clean up afterwards?
    TRAINFILE=${TRAINBASE}.${LINES}
    if [ -e "${TRAINFILE}" ]
    then
	echo "Data file ${TRAINFILE} exists."
    else
	echo "Creating data file ${TRAINFILE}."
	head -n ${LINES} ${TRAINBASE} > ${TRAINFILE}
    fi
    for TIMBL in "-a1 +D" "-a4 +D"  
    do
	RC=0
	for LC in 1 2 3 4 5
	do
	    for DS0 in 1 2 3 4 5 6 7 8 9 a
	    do
		DS=${DS0}
		    ##take N from length(DS)
		N=${#DS}
		    #
		FREE=`df -k . | tail -1 | awk '{print $3}'`
		# 25 577 264 = 25GB
		while [ $FREE -lt 10000000 ]
		do  
		    echo "Space critical, pausing at ${CYCLE}."
		    sleep 60
		    FREE=`df -k . | tail -1 | awk '{print $3}'`
		done
		CYCLESTR=`printf "%05d" ${CYCLE}`
		ID=PRE${CYCLESTR}
		echo ${ID}
		# if exists pdt file, skip?
		PDTFILE=${TESTFILE}_n${N}ds${DS}_${ID}.pdt
		if [ -e "${PDTFILE}" ]
		then
		    echo "${PDTFILE} exists. Skipping."
		else
		echo ${WOPR} -s ${SCRIPT} -p trainset:${TRAINFILE},rc:${RC},lc:${LC},id:${ID},timbl:"${TIMBL}",testset:${TESTFILE},n:${N},ds:${DS},mo:${MO}
		${WOPR} -s ${SCRIPT} -p trainset:${TRAINFILE},rc:${RC},lc:${LC},id:${ID},timbl:"${TIMBL}",testset:${TESTFILE},ds:${DS},n:${N},mo:${MO} > output.${ID}
		fi
		#
		CYCLE=$(( $CYCLE + 1 ))
		#
		# gather restuls for plot file
		#
		#nyt.tail1000_n3ds511_PRE10000.pdt
		PDTFILE=${TESTFILE}_n${N}ds${DS}_${ID}.pdt
		echo ${PDTFILE}
		# BLOB like: T 380773 19189 5.03949
		BLOB=`tail -n1 ${PDTFILE}`
		echo ${BLOB}
                #http://www.arachnoid.com/linux/shell_programming.html
		kps=0
		svd=0
		pct=0
                RX='T (.*) (.*) (.*)'
                if [[ "$BLOB" =~ $RX ]]
                then
		    kps=${BASH_REMATCH[1]}
		    svd=${BASH_REMATCH[2]}
		    pct=${BASH_REMATCH[3]}
                fi
		#
		TSTR=l${LC}r${RC}_"${TIMBL// /}"
		printf -v S "%s %s %s %s %s %s %s %s" ${ID} ${LINES} ${kps} ${svd} ${pct} ${N} ${DS} ${TSTR}
		echo ${S} >> ${PLOT}
	    done
	done
    done
done

