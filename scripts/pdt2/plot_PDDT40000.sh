# Hand made for ../../data/pdt2/DATA.PDTT.40000
#
# 5 3 211 c12_-a4+D l2_-a1+D
#
set title "Combined and individual scores"
# LTR, 50000, depth 5, c4 T2, WRD x, l2 IG, n3ds211"
set xlabel "Lines of data"
set key bottom
set logscale x
set ylabel "Percentage Saved"
set grid
#
plot [1000:][:60] "< grep '5 3 211 c4_-a1+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.40000.plot" using 3:6 w lp t "Combined","< grep '5 3 211 c4_-a1+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.40000.plot" using 3:8 w lp t "Letter c4","< grep '5 3 211 c4_-a1+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.40000.plot" using 3:10 w lp t "Word b211"
#
#"< grep '5 3 211 c8_-a4+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.40000.plot" using 3:6 w lp t "CMB c8","< grep '5 3 211 c8_-a4+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.40000.plot" using 3:8 w lp t "LTR c8"
#,"< grep '5 3 511 c8_-a4+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.40000.plot" using 3:6 w lp t "CMB c8","< grep '5 3 511 c8_-a4+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.40000.plot" using 3:8 w lp t "LTR c8","< grep '5 3 511 c8_-a4+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.40000.plot" using 3:10 w lp t "WRD c8"
#
set terminal push
set terminal postscript eps enhanced monochrome rounded lw 2 'Helvetica' 20
set out 'DATA_PDTT_40000.ps'
replot
!epstopdf 'DATA_PDTT_40000.ps'
set term pop
