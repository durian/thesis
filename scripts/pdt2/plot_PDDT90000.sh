# Hand made for ../../data/pdt2/DATA.PDTT.90000
#
set title "90000 letter context c2-c16, depth 5"
set xlabel "instances"
set key bottom
set logscale x
set ylabel "Percentage Saved"
set grid
#plot [1000:][15:55] "DATA.PDTT.90000.plot" every 12::0 using 2:6 with lines t "c2_-a1+D","DATA.PDTT.90000.plot" every 12::1 using 2:6 with lines t "c3_-a1+D","DATA.PDTT.90000.plot" every 12::2 using 2:6 with lines t "c4_-a1+D","DATA.PDTT.90000.plot" every 12::3 using 2:6 with lines t "c8_-a1+D","DATA.PDTT.90000.plot" every 12::4 using 2:6 with lines t "c12_-a1+D","DATA.PDTT.90000.plot" every 12::5 using 2:6 with lines t "c16_-a1+D"
#
#c2_-a1+D l2_-a1+D
#c3_-a1+D l2_-a1+D
#c4_-a1+D l2_-a1+D
#c8_-a1+D l2_-a1+D
#c12_-a1+D l2_-a1+D
#c16_-a1+D l2_-a1+D
#
# alls plots
#plot [1000:][15:55] "< grep 'c2_-a1+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.90000.plot" using 2:6 w lp t "Igtree c2","< grep 'c3_-a1+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.90000.plot" using 2:6 w lp t "Igtree c3","< grep 'c4_-a1+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.90000.plot" using 2:6 w lp t "Igtree c4","< grep 'c8_-a1+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.90000.plot" using 2:6 w lp t "Igtree c8","< grep 'c12_-a1+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.90000.plot" using 2:6 w lp t "Igtree c12","< grep 'c16_-a1+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.90000.plot | egrep -v '0 0 0 0'" using 2:6 w lp t "Igtree c16",     "< grep 'c2_-a4+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.90000.plot" using 2:6 w lp t "Tribl2 c2","< grep 'c3_-a4+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.90000.plot" using 2:6 w lp t "Tribl2 c3","< grep 'c4_-a4+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.90000.plot" using 2:6 w lp t "Tribl2 c4","< grep 'c8_-a4+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.90000.plot" using 2:6 w lp t "Tribl2 c8","< grep 'c12_-a4+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.90000.plot" using 2:6 w lp t "Tribl2 c12","< grep 'c16_-a4+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.90000.plot | egrep -v '0 0 0 0'" using 2:6 w lp t "Tribl2 c16"
#
# c2 and c3 removed 
plot [1000:][15:55] "< grep 'c4_-a1+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.90000.plot" using 2:6 w lp t "Igtree c4","< grep 'c8_-a1+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.90000.plot" using 2:6 w lp t "Igtree c8","< grep 'c12_-a1+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.90000.plot" using 2:6 w lp t "Igtree c12","< grep 'c16_-a1+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.90000.plot | egrep -v '0 0 0 0'" using 2:6 w lp t "Igtree c16",     "< grep 'c4_-a4+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.90000.plot" using 2:6 w lp t "Tribl2 c4","< grep 'c8_-a4+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.90000.plot" using 2:6 w lp t "Tribl2 c8","< grep 'c12_-a4+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.90000.plot" using 2:6 w lp t "Tribl2 c12","< grep 'c16_-a4+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.90000.plot | egrep -v '0 0 0 0'" using 2:6 w lp t "Tribl2 c16" 
#
set terminal push
set terminal postscript eps enhanced color solid rounded lw 2 'Helvetica' 10
set out 'DATA.PDTT.90000.ps'
replot
!epstopdf 'DATA.PDTT.90000.ps'
set term pop
