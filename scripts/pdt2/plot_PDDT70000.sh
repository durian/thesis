# Hand made for ../../data/pdt2/DATA.PDTT.70000
#
#PDTT70000 50000 1000 136754 60194 44.0163 54626 39.9447 5568 4.07154 5 4 1111 c4_-a1+D l2_-a1+D
#PDTT70001 50000 1000 136754 59716 43.6667 54148 39.5952 5568 4.07154 5 4 1111 c8_-a1+D l2_-a1+D
#PDTT70002 50000 1000 136754 56627 41.4079 51059 37.3364 5568 4.07154 5 4 1111 c12_-a1+D l2_-a1+D
#PDTT70003 50000 1000 136754 56077 41.0057 50509 36.9342 5568 4.07154 5 4 1111 c16_-a1+D l2_-a1+D
#
set title "50000 LTR, x WRD (l2_-a1+D)"
set xlabel "instances"
set key bottom
set logscale x
set ylabel "Percentage Saved, Combined"
set grid
#
plot [1000:][25:65] "< grep 'c4_-a1+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.70000.plot" using 3:6 w lp t "Igtree c4","< grep 'c8_-a1+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.70000.plot" using 3:6 w lp t "Igtree c4","< grep 'c12_-a1+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.70000.plot" using 3:6 w lp t "Igtree c12","< grep 'c16_-a1+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.70000.plot" using 3:6 w lp t "Igtree c16"
#
set terminal push
set terminal postscript eps enhanced color solid rounded lw 2 'Helvetica' 10
set out 'DATA.PDTT.70000.ps'
replot
!epstopdf 'DATA.PDTT.70000.ps'
set term pop
