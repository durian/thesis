# Hand made for ../../data/pdt2/DATA.PDTT.30000
#
# PDTT30008 10000 1000 136754 50694 37.0695 41129 30.0752 9565 6.99431 5 3 521 c3_-a1+D l2_-a1+D
#                 ^                                                        ^^^ ^^
# 30000: LTR: 10000, dl:5, lc:2,3,4,8,12,16
#        WRD: 1000-1e7, lc:2, n3ds 211 221 511 521 551
#        "learning curve, LTR 10000, more contexts, LTR depth 5"
#
set title "30000 LTR 10000, depth 5, c8/12 -a4+D, WRD x, l2_-a1+D, n3ds211"
set xlabel "Lines of data"
set key bottom
set logscale x
set ylabel "Percentage Saved"
set grid
#
plot [1000:][:65] "< grep '5 3 211 c12_-a4+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.30000.plot" using 3:6 w lp t "CMB c12","< grep '5 3 211 c12_-a4+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.30000.plot" using 3:8 w lp t "LTR c12","< grep '5 3 211 c12_-a4+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.30000.plot" using 3:10 w lp t "WRD",\
"< grep '5 3 211 c8_-a4+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.30000.plot" using 3:6 w lp t "CMB c8","< grep '5 3 211 c8_-a4+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.30000.plot" using 3:8 w lp t "LTR c8"
#,"< grep '5 3 511 c8_-a4+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.30000.plot" using 3:6 w lp t "CMB c8","< grep '5 3 511 c8_-a4+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.30000.plot" using 3:8 w lp t "LTR c8","< grep '5 3 511 c8_-a4+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.30000.plot" using 3:10 w lp t "WRD c8"
#
set terminal push
set terminal postscript eps enhanced color solid rounded lw 2 'Helvetica' 10
set out 'DATA.PDTT.30000.ps'
replot
!epstopdf 'DATA.PDTT.30000.ps'
set term pop
