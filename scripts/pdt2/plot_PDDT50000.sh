# Hand made for ../../data/pdt2/DATA.PDTT.50000
#
# LTR: depth: 2..10, c4,c8,c12, a1, a4, 211, 221 => 
#PDTT50000 50000 1000 136754 49845 36.4487 42711 31.232 7134 5.21667 2 3 211 c4_-a1+D c2_-a1+D
#PDTT50001 50000 1000 136754 50024 36.5796 42611 31.1589 7413 5.42068 2 3 221 c4_-a1+D c2_-a1+D
#..
#PDTT50106 50000 1000 136754 50178 36.6922 43044 31.4755 7134 5.21667 a 3 211 c12_-a4+D l2_-a1+D
#PDTT50107 50000 1000 136754 50367 36.8304 42954 31.4097 7413 5.42068 a 3 221 c12_-a4+D l2_-a1+D
#
set title "50000 LTR, depth 5, c4/c8, n3ds211"
set xlabel "Lines of data"
set key bottom
set logscale x
set ylabel "Percentage Saved, Combined"
set grid
#
#plot [1000:][25:65] "< grep '5 3 221 c4_-a1+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.50000.plot" using 3:6 w lp t "Igtree c4","< grep '5 3 221 c8_-a1+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.50000.plot" using 3:6 w lp t "Igtree c8"
plot [1000:][25:65] "< grep '5 3 211 c4_-a1+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.50000.plot" using 3:6 w lp t "Igtree c4","< grep '5 3 211 c8_-a1+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.50000.plot" using 3:6 w lp t "Igtree c8"
#
set terminal push
#set terminal postscript eps enhanced color solid rounded lw 2 'Helvetica' 10
set terminal postscript eps enhanced monochrome  rounded lw 2 'Helvetica' 10
set out 'DATA.PDTT.50000.ps'
replot
!epstopdf 'DATA.PDTT.50000.ps'
set term pop
