# Hand made for ../../data/pdt2/DATA.PDTT.80000
#
#PDTT80000 100 100 136754 25358 18.5428 17643 12.9013 7715 5.64152 3 3 511 c4_-a1+D l2_-a1+D
#PDTT80001 100 100 136754 24792 18.1289 17077 12.4874 7715 5.64152 3 3 511 c8_-a1+D l2_-a1+D
#PDTT80002 100 100 136754 24704 18.0646 16989 12.423 7715 5.64152 3 3 511 c12_-a1+D l2_-a1+D
#PDTT80003 100 100 136754 24702 18.0631 16987 12.4216 7715 5.64152 3 3 511 c16_-a1+D l2_-a1+D
#
set title "Combined scores, equal data, Gigaword corpus (n3ds511)"
set xlabel "Lines of data"
set key bottom
set logscale x
set ylabel "Percentage Saved, Combined"
set grid
#
#plot [1000:][25:65] "< grep 'c4_-a1+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.80000.plot" using 2:6 w lp t "Igtree c4","< grep 'c8_-a1+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.80000.plot" using 2:6 w lp t "Igtree c8","< grep 'c12_-a1+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.80000.plot" using 2:6 w lp t "Igtree c12","< grep 'c16_-a1+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.80000.plot" using 2:6 w lp t "Igtree c16"
plot [1000:][25:65] "< grep 'c4_-a1+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.80000.plot" using 2:6 w lp t "c4","< grep 'c8_-a1+D l2_-a1+D' ../../data/pdt2/DATA.PDTT.80000.plot" using 2:6 w lp t "c8"
#
set terminal push
set terminal postscript eps enhanced monochrome rounded lw 2 'Helvetica' 20
set out 'DATA_PDTT_80000.ps'
replot
!epstopdf 'DATA_PDTT_80000.ps'
set term pop
