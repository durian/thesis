# Hand made for ../../data/pdt3/DATA.PDT3.30000.plot
#
# PDT3_30000 1000 1000 136754 27152 19.8546 22349 16.3425 4803 3.51215 5 1 1 c2_-a1+D l2_-a1+D
#                             |-total-----| |-T0--------| |-T1-------|
# _c8_1_11_111
#
# find . -name '*gnuplot' -print | xargs -t sed -i ".bak" 's/using 2/using 3/g'
# find . -name '*gnuplot' -print | xargs -t sed -i ".bak" 's/1000:/1000:1000000/g'
#
set title "Separate scores, equal data, Gigaword corpus"
set xlabel "Lines of data"
set key left top
set logscale x
set ylabel "Percentage Saved"
set grid
#
plot [1000:1000000][0:50] \
"< grep ' 1 c8_-a1+D l2_-a1+D' ../../data/pdt3/DATA.PDT3.30000.plot" using 3:8 w lp t "letter 1",\
"< grep ' 1 c8_-a1+D l2_-a1+D' ../../data/pdt3/DATA.PDT3.30000.plot" using 3:10 w lp t "word 1",\
"< grep 'OFF 11 c8_-a1+D l2_-a1+D' ../../data/pdt3/DATA.PDT3.30000.plot" using 3:8 w lp t "letter 11",\
"< grep 'OFF 11 c8_-a1+D l2_-a1+D' ../../data/pdt3/DATA.PDT3.30000.plot" using 3:10 w lp t "word 11",\
"< grep ' 111 c8_-a1+D l2_-a1+D' ../../data/pdt3/DATA.PDT3.30000.plot" using 3:8 w lp t "letter 111",\
"< grep ' 111 c8_-a1+D l2_-a1+D' ../../data/pdt3/DATA.PDT3.30000.plot" using 3:10 w lp t "word 111"
#
set terminal push
set terminal postscript eps enhanced monochrome rounded lw 2 'Helvetica' 20
set out 'DATA_PDT3_30000_t0t1_c8_1_11_111.ps'
replot
!epstopdf 'DATA_PDT3_30000_t0t1_c8_1_11_111.ps'
set term pop
