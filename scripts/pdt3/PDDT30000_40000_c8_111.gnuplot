# Hand made for ../../data/pdt3/DATA.PDT3.30000.plot
# and ../../data/pdt3/DATA.PDT3.40000.plot
#
# Shows difference between leter 5 and letter 6 branching
#
# 30000_40000_c8_111
#
set title "Combined scores, equal data, Gigaword corpus"
set xlabel "Lines of data"
set key bottom
set logscale x
set ylabel "Percentage Saved, Combined"
set grid
#
plot [1000:1000000][25:60] \
"< grep ' 1 c8_-a1+D l2_-a1+D' ../../data/pdt3/DATA.PDT3.30000.plot" using 3:6 w lp t "3.1",\
"< grep ' 1 c8_-a1+D l2_-a1+D' ../../data/pdt3/DATA.PDT3.40000.plot" using 3:6 w lp t "4.1",\
"< grep ' 111 c8_-a1+D l2_-a1+D' ../../data/pdt3/DATA.PDT3.30000.plot" using 3:6 w lp t "3.111",\
"< grep ' 111 c8_-a1+D l2_-a1+D' ../../data/pdt3/DATA.PDT3.40000.plot" using 3:6 w lp t "4.111"
#
set terminal push
set terminal postscript eps enhanced monochrome rounded lw 2 'Helvetica' 20
set out 'DATA_PDT3_30000_40000_c8_111.ps'
replot
!epstopdf 'DATA_PDT3_30000_40000_c8_111.ps'
set term pop
