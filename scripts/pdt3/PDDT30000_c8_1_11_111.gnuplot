# Hand made for ../../data/pdt3/DATA.PDT3.30000.plot
#
# PDT3_30000 1000 1000 136754 27152 19.8546 22349 16.3425 4803 3.51215 5 1 1 c2_-a1+D l2_-a1+D
#                             |-total----| |-T0--------| |-T1-------|
# _c8_1_11_111
#
set title "Combined scores, equal data, Gigaword corpus"
set xlabel "Lines of data"
set key bottom
set logscale x
set ylabel "Percentage Saved, Combined"
set grid
#
plot [1000:1000000][25:60] "< grep ' 1 c8_-a1+D l2_-a1+D' ../../data/pdt3/DATA.PDT3.30000.plot" using 3:6 w lp t "1","< grep ' 11 c8_-a1+D l2_-a1+D' ../../data/pdt3/DATA.PDT3.30000.plot" using 3:6 w lp t "11","< grep ' 111 c8_-a1+D l2_-a1+D' ../../data/pdt3/DATA.PDT3.30000.plot" using 3:6 w lp t "111"
#
set terminal push
set terminal postscript eps enhanced monochrome rounded lw 2 'Helvetica' 20
set out 'DATA_PDT3_30000_c8_1_11_111.ps'
replot
!epstopdf 'DATA_PDT3_30000_c8_1_11_111.ps'
set term pop
