# Hand made for ../../data/pdt3/DATA.PDT3.30000.plot
#
# _c4c8c10_111111
#
set title "Combined scores, equal data, Gigaword corpus"
set xlabel "Lines of data"
set key bottom
set logscale x
set ylabel "Percentage Saved, Combined"
set grid
#
plot [1000:1000000][25:60] "< grep ' 111111 c4_-a1+D l2_-a1+D' ../../data/pdt3/DATA.PDT3.30000.plot" using 3:6 w lp t "c4","< grep ' 111111 c8_-a1+D l2_-a1+D' ../../data/pdt3/DATA.PDT3.30000.plot" using 3:6 w lp t "c8","< grep ' 111111 c10_-a1+D l2_-a1+D' ../../data/pdt3/DATA.PDT3.30000.plot" using 3:6 w lp t "c10"
#
set terminal push
set terminal postscript eps enhanced monochrome rounded lw 2 'Helvetica' 20
set out 'DATA_PDT3_30000_c4c8c10_111111.ps'
replot
!epstopdf 'DATA_PDT3_30000_c4c8c10_111111.ps'
set term pop
