# Hand made for ../../data/pdt/DATA...
#
# PRE50540 1000 136754 8400 6.14242 2 51 l4r0_-a4+D
#
# t1_5_51_511
#
set title "Word completion scores, Gigaword corpus"
set xlabel "Lines of data"
set key bottom
set logscale x
set ylabel "Percentage Saved"
set grid
#
plot [1000:1000000][0:25] \
"< grep ' 1 5 l2r0_-a1+D' ../../data/pdt/DATA.PRE.60000.plot" using 2:5 w lp t "b5 w2",\
"< grep ' 2 51 l2r0_-a1+D' ../../data/pdt/DATA.PRE.50000.plot" using 2:5 w lp t "b51 w2",\
"< grep ' 3 511 l2r0_-a1+D' ../../data/pdt/DATA.PRE.10000.plot" using 2:5 w lp t "b511 w2"
#"< grep ' 1 5 l3r0_-a1+D' ../../data/pdt/DATA.PRE.60000.plot" using 2:5 w lp t "b5 l3",\
#"< grep ' 2 51 l3r0_-a1+D' ../../data/pdt/DATA.PRE.50000.plot" using 2:5 w lp t "b51 l3",\
#"< grep ' 3 511 l3r0_-a1+D' ../../data/pdt/DATA.PRE.10000.plot" using 2:5 w lp t "b511 l3"
#
set terminal push
set terminal postscript eps enhanced monochrome rounded lw 2 'Helvetica' 20
set out 'DATA_PRE_t1_5_51_511.ps'
replot
!epstopdf 'DATA_PRE_t1_5_51_511.ps'
set term pop
