# Hand made for DATA.PDT3.30000.plot
#
# PDT3_30000 1000 1000 136754 27152 19.8546 22349 16.3425 4803 3.51215 5 1 1 c2_-a1+D l2_-a1+D
#                             |-total-----| |-T0--------| |-T1-------|
# _c4c6c8c10c12_1
#
# find . -name '*gnuplot' -print | xargs -t sed -i ".bak" 's/using 2/using 3/g'
# find . -name '*gnuplot' -print | xargs -t sed -i ".bak" 's/1000:/1000:1000000/g'
#
set title "Separate scores, equal data, Gigaword corpus"
set xlabel "Lines of data"
set key left top
set logscale x
set ylabel "Percentage Saved"
set grid
#
plot [1000:1000000][25:60] \
"< grep ' 1 c4_-a1+D l2_-a1+D' DATA.PDT3.30000.plot" using 3:6 w lp t "c4",\
"< grep ' 1 c6_-a1+D l2_-a1+D' DATA.PDT3.30000.plot" using 3:6 w lp t "c6",\
"< grep ' 1 c8_-a1+D l2_-a1+D' DATA.PDT3.30000.plot" using 3:6 w lp t "c8",\
"< grep ' 1 c10_-a1+D l2_-a1+D' DATA.PDT3.30000.plot" using 3:6 w lp t "c10",\
"< grep ' 1 c12_-a1+D l2_-a1+D' DATA.PDT3.30000.plot" using 3:6 w lp t "c12"
#
set terminal push
set terminal postscript eps enhanced monochrome rounded lw 2 'Helvetica' 20
set out 'DATA_PDT3_30000_c4c6c8c10c12_1.ps'
replot
!epstopdf 'DATA_PDT3_30000_c4c6c8c10c12_1.ps'
set term pop
