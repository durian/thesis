# Hand made for DATA.PDT3.30000.plot
# and DATA.PDT3.40000.plot
#
# t1_5_51_511
#
set title "Word completion scores, Gigaword corpus"
set xlabel "Lines of data"
set key bottom
set logscale x
set ylabel "Percentage Saved"
set grid
#
plot [1000:10000000][0:25] \
"< grep ' 1 5 c4_-a1+D l2_-a1+D' DATA.PDT3.20000.plot" using 3:10 w lp t "n1d5",\
"< grep ' 2 51 c4_-a1+D l2_-a1+D' DATA.PDT3.20000.plot" using 3:10 w lp t "n2d51",\
"< grep ' 3 511 c4_-a1+D l2_-a1+D' DATA.PDT3.20000.plot" using 3:10 w lp t "n3d511"
#
set terminal push
set terminal postscript eps enhanced monochrome rounded lw 2 'Helvetica' 20
set out 'DATA_PDT3_30000_t1_5_51_511.ps'
replot
!epstopdf 'DATA_PDT3_30000_t1_5_51_511.ps'
set term pop
