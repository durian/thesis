#!/bin/sh
#
# Script to generate gnuplot files from the data file generated
# by do_pre_nnnnn.sh
# Needs the same parameters for TIMBL, LC and RC to work properly.
#
#LSR="noenhanced" #"color"
LSR="enhanced color solid rounded"
#
PREFIX="exp"
if test $# -lt 2
then
  echo "Supply FILE, VAR (and PREFIX)"
  exit
fi
#
PLOT=$1
VAR=$2
ALLDATA=$1
if test $# -eq 3
then
    PREFIX=$3
fi
#
# ${ID}      ${LINES0}
#                 ${LINES1}
#                      KEYPRES T=TOTAL         T0=LETTERCLASS    T1=WORDCLASSIFIER LETT  NUM  SIZE
#                      ${Tkps} ${Tsvd} ${Tpct} ${T0svd} ${T0pct} ${T1svd} ${T1pct} ${DL} ${N} ${DS} ${TSTR0} ${TSTR1}
# PDT3_20000 1000 1000 136754  20784   15.1981 15981    11.6859  4803     3.51215  2     1    1     c2_-a1+D l2_-a1+D
#                                                                                                   14       15
#
#ALGOS=`cut -d' ' -f 8 ${PLOT}  | cut -c6- | sort -u`
#LCS=`cut -d' ' -f 8 ${PLOT}  | cut -c2 | sort -u`
#RCS=`cut -d' ' -f 8 ${PLOT}  | cut -c4 | sort -u`
DLS=`cut -d' ' -f 11 ${PLOT}  | sort -u`   #letter depth
NS=`cut -d' ' -f 12 ${PLOT}  | sort -u`    #N_DS actually one, N is length
DSS=`cut -d' ' -f 13 ${PLOT}  | sort -u`   #of the DS parameter
LETCTXS=`cut -d' ' -f 14 ${PLOT}  | sort -u`
WRDCTXS=`cut -d' ' -f 15 ${PLOT}  | sort -u`
#
VARS='id LINES0 LINES1 Tkps Tsvd Tpct T0svd T0pct T1svd T1pct DL N DS TSTR0 TSTR1'
VAREND=16
echo $VARS
#
IDX=1
for TMP in $VARS
do
    if [[ $VAR == $TMP ]]
    then
	break
    fi
    IDX=$(( $IDX + 1 ))
done 
if test $IDX -eq $VAREND
then
    echo "ERROR, unknown variable ${VAR}"
    exit 1
fi
PREFIX=${PREFIX}_${VAR}
#
echo ${LETCTXS}
echo ${WRDCTXS}
echo ${DLS}
echo ${NS}
echo ${DSS}
#
# First, get the data from all the experiments in their
# own file.
#
#for N in ${NS}
#do
    for DS in ${DSS}
    do
	N=${#DS}
	PRMS=n${N}ds${DS}
	PLOTDATA=${PREFIX}_${PRMS}.data
	echo "Generating ${PLOTDATA}"
	sort -u ${PLOT} | grep " ${N} ${DS} " | egrep -v " 0 0 0 " > ${PLOTDATA}
	#should check if empty here? And delete straight away?
	if [[ ! -s ${PLOTDATA} ]]
	then
	    echo "EMPTY!"
	fi
    done
#done
#
for TIMBL in ${ALGOS}
do
    for LC in ${LCS}
    do
	for RC in ${RCS}
	do
	    TSTR=l${LC}r${RC}_"${TIMBL// /}"
	    PLOTDATA=${PREFIX}_${TSTR}.data
	    echo "Generating ${PLOTDATA}"
	    sort -u ${PLOT} | grep " ${TSTR}\$" | egrep -v " 0 0 0 " > ${PLOTDATA}
	    #should check if empty here? And delete straight away?
	    if [[ ! -s ${PLOTDATA} ]]
	    then
		echo "EMPTY!"
	    fi
	done
    done
done
#
#for N in ${NS}
#do
    for DS in ${DSS}
    do
	N=${#DS}
	PRMS=n${N}ds${DS}
	for TIMBL in ${ALGOS}
	do
	    for LC in ${LCS}
	    do
		for RC in ${RCS}
		do
		    TSTR=l${LC}r${RC}_"${TIMBL// /}"
		    #
		    PLOTDATA=${PREFIX}_${PRMS}_${TSTR}.data
		    echo "Generating ${PLOTDATA}"
		    sort -u ${PLOT} | grep " ${N} ${DS} " | grep " ${TSTR}\$" | egrep -v " 0 0 0 " > ${PLOTDATA}
		    #should check if empty here? And delete straight away?
		    if [[ ! -s ${PLOTDATA} ]]
		    then
			echo "EMPTY!"
		    fi
		done
	    done
	done
    done
#done
#
# ------------------------ End of data generation ----------------------------------
#
# Two ways to plot:
#   1) One algorithm, all context sizes, or
#   2) One context size, over the different algorithms.
# And then there are the different measures to plot...
#
#${ID} ${LINES} ...
#  1     2      3  
#
XR="[1000:10000000]" #NB, some experiments > 1000000
U="using 2:${IDX}"
YR="[]"
if [[ ${VAR:0:2} == "ps" ]]
then
    YR="[0:60]"
fi
#
# ------------
#
# First one... all plots for each ${ALGO}
# (put the ALGOS outside, the rest inside. NB, set PSPLOT and keys!)
#
for TIMBL in ${ALGOS}
do

    TSTR="${TIMBL// /}"
    GNUPLOT=${PREFIX}_${TSTR}.plot
    echo "Generating ${GNUPLOT}"
    echo "# autogen" >${GNUPLOT}
    echo "set title \"${TSTR}\"" >>${GNUPLOT}
    echo "set xlabel \"lines of data\""  >>${GNUPLOT}
    echo "set key bottom"  >>${GNUPLOT}
    echo "set logscale x" >>${GNUPLOT}
    echo "set ylabel \"${VAR}\""  >>${GNUPLOT}
    echo "set grid"  >>${GNUPLOT}	    
    PLOT="plot ${XR}${YR} "

#    for N in ${NS}
#    do
	for DS in ${DSS}
	do
	    N=${#DS}
	    PRMS=n${N}ds${DS}
	    
	    for LC in ${LCS}
	    do
		for RC in ${RCS}
		do
		    PLOTDATA=${PREFIX}_${PRMS}_l${LC}r${RC}_${TSTR}.data
		    echo ${PLOTDATA}
		    if [[ -s ${PLOTDATA} ]]
		    then
			PLOT="${PLOT}\"${PLOTDATA}\" ${U} w lp t \"${PRMS} l${LC}r${RC}\","
		    fi
		done
	    done

	    echo ${PLOT%\,}  >>${GNUPLOT}
	    echo ${PLOT%\,}  >>${GNUPLOT}
	    echo "set terminal push" >>${GNUPLOT}
	    echo "set terminal postscript eps ${LSR} lw 2 'Helvetica' 10" >>${GNUPLOT}
	    PSPLOT=${PREFIX}_${TSTR}.ps
	    echo "set out '${PSPLOT}'" >>${GNUPLOT}
	    echo "replot" >>${GNUPLOT}
	    echo "!epstopdf '${PSPLOT}'" >> ${GNUPLOT}
	    echo "set term pop" >>${GNUPLOT}

	done
#    done
done
#
#  --------------------------------------------------------------------------------------
#
# For each PRMS conbination
#
#for N in ${NS}
#do
    for DS in ${DSS}
    do
	N=${#DS}
	PRMS=n${N}ds${DS}

	GNUPLOT=${PREFIX}_${PRMS}.plot
	echo "Generating ${GNUPLOT}"
	echo "# autogen" >${GNUPLOT}
	echo "set title \"${PRMS}\"" >>${GNUPLOT}
	echo "set xlabel \"lines of data\""  >>${GNUPLOT}
	echo "set key bottom"  >>${GNUPLOT}
	echo "set logscale x" >>${GNUPLOT}
	echo "set ylabel \"${VAR}\""  >>${GNUPLOT}
	echo "set grid"  >>${GNUPLOT}	    
	PLOT="plot ${XR}${YR} "
	
	for TIMBL in ${ALGOS}
	do
	    
	    TSTR="${TIMBL// /}"	    
	    for LC in ${LCS}
	    do
		for RC in ${RCS}
		do
		    PLOTDATA=${PREFIX}_${PRMS}_l${LC}r${RC}_${TSTR}.data
		    echo ${PLOTDATA}
		    if [[ -s ${PLOTDATA} ]]
		    then
			PLOT="${PLOT}\"${PLOTDATA}\" ${U} w lp t \"${TSTR} l${LC}r${RC}\","
		    fi
		done
	    done
	done

	echo ${PLOT%\,}  >>${GNUPLOT}
	echo ${PLOT%\,}  >>${GNUPLOT}
	echo "set terminal push" >>${GNUPLOT}
	echo "set terminal postscript eps ${LSR} lw 2 'Helvetica' 10" >>${GNUPLOT}
	PSPLOT=${PREFIX}_${PRMS}.ps
	echo "set out '${PSPLOT}'" >>${GNUPLOT}
	echo "replot" >>${GNUPLOT}
	echo "!epstopdf '${PSPLOT}'" >> ${GNUPLOT}
	echo "set term pop" >>${GNUPLOT}
	    
    done
#done
#
#
# Per context
#
for LC in ${LCS}
do
    for RC in ${RCS}
    do
	CTX=l${LC}r${RC}

	GNUPLOT=${PREFIX}_${CTX}.plot #PER CONTEXT
	echo "Generating ${GNUPLOT}"
	echo "# autogen" >${GNUPLOT}
	echo "set title \"${CTX}\"" >>${GNUPLOT} #PER CONTEXT
	echo "set xlabel \"lines of data\""  >>${GNUPLOT}
	echo "set key bottom"  >>${GNUPLOT}
	echo "set logscale x" >>${GNUPLOT}
	echo "set ylabel \"${VAR}\""  >>${GNUPLOT}
	echo "set grid"  >>${GNUPLOT}	    
	PLOT="plot ${XR}${YR} "

	for TIMBL in ${ALGOS}
	do
	    TSTR="${TIMBL// /}"
	    for DS in ${DSS}
	    do
		N=${#DS}
		PRMS=n${N}ds${DS}
			    
		PLOTDATA=${PREFIX}_${PRMS}_l${LC}r${RC}_${TSTR}.data
		echo ${PLOTDATA}
		if [[ -s ${PLOTDATA} ]]
		then
		    PLOT="${PLOT}\"${PLOTDATA}\" ${U} w lp t \"${PRMS} ${TSTR}\","
		fi
	    done
	done

	echo ${PLOT%\,}  >>${GNUPLOT}
	echo ${PLOT%\,}  >>${GNUPLOT}
	echo "set terminal push" >>${GNUPLOT}
	echo "set terminal postscript eps ${LSR} lw 2 'Helvetica' 10" >>${GNUPLOT}
	PSPLOT=${PREFIX}_${CTX}.ps #PER CONTEXT
	echo "set out '${PSPLOT}'" >>${GNUPLOT}
	echo "replot" >>${GNUPLOT}
	echo "!epstopdf '${PSPLOT}'" >> ${GNUPLOT}
	echo "set term pop" >>${GNUPLOT}
	
    done
done
