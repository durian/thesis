# Hand made for DATA.PDTT.20000
#
# PDTT20008 10000 1000 136754 53367 39.0241 44097 32.2455 9270 6.7786 a 3 511 c12_-a1+D l2_-a1+D
#
set title "(20000) LTR10000, depth 10, WRD n3ds511, l2_-a1+D"
set xlabel "Lines of data"
set key bottom
set logscale x
set ylabel "Percentage Saved, Combined"
set grid
#
plot [1000:][25:65] "< grep 'a 3 511 c4_-a1+D l2_-a1+D' DATA.PDTT.20000.plot" using 3:6 w lp t "Igtree c4",\
"< grep 'a 3 511 c8_-a1+D l2_-a1+D' DATA.PDTT.20000.plot" using 3:6 w lp t "Igtree c8",\
"< grep 'a 3 511 c12_-a1+D l2_-a1+D' DATA.PDTT.20000.plot" using 3:6 w lp t "Igtree c12",\
"< grep 'a 3 511 c16_-a1+D l2_-a1+D' DATA.PDTT.20000.plot" using 3:6 w lp t "Igtree c16"
#
set terminal push
set terminal postscript eps enhanced color solid rounded lw 2 'Helvetica' 10
set out 'DATA.PDTT.20000.ps'
replot
!epstopdf 'DATA.PDTT.20000.ps'
set term pop
