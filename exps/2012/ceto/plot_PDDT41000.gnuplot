# Hand made for DATA.PDTT.41000
#
#
set title "41000 LTR 100000, depth 5, c8/12 -a4+D, WRD x, l2_-a1+D, n3ds211"
set xlabel "Lines of data"
set key bottom
set logscale x
set ylabel "Percentage Saved"
set grid
#
plot [1000:][:65] "< grep '5 3 211 c12_-a4+D l2_-a1+D' DATA.PDTT.41000.plot" using 3:6 w lp t "CMB c12","< grep '5 3 211 c12_-a4+D l2_-a1+D' DATA.PDTT.41000.plot" using 3:8 w lp t "LTR c12","< grep '5 3 211 c12_-a4+D l2_-a1+D' DATA.PDTT.41000.plot" using 3:10 w lp t "WRD",\
"< grep '5 3 211 c8_-a4+D l2_-a1+D' DATA.PDTT.41000.plot" using 3:6 w lp t "CMB c8","< grep '5 3 211 c8_-a4+D l2_-a1+D' DATA.PDTT.41000.plot" using 3:8 w lp t "LTR c8"
#,"< grep '5 3 511 c8_-a4+D l2_-a1+D' DATA.PDTT.41000.plot" using 3:6 w lp t "CMB c8","< grep '5 3 511 c8_-a4+D l2_-a1+D' DATA.PDTT.41000.plot" using 3:8 w lp t "LTR c8","< grep '5 3 511 c8_-a4+D l2_-a1+D' DATA.PDTT.41000.plot" using 3:10 w lp t "WRD c8"
#
set terminal push
set terminal postscript eps enhanced color solid rounded lw 2 'Helvetica' 10
set out 'DATA.PDTT.41000.ps'
replot
!epstopdf 'DATA.PDTT.41000.ps'
set term pop
