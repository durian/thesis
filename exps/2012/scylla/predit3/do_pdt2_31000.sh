#!/bin/sh
#
TRAINBASE="cgn.4e5"
TESTFILE="cgn.t1000"
#
WOPR="/exp/pberck/wopr/wopr"
SCRIPT="do_pdt2.wopr_script"
#
CYCLE=31000 
LOG="README.PDTT.${CYCLE}.txt"
PLOT="CGNDATA.PDTT.${CYCLE}.plot"
#
# Exps
#
date "+#%Y-%m-%d %H:%M:%S" >> ${LOG}
#
MO=1
TIMBL0=
LC0=
DL=3
LINES0=
#
TIMBL1="-a1 +D"
LC1=2 #l2r0
N=0
DS=
LINES1=
#
for LINES1 in 1000 5000 10000 50000 100000 400000
do
    LINES0=${LINES1}
    # Make the dataset here, clean up afterwards?
    # The TRAINBASE is the same for both!
    #
    TRAINFILE0=${TRAINBASE}.${LINES0}
    if [ -e "${TRAINFILE0}" ]
    then
	echo "Data file ${TRAINFILE0} exists."
    else
	echo "Creating data file ${TRAINFILE0}."
	head -n ${LINES0} ${TRAINBASE} > ${TRAINFILE0}
	echo "head -n ${LINES0} ${TRAINBASE} to ${TRAINFILE0}" >> ${LOG}
    fi
    TRAINFILE1=${TRAINBASE}.${LINES1}
    if [ -e "${TRAINFILE1}" ]
    then
	echo "Data file ${TRAINFILE1} exists."
    else
	echo "Creating data file ${TRAINFILE1}."
	head -n ${LINES1} ${TRAINBASE} > ${TRAINFILE1}
	echo "head -n ${LINES1} ${TRAINBASE} to ${TRAINFILE1}" >> ${LOG}
    fi
    #
    for TIMBL0 in  "-a4 +D"  
    do
	for LC0 in 2 4 8 12 16
	do
	    for DS in 211 221 511 551 555
	    do
		FREE=`df -k . | tail -1 | awk '{print $3}'`
		# 25 577 264 = 25GB
		while [ $FREE -lt 10000000 ]
		do  
		    echo "Space critical, pausing at ${CYCLE}."
		    sleep 60
		    FREE=`df -k . | tail -1 | awk '{print $3}'`
		done
		N=${#DS}
		CYCLESTR=`printf "%05d" ${CYCLE}`
		ID=PDTT${CYCLESTR}
		echo ${ID}
		# if exists pdt file, skip?
		PDTFILE=${TESTFILE}_dl${DL}_n${N}ds${DS}_${ID}.pdt2
		if [ -e "${PDTFILE}" ]
		then
		    echo "${PDTFILE} exists. Skipping."
		else
		echo ${WOPR} -s ${SCRIPT} -p file0:${TRAINFILE0},lc0:${LC0},id:${ID},timbl0:"${TIMBL0}",dl:${DL},test:${TESTFILE},file1:${TRAINFILE1},timbl1:"${TIMBL1}",lc1:${LC1},n:${N},ds:${DS},mo:1
		echo ${WOPR} -s ${SCRIPT} -p file0:${TRAINFILE0},lc0:${LC0},id:${ID},timbl0:"${TIMBL0}",dl:${DL},test:${TESTFILE},file1:${TRAINFILE1},timbl1:"${TIMBL1}",lc1:${LC1},n:${N},ds:${DS},mo:1 >> ${LOG}
		${WOPR} -s ${SCRIPT} -p file0:${TRAINFILE0},lc0:${LC0},id:${ID},timbl0:"${TIMBL0}",dl:${DL},test:${TESTFILE},file1:${TRAINFILE1},timbl1:"${TIMBL1}",lc1:${LC1},n:${N},ds:${DS},mo:1 > output.${ID}
		#
		fi
		CYCLE=$(( $CYCLE + 1 ))
		#
		# gather restuls for plot file
		#
		#nyt.tail1000_n3ds511_PRE10000.pdt
		echo ${PDTFILE}
		# BLOB like: T 380773 19189 5.03949
		BLOB=`tail -n1 ${PDTFILE}`
		echo ${BLOB}
                #http://www.arachnoid.com/linux/shell_programming.html
		Tkps=0
		Tsvd=0
		Tpct=0
                RX='T (.*) (.*) (.*)'
                if [[ "$BLOB" =~ $RX ]]
                then
		    Tkps=${BASH_REMATCH[1]}
                    Tsvd=${BASH_REMATCH[2]}
		    Tpct=${BASH_REMATCH[3]}
                fi
		#
		BLOB=`tail -n2 ${PDTFILE} | head -n1`
		echo ${BLOB}
		T1kps=0
		T1svd=0
		T1pct=0
                RX='T1 (.*) (.*) (.*)'
                if [[ "$BLOB" =~ $RX ]]
                then
		    T1kps=${BASH_REMATCH[1]}
                    T1svd=${BASH_REMATCH[2]}
		    T1pct=${BASH_REMATCH[3]}
                fi
		#
		BLOB=`tail -n3 ${PDTFILE} | head -n1`
		echo ${BLOB}
		T0kps=0
		T0svd=0
		T0pct=0
                RX='T0 (.*) (.*) (.*)'
                if [[ "$BLOB" =~ $RX ]]
                then
		    T0kps=${BASH_REMATCH[1]}
                    T0svd=${BASH_REMATCH[2]}
		    T0pct=${BASH_REMATCH[3]}
                fi
		#
		TSTR0=c${LC0}_"${TIMBL0// /}" # "l" now "c" for characters
		TSTR1=l${LC1}_"${TIMBL1// /}"
		printf -v S "%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s" ${ID} ${LINES0} ${LINES1} ${Tkps} ${Tsvd} ${Tpct} ${T0svd} ${T0pct} ${T1svd} ${T1pct} ${DL} ${N} ${DS} ${TSTR0} ${TSTR1}
		echo ${S} >> ${PLOT}
	    done
	done
    done
done

