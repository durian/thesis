The original predit (PRE) used only word predictions (prediction came at end of
typing, when a space was typed). The pdt2 (PDTT, PDT3) used both a letter and
word predictor.

scylla:
predit, predit2, predit three from SCYLLA
predit3 was plain_text directory in predit2 on SCYLLA

ceto:
From chapters/pdt/snippets.tex:
% Experiments; ceto:/exp2/pberck/prededit2/
%
% 10000: LTR: 10000, dl:3, lc:2,3,4,8,12,16
%        WRD: 1000-1e7, lc:2, n3ds 511 521
%        "learning curve one LTR depth 3"
%
% 20000: LTR: 10000, dl:a, lc:2,3,4,8,12,16
%        WRD: 1000-1e7, lc:2, n3ds 511 521
%        "learning curve two, LTR depth a"
%
% 30000: LTR: 10000, dl:5, lc:2,3,4,8,12,16
%        WRD: 1000-1e7, lc:2, n3ds 211 221 511 521 551
%        "learning curve, LTR 10000, more contexts, LTR depth 5"
%
% 40000: LTR: 50000, dl:5, lc:2,3,4,8,12,16
%        WRD: 1000-1e7, lc:2, n3ds 211 221 511 521 551
%        "Learning curve, LTR 50000 data, LTR depth 5 (like 30000)"
%
% 41000: LTR: 100000, dl:5, lc:2,3,4,8,12,16
%        WRD: 1000-1e7, lc:2, n3ds 211 221 511 521 551
%
% 50000: LTR: 50000, dl:2-a, lc:4,8,12
%        WRD: 1000-1e7, lc:2, n3ds 211 221
%        "LTR 50000, LTR depth over 2-a"
%
% 60000: LTR: 100-1e5, dl:3, lc:2,3,4,8,12,16
%        WRD: 1e6, lc:2, n3ds 211 221
%        "Learning curve over LTR data size, WRD 1e6 data"
%
% 70000: LTR: 50000, dl:5, lc:4,8,12,16
%        WRD: 1000-1e7, lc:2, n4ds 1111
%        "LTR 50000, LTR depth 5, one larger WRD context"
%
% 80000: LTR: 100-1e6, dl:3, lc:4,8,12,16
%        WRD: 100-1e6, lc:2, n3ds 511
%        "Same data LTR/WRD, LTR depth 3, one WRD context."
%
% 90000: LTR context with fixed(ignored) WRD context
%
% NB: the "old" Wopr uses continuous LTR context, pdt2 does not.
%     this can be fixed with wopr_new and -lm:1 to create LTR data
%     which is reset every sentence, like pdt2.
