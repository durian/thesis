#!/bin/sh
#
LMBASE="/exp2/pberck/pbmbmt_wopr/OpenSub-english.train.txt"
WOPR="/exp/pberck/wopr/wopr"
MKDATASCRIPT="wopr_smt_makedata.wopr_script"
LMSCRIPT="wopr_smt.wopr_script" #change to run:mbmt
PBMBMT="./pbmbmt.py"
NC=`which nc`
#
FL="OpenSub-dutch"
TL="OpenSub-english"
PBMBMTDIR="test"
PBMBMTSUF="exp2"
PBMBMTOPTS="-DX"
#
# path: data/test/OpenSub-dutch.exp2/decode.err
#       data/${PBMBMTDIR}/${FL}.${PBMBMTSUF}/decode.err
#
CYCLE=13000
LOG="README.MT.${CYCLE}.txt"
DATA="DATA.MT.${CYCLE}.data"
#
# Run PBMBMT
#
for FILE in ${LMBASE} ${WOPR} ${MKDATASCRIPT} ${LMSCRIPT} ${PBMBMT} ${NC}
do
    if [ ! -e "${FILE}" ]
    then
	echo "${FILE} not found, aborting."
	exit 1
    fi
done
#
#date "+#%Y-%m-%d %H:%M:%S" >> ${LOG}
#
PORT=8888
for LINES in 1000 
do
    #Make the datasets here.
    LMFILE=${LMBASE} #.${LINES} #just the full file
    if [ -e "${LMFILE}" ]
    then
        echo "Data file ${LMFILE} exists."
    else
        echo "Creating data file ${LMFILE}"
        head -n ${LINES} ${LMBASE} > ${LMFILE}
    fi
    #
    for TIMBL in "-a1 +D" "-a4 +D"
    do
        for LC in 2 3 4
        do
            for RC in 0 1 2 3
            do
		for PBMBMTOPTS in "-DX" "-DX -DM0.5" "-DX -DM2"
		do
                FREE=`df -k . | tail -1 | awk '{print $3}'`
                # 25 577 264 = 25GB
                while [ $FREE -lt 10000000 ]
                do  
                    echo "Space critical, pausing at ${CYCLE}."
                    sleep 60
                    FREE=`df -k . | tail -1 | awk '{print $3}'`
                done
		#
                CYCLESTR=`printf "%05d" ${CYCLE}`
                ID=MT${CYCLESTR}
		MTLOG=MTLOG.${ID}
		LMLOG=LMLOG.${ID}
		#
		CYCLE=$(( $CYCLE + 1 ))
		#
		if [ -e "${MTLOG}" ]
		then
		    if [ -s "${MTLOG}" ]
		    then
			echo "ALREADY done, ${ID} exists."
			OL=`grep ONELINE ${MTLOG}`
			OL=${OL#*ONELINER:}
			TSTR=l${LC}r${RC}_"${TIMBL// /}"
			PSTR="${PBMBMTOPTS// /}"
			echo ${ID} ${LMFILE##*/} ${OL} ${TSTR} ${PSTR}
			printf -v S "%s %s %s %s %s" ${ID} ${LMFILE##*/} ${OL} ${TSTR} ${PSTR}
			echo ${S} >> ${DATA}
			continue
		    fi
		fi
		#
		# START LM, including training steps to get 
		# filenames in script. Maybe two script, the
		# first one NOT in background to create data, second
		# one is the server.
		# wopr -s wopr_smt.wopr_script -p filename:rmt.1e5,timbl:"-a1 +D",cs:5000000,lc:2,rc:0
		# Should be in background
     		#
		echo "make data"
		${WOPR} -l -s ${MKDATASCRIPT} -p filename:${LMFILE},timbl:"${TIMBL}",cs:5000000,lc:${LC},rc:${RC},overwrite:1 > ${LMLOG} 2>&1 
		#
		echo "${WOPR} -l -s ${LMSCRIPT} -p filename:${LMFILE},timbl:${TIMBL},cs:5000000,lc:${LC},rc:${RC}"
		nohup ${WOPR} -l -s ${LMSCRIPT} -p filename:${LMFILE},timbl:"${TIMBL}",cs:5000000,lc:${LC},rc:${RC},port:${PORT} >> ${LMLOG} 2>&1 &
		#
		# PBMBMT
		# Wait a little for LM to become ready
		#
		while true;
		do
		    OL=`tail ${LMLOG} | grep Listening`
		    if [ "${OL}" != "" ];
		    then
			break;
		    fi
		    sleep 10
		done
		
		#
		# -Dbranch 1 -DM 0.5:
		# two experiments here with CYCLE++ ? LM is same anyway
		echo ${PBMBMT} -D -S ${PBMBMTOPTS} -DL${PORT} -- ${FL} ${TL} ${PBMBMTDIR} ${PBMBMTSUF}
		time ${PBMBMT} -D -S  ${PBMBMTOPTS} -DL${PORT} -- ${FL} ${TL} ${PBMBMTDIR} ${PBMBMTSUF} > ${MTLOG} 2>&1
		#
		# KILL SERVER
		#
		echo kill `echo "PID" | nc localhost ${PORT}`
		kill `echo "PID" | nc localhost ${PORT}`
		# 
		# Parse scores
		#
		OL=`grep ONELINE ${MTLOG}`
		OL=${OL#*ONELINER:}
		TSTR=l${LC}r${RC}_"${TIMBL// /}"
		PSTR="${PBMBMTOPTS// /}"
		echo ${ID} ${LMFILE##*/} ${OL} ${TSTR} ${PSTR}
		printf -v S "%s %s %s %s %s" ${ID} ${LMFILE##*/} ${OL} ${TSTR} ${PSTR}
                echo ${S} >> ${DATA}
	    done
	done
    done
done
done