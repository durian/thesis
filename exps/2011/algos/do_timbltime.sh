#!/bin/sh
#
CONTEXT="l2r0"
TRAINBASE="europarl.se.txt"
TESTBASE="europarl.se.txt.l1000"
TIMBL="Timbl"
LOG="README.timbltime.txt"
#
#
date "+#%Y-%m-%d %H:%M:%S" >> ${LOG}
#
for LINES in 1000 2000 3000 4000 5000 6000 7000 8000 9000 10000 20000 30000 40000 50000 60000 70000 80000 90000 100000 200000 300000 400000 500000 600000 700000 800000 900000 1000000
do
    #Make the dataset here, clean up afterwards?
    TRAINFILE=${TRAINBASE}.${LINES}.${CONTEXT}
    TESTFILE=${TESTBASE}.${CONTEXT}
    for ALGO in "-a1 +D" "-a4 +D"
    do
	echo "${TIMBL} -f ${TRAINFILE} ${ALGO} -t ${TESTFILE}"
	${TIMBL} -f ${TRAINFILE} ${ALGO} -t ${TESTFILE} >> ${LOG}
    done
done

