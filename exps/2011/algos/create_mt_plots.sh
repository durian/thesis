#!/bin/sh
#
# Script to generate gnuplot files from the data file generated
# by do_pbmbmt...
#
#LSR="noenhanced" #"color"
LSR="enhanced color solid rounded"
#
PREFIX="mt"
if test $# -lt 2
then
  echo "Supply FILE, VAR (and PREFIX)"
  exit
fi
#
PLOT=$1
VAR=$2
ALLDATA=$1
if test $# -eq 3
then
    PREFIX=$3
fi
#
# AL or SC
#TYP=`cut -c1-2 ${PLOT}`
TYP=${PLOT:5:2}
#
#MT24062 OpenSub-english.train.txt 286160 exp1_D0,25K1,0M1O5T1Xa3,3b1,1d7f10,5oexp1 0.2777 0.5059429 5.546 0.563 55.6241 49.0616 l4 -DX-DO5-DT1-DD0.25-DM1-Db1,1-Df10,5-Da3,3-DK1.0-Dd7
#
if [ "$TYP" == "MT" -o "$TYP" == "" ]
then
    MTOPTS=`cut -d' ' -f 4 ${PLOT} | sort -u` #options in PBMBMTOPTS in script (4 or 12)
    ALGOS=`cut -d' ' -f 11 ${PLOT}  | cut -c6- | sort -u` #only for wopr, not srilm
    LCS=`cut -d' ' -f 11 ${PLOT}  | cut -c2 | sort -u`
    RCS=`cut -d' ' -f 11 ${PLOT}  | cut -c4 | sort -u` #always 0 for srilm
    VARS='id LMBASE LINES mtdir BLEU MTR NIST PER WER TER ORDR mtops'
    VAREND=13
fi
echo $VARS
echo $ALGOS
echo $MTOPTS
#
IDX=1
for TMP in $VARS
do
    if [[ $VAR == $TMP ]]
    then
	break
    fi
    IDX=$(( $IDX + 1 ))
done 
if test $IDX -eq $VAREND
then
    echo "ERROR, unknown variable ${VAR}"
    exit 1
fi
PREFIX=${PREFIX}_${VAR}
#
# First, get the data from all the experiments in their
# own file.
#
for MTOPT in ${MTOPTS}
do
    for TIMBL in ${ALGOS}
    do
	for LC in ${LCS}
	do
	    for RC in ${RCS}
	    do
		if test ${LC} -eq "0" -a ${RC} -eq "0"
		then
		    echo "Skipping l${LC}r${RC}"
		    continue
		fi
		TSTR=l${LC}r${RC}_"${TIMBL// /}"_${MTOPT}
		PLOTDATA=${PREFIX}_${TSTR}.data
		TGREP=l${LC}r${RC}_"${TIMBL// /}"
		echo "Generating ${PLOTDATA}"
		#echo "sort -u ${PLOT} | grep  ${TGREP}  | grep ${MTOPT}"
		sort -u ${PLOT} | grep " ${TGREP} " | grep "${MTOPT}" | egrep -v " 0 0 0 " > ${PLOTDATA}
	    done
	done
    done
done
#
# Two ways to plot:
#   1) One algorithm, all context sizes, or
#   2) One context size, over the different algorithms.
# And then there are the different measures to plot...
#
XR="[1000:]"
U="using 3:${IDX}"
YR="[0.2:0.3]"
if [[ ${VAR:0:2} == "WE" ]]
then
    YR="[0:100]"
fi
#
# Op MTOPT niveau
#
for MTOPT in ${MTOPTS}
do
    GNUPLOT=${PREFIX}_${MTOPT}.plot
    echo "Creating PLOT: ${GNUPLOT}"

    echo "# autogen" >${GNUPLOT}
    echo "set title \"${MTOPT}\"" >>${GNUPLOT}
    echo "set xlabel \"lines of data\""  >>${GNUPLOT}
    echo "set key bottom"  >>${GNUPLOT}
    echo "set logscale x" >>${GNUPLOT}
    echo "set ylabel \"${VAR}\""  >>${GNUPLOT}
    echo "set grid"  >>${GNUPLOT}
    PLOT="plot ${XR}${YR} "

    for TIMBL in ${ALGOS}
    do
	for LC in ${LCS}
	do
	    for RC in ${RCS}
	    do
		if test ${LC} -eq "0" -a ${RC} -eq "0"
		then
		    echo "Skipping l${LC}r${RC}"
		    continue
		fi
		#LBL without MTOPT, because that is the title, all in the
		# plot have same MTOPT. The datafile needs the full name.
		LBL=l${LC}r${RC}_"${TIMBL// /}"
		PLOTDATA=${PREFIX}_l${LC}r${RC}_"${TIMBL// /}"_${MTOPT}.data
		echo "  --> $PLOTDATA"
		if [[ -s ${PLOTDATA} ]]
		then
		    PLOT="${PLOT}\"${PLOTDATA}\" ${U} w lp t \"${LBL}\","
		else
		    echo "EMPTY!"
		fi
	    done
	done
    done

    echo ${PLOT%\,}  >>${GNUPLOT}
    echo ${PLOT%\,}  >>${GNUPLOT}
    echo "set terminal push" >>${GNUPLOT}
    echo "set terminal postscript eps ${LSR} lw 2 'Helvetica' 10" >>${GNUPLOT}
    PSPLOT=${PREFIX}_${MTOPT}.ps
    echo "set out '${PSPLOT}'" >>${GNUPLOT}
    echo "replot" >>${GNUPLOT}
    echo "!epstopdf '${PSPLOT}'" >> ${GNUPLOT}
    echo "set term pop" >>${GNUPLOT}

done
#
# Op TIMBL niveau
#
echo "--------------------------------------------"
#
for TIMBL in ${ALGOS}
do
    GNUPLOT=${PREFIX}_${TIMBL// /}.plot
    echo "Creating PLOT: ${GNUPLOT}"

    echo "# autogen" >${GNUPLOT}
    echo "set title \"${TIMBL// /}\"" >>${GNUPLOT}
    echo "set xlabel \"lines of data\""  >>${GNUPLOT}
    echo "set key bottom"  >>${GNUPLOT}
    echo "set logscale x" >>${GNUPLOT}
    echo "set ylabel \"${VAR}\""  >>${GNUPLOT}
    echo "set grid"  >>${GNUPLOT}
    PLOT="plot ${XR}${YR} "

    for MTOPT in ${MTOPTS}
    do
	for LC in ${LCS}
	do
	    for RC in ${RCS}
	    do
		if test ${LC} -eq "0" -a ${RC} -eq "0"
		then
		    echo "Skipping l${LC}r${RC}"
		    continue
		fi
		LBL=l${LC}r${RC}_"${TIMBL// /}"_${MTOPT}
		PLOTDATA=${PREFIX}_${LBL}.data
		echo "  --> $PLOTDATA"
		#
		#LBL without TIMBL, because that is the title, all in the
		# plot have same TIMBL. The datafile needs the full name.
		LBL=l${LC}r${RC}_${MTOPT}
		PLOTDATA=${PREFIX}_l${LC}r${RC}_"${TIMBL// /}"_${MTOPT}.data
		echo "  --> $PLOTDATA"
		if [[ -s ${PLOTDATA} ]]
		then
		    PLOT="${PLOT}\"${PLOTDATA}\" ${U} w lp t \"${LBL}\","
		else
		    echo "EMPTY!"
		fi
	    done
	done
    done

    echo ${PLOT%\,}  >>${GNUPLOT}
    echo ${PLOT%\,}  >>${GNUPLOT}
    echo "set terminal push" >>${GNUPLOT}
    echo "set terminal postscript eps ${LSR} lw 2 'Helvetica' 10" >>${GNUPLOT}
    PSPLOT=${PREFIX}_${TIMBL// /}.ps
    echo "set out '${PSPLOT}'" >>${GNUPLOT}
    echo "replot" >>${GNUPLOT}
    echo "!epstopdf '${PSPLOT}'" >> ${GNUPLOT}
    echo "set term pop" >>${GNUPLOT}

done


exit
#
#
# First one, plot file for each algorithm, to compare
# scores for different contexts.
#
for TIMBL in ${ALGOS}
do
    TSTR="${TIMBL// /}"
    GNUPLOT=${PREFIX}_${TSTR}.plot
    echo "Generating ${GNUPLOT}"
    echo "# autogen" >${GNUPLOT}

    echo "set title \"${TSTR}\"" >>${GNUPLOT}
    echo "set xlabel \"lines of data\""  >>${GNUPLOT}
    echo "set key bottom"  >>${GNUPLOT}
    echo "set logscale x" >>${GNUPLOT}
    echo "set ylabel \"${VAR}\""  >>${GNUPLOT}
    echo "set grid"  >>${GNUPLOT}
    #set xtics (100,1000,10000,20000,100000) 

    PLOT="plot ${XR}${YR} "
    for LC in ${LCS}
    do
	for RC in ${RCS}
	do
	    if test ${LC} -eq "0" -a ${RC} -eq "0"
	    then
		echo "Skipping l${LC}r${RC}"
		continue
	    fi

	    LBL=l${LC}r${RC}_"${TIMBL// /}"_${MTOPT}
	    PLOTDATA=${PREFIX}_${LBL}.data

	    PLOTDATA=${PREFIX}_l${LC}r${RC}_${TSTR}.data
	    #echo "${PLOTDATA} using 2:10 w lp"
	    # Add if it exists.
	    if [[ -s ${PLOTDATA} ]]
	    then
		PLOT="${PLOT}\"${PLOTDATA}\" ${U} w lp t \"l${LC}r${RC}\","
	    fi
	done
    done
    echo ${PLOT%\,}  >>${GNUPLOT}
    echo ${PLOT%\,}  >>${GNUPLOT}
    echo "set terminal push" >>${GNUPLOT}
    echo "set terminal postscript eps ${LSR} lw 2 'Helvetica' 10" >>${GNUPLOT}
    PSPLOT=${PREFIX}_${TSTR}.ps
    echo "set out '${PSPLOT}'" >>${GNUPLOT}
    echo "replot" >>${GNUPLOT}
    echo "!epstopdf '${PSPLOT}'" >> ${GNUPLOT}
    echo "set term pop" >>${GNUPLOT}
    #echo ${PLOT:(-1)}
done
#
# Version 2
#
for LC in ${LCS}
do
    for RC in ${RCS}
    do
	if test ${LC} -eq "0" -a ${RC} -eq "0"
	then
	    echo "Skipping l${LC}r${RC}"
	    continue
	fi
	GNUPLOT=${PREFIX}_l${LC}r${RC}.plot
	echo "Generating ${GNUPLOT}"
	echo "# autogen" >${GNUPLOT}
	
	echo "set title \"l${LC}r${RC}\"" >>${GNUPLOT}
	echo "set xlabel \"lines of data\""  >>${GNUPLOT}
	echo "set key bottom"  >>${GNUPLOT}
	echo "set logscale x" >>${GNUPLOT}
	echo "set ylabel \"${VAR}\""  >>${GNUPLOT}
	echo "set grid"  >>${GNUPLOT}
	#set xtics (100,1000,10000,20000,100000) 
	
	PLOT="plot ${XR}${YR} "

	for TIMBL in ${ALGOS}
	do
	    TSTR="${TIMBL// /}"
	    PLOTDATA=${PREFIX}_l${LC}r${RC}_${TSTR}.data
	    # Add if it exists
	    if [[ -s ${PLOTDATA} ]]
	    then
		PLOT="${PLOT}\"${PLOTDATA}\" ${U} w lp t \"${TSTR}\","
	    fi
	done
	echo ${PLOT%\,}  >>${GNUPLOT}
	echo "set terminal push" >>${GNUPLOT}
	echo "set terminal postscript eps ${LSR} lw 2 'Helvetica' 10" >>${GNUPLOT}
	PSPLOT=${PREFIX}_l${LC}r${RC}.ps
	echo "set out '${PSPLOT}'" >>${GNUPLOT}
	echo "replot" >>${GNUPLOT}
	echo "!epstopdf '${PSPLOT}'" >> ${GNUPLOT}
	echo "set term pop" >>${GNUPLOT}
    done
done
#
#set terminal push
#set terminal postscript eps color lw 2 "Helvetica" 10
#set out 'l2r0_-a1.ps'
#replot
#set term pop
