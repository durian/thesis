#!/bin/sh
#
# Script to generate gnuplot files from the data file generated
# by do_pbmbmt... srilm output only
#
#LSR="noenhanced" #"color"
LSR="enhanced color solid rounded"
#
PREFIX="mt"
if test $# -lt 2
then
  echo "Supply FILE, VAR (and PREFIX)"
  exit
fi
#
PLOT=$1
VAR=$2
ALLDATA=$1
if test $# -eq 3
then
    PREFIX=$3
fi
#
# AL or SC
#TYP=`cut -c1-2 ${PLOT}`
TYP=${PLOT:5:2}
#
#MT24062 OpenSub-english.train.txt 286160 exp1_D0,25K1,0M1O5T1Xa3,3b1,1d7f10,5oexp1 0.2777 0.5059429 5.546 0.563 55.6241 49.0616 l4 -DX-DO5-DT1-DD0.25-DM1-Db1,1-Df10,5-Da3,3-DK1.0-Dd7
#
if [ "$TYP" == "MT" -o "$TYP" == "" ]
then
    ALGOS="srilm"
    MTOPTS=`cut -d' ' -f 4 ${PLOT} | sort -u` #options in PBMBMTOPTS in script (4 or 12)
    LCS=`cut -d' ' -f 11 ${PLOT}  | cut -c2 | sort -u` #the order of the LM
    RCS="0"
    VARS='id LMBASE LINES mtdir BLEU MTR NIST PER WER TER ORDR mtops'
    VAREND=13
fi
echo $VARS
echo $MTOPTS
#
IDX=1
for TMP in $VARS
do
    if [[ $VAR == $TMP ]]
    then
	break
    fi
    IDX=$(( $IDX + 1 ))
done 
if test $IDX -eq $VAREND
then
    echo "ERROR, unknown variable ${VAR}"
    exit 1
fi
echo "Index: ${IDX}"
#
PREFIX=${PREFIX}_${VAR}
#
echo ${ALGOS}
echo ${LCS}
#
# First, get the data from all the experiments in their
# own file. Seperate per LC and MTOPTS.
#
for TIMBL in ${MTOPTS}
do
    echo "CREATING: ${TIMBL}"
    for LC in ${LCS}
    do
	TSTR=l${LC}_"${TIMBL// /}"
	PLOTDATA=${PREFIX}_${TSTR}.data
	echo "Generating ${PLOTDATA}"
	echo sort -u ${PLOT}  grep " l${LC} " grep " ${TIMBL} " egrep -v " 0 0 0 "
	sort -u ${PLOT} | grep " l${LC} " | grep " ${TIMBL} " | egrep -v " 0 0 0 " > ${PLOTDATA}
	    # should check if empty here? And delete straight away?
    done
done
#
XR="[1000:]"
#${ID} .. ${LINES}
#  1    2 3       
#
U="using 3:${IDX}"
YR="[0.2:0.3]"
if [[ ${VAR:0:3} == "WER" ]]
then
    YR="[0:100]"
fi
#
for TIMBL in ${MTOPTS}
do
    TSTR=${TIMBL}
    GNUPLOT=${PREFIX}_${TSTR}.plot
    PSPLOT=${PREFIX}_${TSTR}.ps
    echo "Generating ${GNUPLOT}"
    echo "# autogen" >${GNUPLOT}

    LBL=${TSTR//_/.}
    echo "set title \"${LBL}\"" >>${GNUPLOT}
    echo "set xlabel \"lines of data\""  >>${GNUPLOT}
    echo "set key bottom"  >>${GNUPLOT}
    echo "set logscale x" >>${GNUPLOT}
    echo "set ylabel \"${VAR}\""  >>${GNUPLOT}
    echo "set grid"  >>${GNUPLOT}
    #set xtics (100,1000,10000,20000,100000) 

    PLOT="plot ${XR}${YR} "
    for LC in ${LCS}    #combine all the LCs with this MTOPT in one plot
    do
	TSTR=l${LC}_"${TIMBL// /}"
	PLOTDATA=${PREFIX}_${TSTR}.data
	    #echo "${PLOTDATA} using 2:10 w lp"
	    # Add if it exists.
	    if [[ -s ${PLOTDATA} ]]
	    then
		PLOT="${PLOT}\"${PLOTDATA}\" ${U} w lp t \"l${LC}\","
	    fi
	done
    echo ${PLOT%\,}  >>${GNUPLOT}
    echo ${PLOT%\,}  >>${GNUPLOT}
    echo "set terminal push" >>${GNUPLOT}
    echo "set terminal postscript eps ${LSR} lw 2 'Helvetica' 10" >>${GNUPLOT}
    #PSPLOT=${PREFIX}_${TSTR}.ps
    echo "set out '${PSPLOT}'" >>${GNUPLOT}
    echo "replot" >>${GNUPLOT}
    echo "!epstopdf '${PSPLOT}'" >> ${GNUPLOT}
    echo "set term pop" >>${GNUPLOT}
    #echo ${PLOT:(-1)}
done
#
# Per order of LM, with all the algos (the previous one with
# the inner loop now outer, and vice versa.
#
for LC in ${LCS}    #combine all the LCs with this MTOPT in one plot
do
    TSTR=l${LC}
    echo ${TSTR}
    GNUPLOT=${PREFIX}_${TSTR}.plot
    PSPLOT=${PREFIX}_${TSTR}.ps
    echo "Generating ${GNUPLOT}"
    echo "# autogen" >${GNUPLOT}

    echo "set title \"${TSTR}\"" >>${GNUPLOT}
    echo "set xlabel \"lines of data\""  >>${GNUPLOT}
    echo "set key bottom"  >>${GNUPLOT}
    echo "set logscale x" >>${GNUPLOT}
    echo "set ylabel \"${VAR}\""  >>${GNUPLOT}
    echo "set grid"  >>${GNUPLOT}
    #set xtics (100,1000,10000,20000,100000) 

    PLOT="plot ${XR}${YR} "
    for TIMBL in ${MTOPTS}
    do
	TSTR=l${LC}_"${TIMBL// /}"
	PLOTDATA=${PREFIX}_${TSTR}.data
	    #echo "${PLOTDATA} using 2:10 w lp"
	    # Add if it exists.
	    if [[ -s ${PLOTDATA} ]]
	    then
		LBL=${TIMBL//_/.}
		PLOT="${PLOT}\"${PLOTDATA}\" ${U} w lp t \"${LBL}\","
	    fi
	done
    echo ${PLOT%\,}  >>${GNUPLOT}
    echo ${PLOT%\,}  >>${GNUPLOT}
    echo "set terminal push" >>${GNUPLOT}
    echo "set terminal postscript eps ${LSR} lw 2 'Helvetica' 10" >>${GNUPLOT}
    #PSPLOT=${PREFIX}_${TSTR}.ps
    echo "set out '${PSPLOT}'" >>${GNUPLOT}
    echo "replot" >>${GNUPLOT}
    echo "!epstopdf '${PSPLOT}'" >> ${GNUPLOT}
    echo "set term pop" >>${GNUPLOT}
    #echo ${PLOT:(-1)}
done
