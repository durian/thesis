#!/bin/sh
#
# File for NYT
#
#
TRAINBASE="/exp2/pberck/nyt.3e7"
TESTFILE="nyt.tail1000"
SRILM="/exp/pberck/srilm-1.5.8/bin/i686"
SCORES="DATA.nyt.srilm.plot"
#
CYCLE=1000
#
#for LINES in 1000 
for LINES in 1000 2000 3000 4000 5000 6000 7000 8000 9000 10000 20000 30000 40000 50000 60000 70000 80000 90000 100000 200000 300000 400000 500000 600000 700000 800000 900000 1000000 2000000 3000000 4000000 5000000 6000000 7000000 8000000 9000000 10000000 20000000 30000000 40000000 50000000 60000000 70000000 80000000 90000000 100000000 200000000 300000000
do
    #NEWLINES=$(echo "scale=0; $LINES/23" | bc)
    NEWLINES=${LINES}
    CYCLESTR=`printf "%03d" ${CYCLE}`
    ID=SRI${CYCLESTR}
    TRAINFILE=nyt.srilm.${NEWLINES}
    LMFILE=nyt.srilm.${NEWLINES}.lm

    if [ -e "${TRAINFILE}" ]
    then
	echo "Data file ${TRAINFILE} exists."
    else
	echo head -n ${NEWLINES} ${TRAINBASE} to ${TRAINFILE}
	head -n ${NEWLINES}  ${TRAINBASE} > ${TRAINFILE}
    fi

    OUTFILE=${TRAINFILE}.${ID}

    if [ -e "${LMFILE}" ]
    then
	echo "${LMFILE} file exists."
    else
	echo ${SRILM}/ngram-count -text ${TRAINFILE} -lm ${LMFILE}
	${SRILM}/ngram-count -text ${TRAINFILE} -lm ${LMFILE}
	echo ${SRILM}/ngram -lm ${LMFILE} -ppl ${TESTFILE} to ${OUTFILE}
	${SRILM}/ngram -lm ${LMFILE} -ppl ${TESTFILE} > ${OUTFILE}
    fi

    # Find scores
    pplx_str=`tail ${OUTFILE}`
    echo $pplx_str
    #0 zeroprobs, logprob= -22530.9 ppl= 91.3214 ppl1= 140.422
    #
    regex='.* ppl= (.*) ppl1= (.*).*'
    if [[ "$pplx_str" =~ $regex ]]
    then
	pplx=${BASH_REMATCH[1]}
	pplx1=${BASH_REMATCH[2]}
    else
	pplx=0
	pplx1=0
	echo "No match"
    fi

    echo $OUTFILE ${LINES} $pplx $pplx1 >> ${SCORES} 

    echo rm ${LMFILE} ${TRAINFILE}
    rm ${LMFILE} ${TRAINFILE}

    CYCLE=$(( $CYCLE + 1 ))
done


