#!/bin/sh
#
LMBASE="/exp2/pberck/pbmbmt_wopr/OpenSub-english.train.txt"
WOPR="/exp/pberck/wopr/wopr"
MKDATASCRIPT="wopr_smt_makedata.wopr_script"
LMSCRIPT="wopr_smt.wopr_script"
PBMBMT="./pbmbmt.py"
NC=`which nc`
#
FL="OpenSub-dutch"
TL="OpenSub-english"
PBMBMTDIR="test"
PBMBMTSUF="exp1"
#
# path: data/test/OpenSub-dutch.exp2/decode.err
#       data/${PBMBMTDIR}/${FL}.${PBMBMTSUF}/decode.err
#
CYCLE=11000
LOG="README.MT.${CYCLE}.txt"
DATA="DATA.MT.${CYCLE}.data"
#
# Run PBMBMT
#
for FILE in ${LMBASE} ${WOPR} ${MKDATASCRIPT} ${LMSCRIPT} ${PBMBMT} ${NC}
do
    if [ ! -e "${FILE}" ]
    then
	echo "${FILE} not found, aborting."
	exit 1
    fi
done
#
#date "+#%Y-%m-%d %H:%M:%S" >> ${LOG}
#
PORT=8899
for LINES in 1000  #ignored
do
    #Make the datasets here.
    LMFILE=${LMBASE} #.${LINES} #just the full file
    if [ -e "${LMFILE}" ]
    then
        echo "Data file ${LMFILE} exists."
    else
        echo "Creating data file ${LMFILE}"
        head -n ${LINES} ${LMBASE} > ${LMFILE}
    fi
    #
    for TIMBL in "-a1 +D" "-a4 +D"
    do
        for LC in 2 
        do
            for RC in 0 
            do
		for M in 0.1 0.25 0.5 1 2 4 10 #PBMBMT weight of LM
		do
                    FREE=`df -k . | tail -1 | awk '{print $3}'`
                    # 25 577 264 = 25GB
                    while [ $FREE -lt 10000000 ]
                    do  
			echo "Space critical, pausing at ${CYCLE}."
			sleep 60
			FREE=`df -k . | tail -1 | awk '{print $3}'`
                    done
		    #
                    CYCLESTR=`printf "%05d" ${CYCLE}`
                    ID=MT${CYCLESTR}
		    echo "STARTING: ${ID}"
		    MTLOG=MTLOG.${ID}
		    LMLOG=LMLOG.${ID}
		    #
		    CYCLE=$(( $CYCLE + 1 ))
		    #
		    if [ ! -e "${MTLOG}" ]
		    then
		#
		# START LM, including training steps to get 
		# filenames in script. Maybe two script, the
		# first one NOT in background to create data, second
		# one is the server.
		# wopr -s wopr_smt.wopr_script -p filename:rmt.1e5,timbl:"-a1 +D",cs:5000000,lc:2,rc:0
		# Should be in background
     		#
			echo "make data"
			${WOPR} -l -s ${MKDATASCRIPT} -p filename:${LMFILE},timbl:"${TIMBL}",cs:5000000,lc:${LC},rc:${RC} > ${LMLOG} 2>&1 
		#
			echo "${WOPR} -l -s ${LMSCRIPT} -p filename:${LMFILE},timbl:${TIMBL},cs:5000000,lc:${LC},rc:${RC}"
			nohup ${WOPR} -l -s ${LMSCRIPT} -p filename:${LMFILE},timbl:"${TIMBL}",cs:5000000,lc:${LC},rc:${RC},port:${PORT} >> ${LMLOG} 2>&1 &
		#
		# PBMBMT
		# Wait a little for LM to become ready
		#
			sleep 30
		#
		# -Dbranch 1 -DM 0.5:
			echo ${PBMBMT} -D -S -DM ${M} -DL${PORT} -- ${FL} ${TL} ${PBMBMTDIR} ${PBMBMTSUF}
			${PBMBMT} -D -S -DM ${M} -DL${PORT} -- ${FL} ${TL} ${PBMBMTDIR} ${PBMBMTSUF} > ${MTLOG} 2>&1
		#
		# KILL SERVER
		#
			echo kill `echo "PID" | nc localhost ${PORT}`
			kill `echo "PID" | nc localhost ${PORT}`
		    fi
		# 
		# Parse scores
		#
		    OL=`grep ONELINE ${MTLOG}`
		    OL=${OL#*ONELINER:}
		    TSTR=l${LC}r${RC}_"${TIMBL// /}"
		    echo ${ID} ${LMFILE##*/} ${OL} ${TSTR} ${M}
		    printf -v S "%s %s %s %s %s" ${ID} ${LMFILE##*/} ${OL} ${TSTR} ${M}
                    echo ${S} >> ${DATA}
		done
	    done
	done
    done
done
