#!/bin/sh
#
TRAINBASE="/exp2/pberck/nyt.3e7"
TESTFILE="nyt.tail1000"
WOPR="/exp/pberck/wopr/wopr"
SCRIPT="do_algos.wopr_script"
PXSCRIPT="/exp/pberck/wopr/etc/pplx_px.pl"
LOG="README.ALG.nyt.txt"
PLOT="DATA.ALG.nyt.plot"
#
CYCLE=5000
#
date "+#%Y-%m-%d %H:%M:%S" >> ${LOG}
#
for LINES in 1000 2000 3000 4000 5000 6000 7000 8000 9000 10000 20000 30000 40000 50000 60000 70000 80000 90000 100000 200000 300000 400000 500000 600000 700000 800000 900000 1000000
do
    for TIMBL in "-a1 +D" "-a4 +D" "-a4 +D -w0" "-a4 +D -w0 -dIL" "-a4 +D -k2" "-a4 +D -w0 -k2" "-a4 +D -w0 -dIL -k2" "-a4 +D -k3" "-a4 +D -w0 -k3" "-a4 +D -w0 -dIL -k3" 
    do
	for LC in 2 3
	do
	    for RC in 0 1 2 3
	    do
		CYCLESTR=`printf "%04d" ${CYCLE}`
		ID=ALG${CYCLESTR}
		CYCLE=$(( $CYCLE + 1 ))
		PXFILE=${TESTFILE}.l${LC}r${RC}_${ID}.px
		echo ${PXFILE}
		if [ ! -e "${PXFILE}" ]
		then
		    continue
		fi
		adc=0
		ads=0
		cg=0
		cd=0
		ic=0
		mrr_cd=0
		mrr_cg=0
		mrr_gd=0
		pplx=0
		pplx1=0
		#change to incorporate average over dist.
		# 0.96698110  -0.0484  -0.0146  4 1 [00110] .
		# 00110:  5137 ( 22.50%)
		#
		BLOB=`perl ${PXSCRIPT} -f ${PXFILE} -l ${LC} -r ${RC} | tail -n18`
		#http://www.arachnoid.com/linux/shell_programming.html

		# dist_freq sum: 2266012, ave: ( 99.26)
		# dist_sum sum: 44968874, ave: (1969.81)
		STR=${BLOB#*dist_freq sum}
		RX=': ([0123456789]*)\, ave: \(([0123456789. ]*)\)'
		if [[ "$STR" =~ $RX ]]
		then
		    adc=${BASH_REMATCH[2]}
		fi
		#
		STR=${BLOB#*dist_sum sum}
		RX=': (.*)\, ave: \(([0123456789. ]*)\)'
		if [[ "$STR" =~ $RX ]]
		then
		    ads=${BASH_REMATCH[2]}
		fi
		#
		STR=${BLOB#*Column:  2}
		RX='.* \((.*)%\).*3.* \((.*)%\).*4.* \((.*)%\).*'
		if [[ "$STR" =~ $RX ]]
		then
		    cg=${BASH_REMATCH[1]}
		    cd=${BASH_REMATCH[2]}
		    ic=${BASH_REMATCH[3]}
		fi
		#echo ${BLOB}
		STR=${BLOB#*Wopr ppl}
		RX=': (.*) Wopr ppl1: (.*) \(.*'
		#
		STR=${BLOB#*Wopr ppl}
		RX=': (.*) Wopr ppl1: (.*) \(.*'
		if [[ "${STR}" =~ $RX ]]
		then
		    pplx=${BASH_REMATCH[1]}
		    pplx1=${BASH_REMATCH[2]}
		fi
		#
		STR=${BLOB#*RR(cd)}
		#echo $STR
		RX='.* MRR: (.*).*RR\(cg\).* MRR: (.*).*RR\(gd\).* MRR: (.*)'
		if [[ "${STR}" =~ $RX ]]
		then
		    mrr_cd=${BASH_REMATCH[1]}
		    mrr_cg=${BASH_REMATCH[2]}
		    mrr_gd=${BASH_REMATCH[3]}
		fi
		TSTR=l${LC}r${RC}_"${TIMBL// /}"
		printf -v S "%s %s %s %s %s %s %s %s %s %s %s %s %s" ${ID} ${LINES} ${cg} ${cd} ${ic} ${pplx} ${pplx1} ${mrr_cd} ${mrr_cg} ${mrr_gd} ${adc} ${ads} ${TSTR}
		echo ${S} #>> ${PLOT}
	    done
	done
    done
done

