#!/bin/sh
#
TRAINBASE="/exp2/pberck/nyt.3e7"
ORIGTESTFILE="nyt.tail1000"
TESTFILE="nyt.tail1000sc"
#
WOPR="/exp/pberck/wopr/wopr"
SCRIPT="do_spelcorr.wopr_script"
SCSCRIPT="/exp/pberck/wopr/etc/check_spelerr.pl"
#
CYCLE=10000
LOG="README.SC.${CYCLE}.txt"
PLOT="DATA.SC.${CYCLE}.plot"
#
# Do spelling correction with known instance bases.
#
if [ ! -e "${ORIGTESTFILE}" ]
then
    echo "${ORIGTESTFILE} nor found, aborting."
    exit 1
fi
if [ ! -e "${TESTFILE}" ]
then
    echo "${TESTFILE} nor found, aborting."
    exit 1
fi
#
date "+#%Y-%m-%d %H:%M:%S" >> ${LOG}
#
for LINES in 1000 5000 10000 50000 100000 500000 1000000
do
    #Make the datasets here.
    TRAINFILE=${TRAINBASE}.${LINES}
    if [ -e "${TRAINFILE}" ]
    then
	echo "Data file ${TRAINFILE} exists."
    else
	echo "Creating data file ${TRAINFILE}."
	head -n ${LINES} ${TRAINBASE} > ${TRAINFILE}
    fi
    for TIMBL in "-a1 +D" "-a4 +D"
    do
	for LC in 2 3 4
	do
	    for RC in 0 1 2 3
	    do
		FREE=`df -k . | tail -1 | awk '{print $3}'`
		# 25 577 264 = 25GB
		if [ $FREE -lt 10000000 ]; then  #10 GB
		    echo "Space critical, exiting."
		    exit 1
		fi
		CYCLESTR=`printf "%05d" ${CYCLE}`
		ID=SC${CYCLESTR}
		echo ${ID}
		# if exists PXFILE=${TESTFILE}.l${LC}r${RC}_${ID}.sc, skip?
		SCFILE=${TESTFILE}.l${LC}r${RC}_${ID}.sc
		OUT=output.${ID}
		if [ -e "${SCFILE}" ]
		then
		    echo "${SCFILE} exists. Skipping."
		else
		    echo "#${ID},${TRAINFILE},timbl:'${TIMBL}',lc:${LC},rc:${RC}" >> ${LOG}
		    echo ${WOPR} -s ${SCRIPT} -p trainfile:${TRAINFILE},rc:${RC},lc:${LC},id:${ID},timbl:${TIMBL},testfile:${TESTFILE}
		    ${WOPR} -s ${SCRIPT} -p trainset:${TRAINFILE},rc:${RC},lc:${LC},id:${ID},timbl:"${TIMBL}",testset:${TESTFILE} >${OUT}
		fi
		CYCLE=$(( $CYCLE + 1 ))
		#
		#reuters.martin.tok.1000.l3r2_ALG023.sc
		echo "perl ${SCSCRIPT} -s ${SCFILE} -o ${ORIGTESTFILE}.l${LC}r${RC}" >> ${OUT}
		BLOB=`perl ${SCSCRIPT} -s ${SCFILE} -o ${ORIGTESTFILE}.l${LC}r${RC}`
		#http://wiki.bash-hackers.org/commands/builtin/printf
		#printf -v S "%s %s" ${ID} ${BLOB}
		TSTR=l${LC}r${RC}_"${TIMBL// /}"
		#blob: lines errors good_sugg bad_sugg wrong_sugg no_sugg
		echo "${ID} ${LINES} ${BLOB} ${TSTR}" >> ${PLOT}
	    done
	done
    done
done

