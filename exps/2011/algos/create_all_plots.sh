#!/bin/sh
#
# Script to generate all plots/pdfs
# sh create_all_plots.sh DATAFILE PREFIX
# sh create_all_plots.sh DATA.ALG.nyt.plot NY
#
PREFIX="exp"
if test $# -lt 2
then
  echo "Supply FILE and PREFIX"
  exit
fi
#
PLOT=$1
PREFIX=$2
#
TYP=${PLOT:5:2}
#
if [ "$TYP" == "AL" -o "$TYP" == "" -o "$TYP" == "MR" ]
then
    VARS='cg cd ic pplx pplx1 mrr_cd mrr_cg mrr_gd adc ads'
else
    VARS='errors gsa bsa wsa nsa gs bs ws ns'
fi
#
for VAR in ${VARS} #"cg" "cd" "ic" "pplx" "mrr_cd" "mrr_cg" "mrr_gd" "adc" "ads"
do
    echo "sh create_plots.sh $PLOT $VAR $PREFIX"
    sh create_plots.sh $PLOT $VAR $PREFIX
done
#
for FILE in ${PREFIX}*.plot;
do
    echo "gnuplot ${FILE}"
    gnuplot ${FILE}
done