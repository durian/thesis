scp -P223 pberck@portal.ticc.uvt.nl:DATA.ALG.nyt.plot .
sh create_plots.sh DATA.ALG.nyt.plot mrr_gd NYT

Gemiddelde grootte van de dist. wordt natuurlijk hoger bij meer
training data. Er spelen een hoop dingen door elkaar...

NYT data.

Let us look at the 'classical' l2r0 experiments.

The 'correct guess' scores:

sh create_plots.sh DATA.ALG.nyt.plot cg NYT

All l2r0 plots are in: NYT_cg_l2r0.plot


Variations:

a1 versus a4 (IGTree versus Tribl2)

The major difference with the IB1 algorithm originally proposed by
(?), is that in our version the value of k refers to k-nearest
distances rather than k-nearest examples. With k = 1, for instance,
TiMBL’s nearest neighbor set can contain several instances that are
equally distant to the test instance. Arguably, our k-NN kernel could
therefore be called k-nearest distances classification.

k1, k2 and k3

-k <n> : number of nearest neighbors used for extrapolation. Only
 applicable in conjunction with IB1 (-a 0), TRIBL (-a 2), TRIBL2 (-a
 4) and IB2 (-a 3). The default is 1. Especially with the MVDM metric
 it is often useful to determine a good value larger than 1 for this
 parameter (usually an odd number, to avoid ties). Note that due to
 ties (instances with exactly the same similarity to the test
 instance) the number of instances used to extrapolate might in fact
 be much larger than this parameter.

-w n      : Weighting
   0 or nw: No Weighting

By setting the parameter -w to 0, an unweighted overlap definition of
similarity is created where each feature is considered equally
relevant. In that case, similarity reduces to the number of equal
values in the same position in the two patterns being compared

-d val    : weight neighbors as function of their distance:
   IL     : Inverse Linear

mvdm or Jeffrey divergence?

-mM -mJ

The Overlap metric is all- or-nothing. For measuring the similarity
between numeric or atomically symbolic values this may suffice, but
there are cases (such as in natural language processing) in which
string-valued feature values occur that can mismatch with other string
values in a meaningfully graded way. For example, the value pair
“bathe” and “bathes” only differs in one letter; counting them as more
similar than “bathe” and “rumour”, for example, may be useful for the
classification task at hand. We implemented two additional metrics,
Levenshtein distance and the Dice coefficient, that each provide a
graded similarity score between pairs of strings.

-mDC or -mL

Hence IGTREE is not to be used with e.g. “no weights” (-w
0). Also, setting the -k parameter obviously has no effect on IGTREE
performance.

a4 +D met:
  -mM -mJ -mDC -mL -k1 -k3 -w0 -dIL



// ---- 2010-12-22 11:15:57 Wed -----------------------------------------------

-m distance metric (default overlap, -mO)

-w feature weighing (default GR)

-k neighest neighbours (not with -a1) default is 1. But it means: "k-nearest
distances", so all which are at distance "closest" is -k1.

-d : The type of class voting weights that are used for extrapolation
 from the nearest neighbor set. val can be one of:

// ---- 2010-12-26 20:00:00  -----------------------------------------------

Nieuwe Timbl op scylla

// ---- 2011-01-04 10:44:37 Tue -----------------------------------------------

Recap.

Two experiments on scylla, swedish and NYT.

SE:"-a1 +D" "-a1 +D -mM" "-a1 +D -mJ" "-a4 +D" "-a4 +D -mM" "-a4 +
D -mJ"  "-a4 +D -k2" "-a4 +D -k2 -mM" "-a4 +D -k2 -mJ"  "-a4 +D -k3" "-a4 +D -k3
 -mM" "-a4 +D -k3 -mJ"

NYT: "-a1 +D" "-a4 +D" "-a4 +D -w0" "-a4 +D -w0 -dIL" "-a4 +D -k2" "
-a4 +D -w0 -k2" "-a4 +D -w0 -dIL -k2" "-a4 +D -k3" "-a4 +D -w0 -k3" "-a4 +D -w0 
-dIL -k3" 

First, look at classical word prediction: l2r0 (rather lNr0). We look
at the correct-guess scores (cg).

(see also: NY_cg_-a4+D.plot for influence of r parameter in
context. >r1 niet echt veel winst.)

SE:
l2r0: -a4+D (cg) best
l3r0: -a4+D-k2 (cg) best

NYT:
l2r0: -a4+D-k2/3 (cg) best
l3r0: -a4+D-k2/3 (cg) best

Less classical:

SE:
l2r1: -a4+D-k3-mM (cg) best
l2r2: -a4+D-k3-mM (cg) best
l3r(123): -a4+D-k3-mM (cg) best

NYT:
l2r1: -a4+D (cg) best
l2r2: -a4+D (cg) best
l3r(123): -a4+D-k2 (cg) best

So, -a4 better than IGTree

----

Now a look with the same data at mrr_gd

SE:
l2r0: euh
l3r0: euh
l3r2: bijna 0.9 mrr? is dat goed? veel fout, fout niet gerekend als 0?
mrr_gd rekent alleen mrr voor goede guesses. (ofcourse, but).

Hele kleine distributies, een hele group algos blijft onder de asc:500

---

Perplexity? Meaninles :-)

Conclusion: l2/3r1 with -a4+D

timings? is a4 much slower?

Learning (scylla):

algos$ Timbl -f europarl.se.txt.10000.l2r0 -a4 +D -Iblah
Learning took 12 seconds, 882 milliseconds and 358 microseconds

algos$ Timbl -f europarl.se.txt.10000.l2r0 -a1 +D -Iblah
Learning took 15 seconds, 106 milliseconds and 560 microseconds


Timbl -f europarl.se.txt.100000.l2r0 -a4 +D -Iblah
Learning took 200 seconds, 558 milliseconds and 192 microseconds

Timbl -f europarl.se.txt.100000.l2r0 -a1 +D -Iblah
Learning took 215 seconds, 15 milliseconds and 671 microseconds

Testing:

Timbl -f europarl.se.txt.10000.l2r0 -a4 +D -t europarl.se.txt.l1000.l2r0
..
Size of InstanceBase = 225248 Nodes, (9009920 bytes), 26.39 % compression
Learning took 15 seconds, 825 milliseconds and 875 microseconds 
...
Seconds taken: 881.1027 (24.49 p/s)

overall accuracy:        0.153736  (3317/21576), of which 12631 exact matches 
There were 5009 ties of which 416 (8.31%) were correctly resolved
---

Timbl -f europarl.se.txt.10000.l2r0 -a1 +D -t europarl.se.txt.l1000.l2r0
..
Size of InstanceBase = 87932 Nodes, (3517280 bytes), 71.26 % compression
Learning took 14 seconds, 855 milliseconds and 304 microseconds
...
Seconds taken: 24.8150 (869.47 p/s)

overall accuracy:        0.149935  (3235/21576)
--

Timbl -f europarl.se.txt.100000.l2r0 -a4 +D -t europarl.se.txt.l1000.l2r0 
..
Size of InstanceBase = 1378641 Nodes, (55145640 bytes), 28.69 % compression
Learning took 205 seconds, 361 milliseconds and 867 microseconds
..
Seconds taken: 1055.8278 (20.44 p/s)

overall accuracy:        0.186411  (4022/21576), of which 16524 exact matches 
There were 3736 ties of which 294 (7.87%) were correctly resolved
--

Timbl -f europarl.se.txt.100000.l2r0 -a1 +D -t europarl.se.txt.l1000.l2r0 
..
Size of InstanceBase = 535609 Nodes, (21424360 bytes), 72.30 % compression
Learning took 154 seconds, 768 milliseconds and 445 microseconds
..
Seconds taken: 89.5383 (240.97 p/s)

overall accuracy:        0.184742  (3986/21576)
---

// ---- 2011-01-05 11:41:50 Wed -----------------------------------------------

Redid the previous in a script.

Tokenize the swedish data? ucto--

// ---- 2011-01-11 10:50:24 Tue -----------------------------------------------

More data.

sh create_plots.sh DATA.ALG.plot mrr_gd SE
sh create_plots.sh DATA.ALG.plot adc SE

Linecounts:

pberck@scylla:/exp/pberck/svenska/algos$ wc -l *l2r0 | sort -n
    21576 europarl.se.txt.l1000.l2r0
    22829 nyt.tail1000.l2r0
    23561 europarl.se.txt.1000.l2r0
    46273 europarl.se.txt.2000.l2r0
    67324 europarl.se.txt.3000.l2r0
    88950 europarl.se.txt.4000.l2r0
   111710 europarl.se.txt.5000.l2r0
   134716 europarl.se.txt.6000.l2r0
   155028 europarl.se.txt.7000.l2r0
   178997 europarl.se.txt.8000.l2r0
   201540 europarl.se.txt.9000.l2r0
   223303 europarl.se.txt.10000.l2r0
   452137 europarl.se.txt.20000.l2r0
   681193 europarl.se.txt.30000.l2r0
   898396 europarl.se.txt.40000.l2r0
  1116784 europarl.se.txt.50000.l2r0
  1337867 europarl.se.txt.60000.l2r0
  1568078 europarl.se.txt.70000.l2r0
  1795591 europarl.se.txt.80000.l2r0
  2017458 europarl.se.txt.90000.l2r0
  2240589 europarl.se.txt.100000.l2r0

pberck@scylla:/exp/pberck/svenska/algos$ wc -l /exp2/pberck/*l2r0 | sort -n
    21922 /exp2/pberck/nyt.3e7.1000.l2r0
    45569 /exp2/pberck/nyt.3e7.2000.l2r0
    67725 /exp2/pberck/nyt.3e7.3000.l2r0
    92208 /exp2/pberck/nyt.3e7.4000.l2r0
   115304 /exp2/pberck/nyt.3e7.5000.l2r0
   139204 /exp2/pberck/nyt.3e7.6000.l2r0
   162505 /exp2/pberck/nyt.3e7.7000.l2r0
   184168 /exp2/pberck/nyt.3e7.8000.l2r0
   207788 /exp2/pberck/nyt.3e7.9000.l2r0
   231817 /exp2/pberck/nyt.3e7.10000.l2r0
   466085 /exp2/pberck/nyt.3e7.20000.l2r0
   697373 /exp2/pberck/nyt.3e7.30000.l2r0
   922913 /exp2/pberck/nyt.3e7.40000.l2r0
  1155166 /exp2/pberck/nyt.3e7.50000.l2r0
  1368696 /exp2/pberck/nyt.3e7.60000.l2r0
  1585018 /exp2/pberck/nyt.3e7.70000.l2r0
  1812531 /exp2/pberck/nyt.3e7.80000.l2r0
  2038657 /exp2/pberck/nyt.3e7.90000.l2r0

// ---- 2011-01-27 13:30:26 Thu -----------------------------------------------

// ---- 2011-01-30 18:29:05 Sun -----------------------------------------------

Scylla dood :-(

Last:

tail DATA.ALG.nyt.plot
ALG6804 500000 15.92 28.78 55.30 443.00 443.00 0.232 1.000 0.505 4298.38 154612.
26 l3r0_-a4+D-w0-k2

500000 started with: ALG6760

tail DATA.ALG.plot
ALG2257 9000 20.32 6.05 73.62 233.54 233.54 0.510 1.000 0.887 257.66 257.66 l3r1
_-a4+D-k2-mJ

9000 startd with: ALG2152

_restart scripts and restarted both experiments

// ---- 2011-02-18 09:03:34 Fri -----------------------------------------------

Scatter experiment op ceto

// ---- 2011-07-21 09:15:29 Thu -----------------------------------------------

Nieuw experiment op SCYLLA; do_nyt_10000.sh and 20000.sh to compare
tokenizing versus "plain".

10000 and 20000 are nyt
15000 and 25000 are europarl.se

pberck@scylla:/exp2/pberck/svenska/algos$ sort -n -k3
DATA.ALG.10000.plot
...
ALG10259 100000 26.49 10.02 63.49 239.07 239.07 0.385 1.000 0.831 37.40 222.52 l3r3_-a4+D
ALG10255 100000 26.59 12.25 61.16 225.64 225.64 0.353 1.000 0.796 114.48 1501.74 l2r3_-a4+D
ALG10254 100000 26.65 15.69 57.65 208.38 208.38 0.323 1.000 0.749 193.12 2824.46 l2r2_-a4+D

and

pberck@scylla:/exp2/pberck/svenska/algos$ sort -n -k3
DATA.ALG.20000.plot
...
ALG20246 100000 28.06 27.41 44.53 164.95 164.95 0.245 1.000 0.627 3810.96 115244.13 l3r2_-a1+D
ALG20247 100000 28.07 27.23 44.70 165.81 165.81 0.245 1.000 0.628 3810.05 115238.75 l3r3_-a1+D
ALG20251 100000 28.07 27.21 44.72 165.92 165.92 0.245 1.000 0.629 3809.73 115235.44 l4r3_-a1+D

on macbook:
durian:algos pberck$ bash create_plots.sh DATA.ALG.10000.plot cg CG
gnuplot CG_cg_l2r0.plot
open CG_cg_l2r0.pdf 

and

bash create_plots.sh DATA.ALG.20000.plot cg ALG20000
durian:algos pberck$ gnuplot ALG20000_cg_l2r0.plot
durian:algos pberck$ open ALG20000_cg_l2r0.pdf

