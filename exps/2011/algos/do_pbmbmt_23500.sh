#!/bin/sh
#
# As 13500, but with SRILM
#
LMBASE="/exp2/pberck/pbmbmt_wopr/OpenSub-english.train.txt"
WOPR="/exp/pberck/wopr/wopr"
MKDATASCRIPT="wopr_smt_makedata.wopr_script"
LMSCRIPT="wopr_smt.wopr_script"
PBMBMT="./pbmbmt.py"
NC=`which nc`
NGRAMC="/exp/pberck/srilm-1.5.8/bin/i686/ngram-count"
#
FL="OpenSub-dutch"
TL="OpenSub-english"
PBMBMTDIR="test"
PBMBMTSUF="exp1" #!!
PBMBMTOPTS="-DX" #will be looped later
#
# path: data/test/OpenSub-dutch.exp2/decode.err
#       data/${PBMBMTDIR}/${FL}.${PBMBMTSUF}/decode.err
#
CYCLE=23500
LOG="README.MT.${CYCLE}.txt"
DATA="DATA.MT.${CYCLE}.data"
#
# Run PBMBMT
#
for FILE in ${LMBASE} ${WOPR} ${MKDATASCRIPT} ${LMSCRIPT} ${PBMBMT} ${NC}
do
    if [ ! -e "${FILE}" ]
    then
	echo "${FILE} not found, aborting."
	exit 1
    fi
done
#
#date "+#%Y-%m-%d %H:%M:%S" >> ${LOG}
#
PORT=8888
for LINES in 1000 2000 3000 4000 5000 6000 7000 8000 9000 10000 20000 30000 40000 50000 60000 70000 80000 90000 100000 200000 0
do
    #Make the datasets here.
    #LMFILE=${LMBASE} #.${LINES} #just the full file
    if [ $LINES -eq 0 ]
    then
	LINES=`wc ${LMBASE} | awk {'print $1'}`
    fi
    LMFILE=${LMBASE}.${LINES}
    if [ -e "${LMFILE}" ]
    then
        echo "Data file ${LMFILE} exists."
    else
        echo "Creating data file ${LMFILE}"
        head -n ${LINES} ${LMBASE} > ${LMFILE}
    fi
    #
        for LC in 2 3 4
        do
            for RC in 0 
            do
		for PBMBMTOPTS in "-DX -DO 5 -DT 1 -DD 0.25 -DM 2 -Db 1,1 -Df 10,5 -Da 3,3 -DK 1.0 -Dd 7"
		do
                FREE=`df -k . | tail -1 | awk '{print $3}'`
                # 25 577 264 = 25GB
                while [ $FREE -lt 10000000 ]
                do  
                    echo "Space critical, pausing at ${CYCLE}."
                    sleep 60
                    FREE=`df -k . | tail -1 | awk '{print $3}'`
                done
		#
                CYCLESTR=`printf "%05d" ${CYCLE}`
                ID=MT${CYCLESTR}
		MTLOG=MTLOG.${ID}
		LMLOG=LMLOG.${ID}
		#
		CYCLE=$(( $CYCLE + 1 ))
		#
		if [ -e "${MTLOG}" ]
		then
		    if [ -s "${MTLOG}" ]
		    then
			echo "ALREADY done, ${ID} exists."
			OL=`grep ONELINE ${MTLOG}`
			OL=${OL#*ONELINER:}
			TSTR=l${LC}r${RC}_"${TIMBL// /}"
			TSTR=l${LC}
			PSTR="${PBMBMTOPTS// /}"
			echo ${ID} ${LMBASE##*/} ${LINES} ${OL} ${TSTR} ${PSTR}
			printf -v S "%s %s %s %s %s %s %s %s %s %s %s %s" ${ID} ${LMBASE##*/} ${LINES} ${OL} ${TSTR} ${PSTR}
			echo ${S} >> ${DATA}
			continue
		    fi
		fi
		#
		# CREATE LM.
     		#
		echo "make LM"
		#ngram-count -order 3 -interpolate -kndiscount -text OpenSub-english.train.txt -lm OpenSub-english.lm
		echo "${NGRAMC} -order ${LC} -interpolate -kndiscount -text ${LMFILE} -lm ${LMFILE}.${LC}.lm"
		${NGRAMC} -order ${LC} -interpolate -kndiscount -text ${LMFILE} -lm ${LMFILE}.${LC}.lm
		#
		# PBMBMT
		#
		echo ${PBMBMT} -D -S -Dbranch 1 ${PBMBMTOPTS} --Dsrilm=${LMFILE}.${LC}.lm -- ${FL} ${TL} ${PBMBMTDIR} ${PBMBMTSUF}
		time ${PBMBMT} -D -Dbranch 1 -S  ${PBMBMTOPTS} --Dsrilm=${LMFILE}.${LC}.lm -- ${FL} ${TL} ${PBMBMTDIR} ${PBMBMTSUF} > ${MTLOG} 2>&1
		#
		# Parse scores
		#
		OL=`grep ONELINE ${MTLOG}`
		OL=${OL#*ONELINER:}
		#TSTR=l${LC}r${RC}_"${TIMBL// /}"
		TSTR=l${LC}
		PSTR="${PBMBMTOPTS// /}"
		echo ${ID} ${LMBASE##*/} ${LINES} ${OL} ${TSTR} ${PSTR}
		printf -v S "%s %s %s %s %s %s %s %s %s %s %s %s" ${ID} ${LMBASE##*/} ${LINES} ${OL} ${TSTR} ${PSTR}    
		echo ${S} >> ${DATA}
	    done
	done
    done
done
