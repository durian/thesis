#!/bin/sh
#
#for FILE in *srilm*RUN*;
for FILE in $(ls nyt.srilm*SRI1* | sort -n); 
do
    echo Processing: ${FILE} >/dev/stderr 

    pplx_str=`tail ${FILE}`
    echo $pplx_str
    #0 zeroprobs, logprob= -22530.9 ppl= 91.3214 ppl1= 140.422
    #
    regex='.* ppl= (.*) ppl1= (.*).*'
    if [[ "$pplx_str" =~ $regex ]]
    then
	pplx=${BASH_REMATCH[1]}
	pplx1=${BASH_REMATCH[2]}
    else
	pplx=0
	pplx1=0
	echo "No match"
    fi

    echo $FILE $pplx $pplx1
done


