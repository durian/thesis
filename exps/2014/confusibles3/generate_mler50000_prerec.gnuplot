#
# Use directly in gnuplot
#
#set style line 1 lt -1 lw 0.5 pi -1 pt 7 ps 1.0
#set style line 2 lt -1 lw 0.5 pi -1 pt 6
#set style line 3 lt -1 lw 0.5 pi -1 pt 7
#set style line 4 lt 2 lw 0.5 pi -1 pt 7 ps 0.4
set style line 1 lt -1 lw 0.5 pi -1 pt 1 ps 0.9
set style line 2 lt -1 lw 0.5 pi -1 pt 2 ps 0.9
set style line 3 lt -1 lw 0.5 pi -1 pt 4 ps 0.9
set style line 4 lt -1 lw 0.5 pi -1 pt 5 ps 0.9
set style line 5 lt -1 lw 0.5 pi -1 pt 6 ps 0.9
set style line 6 lt -1 lw 0.5 pi -1 pt 7 ps 0.9
set style line 7 lt -1 lw 0.5 pi -1 pt 8 ps 0.9
#set title "Precision versus Recall, 1e6"
set ylabel "Precision"
set xlabel "Recall"
set key bottom
#set grid
set border 3
set xtics out nomirror
set mxtics 2
set ytics out nomirror
set mytics 2
#
##set label "0.95, 0.99" at first 40.4, first 68
##set label "0-0.5" at first 51, first 67
#
# l2t1r2 a1 99.53 69.40 40.97 51.52 confidence 0 EXER41000 2000000 227 0.61
# l2t1r2 a1 99.53 69.40 40.97 51.52 confidence 0.1 EXER41006 2000000 227 0.61
#
#plot [15:55][62:82]
plot [10:60][40:80]\
"< grep a1 STATCF.MLER.50000.txt | grep cf350 | grep cf200| awk -f p4txt.awk" using 5:4 with lp ls 5 t "igtree",\
"< grep a1 STATCF.MLER.50000.txt | grep cf350 | grep cf200|egrep  '(0.9$)' | awk -f p4txt.awk" using 5:4:8 with labels offset .4,.8 notitle,\
"< grep a1 STATCF.MLER.50000.txt | grep cf350 | grep cf200|egrep  '(0.5$)' | awk -f p4txt.awk" using 5:4:8 with labels offset .4,-0.8 notitle,\
"< grep a4 STATCF.MLER.50000.txt | grep cf350 | grep cf200| awk -f p4txt.awk" using 5:4 with lp ls 6 t "tribl2",\
"< grep a4 STATCF.MLER.50000.txt | grep cf350 | grep cf200|egrep  '(0.8$)' | awk -f p4txt.awk" using 5:4:8 with labels offset .4,.8 notitle,\
"< grep a4 STATCF.MLER.50000.txt | grep cf350 | grep cf200|egrep  '(0.99$)' | awk -f p4txt.awk" using 5:4:8 with labels offset .4,-0.8 notitle
#
#"< grep a4 STATCF.MLER.50000.txt | grep cf350 | grep cf200|egrep -v '(0$|0.1$|0.2$|0.3$|0.4$|0.5$|0.95$|0.99$)'  | awk -f p4txt.awk" using 5:4:8 with labels offset 2,-0.8 notitle,\
#"< grep a1 STATCF.MLER.50000.txt | grep cf350 | grep cf200|egrep -v '(0$|0.1$|0.2$|0.3$|0.4$|0.5$|0.95$|0.99$)'  | awk -f p4txt.awk" using 5:4:8 with labels offset 2,-0.8 notitle
#"bla"  using 5:4 with lp ls 1 t "P v R"
set terminal push
#set terminal postscript eps enhanced color solid rounded lw 2 'Helvetica' 10
set terminal postscript eps enhanced dashed rounded lw 2 'Helvetica' 20
set out 'MLER50000_pre_rec.ps'
replot
!epstopdf 'MLER50000_pre_rec.ps'
set term pop
