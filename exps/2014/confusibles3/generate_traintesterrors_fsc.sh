#!/bin/bash
#
# Plot to show percentage errors in train versus % errors in test, f-score
#
STAT0="STATCF.MLER.70000.txt"
STAT1="STATCF.MLER.72000.txt" #cf209/cf210, 0.3 and 0.6% errors
ALGO="a4"
TRAIN=$1
#TEST=$2
#
#for TRAIN in "cf200" "cf201" "cf202" "cf203" "cf204" "cf205" "cf206" "cf207" "cf208"
#do
F="foobarquux.tmp.${TRAIN}.txt"
echo "" > ${F}
for TEST in "cf350" "cf351" "cf352" "cf353" "cf354" "cf355" "cf356" "cf357" "cf358" "cf359" "cf360"
#for TEST in 0.6 1.1 2.0 3.0 4.0 5.0 5.9 6.0 8.1 8.8 9.7 
do
grep ${ALGO} ${STAT1} | grep ${TRAIN} | grep ${TEST} | awk -v a=${ALGO}' '${TRAIN}' '${TEST} 'BEGIN { FS = "[, :]+" } {print $14,$15,$16,$17,$22,$2,a}' | sort -nk5 >>${F}
grep ${ALGO} ${STAT0} | grep ${TRAIN} | grep ${TEST} | awk -v a=${ALGO}' '${TRAIN}' '${TEST} 'BEGIN { FS = "[, :]+" } {print $14,$15,$16,$17,$22,$2,a}' | sort -nk5 >>${F}
done
#done
#very wrong, but it works
#
sed -i0 s/cf350/0.6/ ${F}
sed -i0 s/cf351/1.1/ ${F}
sed -i0 s/cf352/2.0/ ${F}
sed -i0 s/cf353/3.0/ ${F}
sed -i0 s/cf354/4.0/ ${F}
sed -i0 s/cf355/5.0/ ${F}
sed -i0 s/cf356/5.9/ ${F}
sed -i0 s/cf357/6.9/ ${F}
sed -i0 s/cf358/8.1/ ${F}
sed -i0 s/cf359/8.8/ ${F}
sed -i0 s/cf360/9.7/ ${F}
#
sed -i0 s/cf200/1.1/ ${F}
sed -i0 s/cf201/2.5/ ${F}
sed -i0 s/cf202/5.0/ ${F}
sed -i0 s/cf203/7.4/ ${F}
sed -i0 s/cf204/9.8/ ${F}
sed -i0 s/cf205/14.7/ ${F}
sed -i0 s/cf206/19.5/ ${F}
sed -i0 s/cf207/24.3/ ${F}
sed -i0 s/cf208/29.2/ ${F}
sed -i0 s/cf209/0.6/ ${F}
sed -i0 s/cf210/0.3/ ${F}

#
cat ${F}
