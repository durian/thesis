#!/bin/sh
#
TRAINBASE="utexas.10e6.dt3"           #w/o errors
ORIGBASE="utexas.10e6.dt3"            #w/o errors
ORIGTESTFILE="utexas.10e6.dt3.t1e5"   #w/o errors
#
WOPR="/exp2/pberck/wopr_nt/src/wopr"
SCRIPT="do_spelcorr.wopr_script"
SCSCRIPT="/exp2/pberck/wopr_nt/etc/check_spelerr2.pl -n "
#
ME="$(basename "$(test -L "$0" && readlink "$0" || echo "$0")")"
CYCLE=`echo ${ME} | tr -dc "0-9"`
I=ML
#
LOG="README.${I}.${CYCLE}.txt"
STAT="STAT.${I}.${CYCLE}.txt"
STATCF="STATCF.${I}.${CYCLE}.txt"
CFSET="goldingroth3.txt"
#
for TESTFILE in "utexas.10e6.dt3.t1e5.cf300" "utexas.10e6.dt3.t1e5.cf301" "utexas.10e6.dt3.t1e5.cf302" "utexas.10e6.dt3.t1e5.cf303" "utexas.10e6.dt3.t1e5.cf304" "utexas.10e6.dt3.t1e5.cf305" "utexas.10e6.dt3.t1e5.cf306" "utexas.10e6.dt3.t1e5.cf307" "utexas.10e6.dt3.t1e5.cf308" "utexas.10e6.dt3.t1e5.cf309" "utexas.10e6.dt3.t1e5.cf310"
do
#
# Do spelling correction
#
if [ ! -e "${ORIGTESTFILE}" ]
then
    echo "${ORIGTESTFILE} nor found, aborting."
    exit 1
fi
if [ ! -e "${TESTFILE}" ]
then
    echo "${TESTFILE} nor found, aborting."
    exit 1
fi
#
date "+#%Y-%m-%d %H:%M:%S" >> ${LOG}
#
PARAMS="mwl:1,mld:10,max_ent:5,max_distr:3,min_ratio:0,max_tf:10000000,triggerfile:${CFSET}"
#
for LINES in 1000000
do
    #Make the datasets here.
    TRAINFILE=${TRAINBASE}.${LINES}
    if [ -e "${TRAINFILE}" ]
    then
	echo "Data file ${TRAINFILE} exists."
    else
	echo "Creating data file ${TRAINFILE}."
	head -n ${LINES} ${TRAINBASE} > ${TRAINFILE}
    fi
    for TIMBL in "-a1 +D" 
    do
	for LC in 2
	do
	    for RC in 2
	    do
		FREE=`df -k . | tail -1 | awk '{print $3}'`
		# 25 577 264 = 25GB
		while [ $FREE -lt 10000000 ]
		do  
		    echo "Space critical, pausing at ${CYCLE}."
		    sleep 60
		    FREE=`df -k . | tail -1 | awk '{print $3}'`
		done
		# original windowed test set
		${WOPR} -r window_lr -p filename:${ORIGTESTFILE},lc:${LC},rc:${RC}
		CYCLESTR=`printf "%05d" ${CYCLE}`
		ID=${I}${CYCLESTR}
		echo ${ID}
		KVS="${ID}.kvs"
		# if exists PXFILE=${TESTFILE}.l${LC}r${RC}_${ID}.sc, skip?
		SCFILE=${TESTFILE}.l${LC}r${RC}_${ID}.sc
		OUT=output.${ID}
		if [ -e "${SCFILE}" ]
		then
		    echo "${SCFILE} exists. Skipping."
		else
		    echo "#${ID},${TRAINFILE},timbl:'${TIMBL}',lc:${LC},rc:${RC},${PARAMS}" >> ${LOG}
		    echo ${WOPR} -s ${SCRIPT} -p trainfile:${TRAINFILE},rc:${RC},lc:${LC},id:${ID},timbl:${TIMBL},testfile:${TESTFILE},${PARAMS},kvsfile:${KVS}
		    ${WOPR} -s ${SCRIPT} -p trainset:${TRAINFILE},rc:${RC},lc:${LC},id:${ID},timbl:"${TIMBL}",testset:${TESTFILE},${PARAMS},kvsfile:${KVS} >>${OUT}
		fi
		CYCLE=$(( $CYCLE + 1 ))
		#
		#reuters.martin.tok.1000.l3r2_ALG023.sc
		echo "perl ${SCSCRIPT} -s ${SCFILE} -o ${ORIGTESTFILE}.l${LC}r${RC}" >> ${OUT}
		BLOB=`perl ${SCSCRIPT} -s ${SCFILE} -o ${ORIGTESTFILE}.l${LC}r${RC}`

		#http://wiki.bash-hackers.org/commands/builtin/printf
		#printf -v S "%s %s" ${ID} ${BLOB}
		TSTR=l${LC}r${RC}_"${TIMBL// /}"
		#
		#md5sum testfile
		MD5=${TESTFILE}.l${LC}r${RC}
		#blob: lines errors good_sugg bad_sugg wrong_sugg no_sugg
		#
		# Precision, recall, accuracy
		echo "perl ${SCSCRIPT} -s ${SCFILE} -o ${ORIGTESTFILE}.l${LC}r${RC}" >> ${OUT}
                BLOB=`perl ${SCSCRIPT} -s ${SCFILE} -o ${ORIGTESTFILE}.l${LC}r${RC}`
		echo "${ID} ${TSTR} ${BLOB} ${TRAINFILE} ${LINES} ${TESTFILE}" >> ${STAT}
		#
		echo "perl ${SCSCRIPT} -s ${SCFILE} -o ${ORIGTESTFILE}.l${LC}r${RC} -c ${CFSET}" >> ${OUT}
                BLOB=`perl ${SCSCRIPT} -s ${SCFILE} -o ${ORIGTESTFILE}.l${LC}r${RC} -c ${CFSET}`
		echo "${ID} ${TSTR} ${BLOB} ${TRAINFILE} ${LINES} ${TESTFILE}" >> ${STATCF}
		
	    done
	    #rm ${TESTFILE}.l${LC}r${RC}  #and ORIGTESTFILE?
	done
    done
    #rm ${TRAINFILE}
done
done
