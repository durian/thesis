#!/bin/bash
#
# Plots for the KLER classifier.
#
STATFILE=STATCF.MLER.61000.txt
TRAIN="cf202"
TEST="cf350"
INFO="Train 1e6, 5\%, test 1e5, 0.6\%"

# min_md setting in wopr is >= min_md

DATAFILE=STATCF_MLER_61000_minmd_fsc #same data in all three actually
TEMPLATE=TEMPLATE.MLER.MINMD.FSC.GNUPLOT
#
#with * in filenames, we need to shift the $fields 
#
grep "${TRAIN}" ${STATFILE} | grep "${TEST}" | awk 'BEGIN { FS = "[, :]+" } {print $14,$15,$16,$17,$22,$2}' | sort -nk5  > ${DATAFILE}
#
# 45.20 0.23 20.70 0.46 0 l2r2_-a1+D
# acc   pre  rec   fscr c algo
#
sed "s/__FILE__/${DATAFILE}/g" ${TEMPLATE} > ${DATAFILE}.gnuplot.1
sed "s/__TITLE__/${INFO}/g" ${DATAFILE}.gnuplot.1 > ${DATAFILE}.gnuplot
rm ${DATAFILE}.gnuplot.1
gnuplot ${DATAFILE}.gnuplot

#

DATAFILE=STATCF_MLER_61000_minmd_rec
TEMPLATE=TEMPLATE.MLER.MINMD.REC.GNUPLOT
#
#with * in filenames, we need to shift the $fields 
#
grep "${TRAIN}" ${STATFILE} | grep "${TEST}" | awk 'BEGIN { FS = "[, :]+" } {print $14,$15,$16,$17,$22,$2}' | sort -nk5  > ${DATAFILE}
#
# 45.20 0.23 20.70 0.46 0 l2r2_-a1+D
# acc   pre  rec   fscr c algo
#
sed "s/__FILE__/${DATAFILE}/g" ${TEMPLATE} > ${DATAFILE}.gnuplot.1
sed "s/__TITLE__/${INFO}/g" ${DATAFILE}.gnuplot.1 > ${DATAFILE}.gnuplot
rm ${DATAFILE}.gnuplot.1
gnuplot ${DATAFILE}.gnuplot

#

DATAFILE=STATCF_MLER_61000_minmd_pre
TEMPLATE=TEMPLATE.MLER.MINMD.PRE.GNUPLOT
#
#with * in filenames, we need to shift the $fields 
#
grep "${TRAIN}" ${STATFILE} | grep "${TEST}" | awk 'BEGIN { FS = "[, :]+" } {print $14,$15,$16,$17,$22,$2}' | sort -nk5  > ${DATAFILE}
#
# 45.20 0.23 20.70 0.46 0 l2r2_-a1+D
# acc   pre  rec   fscr c algo
#
sed "s/__FILE__/${DATAFILE}/g" ${TEMPLATE} > ${DATAFILE}.gnuplot.1
sed "s/__TITLE__/${INFO}/g" ${DATAFILE}.gnuplot.1 > ${DATAFILE}.gnuplot
rm ${DATAFILE}.gnuplot.1
gnuplot ${DATAFILE}.gnuplot
