#!/bin/bash
#
#STATFILE=STATCF.MLER.90000.txt
STATFILE=STATCF.MLER.91000.txt
TRAIN="cf200" #these are the only one cf202 in MLER.90000
TEST="cf350"  #in the data
INFO="Train, 1.1\%, test 1e5, 0.6\%"

#MLER90000 l2t1r2_-a1+D 2522289  37052    227    97   130     0   239    97   239   130 36586 99.00 28.87 42.73 34.46 utexas.10e6.dt3.cf202.100000 100000 utexas.10e6.dt3.t1e5d.cf350 max_distr:100,triggerfile:goldingroth3.txt,max_ent:10

DATAFILE=${STATFILE}_trs_fsc 
TEMPLATE=TEMPLATE.MLER.TRAINSIZE.FSC.GNUPLOT
#
#
grep "${TRAIN}" ${STATFILE} | grep "${TEST}" | awk 'BEGIN { FS = "[, :]+" } {print $14,$15,$16,$17,$19,$2}' | sort -nk5  > ${DATAFILE}
#
# 45.20 0.23 20.70 0.46 0 l2r2_-a1+D
# acc   pre  rec   fscr c algo
#
sed "s/__FILE__/${DATAFILE}/g" ${TEMPLATE} > ${DATAFILE}.gnuplot.1
sed "s/__TITLE__/${INFO}/g" ${DATAFILE}.gnuplot.1 > ${DATAFILE}.gnuplot
rm ${DATAFILE}.gnuplot.1
gnuplot ${DATAFILE}.gnuplot

# 

TEMPLATE=TEMPLATE.MLER.TRAINSIZE.REC.GNUPLOT
#DATAFILE=STATCF_MLER_90000_trs_rec 
DATAFILE=${STATFILE}_trs_rec
#
# 45.20 0.23 20.70 0.46 0 l2r2_-a1+D
# acc   pre  rec   fscr c algo
#
grep "${TRAIN}" ${STATFILE} | grep "${TEST}" | awk 'BEGIN { FS = "[, :]+" } {print $14,$15,$16,$17,$19,$2}' | sort -nk5  > ${DATAFILE}
sed "s/__FILE__/${DATAFILE}/g" ${TEMPLATE} > ${DATAFILE}.gnuplot.1
sed "s/__TITLE__/${INFO}/g" ${DATAFILE}.gnuplot.1 > ${DATAFILE}.gnuplot
rm ${DATAFILE}.gnuplot.1
gnuplot ${DATAFILE}.gnuplot

TEMPLATE=TEMPLATE.MLER.TRAINSIZE.PRE.GNUPLOT
#DATAFILE=STATCF_MLER_90000_trs_pre
DATAFILE=${STATFILE}_trs_pre
grep "${TRAIN}" ${STATFILE} | grep "${TEST}" | awk 'BEGIN { FS = "[, :]+" } {print $14,$15,$16,$17,$19,$2}' | sort -nk5  > ${DATAFILE}
sed "s/__FILE__/${DATAFILE}/g" ${TEMPLATE} > ${DATAFILE}.gnuplot.1
sed "s/__TITLE__/${INFO}/g" ${DATAFILE}.gnuplot.1 > ${DATAFILE}.gnuplot
rm ${DATAFILE}.gnuplot.1
gnuplot ${DATAFILE}.gnuplot

# ALL

TEMPLATE=TEMPLATE.MLER.TRAINSIZE.ALL.GNUPLOT
#DATAFILE=STATCF_MLER_90000_trs_all
DATAFILE=${STATFILE}_trs_all
grep "${TRAIN}" ${STATFILE} | grep "${TEST}" | awk 'BEGIN { FS = "[, :]+" } {print $14,$15,$16,$17,$19,$2}' | sort -nk5  > ${DATAFILE}
sed "s/__FILE__/${DATAFILE}/g" ${TEMPLATE} > ${DATAFILE}.gnuplot.1
sed "s/__TITLE__/${INFO}/g" ${DATAFILE}.gnuplot.1 > ${DATAFILE}.gnuplot
rm ${DATAFILE}.gnuplot.1
gnuplot ${DATAFILE}.gnuplot
