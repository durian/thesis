#!/bin/sh
#
TRAINBASE="utexas.10e6.dt3"           #no errors
ORIGBASE="utexas.10e6.dt3"            #w/o errors
ORIGTESTFILE="utexas.10e6.dt3.t1e5"   #w/o errors
# LOOP TESTFILE="utexas.10e6.dt3.t1e5.cf081" #with errors, 0.50518%
#
WOPR="/exp2/pberck/wopr_nt/src/wopr"
SCRIPT="do_spcerr.wopr_script"
SCSCRIPT="/exp2/pberck/wopr_nt/etc/check_spelerr2.pl -n"
SPLIT="/exp2/pberck/wopr_nt/etc/split_data_confusibles2.py"
SPLITFILE="goldingroth3.txt"
#
ME="$(basename "$(test -L "$0" && readlink "$0" || echo "$0")")"
CYCLE=`echo ${ME} | tr -dc "0-9"`
I=EX #expert, no-errors
#
LOG="README.${I}.${CYCLE}.txt"
STAT="STAT.${I}.${CYCLE}.txt"
STATCF="STATCF.${I}.${CYCLE}.txt"
CFSET="goldingroth3.txt"
#
for TESTFILE in "utexas.10e6.dt3.t1e5.cf300" "utexas.10e6.dt3.t1e5.cf301" "utexas.10e6.dt3.t1e5.cf302" "utexas.10e6.dt3.t1e5.cf303" "utexas.10e6.dt3.t1e5.cf304" "utexas.10e6.dt3.t1e5.cf305" "utexas.10e6.dt3.t1e5.cf306" "utexas.10e6.dt3.t1e5.cf307" "utexas.10e6.dt3.t1e5.cf308" "utexas.10e6.dt3.t1e5.cf309" "utexas.10e6.dt3.t1e5.cf310"
do
#
# Do spelling correction
#
if [ ! -e "${ORIGTESTFILE}" ]
then
    echo "${ORIGTESTFILE} nor found, aborting."
    exit 1
fi
if [ ! -e "${TESTFILE}" ]
then
    echo "${TESTFILE} nor found, aborting."
    exit 1
fi
#
date "+#%Y-%m-%d %H:%M:%S" >> ${LOG}
#
# it:0 is hardcoded everywhere
T=
PARAMS="mwl:1,mld:10,max_ent:5,max_distr:100,min_ratio:0,max_tf:10000000,triggerfile:${CFSET}"
#
for LINES in 1000000
do
    #Make the datasets here.
    TRAINFILE=${TRAINBASE}.${LINES}
    if [ -e "${TRAINFILE}" ]
    then
	echo "Data file ${TRAINFILE} exists."
    else
	echo "Creating data file ${TRAINFILE}."
	head -n ${LINES} ${TRAINBASE} > ${TRAINFILE}
    fi
    ORIGFILE=${ORIGBASE}.${LINES}
    if [ -e "${ORIGFILE}" ]
    then
	echo "Data file ${ORIGFILE} exists."
    else
	echo "Creating data file ${ORIGFILE}."
	head -n ${LINES} ${ORIGBASE} > ${ORIGFILE}
    fi
    for TIMBL in "-a1 +D" 
    do
	for LC in 2
	do
	    for RC in 2
	    do
		FREE=`df -k . | tail -1 | awk '{print $3}'`
		# 25 577 264 = 25GB
		while [ $FREE -lt 10000000 ]
		do  
		    echo "Space critical, pausing at ${CYCLE}."
		    sleep 60
		    FREE=`df -k . | tail -1 | awk '{print $3}'`
		done
		#
		CYCLESTR=`printf "%05d" ${CYCLE}`
		ID=${I}${CYCLESTR}
		echo ${ID}
		KVS="${ID}.kvs"
		OUT=output.${ID}
		#
		# Window train and testset(s)
		${WOPR} -l -r window_lr -p filename:${ORIGTESTFILE},lc:${LC},rc:${RC},it:0
		${WOPR} -l -r window_lr -p filename:${TESTFILE},lc:${LC},rc:${RC},it:0  	
		${WOPR} -l -r lexicon,window_lr -p filename:${TRAINFILE},lc:${LC},rc:${RC},it:0,targetfile:${ORIGFILE}
		LEX=${TRAINFILE}.lex
		#
		# if exists, skip? Note the extra 
		SCFILE=${TESTFILE}.l${LC}r${RC}_${ID}.sc
		if [ -e "${SCFILE}" ]
		then
		    echo "${SCFILE} exists. Skipping."
		else
		    python ${SPLIT} -f ${TRAINFILE}.l${LC}r${RC} -c ${SPLITFILE} -t "${TIMBL}" -i ${ID} >> ${OUT}
		    
		    #utexas.1000000.cf03.100000.l2r0_EXP01.cs0
		    bash make_ibases_${ID}.sh >> ${OUT}

		    ${WOPR} -l -r mcorrect -p configfile:configfile_${ID}.txt,filename:${TESTFILE}.l${LC}r${RC},timbl:"${TIMBL}",mld:10,id:${ID},${PARAMS},kvsfile:${KVS},lexicon:${LEX} >> ${OUT}
		    
		    #perl /exp2/pberck/wopr/etc/check_spelerr.pl -s utexas_iit.tail1000.cf03.l2r0_EXP01.sc -o utexas_iit.tail1000.l2r0 -t -a
		fi
		CYCLE=$(( $CYCLE + 1 ))
		#
		#reuters.martin.tok.1000.l3t1r2_ALG023.sc
		echo "perl ${SCSCRIPT} -s ${SCFILE} -o ${ORIGTESTFILE}.l${LC}r${RC}" >> ${OUT}
		BLOB=`perl ${SCSCRIPT} -s ${SCFILE} -o ${ORIGTESTFILE}.l${LC}r${RC}`

		#http://wiki.bash-hackers.org/commands/builtin/printf
		#printf -v S "%s %s" ${ID} ${BLOB}
		TSTR=l${LC}r${RC}_"${TIMBL// /}"
		#
		#md5sum testfile
		MD5=${TESTFILE}.l${LC}r${RC}
		#blob: lines errors good_sugg bad_sugg wrong_sugg no_sugg
		#
		# Precision, recall, accuracy
		echo "perl ${SCSCRIPT} -s ${SCFILE} -o ${ORIGTESTFILE}.l${LC}r${RC}" >> ${OUT}
                BLOB=`perl ${SCSCRIPT} -s ${SCFILE} -o ${ORIGTESTFILE}.l${LC}r${RC}`
		echo "${ID} ${TSTR} ${BLOB} ${TRAINFILE} ${LINES} ${TESTFILE}" >> ${STAT}
		#
		echo "perl ${SCSCRIPT} -s ${SCFILE} -o ${ORIGTESTFILE}.l${LC}r${RC} -c ${CFSET}" >> ${OUT}
                BLOB=`perl ${SCSCRIPT} -s ${SCFILE} -o ${ORIGTESTFILE}.l${LC}r${RC} -c ${CFSET}`
		echo "${ID} ${TSTR} ${BLOB} ${TRAINFILE} ${LINES} ${TESTFILE}" >> ${STATCF}
		
	    done
	    #rm ${TESTFILE}.l${LC}r${RC}  #and ORIGTESTFILE?
	done
    done
    #rm ${TRAINFILE}
done
done
