#!/bin/bash
#
# Plots for the EX classifier, general plots without parameters
#
STATFILE0=STATCF.EX.70000.txt
TRAIN=1000000
TEST="cf350"
INFO="Train 1e6, test 1e5, 0.6\%"

# Just the combined plot. Uhm, only 2 points, not a curve

DATAFILE=STATCF_EX_70000_general_all
TEMPLATE=TEMPLATE.EX.GENERAL.ALL.GNUPLOT
grep "${TRAIN}" ${STATFILE0} | grep "${TEST}" | awk 'BEGIN { FS = "[, :]+" } {print $14,$15,$16,$17,$22,$2}' | sort -nk5  > ${DATAFILE}
sed "s/__FILE__/${DATAFILE}/g" ${TEMPLATE} > ${DATAFILE}.gnuplot.1
sed "s/__TITLE__/${INFO}/g" ${DATAFILE}.gnuplot.1 > ${DATAFILE}.gnuplot
rm ${DATAFILE}.gnuplot.1
gnuplot ${DATAFILE}.gnuplot
