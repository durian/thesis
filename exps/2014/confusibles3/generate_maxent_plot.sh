#!/bin/bash
#
DATAFILE=STATCF_ML_26000_maxent
TEMPLATE=TEMPLATE.ML.MAXENT.GNUPLOT
TEST="cf350"
#
#grep "max_ent" STATCF.ML.26*.txt | grep a | awk 'BEGIN { FS = "[, :]+" } {print $17,$32,$2}' > ${DATAFILE}
#with * in filenames, we need to shift the $fields 
grep "max_ent" STATCF.ML.26*.txt | grep ${TEST} |grep a | awk 'BEGIN { FS = "[, :]+" } {print $18,$33,$3}' | sort -nk2  > ${DATAFILE}
#  0.65 1 l2r2_-a1+D
sed "s/__FILE__/${DATAFILE}/g" ${TEMPLATE} > ${DATAFILE}.gnuplot
gnuplot ${DATAFILE}.gnuplot
