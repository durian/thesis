# confidence/precision
#
set style line 1 lt -1 lw 0.5 pi -1 pt 7 ps 0.4
set style line 2 lt -1 lw 0.5 pi -1 pt 6
set style line 3 lt -1 lw 0.5 pi -1 pt 7
set style line 4 lt 2 lw 0.5 pi -1 pt 7 ps 0.4
#set title "F-score vs confidence"
set title "__TITLE__"
set xlabel "confidence"
set ylabel "Precision"
set key bottom
#set grid
set border 3
set xtics .1 out nomirror
set mxtics 2
set ytics out nomirror
set mytics 4
#
#
#
# 45.20 0.23 20.70 0.46 0 l2r2_-a1+D
# acc   pre  rec   fscr c algo
plot [0:1][0:1]\
"< grep 'a1' __FILE__" using 5:2 with lp ls 3 t "a1",\
"< grep 'a4' __FILE__" using 5:2 with lp ls 2 t "a4"
#"< grep 'a1' __FILE__" using 5:($3/100) with lp ls 3 t "a1",\
#"< grep 'a4' __FILE__" using 5:($3/100) with lp ls 2 t "a4"
#
#
set terminal push
#set terminal postscript eps enhanced color solid rounded lw 2 'Helvetica' 10
set terminal postscript eps enhanced dashed rounded lw 2 'Helvetica' 20
set out '__FILE__.ps'
replot
!epstopdf '__FILE__.ps'
set term pop
