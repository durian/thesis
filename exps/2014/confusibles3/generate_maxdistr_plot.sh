#!/bin/bash
#
DATAFILE=STATCF_ML_24000_maxdistr
TEMPLATE=TEMPLATE.ML.MAXDISTR.GNUPLOT
#
#with * in filenames, we need to shift the $fields 
grep "max_distr" STATCF.ML.24000.txt | grep a | awk 'BEGIN { FS = "[, :]+" } {print $17,$34,$2}' | sort -nk2 > ${DATAFILE}
#  0.65 1 l2r2_-a1+D
sed "s/__FILE__/${DATAFILE}/g" ${TEMPLATE} > ${DATAFILE}.gnuplot
gnuplot ${DATAFILE}.gnuplot
