#!/bin/sh
#
TRAINBASE="utexas.10e6.dt3"           #w/o errors
ORIGBASE="utexas.10e6.dt3"            #w/o errors
ORIGTESTFILE="utexas.10e6.dt3.t1e5d"  #w/o errors
#
WOPR="wopr"
SCRIPT="do_spelcorr.wopr_script"
SCSCRIPT="./check_spelerr3.pl -n "
#
ME="$(basename "$(test -L "$0" && readlink "$0" || echo "$0")")"
CYCLE=`echo ${ME} | tr -dc "0-9"`
I=ML
#
LOG="README.${I}.${CYCLE}.txt"
STAT="STAT.${I}.${CYCLE}.txt"
STATCF="STATCF.${I}.${CYCLE}.txt"
CFSET="goldingroth3.txt"
#
for TESTFILE in "utexas.10e6.dt3.t1e5d.cf350" "utexas.10e6.dt3.t1e5d.cf351" "utexas.10e6.dt3.t1e5d.cf352" "utexas.10e6.dt3.t1e5d.cf353" "utexas.10e6.dt3.t1e5d.cf354" "utexas.10e6.dt3.t1e5d.cf355" "utexas.10e6.dt3.t1e5d.cf356" "utexas.10e6.dt3.t1e5d.cf357" "utexas.10e6.dt3.t1e5d.cf358" "utexas.10e6.dt3.t1e5d.cf359" "utexas.10e6.dt3.t1e5d.cf360"
do
#
# Do spelling correction
#
if [ ! -e "${ORIGTESTFILE}" ]
then
    echo "${ORIGTESTFILE} nor found, aborting."
    exit 1
fi
if [ ! -e "${TESTFILE}" ]
then
    echo "${TESTFILE} nor found, aborting."
    exit 1
fi
#
date "+#%Y-%m-%d %H:%M:%S" >> ${LOG}
#
for LPARAMS0 in "max_ent:1" "max_ent:2" "max_ent:3" "max_ent:4" "max_ent:5" "max_ent:6" "max_ent:7" "max_ent:8" "max_ent:9" "max_ent:10" 
do
#
for LPARAMS1 in "max_distr:100" #"max_distr:1" "max_distr:2" "max_distr:3" "max_distr:4" "max_distr:5" "max_distr:6" "max_distr:7" "max_distr:8" "max_distr:9" "max_distr:10" "max_distr:11" "max_distr:12" "max_distr:13" "max_distr:14" "max_distr:15" "max_distr:20" "max_distr:30" "max_distr:40" "max_distr:50" 
do
#
PARAMS="mwl:1,mld:10,min_ratio:0,max_tf:10000000,triggerfile:${CFSET},"${LPARAMS0},${LPARAMS1}
#
for LINES in 1000000
do
    #Make the datasets here.
    TRAINFILE=${TRAINBASE}.${LINES}
    if [ -e "${TRAINFILE}" ]
    then
	echo "Data file ${TRAINFILE} exists."
    else
	echo "Creating data file ${TRAINFILE}."
	head -n ${LINES} ${TRAINBASE} > ${TRAINFILE}
    fi
    for TIMBL in "-a1 +D" "-a4 +D" 
    do
	for LC in 2
	do
	    for RC in 2
	    do
		FREE=`df -k . | tail -1 | awk '{print $3}'`
		# 25 577 264 = 25GB
		while [ $FREE -lt 10000000 ]
		do  
		    echo "Space critical, pausing at ${CYCLE}."
		    sleep 60
		    FREE=`df -k . | tail -1 | awk '{print $3}'`
		done
		# original windowed test set
		${WOPR} -r window_lr -p filename:${ORIGTESTFILE},lc:${LC},rc:${RC}
		CYCLESTR=`printf "%05d" ${CYCLE}`
		ID=${I}${CYCLESTR}
		echo ${ID}
		KVS="${ID}.kvs"
		# if exists PXFILE=${TESTFILE}.l${LC}r${RC}_${ID}.sc, skip?
		SCFILE=${TESTFILE}.l${LC}r${RC}_${ID}.sc
		OUT=output.${ID}
		if [ -e "${SCFILE}" ]
		then
		    echo "${SCFILE} exists. Skipping."
		else
		    echo "#${ID},${TRAINFILE},timbl:'${TIMBL}',lc:${LC},rc:${RC},${PARAMS}" >> ${LOG}
		    echo ${WOPR} -s ${SCRIPT} -p trainfile:${TRAINFILE},rc:${RC},lc:${LC},id:${ID},timbl:${TIMBL},testfile:${TESTFILE},${PARAMS},kvsfile:${KVS}
		    ${WOPR} -s ${SCRIPT} -p trainset:${TRAINFILE},rc:${RC},lc:${LC},id:${ID},timbl:"${TIMBL}",testset:${TESTFILE},${PARAMS},kvsfile:${KVS} >>${OUT}
		fi
		CYCLE=$(( $CYCLE + 1 ))
		#
		#reuters.martin.tok.1000.l3r2_ALG023.sc
		echo "perl ${SCSCRIPT} -s ${SCFILE} -o ${ORIGTESTFILE}.l${LC}r${RC}" >> ${OUT}
		BLOB=`perl ${SCSCRIPT} -s ${SCFILE} -o ${ORIGTESTFILE}.l${LC}r${RC}`

		#http://wiki.bash-hackers.org/commands/builtin/printf
		#printf -v S "%s %s" ${ID} ${BLOB}
		TSTR=l${LC}r${RC}_"${TIMBL// /}"
		#
		#md5sum testfile
		MD5=${TESTFILE}.l${LC}r${RC}
		#blob: lines errors good_sugg bad_sugg wrong_sugg no_sugg
		#
		# Precision, recall, accuracy
		echo "perl ${SCSCRIPT} -s ${SCFILE} -o ${ORIGTESTFILE}.l${LC}r${RC}" >> ${OUT}
                BLOB=`perl ${SCSCRIPT} -s ${SCFILE} -o ${ORIGTESTFILE}.l${LC}r${RC}`
		echo "${ID} ${TSTR} ${BLOB} ${TRAINFILE} ${LINES} ${TESTFILE} ${PARAMS}" >> ${STAT}
		#
		echo "perl ${SCSCRIPT} -s ${SCFILE} -o ${ORIGTESTFILE}.l${LC}r${RC} -c ${CFSET}" >> ${OUT}
                BLOB=`perl ${SCSCRIPT} -s ${SCFILE} -o ${ORIGTESTFILE}.l${LC}r${RC} -c ${CFSET}`
		echo "${ID} ${TSTR} ${BLOB} ${TRAINFILE} ${LINES} ${TESTFILE} ${PARAMS}" >> ${STATCF}
		
	    done
	    #rm ${TESTFILE}.l${LC}r${RC}  #and ORIGTESTFILE?
	done
    done
    #rm ${TRAINFILE}
done
done
done
done
