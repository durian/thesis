#!/bin/sh
#
#LOOP TRAINBASE="utexas.10e6.dt3.cf200"     #with errors, 1%
ORIGBASE="utexas.10e6.dt3"            #w/o errors
ORIGTESTFILE="utexas.10e6.dt3.t1e5d"  #w/o errors
# LOOP TESTFILE="utexas.10e6.dt3.t1e5.cf081" #with errors, 0.50518%
#
WOPR="wopr"
SCRIPT="do_tspcerr.wopr_script"
SCSCRIPT="./check_spelerr3.pl -n "
#
ME="$(basename "$(test -L "$0" && readlink "$0" || echo "$0")")"
CYCLE=`echo ${ME} | tr -dc "0-9"`
I=MLER
#
LOG="README.${I}.${CYCLE}.txt"
STAT="STAT.${I}.${CYCLE}.txt"
STATCF="STATCF.${I}.${CYCLE}.txt"
CFSET="goldingroth3.txt"
#
for TRAINBASE in "utexas.10e6.dt3.cf200" "utexas.10e6.dt3.cf201" "utexas.10e6.dt3.cf202" "utexas.10e6.dt3.cf203" "utexas.10e6.dt3.cf204" "utexas.10e6.dt3.cf205" "utexas.10e6.dt3.cf206" "utexas.10e6.dt3.cf207" "utexas.10e6.dt3.cf208"
do
#
for TESTFILE in "utexas.10e6.dt3.t1e5d.cf350" #"utexas.10e6.dt3.t1e5d.cf351" "utexas.10e6.dt3.t1e5d.cf352" "utexas.10e6.dt3.t1e5d.cf353" "utexas.10e6.dt3.t1e5d.cf354" "utexas.10e6.dt3.t1e5d.cf355" "utexas.10e6.dt3.t1e5d.cf356" "utexas.10e6.dt3.t1e5d.cf357" "utexas.10e6.dt3.t1e5d.cf358" "utexas.10e6.dt3.t1e5d.cf359" "utexas.10e6.dt3.t1e5d.cf360"
do
#
# Do spelling correction
#
if [ ! -e "${TRAINBASE}" ]
then
    echo "${TRAINBASE} nor found, aborting."
    exit 1
fi
if [ ! -e "${ORIGBASE}" ]
then
    echo "${ORIGBASE} nor found, aborting."
    exit 1
fi
if [ ! -e "${ORIGTESTFILE}" ]
then
    echo "${ORIGTESTFILE} nor found, aborting."
    exit 1
fi
if [ ! -e "${TESTFILE}" ]
then
    echo "${TESTFILE} nor found, aborting."
    exit 1
fi
#
date "+#%Y-%m-%d %H:%M:%S" >> ${LOG}
#
# it:1 is hardcoded everywhere
T="t1"
for LPARAMS in "confidence:0.0" "confidence:0.1" "confidence:0.2" "confidence:0.3" "confidence:0.4" 
do
PARAMS="max_ent:5,max_distr:100,triggerfile:${CFSET},"${LPARAMS}
#
for LINES in 1000000 
do
    #Make the datasets here.
    TRAINFILE=${TRAINBASE}.${LINES}
    if [ -e "${TRAINFILE}" ]
    then
	echo "Data file ${TRAINFILE} exists."
    else
	echo "Creating data file ${TRAINFILE}."
	head -n ${LINES} ${TRAINBASE} > ${TRAINFILE}
    fi
    ORIGFILE=${ORIGBASE}.${LINES}
    if [ -e "${ORIGFILE}" ]
    then
	echo "Data file ${ORIGFILE} exists."
    else
	echo "Creating data file ${ORIGFILE}."
	head -n ${LINES} ${ORIGBASE} > ${ORIGFILE}
    fi
    #
    echo "ORIGFILE ${ORIGFILE}"
    echo "TRAINFILE ${TRAINFILE}"
    echo "ORIGTESTFILE ${ORIGTESTFILE}"
    echo "TESTFILE ${TESTFILE}"
    #
    for TIMBL in "-a1 +D" "-a4 +D"
    do
	for LC in 2 
	do
	    for RC in 2
	    do
		FREE=`df -k . | tail -1 | awk '{print $3}'`
		# 25 577 264 = 25GB
		while [ $FREE -lt 10000000 ]
		do  
		    echo "Space critical, pausing at ${CYCLE}."
		    sleep 60
		    FREE=`df -k . | tail -1 | awk '{print $3}'`
		done
		# original windowed test set. Not in script.
		${WOPR} -r window_lr -p filename:${ORIGTESTFILE},lc:${LC},rc:${RC},it:1 
		#${WOPR} -r window_lr -p filename:${TESTFILE},lc:${LC},rc:${RC},it:1 
		CYCLESTR=`printf "%05d" ${CYCLE}`
		ID=${I}${CYCLESTR}
		echo ${ID}
		KVS="${ID}.kvs"
		# if exists, skip? Note the extra t1
		SCFILE=${TESTFILE}.l${LC}${T}r${RC}_${ID}.sc
		OUT=output.${ID}
		if [ -e "${SCFILE}" ]
		then
		    echo "${SCFILE} exists. Skipping."
		else
		    echo "#${ID},${TRAINFILE},timbl:'${TIMBL}',lc:${LC},rc:${RC}" >> ${LOG}
		    echo ${WOPR} -s ${SCRIPT} -p trainset:${TRAINFILE},rc:${RC},lc:${LC},id:${ID},timbl:"${TIMBL}",testset:${TESTFILE},origtrain:${ORIGFILE},it:1,${PARAMS},kvsfile:${KVS}
		    ${WOPR} -s ${SCRIPT} -p trainset:${TRAINFILE},rc:${RC},lc:${LC},id:${ID},timbl:"${TIMBL}",testset:${TESTFILE},origtrain:${ORIGFILE},it:1,${PARAMS},kvsfile:${KVS} >>${OUT}
		fi
		CYCLE=$(( $CYCLE + 1 ))
		#
		#reuters.martin.tok.1000.l3t1r2_ALG023.sc
		echo "perl ${SCSCRIPT} -s ${SCFILE} -o ${ORIGTESTFILE}.l${LC}${T}r${RC}" >> ${OUT}
		BLOB=`perl ${SCSCRIPT} -s ${SCFILE} -o ${ORIGTESTFILE}.l${LC}${T}r${RC}`
		#http://wiki.bash-hackers.org/commands/builtin/printf
		#printf -v S "%s %s" ${ID} ${BLOB}
		TSTR=l${LC}${T}r${RC}_"${TIMBL// /}"
		#
		#md5sum testfile
		MD5=${TESTFILE}.l${LC}${T}r${RC}
		#blob: lines errors good_sugg bad_sugg wrong_sugg no_sugg
		#
		# Precision, recall, accuracy
		echo "perl ${SCSCRIPT} -s ${SCFILE} -o ${ORIGTESTFILE}.l${LC}${T}r${RC}" >> ${OUT}
                BLOB=`perl ${SCSCRIPT} -s ${SCFILE} -o ${ORIGTESTFILE}.l${LC}${T}r${RC}`
		echo "${ID} ${TSTR} ${BLOB} ${TRAINFILE} ${LINES} ${TESTFILE} ${LPARAMS}" >> ${STAT}
		#
		echo "perl ${SCSCRIPT} -s ${SCFILE} -o ${ORIGTESTFILE}.l${LC}${T}r${RC} -c ${CFSET}" >> ${OUT}
                BLOB=`perl ${SCSCRIPT} -s ${SCFILE} -o ${ORIGTESTFILE}.l${LC}${T}r${RC} -c ${CFSET}`
		echo "${ID} ${TSTR} ${BLOB} ${TRAINFILE} ${LINES} ${TESTFILE} ${LPARAMS}" >> ${STATCF}
		
	    done
	    #rm ${TESTFILE}.l${LC}r${RC}  #and ORIGTESTFILE?
	done
    done
    #rm ${TRAINFILE}
done
done
done
done
