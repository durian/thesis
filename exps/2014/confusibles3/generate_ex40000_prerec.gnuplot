#
# Use directly in gnuplot
#
set style line 1 lt -1 lw 0.5 pi -1 pt 7 ps 1.0
set style line 2 lt -1 lw 0.5 pi -1 pt 6
set style line 3 lt -1 lw 0.5 pi -1 pt 7
set style line 4 lt 2 lw 0.5 pi -1 pt 7 ps 0.4
#set title "Precision versus Recall, 1e6"
set ylabel "Precision"
set xlabel "Recall"
set key bottom
#set grid
set border 3
set xtics out nomirror
set mxtics 2
set ytics out nomirror
set mytics 2
#
##set label "0.95, 0.99" at first 40.4, first 68
##set label "0-0.5" at first 51, first 67
#
# l2t1r2 a1 99.53 69.40 40.97 51.52 confidence 0 EX40000 2000000 227 0.61
# l2t1r2 a1 99.53 69.40 40.97 51.52 confidence 0.1 EX40006 2000000 227 0.61
#
plot [10:100][0:30]\
"< grep a4 STATCF.EX.40000.txt | grep cf350 | awk -f p2txt.awk" using 5:4 with lp ls 1 notitle,\
"< grep a4 STATCF.EX.40000.txt | grep cf350 |egrep -v '(0$|0.1$|0.2$|0.3$|0.4$|0.5$|0.99$)'  | awk -f p2txt.awk" using 5:4:8 with labels offset 2,-0.8 notitle,\
"< grep a1 STATCF.EX.40000.txt | grep cf350 | awk -f p2txt.awk" using 5:4 with lp ls 2 notitle,\
"< grep a1 STATCF.EX.40000.txt | grep cf350 |egrep -v '(0$|0.1$|0.2$|0.3$|0.4$|0.5$|0.99$)'  | awk -f p2txt.awk" using 5:4:8 with labels offset 2,-0.8 notitle
#
set terminal push
#set terminal postscript eps enhanced color solid rounded lw 2 'Helvetica' 10
set terminal postscript eps enhanced dashed rounded lw 2 'Helvetica' 20
set out 'EX40000_pre_rec.ps'
replot
!epstopdf 'EX40000_pre_rec.ps'
set term pop
