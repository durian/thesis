#!/bin/bash
#
# Plots for the MLER classifier.
# MLER84000 is between 0.0-2.0/0.1
#
STATFILE0=STATCF.MLER.80000.txt
STATFILE1=STATCF.MLER.84000.txt
TRAIN="cf202" #
TEST="cf350"
INFO="Train 1e6, 5\% errors, test 1e5, 0.6\% errors"

DATAFILE=STATCF_MLER_80000_maxent_fsc #same data in all three actually
TEMPLATE=TEMPLATE.MLER.MAXENT.FSC.GNUPLOT
#
#with * in filenames, we need to shift the $fields 
# we can skip 0, 1 and 2 form STATFILE0 because included in STATFILE1
#
grep "${TRAIN}" ${STATFILE1} | grep ${TEST} | egrep -v "max_ent:0$" | awk 'BEGIN { FS = "[, :]+" } {print $14,$15,$16,$17,$26,$2}' | sort -nk5 > ${DATAFILE}
grep "${TRAIN}" ${STATFILE0} | grep ${TEST} | egrep -v "max_ent:[012]$" | awk 'BEGIN { FS = "[, :]+" } {print $14,$15,$16,$17,$26,$2}' | sort -nk5 >> ${DATAFILE}
#
# 45.20 0.23 20.70 0.46 0      l2r2_-a1+D
# acc   pre  rec   fscr maxent algo
#
sed "s/__FILE__/${DATAFILE}/g" ${TEMPLATE} > ${DATAFILE}.gnuplot.1
sed "s/__TITLE__/${INFO}/g" ${DATAFILE}.gnuplot.1 > ${DATAFILE}.gnuplot
rm ${DATAFILE}.gnuplot.1
gnuplot ${DATAFILE}.gnuplot

#
DATAFILE=STATCF_MLER_80000_maxent_rec #same data in all three actually
TEMPLATE=TEMPLATE.MLER.MAXENT.REC.GNUPLOT
#
#with same datafile, do not need to create again really
#
grep "${TRAIN}" ${STATFILE1} | grep ${TEST} | egrep -v "max_ent:0$" | awk 'BEGIN { FS = "[, :]+" } {print $14,$15,$16,$17,$26,$2}' | sort -nk5 > ${DATAFILE}
grep "${TRAIN}" ${STATFILE0} | grep ${TEST} | egrep -v "max_ent:[012]$" | awk 'BEGIN { FS = "[, :]+" } {print $14,$15,$16,$17,$26,$2}' | sort -nk5 >> ${DATAFILE}
#
sed "s/__FILE__/${DATAFILE}/g" ${TEMPLATE} > ${DATAFILE}.gnuplot.1
sed "s/__TITLE__/${INFO}/g" ${DATAFILE}.gnuplot.1 > ${DATAFILE}.gnuplot
rm ${DATAFILE}.gnuplot.1
gnuplot ${DATAFILE}.gnuplot

#

DATAFILE=STATCF_MLER_80000_maxent_pre #same data in all three actually
TEMPLATE=TEMPLATE.MLER.MAXENT.PRE.GNUPLOT
#
#with same datafile, do not need to create again really
#
grep "${TRAIN}" ${STATFILE1} | grep ${TEST} | egrep -v "max_ent:0$" | awk 'BEGIN { FS = "[, :]+" } {print $14,$15,$16,$17,$26,$2}' | sort -nk5 > ${DATAFILE}
grep "${TRAIN}" ${STATFILE0} | grep ${TEST} | egrep -v "max_ent:[012]$" | awk 'BEGIN { FS = "[, :]+" } {print $14,$15,$16,$17,$26,$2}' | sort -nk5 >> ${DATAFILE}
#
#
sed "s/__FILE__/${DATAFILE}/g" ${TEMPLATE} > ${DATAFILE}.gnuplot.1
sed "s/__TITLE__/${INFO}/g" ${DATAFILE}.gnuplot.1 > ${DATAFILE}.gnuplot
rm ${DATAFILE}.gnuplot.1
gnuplot ${DATAFILE}.gnuplot

# ALL

DATAFILE=STATCF_MLER_80000_maxent_all #same data in all three actually
TEMPLATE=TEMPLATE.MLER.MAXENT.ALL.GNUPLOT
grep "${TRAIN}" ${STATFILE1} | grep ${TEST} | egrep -v "max_ent:0$" | awk 'BEGIN { FS = "[, :]+" } {print $14,$15,$16,$17,$26,$2}' | sort -nk5 > ${DATAFILE}
grep "${TRAIN}" ${STATFILE0} | grep ${TEST} | egrep -v "max_ent:[012]$" | awk 'BEGIN { FS = "[, :]+" } {print $14,$15,$16,$17,$26,$2}' | sort -nk5 >> ${DATAFILE}
sed "s/__FILE__/${DATAFILE}/g" ${TEMPLATE} > ${DATAFILE}.gnuplot.1
sed "s/__TITLE__/${INFO}/g" ${DATAFILE}.gnuplot.1 > ${DATAFILE}.gnuplot
rm ${DATAFILE}.gnuplot.1
gnuplot ${DATAFILE}.gnuplot