#!/bin/sh
#
#LOOP TRAINBASE="utexas.10e6.dt3.cf200"     #with errors
ORIGBASE="utexas.10e6.dt3"            #w/o errors
ORIGTESTFILE="utexas.10e6.dt3.t1e5"   #w/o errors
# LOOP TESTFILE="utexas.10e6.dt3.t1e5.cf081" #with errors, 0.50518%
#
WOPR="/exp2/pberck/wopr_nt/src/wopr"
SCRIPT="do_spcerr.wopr_script"
SCSCRIPT="/exp2/pberck/wopr_nt/etc/check_spelerr3.pl -n "
#
ME="$(basename "$(test -L "$0" && readlink "$0" || echo "$0")")"
CYCLE=`echo ${ME} | tr -dc "0-9"`
I=MLER
#
LOG="README.${I}.${CYCLE}.txt"
STAT="STAT.${I}.${CYCLE}.txt"
STATCF="STATCF.${I}.${CYCLE}.txt"
CFSET="goldingroth3.txt"
#
for TRAINBASE in "utexas.10e6.dt3.cf200" "utexas.10e6.dt3.cf201" "utexas.10e6.dt3.cf202" "utexas.10e6.dt3.cf203" "utexas.10e6.dt3.cf204" "utexas.10e6.dt3.cf205" "utexas.10e6.dt3.cf206" "utexas.10e6.dt3.cf207" "utexas.10e6.dt3.cf208"
do
#
for TESTFILE in "utexas.10e6.dt3.t1e5.cf300" "utexas.10e6.dt3.t1e5.cf301" "utexas.10e6.dt3.t1e5.cf302" "utexas.10e6.dt3.t1e5.cf303" "utexas.10e6.dt3.t1e5.cf304" "utexas.10e6.dt3.t1e5.cf305" "utexas.10e6.dt3.t1e5.cf306" "utexas.10e6.dt3.t1e5.cf307" "utexas.10e6.dt3.t1e5.cf308" "utexas.10e6.dt3.t1e5.cf309" "utexas.10e6.dt3.t1e5.cf310"
do
#
# Do spelling correction
#
if [ ! -e "${TRAINBASE}" ]
then
    echo "${TRAINBASE} nor found, aborting."
    exit 1
fi
if [ ! -e "${ORIGBASE}" ]
then
    echo "${ORIGBASE} nor found, aborting."
    exit 1
fi
if [ ! -e "${ORIGTESTFILE}" ]
then
    echo "${ORIGTESTFILE} nor found, aborting."
    exit 1
fi
if [ ! -e "${TESTFILE}" ]
then
    echo "${TESTFILE} nor found, aborting."
    exit 1
fi
#
date "+#%Y-%m-%d %H:%M:%S" >> ${LOG}
#
# it:1 is hardcoded everywhere
T="t1"
#
for LPARAMS in "max_distr:1" "max_distr:2" "max_distr:3" "max_distr:4" "max_distr:5" "max_distr:6" "max_distr:7" "max_distr:8" "max_distr:9" "max_distr:10" "max_distr:11" "max_distr:12" "max_distr:13" "max_distr:14" "max_distr:15" "max_distr:20" "max_distr:30" "max_distr:40" "max_distr:50" 
do
PARAMS="mwl:1,mld:10,max_ent:5,min_ratio:0,max_tf:10000000,triggerfile:${CFSET},"${LPARAMS}
#
for LINES in 1000000 
do
    #Make the datasets here.
    TRAINFILE=${TRAINBASE}.${LINES}
    if [ -e "${TRAINFILE}" ]
    then
	echo "Data file ${TRAINFILE} exists."
    else
	echo "Creating data file ${TRAINFILE}."
	head -n ${LINES} ${TRAINBASE} > ${TRAINFILE}
    fi
    ORIGFILE=${ORIGBASE}.${LINES}
    if [ -e "${ORIGFILE}" ]
    then
	echo "Data file ${ORIGFILE} exists."
    else
	echo "Creating data file ${ORIGFILE}."
	head -n ${LINES} ${ORIGBASE} > ${ORIGFILE}
    fi
    #
    echo "ORIGFILE ${ORIGFILE}"
    echo "TRAINFILE ${TRAINFILE}"
    echo "ORIGTESTFILE ${ORIGTESTFILE}"
    echo "TESTFILE ${TESTFILE}"
    #
    for TIMBL in "-a1 +D" 
    do
	for LC in 2 
	do
	    for RC in 2
	    do
		FREE=`df -k . | tail -1 | awk '{print $3}'`
		# 25 577 264 = 25GB
		while [ $FREE -lt 10000000 ]
		do  
		    echo "Space critical, pausing at ${CYCLE}."
		    sleep 60
		    FREE=`df -k . | tail -1 | awk '{print $3}'`
		done
		# original windowed test set. Not in script.
		${WOPR} -r window_lr -p filename:${ORIGTESTFILE},lc:${LC},rc:${RC},it:1 
		#${WOPR} -r window_lr -p filename:${TESTFILE},lc:${LC},rc:${RC},it:1 
		CYCLESTR=`printf "%05d" ${CYCLE}`
		ID=${I}${CYCLESTR}
		echo ${ID}
		KVS="${ID}.kvs"
		# if exists, skip? Note the extra t1
		SCFILE=${TESTFILE}.l${LC}${T}r${RC}_${ID}.sc
		OUT=output.${ID}
		if [ -e "${SCFILE}" ]
		then
		    echo "${SCFILE} exists. Skipping."
		else
		    echo "#${ID},${TRAINFILE},timbl:'${TIMBL}',lc:${LC},rc:${RC}" >> ${LOG}
		    echo ${WOPR} -s ${SCRIPT} -p trainset:${TRAINFILE},rc:${RC},lc:${LC},id:${ID},timbl:"${TIMBL}",testset:${TESTFILE},origtrain:${ORIGFILE},it:1,${PARAMS},kvsfile:${KVS}
		    ${WOPR} -s ${SCRIPT} -p trainset:${TRAINFILE},rc:${RC},lc:${LC},id:${ID},timbl:"${TIMBL}",testset:${TESTFILE},origtrain:${ORIGFILE},it:1,${PARAMS},kvsfile:${KVS} >>${OUT}
		fi
		CYCLE=$(( $CYCLE + 1 ))
		#
		#reuters.martin.tok.1000.l3t1r2_ALG023.sc
		echo "perl ${SCSCRIPT} -s ${SCFILE} -o ${ORIGTESTFILE}.l${LC}${T}r${RC}" >> ${OUT}
		BLOB=`perl ${SCSCRIPT} -s ${SCFILE} -o ${ORIGTESTFILE}.l${LC}${T}r${RC}`
		#http://wiki.bash-hackers.org/commands/builtin/printf
		#printf -v S "%s %s" ${ID} ${BLOB}
		TSTR=l${LC}${T}r${RC}_"${TIMBL// /}"
		#
		#md5sum testfile
		MD5=${TESTFILE}.l${LC}${T}r${RC}
		#blob: lines errors good_sugg bad_sugg wrong_sugg no_sugg
		#
		# Precision, recall, accuracy
		echo "perl ${SCSCRIPT} -s ${SCFILE} -o ${ORIGTESTFILE}.l${LC}${T}r${RC}" >> ${OUT}
                BLOB=`perl ${SCSCRIPT} -s ${SCFILE} -o ${ORIGTESTFILE}.l${LC}${T}r${RC}`
		echo "${ID} ${TSTR} ${BLOB} ${TRAINFILE} ${LINES} ${TESTFILE} ${LPARAMS}" >> ${STAT}
		#
		echo "perl ${SCSCRIPT} -s ${SCFILE} -o ${ORIGTESTFILE}.l${LC}${T}r${RC} -c ${CFSET}" >> ${OUT}
                BLOB=`perl ${SCSCRIPT} -s ${SCFILE} -o ${ORIGTESTFILE}.l${LC}${T}r${RC} -c ${CFSET}`
		echo "${ID} ${TSTR} ${BLOB} ${TRAINFILE} ${LINES} ${TESTFILE} ${LPARAMS}" >> ${STATCF}
		
	    done
	    #rm ${TESTFILE}.l${LC}r${RC}  #and ORIGTESTFILE?
	done
    done
    #rm ${TRAINFILE}
done
done
done
done
