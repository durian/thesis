#!/bin/bash
#
# generates precision versus recall plots, generates the gnuplot file from the TEMPLATE
#
for FILE in "STATCF.ML.50000" "STATCF.ML.70000"
do
	for TESTSET in "t1e5d.cf350"
	do
		for ALG in "a1" "a4"
		do
			for CTX in "l2r2"
			do
				DATAFILE="${FILE}.${ALG}.${CTX}.${TESTSET}.data"
				echo -n "grep ${TESTSET} ${FILE} | grep ${ALG} | grep ${CTX} \> ${DATAFILE}"
				grep ${TESTSET} ${FILE}.txt | grep ${ALG} | grep ${CTX} > ${DATAFILE}
				if [ -s ${DATAFILE} ]; then
					echo " OKAY"
					sed "s/__FILE__/${DATAFILE}/g" TEMPLATE.ML.GNUPLOT > ${DATAFILE}.gnuplot
					gnuplot ${DATAFILE}.gnuplot
					#open ${DATAFILE}.pdf
				else
					echo " EMPTY"
				fi
			done
		done
	done
done

