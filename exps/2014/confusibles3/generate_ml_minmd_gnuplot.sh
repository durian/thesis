#!/bin/bash
#
# Plots for the ML classifier.
#
STATFILE1=STATCF.ML.70000.txt #igtree
STATFILE4=STATCF.ML.71000.txt #tribl2
TRAIN="utexas.10e6.dt3.1000000"
TEST="cf350"
INFO="Train 1e6, test 1e5, 0.6\%"

# min_md setting in wopr is >= min_md

DATAFILE=STATCF_ML_70000_minmd_fsc #same data in all three actually
TEMPLATE=TEMPLATE.ML.MINMD.FSC.GNUPLOT
#
#with * in filenames, we need to shift the $fields 
#
#ML70000 l2r2_-a1+D 2522289  37052    227    47   110    70 20125    47 20125   180 16700 45.20  0.23 20.70  0.46 utexas.10e6.dt3.1000000 1000000 utexas.10e6.dt3.t1e5d.cf350 confidence:0 min_md:0
# $24 in the awk line is the min_md:0 value
grep "${TRAIN}" ${STATFILE1} | grep "${TEST}" | grep "confidence:0 " | awk 'BEGIN { FS = "[, :]+" } {print $14,$15,$16,$17,$24,$2}' | sort -nk5  > ${DATAFILE}
grep "${TRAIN}" ${STATFILE4} | grep "${TEST}" | grep "confidence:0 " | awk 'BEGIN { FS = "[, :]+" } {print $14,$15,$16,$17,$24,$2}' | sort -nk5  >> ${DATAFILE}
#
# 45.20 0.23 20.70 0.46 0 l2r2_-a1+D
# acc   pre  rec   fscr c algo
#
sed "s/__FILE__/${DATAFILE}/g" ${TEMPLATE} > ${DATAFILE}.gnuplot.1
sed "s/__TITLE__/${INFO}/g" ${DATAFILE}.gnuplot.1 > ${DATAFILE}.gnuplot
rm ${DATAFILE}.gnuplot.1
gnuplot ${DATAFILE}.gnuplot

#

DATAFILE=STATCF_ML_70000_minmd_rec
TEMPLATE=TEMPLATE.ML.MINMD.REC.GNUPLOT
#
#with same datafile, do not need to create again really
#
grep "${TRAIN}" ${STATFILE1} | grep "${TEST}" | grep "confidence:0 " | awk 'BEGIN { FS = "[, :]+" } {print $14,$15,$16,$17,$24,$2}' | sort -nk5  > ${DATAFILE}
grep "${TRAIN}" ${STATFILE4} | grep "${TEST}" | grep "confidence:0 " | awk 'BEGIN { FS = "[, :]+" } {print $14,$15,$16,$17,$24,$2}' | sort -nk5  >> ${DATAFILE}
#
# 45.20 0.23 20.70 0.46 0 l2r2_-a1+D
# acc   pre  rec   fscr c algo
#
sed "s/__FILE__/${DATAFILE}/g" ${TEMPLATE} > ${DATAFILE}.gnuplot.1
sed "s/__TITLE__/${INFO}/g" ${DATAFILE}.gnuplot.1 > ${DATAFILE}.gnuplot
rm ${DATAFILE}.gnuplot.1
gnuplot ${DATAFILE}.gnuplot

#

DATAFILE=STATCF_ML_70000_minmd_prec
TEMPLATE=TEMPLATE.ML.MINMD.PRE.GNUPLOT
#
#with * in filenames, we need to shift the $fields 
#
grep "${TRAIN}" ${STATFILE1} | grep "${TEST}" | grep "confidence:0 " | awk 'BEGIN { FS = "[, :]+" } {print $14,$15,$16,$17,$24,$2}' | sort -nk5  > ${DATAFILE}
grep "${TRAIN}" ${STATFILE4} | grep "${TEST}" | grep "confidence:0 " | awk 'BEGIN { FS = "[, :]+" } {print $14,$15,$16,$17,$24,$2}' | sort -nk5  >> ${DATAFILE}
#
# 45.20 0.23 20.70 0.46 0 l2r2_-a1+D
# acc   pre  rec   fscr c algo
#
sed "s/__FILE__/${DATAFILE}/g" ${TEMPLATE} > ${DATAFILE}.gnuplot.1
sed "s/__TITLE__/${INFO}/g" ${DATAFILE}.gnuplot.1 > ${DATAFILE}.gnuplot
rm ${DATAFILE}.gnuplot.1
gnuplot ${DATAFILE}.gnuplot
