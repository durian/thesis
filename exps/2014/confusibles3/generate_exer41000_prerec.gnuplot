#
# Use directly in gnuplot
#
set style line 1 lt -1 lw 0.5 pi -1 pt 7 ps 1.0
set style line 2 lt -1 lw 0.5 pi -1 pt 6
set style line 3 lt -1 lw 0.5 pi -1 pt 7
set style line 4 lt 2 lw 0.5 pi -1 pt 7 ps 0.4
set title "Precision versus Recall, 2e6"
set ylabel "Precision"
set xlabel "Recall"
set key bottom
#set grid
set border 3
set xtics out nomirror
set mxtics 4
set ytics out nomirror
set mytics 4
#
#grep a1 STATCF.EXER.41000.txt | grep cf350 | grep cf200| grep " 2000000 " |awk -f p2txt.awk 
# l2t1r2 a1 99.53 69.40 40.97 51.52 confidence 0 EXER41000 2000000 227 0.61
# l2t1r2 a1 99.53 69.40 40.97 51.52 confidence 0.1 EXER41006 2000000 227 0.61
#
plot [40:60][60:70]\
"< grep a4 STATCF.EXER.41000.txt | grep cf350 | grep cf200| grep 2000000 |awk -f p2txt.awk" using 5:4 with p ls 1 notitle,\
"< grep a4 STATCF.EXER.41000.txt | grep cf350 | grep cf200| grep 2000000 |awk -f p2txt.awk" using 5:4:8 with labels offset 2 notitle
#
#"bla"  using 5:4 with lp ls 1 t "P v R"
set terminal push
#set terminal postscript eps enhanced color solid rounded lw 2 'Helvetica' 10
set terminal postscript eps enhanced dashed rounded lw 2 'Helvetica' 20
set out 'EXER41000_pre_rec.ps'
replot
!epstopdf 'EXER41000_pre_rec.ps'
set term pop
