#!/bin/sh
#
# 2013, calculate ROC
#
TRAINBASE="/exp2/pberck/nyt.3e7"
#
WOPR="/exp2/pberck/wopr/wopr"
SCRIPT="do_algos.wopr_script" 
ROCSCRIPT="/exp2/pberck/wopr/etc/roc.py"
#
CYCLE=10000
LOG="README.ROC.${CYCLE}.txt"
PLOT="DATA.ROC.${CYCLE}.plot"
#
date "+#%Y-%m-%d %H:%M:%S" >> ${LOG}
#
BLOB=`${WOPR} | head -n1` #09:30:51.01: Starting wopr 1.35.5
#http://www.arachnoid.com/linux/shell_programming.html
STR=${BLOB#*wopr }
RX='([0123456789.]*)\.([0123456789.]*)'
if [[ "$STR" =~ $RX ]]
then
    WV=${BASH_REMATCH[1]}
    #echo ${WV}
    if [[ "${WV}" < "1.35" ]]
    then
        echo "NO UPTODATE WOPR VERSION."
        exit 1
    fi
fi
#
TLINES=1000
#
# Test file. Create on the fly if necessary
#
TESTFILE=nyt.t${TLINES}
if [ -e "${TESTFILE}" ]
then
    echo "Data file ${TESTFILE} exists."
else
    echo "Creating data file ${TESTFILE}."
    tail -n ${TLINES} ${TRAINBASE} > ${TESTFILE}
fi
#
for LINES in 1000 10000 
do
    #Make the dataset here.
    TRAINFILE=${TRAINBASE}.${LINES}
    if [ -e "${TRAINFILE}" ]
    then
	echo "Data file ${TRAINFILE} exists."
    else
	echo "Creating data file ${TRAINFILE}."
	head -n ${LINES} ${TRAINBASE} > ${TRAINFILE}
    fi
    for TIMBL in "-a1 +D" "-a4 +D"
    do
	for LC in 2 3
	do
	    for RC in 0 
	    do
		FREE=`df -k . | tail -1 | awk '{print $3}'`
		# 25 577 264 = 25GB
		while [ $FREE -lt 10000000 ]
		do  
		    echo "Space critical, pausing at ${CYCLE}."
		    sleep 60
		    FREE=`df -k . | tail -1 | awk '{print $3}'`
		done
		CYCLESTR=`printf "%05d" ${CYCLE}`
		ID=ROC${CYCLESTR}
		echo ${ID}
		# if exists PXFILE=${TESTFILE}.l${LC}r${RC}_${ID}.px, skip?
		PXFILE=${TESTFILE}.l${LC}r${RC}_${ID}.px
		if [ -e "${PXFILE}" ]
		then
		    echo "${PXFILE} exists. Skipping."
		else
		    echo "#${ID},${TRAINFILE},timbl:'${TIMBL}',lc:${LC},rc:${RC}" >> ${LOG}
		    echo ${WOPR} -s ${SCRIPT} -p trainfile:${TRAINFILE},rc:${RC},lc:${LC},id:${ID},timbl:${TIMBL},testfile:${TESTFILE}
		    ${WOPR} -s ${SCRIPT} -p trainset:${TRAINFILE},rc:${RC},lc:${LC},id:${ID},timbl:"${TIMBL}",testset:${TESTFILE} > output.${ID}
		fi
		CYCLE=$(( $CYCLE + 1 ))
		#
		#reuters.martin.tok.1000.l3r2_ALG023.px
		PXFILE=${TESTFILE}.l${LC}r${RC}_${ID}.px
		LEXFILE=${TRAINFILE}.lex
		ROCFILE=${PXFILE}.roc
		python ${ROCSCRIPT} -f ${PXFILE} -l ${LC} -r ${RC} > ${ROCFILE}
	    done
	done
    done
done

