#!/bin/bash
#
# Plots for the MLER classifier.
#
STATFILE0=STATCF.MLER.53000.txt
STATFILE1=STATCF.MLER.50000.txt
TRAIN="cf200"
TEST="cf350"
INFO="Train 1e6, 1.1\%, test 1e5, 0.6\%"

#

DATAFILE=STATCF_MLER_50000_conf_fsc #same data in all three actually
TEMPLATE=TEMPLATE.MLER.CONFIDENCE.FSC.GNUPLOT
#
#with * in filenames, we need to shift the $fields 
#
grep "${TRAIN}" ${STATFILE0} | grep "${TEST}" | awk 'BEGIN { FS = "[, :]+" } {print $14,$15,$16,$17,$22,$2}' | sort -nk5  > ${DATAFILE}
grep "${TRAIN}" ${STATFILE1} | grep "${TEST}" | awk 'BEGIN { FS = "[, :]+" } {print $14,$15,$16,$17,$22,$2}' | sort -nk5  >> ${DATAFILE}
#
# 45.20 0.23 20.70 0.46 0 l2r2_-a1+D
# acc   pre  rec   fscr c algo
#
sed "s/__FILE__/${DATAFILE}/g" ${TEMPLATE} > ${DATAFILE}.gnuplot.1
sed "s/__TITLE__/${INFO}/g" ${DATAFILE}.gnuplot.1 > ${DATAFILE}.gnuplot
rm ${DATAFILE}.gnuplot.1
gnuplot ${DATAFILE}.gnuplot

#

DATAFILE=STATCF_MLER_50000_conf_rec
TEMPLATE=TEMPLATE.MLER.CONFIDENCE.REC.GNUPLOT
#
#with * in filenames, we need to shift the $fields 
#
grep "${TRAIN}" ${STATFILE0} | grep "${TEST}" | awk 'BEGIN { FS = "[, :]+" } {print $14,$15,$16,$17,$22,$2}' | sort -nk5  > ${DATAFILE}
grep "${TRAIN}" ${STATFILE1} | grep "${TEST}" | awk 'BEGIN { FS = "[, :]+" } {print $14,$15,$16,$17,$22,$2}' | sort -nk5  >> ${DATAFILE}
#
# 45.20 0.23 20.70 0.46 0 l2r2_-a1+D
# acc   pre  rec   fscr c algo
#
sed "s/__FILE__/${DATAFILE}/g" ${TEMPLATE} > ${DATAFILE}.gnuplot.1
sed "s/__TITLE__/${INFO}/g" ${DATAFILE}.gnuplot.1 > ${DATAFILE}.gnuplot
rm ${DATAFILE}.gnuplot.1
gnuplot ${DATAFILE}.gnuplot

#

DATAFILE=STATCF_MLER_50000_conf_pre
TEMPLATE=TEMPLATE.MLER.CONFIDENCE.PRE.GNUPLOT
#
#with * in filenames, we need to shift the $fields 
#
grep "${TRAIN}" ${STATFILE0} | grep "${TEST}" | awk 'BEGIN { FS = "[, :]+" } {print $14,$15,$16,$17,$22,$2}' | sort -nk5  > ${DATAFILE}
grep "${TRAIN}" ${STATFILE1} | grep "${TEST}" | awk 'BEGIN { FS = "[, :]+" } {print $14,$15,$16,$17,$22,$2}' | sort -nk5  >> ${DATAFILE}
#
# 45.20 0.23 20.70 0.46 0 l2r2_-a1+D
# acc   pre  rec   fscr c algo
#
sed "s/__FILE__/${DATAFILE}/g" ${TEMPLATE} > ${DATAFILE}.gnuplot.1
sed "s/__TITLE__/${INFO}/g" ${DATAFILE}.gnuplot.1 > ${DATAFILE}.gnuplot
rm ${DATAFILE}.gnuplot.1
gnuplot ${DATAFILE}.gnuplot

# ALL

DATAFILE=STATCF_MLER_50000_conf_all
TEMPLATE=TEMPLATE.MLER.CONFIDENCE.ALL.GNUPLOT
grep "${TRAIN}" ${STATFILE0} | grep "${TEST}" | awk 'BEGIN { FS = "[, :]+" } {print $14,$15,$16,$17,$22,$2}' | sort -nk5  > ${DATAFILE}
grep "${TRAIN}" ${STATFILE1} | grep "${TEST}" | awk 'BEGIN { FS = "[, :]+" } {print $14,$15,$16,$17,$22,$2}' | sort -nk5  >> ${DATAFILE}
sed "s/__FILE__/${DATAFILE}/g" ${TEMPLATE} > ${DATAFILE}.gnuplot.1
sed "s/__TITLE__/${INFO}/g" ${DATAFILE}.gnuplot.1 > ${DATAFILE}.gnuplot
rm ${DATAFILE}.gnuplot.1
gnuplot ${DATAFILE}.gnuplot
