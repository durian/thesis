#!/usr/bin/python
# -*- coding: utf-8 -*-
#
import getopt
import sys
import time
import os
import datetime
import random
import re
import platform

'''
python count_all_wr.py -f utexas.10e6.dt3.t1e5d.cf350.l2r2_EX00001.sc.wr -c goldingroth3.txt 
                       | sort  -nk2

2015-03-22 Added m2mode
'''

wrfile = None
confusible_file = None
latex = False
m2mode = False #corrected, proposed, gold

try:
    opts, args = getopt.getopt(sys.argv[1:], "f:c:lm", ["file="])
except getopt.GetoptError, err:
    print str(err)
    sys.exit(1)
for o, a in opts:
    if o in ("-c"):
        confusible_file = a 
    elif o in ("-f"):
        wrfile = a
    elif o in ("-l"):
        latex = True
    elif o in ("-m"):
        m2mode = True
    else:
        assert False, "unhandled option"

confusibles = []
with open(confusible_file, "r") as f:
    for l in f:
        if l[0:1] == "#":
            continue
        bits = l.split()
        for b in bits:
            confusibles.append( b )
#print confusibles
if latex:
    if m2mode:
        print "Confusible & count & Corr & Prop & Gold & acc & pre & rec & fsc \\\\"
    else:
        print "Confusible & count & TP & FN & TN & FP & acc & pre & rec & fsc \\\\"
else:
    if m2mode:
        print "confusible     c CORR  c PROP  c GOLD  c    ACC    PRE    REC    F1S"
    else:
        print "confusible     c ER    c EW    c NR    c NW    c    ACC    PRE    REC    F1S"

total_stats = {}
total_total = 0
for typ in ["ER", "EW", "NR", "NW"]:
    total_stats[typ] = 0

for confusible in confusibles:
    stats = {}
    total = 0

    for typ in ["ER", "EW", "NR", "NW"]:
        stats[typ] = 0

    # not efficient, can be done in one go
    with open(wrfile, "r") as f:
        for l in f:
            # .wr contains: TYP TARGET CLASSIFICATION
            bits = l.split()
            if bits[1] == confusible:
                stats[bits[0]] += 1
                total += 1
    #
    for typ in ["ER", "EW", "NR", "NW"]:
        total_stats[typ] += stats[typ] 
    total_total += total
    #
    TP = int(stats["ER"]) #error, corrected
    TN = int(stats["NR"]) #no error, not corrected
    FP = int(stats["NW"]) #no error, but corrected
    FN = int(stats["EW"]) #error, wrong correction
    #
    CORR = TP
    PROP = TP + FP
    GOLD = TP + FN
    #
    if total > 0:
        acc = ( (TP + TN) / float(total)) * 100.0
    else:
        acc = 0
    if TP + FP > 0:
        pre = (TP / float(TP + FP)) * 100.0
    else:
        pre = 0
    if TP + FN > 0:
        rec = (TP / float(TP + FN)) * 100.0
    else:
        rec = 0
    if pre+rec > 0:
        f1s = 2 * ((pre * rec) / (pre + rec))
    else:
        f1s = 0
    #
    acc = "{0:6.2f}".format(acc)
    pre = "{0:6.2f}".format(pre)
    rec = "{0:6.2f}".format(rec)
    f1s = "{0:6.2f}".format(f1s)
    #
    if latex:
        if m2mode:
            out = "{0:10s}".format(confusible)+" & "+"\\num{"+str(total)+"}"
            out += " & \\num{"+str(CORR)+"} & \\num{"+str(PROP)+"} & \\num{"+str(GOLD)+"}"
            out += " & \\num{"+str(acc)+"} & \\num{"+str(pre)+"} & \\num{"+str(rec)+"} & \\num{"+str(f1s)+"} \\\\"
        else:
            out = "{0:10s}".format(confusible)+" & "+"\\num{"+str(total)+"}"
            for typ in ["ER", "EW", "NR", "NW"]:
                out += " & "+"\\num{"+str(stats[typ])+"}"
            out += " & \\num{"+str(acc)+"} & \\num{"+str(pre)+"} & \\num{"+str(rec)+"} & \\num{"+str(f1s)+"} \\\\"
    else:
        if m2mode:
            out = "{0:10s}".format(confusible)+" "+"{0:5n}".format(total)
            out += " CORR "+"{0:4n}".format(CORR)
            out += " PROP "+"{0:4n}".format(PROP)
            out += " GOLD "+"{0:4n}".format(GOLD)
            out += " "+str(acc)+" "+str(pre)+" "+str(rec)+" "+str(f1s)
        else:
            out = "{0:10s}".format(confusible)+" "+"{0:5n}".format(total)
            for typ in ["ER", "EW", "NR", "NW"]:
                out += " "+typ+" "+"{0:4n}".format(stats[typ])
            out += " "+str(acc)+" "+str(pre)+" "+str(rec)+" "+str(f1s)
    print out

# Grand total

TP = int(total_stats["ER"]) #error, corrected
TN = int(total_stats["NR"]) #no error, not corrected
FP = int(total_stats["NW"]) #no error, but corrected
FN = int(total_stats["EW"]) #error, wrong correction
#
CORR = TP
PROP = TP + FP
GOLD = TP + FN
#
total = total_total
if total > 0:
    acc = ( (TP + TN) / float(total)) * 100.0
else:
    acc = 0
if TP + FP > 0:
    pre = (TP / float(TP + FP)) * 100.0
else:
    pre = 0
if TP + FN > 0:
    rec = (TP / float(TP + FN)) * 100.0
else:
    rec = 0
if pre+rec > 0:
    f1s = 2 * ((pre * rec) / (pre + rec))
else:
    f1s = 0
#
acc = "{0:6.2f}".format(acc)
pre = "{0:6.2f}".format(pre)
rec = "{0:6.2f}".format(rec)
f1s = "{0:6.2f}".format(f1s)
#
confusible = "TOTAL"
if latex:
    if m2mode:
        out = "{0:10s}".format(confusible)+" & "+"\\num{"+str(total)+"}"
        out += " & \\num{"+str(CORR)+"} & \\num{"+str(PROP)+"} & \\num{"+str(GOLD)+"}"
        out += " & \\num{"+str(acc)+"} & \\num{"+str(pre)+"} & \\num{"+str(rec)+"} & \\num{"+str(f1s)+"} \\\\"
    else:
        out = "{0:10s}".format(confusible)+" & "+"\\num{"+str(total)+"}"
        for typ in ["ER", "EW", "NR", "NW"]:
            out += " & "+"\\num{"+str(total_stats[typ])+"}"
        out += " & \\num{"+str(acc)+"} & \\num{"+str(pre)+"} & \\num{"+str(rec)+"} & \\num{"+str(f1s)+"} \\\\"
else:
    if m2mode:
        out = "{0:10s}".format(confusible)+" "+"{0:5n}".format(total)
        out += " CORR "+"{0:4n}".format(CORR)
        out += " PROP "+"{0:4n}".format(PROP)
        out += " GOLD "+"{0:4n}".format(GOLD)
        out += " "+str(acc)+" "+str(pre)+" "+str(rec)+" "+str(f1s)
    else:
        out = "{0:10s}".format(confusible)+" "+"{0:5n}".format(total)
        for typ in ["ER", "EW", "NR", "NW"]:
            out += " "+typ+" "+"{0:4n}".format(total_stats[typ])
        out += " "+str(acc)+" "+str(pre)+" "+str(rec)+" "+str(f1s)
print out

