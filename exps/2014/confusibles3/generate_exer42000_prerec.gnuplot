#
# Use directly in gnuplot
#
set style line 1 lt -1 lw 0.5 pi -1 pt 7 ps 1.0
set style line 2 lt -1 lw 0.5 pi -1 pt 6
set style line 3 lt -1 lw 0.5 pi -1 pt 7
set style line 4 lt 2 lw 0.5 pi -1 pt 7 ps 0.4
set title "Precision versus Recall, EXER, min_dsum"
set ylabel "Precision"
set xlabel "Recall"
set key top
#set grid
set border 3
set xtics out nomirror
set mxtics 4
set ytics out nomirror
set mytics 4
#
#grep a1 STATCF.EXER.42000.txt | grep cf350 | grep cf200 |awk -f p2txt.awk 
#
plot [0:100][0:100]\
"< grep a4 STATCF.EXER.42000.txt | grep cf350 | grep cf200| awk -f p2txt.awk" using 5:4 with lp ls 1 title "a4",\
"< grep a4 STATCF.EXER.42000.txt | grep cf350 | grep cf200| awk -f p2txt.awk" using 5:4:8 with labels offset 2 notitle,\
"< grep a1 STATCF.EXER.42000.txt | grep cf350 | grep cf200| awk -f p2txt.awk" using 5:4 with lp ls 2title "a1",\
"< grep a1 STATCF.EXER.42000.txt | grep cf350 | grep cf200| awk -f p2txt.awk" using 5:4:8 with labels offset 2 notitle
#
#"bla"  using 5:4 with lp ls 1 t "P v R"
set terminal push
#set terminal postscript eps enhanced color solid rounded lw 2 'Helvetica' 10
set terminal postscript eps enhanced dashed rounded lw 2 'Helvetica' 20
set out 'EXER42000_pre_rec.ps'
replot
!epstopdf 'EXER42000_pre_rec.ps'
set term pop
