#!/usr/bin/python
# -*- coding: utf-8 -*-
#
import getopt
import sys
import time
import os
import datetime
import random
import re
import platform

wrfile = None
confusible = None
try:
    opts, args = getopt.getopt(sys.argv[1:], "f:c:", ["file="])
except getopt.GetoptError, err:
    print str(err)
    sys.exit(1)
for o, a in opts:
    if o in ("-c"):
        confusible = a 
    elif o in ("-f"):
        wrfile = a
    else:
        assert False, "unhandled option"

stats = {}
total = 0
for typ in ["ER", "EW", "NR", "NW"]:
    stats[typ] = 0

with open(wrfile, "r") as f:
    for l in f:
        # .wr contains: TYP TARGET CLASSIFICATION
        bits = l.split()
        if bits[1] == confusible:
            stats[bits[0]] += 1
            total += 1
print stats, total
#
TP = int(stats["ER"]) #error, corrected
TN = int(stats["NR"]) #no error, not corrected
FP = int(stats["NW"]) #no error, but corrected
FN = int(stats["EW"]) #error, wrong correction
#
acc = ( (TP + TN) / float(total)) * 100.0
print "acc =", acc

if TP + FP > 0:
    pre = (TP / float(TP + FP)) * 100.0
else:
    pre = 0
print "pre =", pre

if TP + FN > 0:
    rec = (TP / float(TP + FN)) * 100.0
else:
    rec = 0
print "rec =", rec

if pre+rec > 0:
    f1s = 2 * ((pre * rec) / (pre + rec))
else:
    f1s = 0
print "f1s =", f1s
#
acc = "{0:6.2f}".format(acc)
pre = "{0:6.2f}".format(pre)
rec = "{0:6.2f}".format(rec)
f1s = "{0:6.2f}".format(f1s)
out = confusible
for typ in ["ER", "EW", "NR", "NW"]:
    out += " "+typ+":"+str(stats[typ])

out += " "+str(acc)+" "+str(pre)+" "+str(rec)+" "+str(f1s)
print out
