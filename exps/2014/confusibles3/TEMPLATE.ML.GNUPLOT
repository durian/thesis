#
#
# id      ctx        Tinstanc confus.  errs   GS    WS    NS    BS    TP    FP    FN    TN   ACC   PRE   REC   F1S train                   lines   test
# ML50000 l2r2_-a1+D 2522289  37052    227    33    55   139  9655    33  9655   194 27170 73.42  0.34 14.54  0.67 utexas.10e6.dt3.1000000 1000000 utexas.10e6.dt3.t1e5d.cf350
# 
#
# Needs the following variables:
#  __TITLE__   __XLABEL__   __YLABEL__   __FILE__
#
# plot data.dat w l, "< awk '(NR==2) {print $0}' data.dat"
# plot "< grep M data.dat" 
#
#
set style line 1 lt -1 lw 0.5 pi -1 pt 7 ps 0.4
set style line 2 lt -1 lw 0.5 pi -1 pt 6
set style line 3 lt -1 lw 0.5 pi -1 pt 7
set style line 4 lt 2 lw 0.5 pi -1 pt 7 ps 0.4
set title "Precision versus Recall"
set xlabel "Recall"
set ylabel "Precision"
set key bottom
#set grid
set border 3
set xtics out nomirror
set mxtics 4
set ytics out nomirror
set mytics 4
#
#
# "< grep 'l2r2.*a1.*1000000*' STATCF.__FILE__.txt" using 15:16 with p ls 1 t "PRE vs REC"
#
# ------------------------------------------
# One million lines, all contexts, a1 and a4
# ------------------------------------------
#
plot [0:100][0:100]\
"< grep '.' __FILE__" using 15:16 with p ls 1 t "PRE vs REC"
#
#
set terminal push
#set terminal postscript eps enhanced color solid rounded lw 2 'Helvetica' 10
set terminal postscript eps enhanced dashed rounded lw 2 'Helvetica' 20
set out '__FILE__.ps'
replot
!epstopdf '__FILE__.ps'
set term pop
