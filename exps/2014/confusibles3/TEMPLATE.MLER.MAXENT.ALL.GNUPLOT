# confidence/fscore
#
# from gnuplot/test:
#  pt  1 = plus
#  pt  2 = x
#  pt  3 = plus-x
#  pt  4 = square with dot
#  pt  5 = solid square
#  pt  6 = circle with dot
#  pt  7 = solid circle
#  pt  8 = triangle with dot (up)
#  pt  9 = solid triangle (up)
#  pt 10 = triangle with dot (down)
#  pt 11 = solid triangle (down)
#  pt 12 = rhombus with dot
#  pt 13 = solid rhombus
#
# lt -1 means black line
#
set style line 1 lt -1 lw 0.5 pi -1 pt 4 ps 1
set style line 2 lt -1 lw 0.5 pi -1 pt 5 ps 1
set style line 3 lt -1 lw 0.5 pi -1 pt 6 ps 1
set style line 4 lt -1 lw 0.5 pi -1 pt 7 ps 1
set style line 5 lt -1 lw 0.5 pi -1 pt 8 ps 1
set style line 6 lt -1 lw 0.5 pi -1 pt 9 ps 1
#
set title "__TITLE__"
set xlabel "max ent"
set ylabel "All"
set key bottom
#set grid
set border 3
set xtics 1 out nomirror
set mxtics 1
set ytics out nomirror
set mytics 4
#
#
plot [-0.1:5][0:100]\
"< grep 'a4' __FILE__" using 5:4 with lp ls 2 t "fscore a4",\
"< grep 'a4' __FILE__" using 5:3 with lp ls 4 t "recall a4",\
"< grep 'a4' __FILE__" using 5:2 with lp ls 6 t "precision a4"
#"< grep 'a1' __FILE__" using 5:4 with lp ls 1 t " fscore a1",\
#"< grep 'a4' __FILE__" using 5:4 with lp ls 2 t "fscore a4",\
#"< grep 'a1' __FILE__" using 5:3 with lp ls 3 t "recall a1",\
#"< grep 'a4' __FILE__" using 5:3 with lp ls 4 t "recall a4",\
#"< grep 'a1' __FILE__" using 5:2 with lp ls 5 t "precision a1",\
#"< grep 'a4' __FILE__" using 5:2 with lp ls 6 t "precision a4"
#
#
set terminal push
#set terminal postscript eps enhanced color solid rounded lw 2 'Helvetica' 10
set terminal postscript eps enhanced dashed rounded lw 2 'Helvetica' 20
set out '__FILE__.ps'
replot
!epstopdf '__FILE__.ps'
set term pop
