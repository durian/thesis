#!/bin/bash
#
# Plots for the monolith classifier.
#
DATAFILE=STATCF_ML_70000_conf_fsc
TEMPLATE=TEMPLATE.ML.CONFIDENCE.FSC.GNUPLOT
TEST="cf350"
INFO="" #Train 1e6, ML, test 1e5, 0.6\%"
#
#with * in filenames, we need to shift the $fields 
#
# cf350 is data with ?
grep "${TEST}" STATCF.ML.70000.txt | grep "min_md:0"|  awk 'BEGIN { FS = "[, :]+" } {print $14,$15,$16,$17,$22,$2}' | sort -nk5  > ${DATAFILE}
grep "${TEST}" STATCF.ML.71000.txt | grep "min_md:0"|  awk 'BEGIN { FS = "[, :]+" } {print $14,$15,$16,$17,$22,$2}' | sort -nk5  >> ${DATAFILE}
#
# 45.20 0.23 20.70 0.46 0 l2r2_-a1+D
# acc   pre  rec   fscr c algo
#
sed "s/__FILE__/${DATAFILE}/g" ${TEMPLATE} > ${DATAFILE}.gnuplot.1
sed "s/__TITLE__/${INFO}/g" ${DATAFILE}.gnuplot.1 > ${DATAFILE}.gnuplot
rm ${DATAFILE}.gnuplot.1
gnuplot ${DATAFILE}.gnuplot

#

DATAFILE=STATCF_ML_70000_conf_rec
TEMPLATE=TEMPLATE.ML.CONFIDENCE.REC.GNUPLOT
#
#with * in filenames, we need to shift the $fields 
#
# cf350 is data with ?
grep "${TEST}" STATCF.ML.70000.txt | grep "min_md:0"|  awk 'BEGIN { FS = "[, :]+" } {print $14,$15,$16,$17,$22,$2}' | sort -nk5  > ${DATAFILE}
grep "${TEST}" STATCF.ML.71000.txt | grep "min_md:0"|  awk 'BEGIN { FS = "[, :]+" } {print $14,$15,$16,$17,$22,$2}' | sort -nk5  >> ${DATAFILE}
#
# 45.20 0.23 20.70 0.46 0 l2r2_-a1+D
# acc   pre  rec   fscr c algo
#
sed "s/__FILE__/${DATAFILE}/g" ${TEMPLATE} > ${DATAFILE}.gnuplot.1
sed "s/__TITLE__/${INFO}/g" ${DATAFILE}.gnuplot.1 > ${DATAFILE}.gnuplot
rm ${DATAFILE}.gnuplot.1
gnuplot ${DATAFILE}.gnuplot

#

DATAFILE=STATCF_ML_70000_conf_pre
TEMPLATE=TEMPLATE.ML.CONFIDENCE.PRE.GNUPLOT
#
#with * in filenames, we need to shift the $fields 
#
# cf350 is data with ?
grep "${TEST}" STATCF.ML.70000.txt | grep "min_md:0"|  awk 'BEGIN { FS = "[, :]+" } {print $14,$15,$16,$17,$22,$2}' | sort -nk5  > ${DATAFILE}
grep "${TEST}" STATCF.ML.71000.txt | grep "min_md:0"|  awk 'BEGIN { FS = "[, :]+" } {print $14,$15,$16,$17,$22,$2}' | sort -nk5  >> ${DATAFILE}
#
# 45.20 0.23 20.70 0.46 0 l2r2_-a1+D
# acc   pre  rec   fscr c algo
#
sed "s/__FILE__/${DATAFILE}/g" ${TEMPLATE} > ${DATAFILE}.gnuplot.1
sed "s/__TITLE__/${INFO}/g" ${DATAFILE}.gnuplot.1 > ${DATAFILE}.gnuplot
rm ${DATAFILE}.gnuplot.1
gnuplot ${DATAFILE}.gnuplot
