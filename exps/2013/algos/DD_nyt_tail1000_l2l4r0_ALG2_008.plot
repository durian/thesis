set terminal postscript eps enhanced monochrome rounded lw 2 'Helvetica' 20
set out 'DD_nyt_tail1000_l2l4r0_ALG2_008.ps'
set style line 1 lc rgb '#1047a9' lt 1 lw 5 pt 7 pi -1 ps 1.5
set style line 2 lc rgb '#4577d4' lt 1 lw 5 pt 7 pi -1 ps 1.5
set style line 3 lc rgb '#6b90d4' lt 1 lw 5 pt 7 pi -1 ps 1.5
#set title "Distribution Size Distibution, l2r0, 100000 lines"
#set xlabel "Size"
set size 1.5,1
set xtics out
set xtics rotate by 270
set ytics out
set ylabel "number"
set multiplot layout 2,1
set bmargin 0
set format x ""
set key bottom
plot [0:50][0:7000] 'nyt.tail1000.l2r0_ALG20008.px.ds.data' using  ($1-0.3):2 with impulses ls 1 title "igtree",\
'nyt.tail1000.l2r0_ALG21008.px.ds.data' using ($1):2 with impulses ls 2 title "tribl2",\
'nyt.tail1000.l2r0_ALG22008.px.ds.data' using ($1+0.3):2 with impulses ls 3 title "ib1"
set bmargin
set tmargin 0
set format x "%g"
set key top
plot [0:50][0:7000] 'nyt.tail1000.l4r0_ALG20009.px.ds.data' using  ($1-0.3):2 with impulses ls 1 title "igtree",\
'nyt.tail1000.l4r0_ALG21009.px.ds.data' using ($1):2 with impulses ls 2 title "tribl2",\
'nyt.tail1000.l4r0_ALG22009.px.ds.data' using ($1+0.3):2 with impulses ls 3 title "ib1"
unset multiplot

