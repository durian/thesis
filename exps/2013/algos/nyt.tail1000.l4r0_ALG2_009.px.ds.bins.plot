#hand combined
# 2014-05-27 12:07:49 #changes
# 
#set style line 1 lc rgb '#0060ad' lt 1 lw 1
set style line 1 lc rgb '#1047a9' lt 1 lw 5 pt 7 pi -1 ps 1.5
set style line 2 lc rgb '#4577d4' lt 1 lw 5 pt 7 pi -1 ps 1.5
set style line 3 lc rgb '#6b90d4' lt 1 lw 5 pt 7 pi -1 ps 1.5
#set title "Distribution Size, l4r0" #caption
set size 1.3,1
set xlabel "Size of distribution"
set xrange []
set xtics out
set xtics rotate by 295 nomirror
set xtics font "Helvetica,14"
set rmargin 5
set ytics out nomirror
set border 3
set yrange [0:7000]
set style data histogram
set style histogram cluster gap 1
set style fill solid
set ylabel "Frequency of distribution size"
plot 'nyt.tail1000.l4r0_ALG20009.px.ds.bins' using 2:xticlabels(6) ls 1 title "igtree",\
'nyt.tail1000.l4r0_ALG21009.px.ds.bins' using 2:xticlabels(6) ls 2 title "tribl2",\
'nyt.tail1000.l4r0_ALG22009.px.ds.bins' using 2:xticlabels(6) ls 3 title "ib1"
set terminal push
set terminal postscript eps enhanced rounded lw 2 'Helvetica' 18
set out 'nyt_tail1000_l4r0_ALG2_009_px_ds_bins.ps'
replot
!epstopdf 'nyt_tail1000_l4r0_ALG2_009_px_ds_bins.ps'
set term pop
