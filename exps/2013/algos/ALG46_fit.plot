# autogen
set style line 1 lt -1 lw 0.5 pi -1 pt 7 ps 0.4
set style line 2 lt -1 lw 0.5 pi -1 pt 6
set style line 3 lt -1 lw 0.5 pi -1 pt 7
set style line 4 lt 2 lw 0.5 pi -1 pt 7 ps 0.4
set title "Fit of f(x) = a {\267} log_{10}(x) + b"
set xlabel "lines of data"
set key bottom
set logscale x
set ylabel "cg"
#set grid
set border 3
set xtics out nomirror
set ytics out nomirror
set mytics 2
set xtics add ("1,000" 1000,"10,000" 10000,"100,000" 100000,"1,000,000" 1000000,"10,000,000" 10000000)
#
a = 2
b = -5
f(x) = a*log10(x) + b
d = 2
e = -5
g(x) = d*log10(x) + e
#
# 1000 11.67
# 5623413 29.88
#
fit [1000:5623413][11:30] f(x) "ALG46_cg_l1r1_-a1+D.data" using 2:3 via a,b
vf = sprintf("%.2f * log_{10}(x) + %.2f", a, b)
fit  g(x) "TEXAS_cg_l2r0_-a1+D.data" using 2:3 via d ,e
vg = sprintf("%.2f * log_{10}(x) + %.2f", d, e)
#
#[900:][0:40]
# TEXAS_cg_l2r0_-a1+D.data ALG46_cg_l1r1_-a1+D.data
#
# "set samples 10" and "f(x) with points" to plot 10 points
plot [900:1e12][0:50] "ALG46_cg_l1r1_-a1+D.data" using 2:3 w lp ls 3 t "l1r1 -a1+D",\
 "TEXAS_cg_l2r0_-a1+D.data" using 2:3 w lp ls 2 t "l2r0 -a1+D",\
f(x) t vf,\
g(x) t vg
set terminal push
#set terminal postscript eps enhanced color solid rounded lw 2 'Helvetica' 10
set terminal postscript eps enhanced dashed rounded lw 2 'Helvetica' 10
set out 'ALG46_fit.ps'
replot
!epstopdf 'ALG46_fit.ps'
set term pop
