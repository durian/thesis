# hand combined
# 2014-05-26 16:59:02 Mon larger font, different xtics
#
set style line 1 lt -1 lw 0.5 pi -1 pt 7 ps 0.4
set style line 2 lt -1 lw 0.5 pi -1 pt 6
set style line 3 lt -1 lw 0.5 pi -1 pt 7
set style line 4 lt 2 lw 0.5 pi -1 pt 7 ps 0.4 #green on X11, dashed on postscript
#set title "l2r0" #in caption
set xlabel "Lines of data"
set key left bottom
set logscale x
set xtics out nomirror
#set xtics ("1,000" 1000,"10,000" 10000,"100,000" 100000,"1,000,000" 1000000,"10,000,000" 10000000)
set xtics ( "10^3" 1000, "10^4" 10000 )
set xtics add ( "10^5" 100000, "10^6" 1000000 )
set xtics add ( "10^7" 10000000 )
set ytics out nomirror
set ylabel "perplexity"
#set grid
# to fit the 10000000 label:
#set rmargin at screen 0.95 #not needed anymore
set border 3
plot [1000:][100:400] "PPLX20_pplx_l2r0_-a1+D.data" using 2:6 w lp ls 1 t "igtree",\
"PPLX21_pplx_l2r0_-a4+D.data" using 2:6 w lp ls 2 t "tribl2",\
"PPLX22_pplx_l2r0_-a0+D.data" using 2:6 w lp ls 3 t "ib1",\
"../srilm/srilm_pplx.data" using 11:9 w lp ls 4 t "srilm"
set terminal push
#                                 solid  color
#set terminal postscript eps enhanced dashed rounded lw 2 'Helvetica' 10
set terminal postscript eps enhanced dashed rounded lw 2 'Helvetica' 18
#set terminal pdfcairo dashed rounded lw 2 
set out 'PPLX_3algos_srilm_l2r0.ps'
#set out 'PPLX_3algos_l2r0.pdf'
replot
!epstopdf 'PPLX_3algos_srilm_l2r0.ps'
set term pop
