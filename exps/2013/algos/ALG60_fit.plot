# autogen
# 2014-05-27 15:21:52 fixes
# 2016-03-17 removed l4
set terminal dumb
#
set style line 1 lt -1 lw 0.5 pi -1 pt 7 ps 0.4
set style line 2 lt -1 lw 0.5 pi -1 pt 6 ps 0.8
set style line 3 lt -1 lw 0.5 pi -1 pt 7 ps 0.8
set style line 4 lt 2 lw 0.5 pi -1 pt 7 ps 0.4
#set title "tribl2, l2r0, l2r2 and l4r0, fit of f(x) = a {\267} log_{10}(x) + b" #caption
set xlabel "Number of instances"
set key bottom
set logscale x
set ylabel "Percentage correct classifications"
#set grid
set key  samplen 3 spacing 1.4 #reverse
set border 3
set xtics out nomirror
set ytics out nomirror
set mytics 2
#set xtics add ("1{\267}10^3" 1000,"10{\267}10^3" 10000,"100{\267}10^3" 100000,"1{\267}10^6" 1e6,"10{\267}10^6" 1e7)
#set xtics add ("100{\267}10^6" 1e8, "1{\267}10^9" 1e9)
set xtics add ("1{\267}10^3" 1000,"1{\267}10^4" 10000,"1{\267}10^5" 100000,"1{\267}10^6" 1e6,"1{\267}10^7" 1e7)
set xtics add ("1{\267}10^8" 1e8, "1{\267}10^9" 1e9)
#
a = 2
b = -5
f(x) = a*log10(x) + b
g(x) = c*log10(x) + d
h(x) = e*log10(x) + f
#
fit f(x) "DATA.ALG.60000.plot" using 14:3 via a,b
#fit g(x) "DATA.ALG.61000.plot" using 14:3 via c,d #removed
fit h(x) "DATA.ALG.62000.plot" using 14:3 via e,f
vf = sprintf("{/Palatino-Roman=14 %.2f * log_{10}(x) + %.2f}", a, b)
#vg = sprintf("{/Palatino-Roman=14 %.2f * log_{10}(x) + %.2f}", c, d) #removed
vh = sprintf("{/Palatino-Roman=14 %.2f * log_{10}(x) + %.2f}", e, f)
#
# Removed this from below:
#"DATA.ALG.61000.plot" using 14:3 w lp ls 3 t "l4r0",\
#g(x) t vg,\
#
# "set samples 10" and "f(x) with points" to plot 10 points
plot [10000:1e9][0:40] "DATA.ALG.60000.plot" using 14:3 w lp ls 2 t "l2r0",\
f(x) t vf,\
"DATA.ALG.62000.plot" using 14:3 w lp ls 1 t "l2r2",\
h(x) t vh
set terminal push
#set terminal postscript eps enhanced color solid rounded lw 2 'Helvetica' 10
#set terminal postscript eps enhanced dashed rounded lw 2 'Helvetica' 10
set terminal postscript eps enhanced dashed rounded lw 2 'Helvetica' 18
set out 'ALG60_fit.ps'
replot
!epstopdf 'ALG60_fit.ps'
set term pop
