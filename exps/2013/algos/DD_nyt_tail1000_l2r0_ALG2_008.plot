set style line 1 lc rgb '#1047a9' lt 1 lw 5 pt 7 pi -1 ps 1.5
set style line 2 lc rgb '#4577d4' lt 1 lw 5 pt 7 pi -1 ps 1.5
set style line 3 lc rgb '#6b90d4' lt 1 lw 5 pt 7 pi -1 ps 1.5
set style line 4 lc rgb '#00AA72' lt 1 lw 5 pt 7 pi -1 ps 1.5
set style line 5 lc rgb '#ffffff' lt 1 lw 5  #white
set title "Distribution Size Distibution, l2r0, 100000 lines" noenhanced
set xlabel "Size of distribution" noenhanced
set size 1.5,1
#set tmargin at screen 0.90
set xrange [0:30]
set xtics out
set xtics rotate by 270
set ytics out
set yrange [0:7000]
set ylabel "frequency of size"
plot 'nyt.tail1000.l2r0_ALG20008.px.ds.data' using  ($1-0.5):2 with impulses ls 1 title "igtree",\
'nyt.tail1000.l2r0_ALG21008.px.ds.data' using ($1-0.25):2 with impulses ls 2 title "tribl2",\
'nyt.tail1000.l2r0_ALG22008.px.ds.data' using ($1+0):2 with impulses ls 3 title "ib1",\
'nyt.tail1000.l2r0_HPX66000.hpx1.px.ds.data' using ($1+0.25):2 with impulses ls 4 title "igtree hpx1",\
'nyt.tail1000.l2r0_HPX66000.hpx1.px.ds.data' using ($1+0.5):2 with impulses ls 5 title "igtree hpx1"
set terminal push
set terminal postscript eps enhanced rounded lw 2 'Helvetica' 20
set out 'DD_nyt_tail1000_l2r0_ALG2_008.ps'
replot
!epstopdf 'DD_nyt_tail1000_l2r0_ALG2_008.ps'
set term pop

