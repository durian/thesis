#!/bin/sh
#
# Like do_algos_10000.sh, but only the larger data sizes
# We continue with 10500 so it can be pasted onto the older
# output.
#
TRAINBASE="/exp2/pberck/nyt.3e7"
TESTFILE="nyt.tail1000"
#
WOPR="/exp2/pberck/wopr/wopr"
SCRIPT="do_algos.wopr_script"
PXSCRIPT="/exp2/pberck/wopr/etc/pplx_px.pl"
SCATTERSCRIPT="/exp2/pberck/wopr/etc/create_cg_f_scatterplot.pl"
LOG="README.ALG.10000.txt"
PLOT="DATA.ALG.10000.plot"
#
CYCLE=10500
#
date "+#%Y-%m-%d %H:%M:%S" >> ${LOG}
#
for LINES in 2000000 5000000 10000000
do
    #Make the dataset here, clean up afterwards?
    TRAINFILE=${TRAINBASE}.${LINES}
    if [ -e "${TRAINFILE}" ]
    then
	echo "Data file ${TRAINFILE} exists."
    else
	echo "Creating data file ${TRAINFILE}."
	head -n ${LINES} ${TRAINBASE} > ${TRAINFILE}
    fi
    for TIMBL in "-a1 +D" "-a4 +D" "-a4 +D -k3" "-a4 +D -k5" "-a4 +D -k7" 
    do
	for LC in 2 3 4
	do
	    for RC in 0 1 2 3
	    do
		CYCLESTR=`printf "%05d" ${CYCLE}`
		ID=ALG${CYCLESTR}
		echo ${ID}
		# if exists PXFILE=${TESTFILE}.l${LC}r${RC}_${ID}.px, skip?
		PXFILE=${TESTFILE}.l${LC}r${RC}_${ID}.px
		if [ -e "${PXFILE}" ]
		then
		    echo "${PXFILE} exists. Skipping."
		else
		    echo "#${ID},${TRAINFILE},timbl:'${TIMBL}',lc:${LC},rc:${RC}" >> ${LOG}
		    echo ${WOPR} -s ${SCRIPT} -p trainfile:${TRAINFILE},rc:${RC},lc:${LC},id:${ID},timbl:${TIMBL},testfile:${TESTFILE}
		    ${WOPR} -s ${SCRIPT} -p trainset:${TRAINFILE},rc:${RC},lc:${LC},id:${ID},timbl:"${TIMBL}",testset:${TESTFILE}
		fi
		CYCLE=$(( $CYCLE + 1 ))
		#
		#reuters.martin.tok.1000.l3r2_ALG023.px
		PXFILE=${TESTFILE}.l${LC}r${RC}_${ID}.px
		WSFILE=${PXFILE}.ws
		LEXFILE=${TRAINFILE}.lex
		perl ${PXSCRIPT} -f ${PXFILE} -l ${LC} -r ${RC} -w > ${WSFILE}
		perl ${SCATTERSCRIPT} -w ${WSFILE} -l ${LEXFILE}
		#http://wiki.bash-hackers.org/commands/builtin/printf
		#printf -v S "%s %s" ${ID} ${PXFILE}
		#echo $S
		adc=0
		ads=0
		cg=0
		cd=0
		ic=0
		mrr_cd=0
		mrr_cg=0
		mrr_gd=0
		pplx=0
		pplx1=0
		#change to incorporate average over dist.
		# 0.96698110  -0.0484  -0.0146  4 1 [00110] .
		# 00110:  5137 ( 22.50%)
		#
		BLOB=`perl ${PXSCRIPT} -f ${PXFILE} -l ${LC} -r ${RC} | tail -n18`
		#http://www.arachnoid.com/linux/shell_programming.html

		# dist_freq sum: 2266012, ave: ( 99.26)
		# dist_sum sum: 44968874, ave: (1969.81)
		STR=${BLOB#*dist_freq sum}
		RX=': ([0123456789]*)\, ave: \(([0123456789. ]*)\)'
		if [[ "$STR" =~ $RX ]]
		then
		    adc=${BASH_REMATCH[2]}
		fi
		#
		STR=${BLOB#*dist_sum sum}
		RX=': (.*)\, ave: \(([0123456789. ]*)\)'
		if [[ "$STR" =~ $RX ]]
		then
		    ads=${BASH_REMATCH[2]}
		fi
		#
		STR=${BLOB#*Column:  2}
		RX='.* \((.*)%\).*3.* \((.*)%\).*4.* \((.*)%\).*'
		if [[ "$STR" =~ $RX ]]
		then
		    cg=${BASH_REMATCH[1]}
		    cd=${BASH_REMATCH[2]}
		    ic=${BASH_REMATCH[3]}
		fi
		#echo ${BLOB}
		STR=${BLOB#*Wopr ppl}
		RX=': (.*) Wopr ppl1: (.*) \(.*'
		#
		STR=${BLOB#*Wopr ppl}
		RX=': (.*) Wopr ppl1: (.*) \(.*'
		if [[ "${STR}" =~ $RX ]]
		then
		    pplx=${BASH_REMATCH[1]}
		    pplx1=${BASH_REMATCH[2]}
		fi
		#
		STR=${BLOB#*RR(cd)}
		#echo $STR
		RX='.* MRR: (.*).*RR\(cg\).* MRR: (.*).*RR\(gd\).* MRR: (.*)'
		if [[ "${STR}" =~ $RX ]]
		then
		    mrr_cd=${BASH_REMATCH[1]}
		    mrr_cg=${BASH_REMATCH[2]}
		    mrr_gd=${BASH_REMATCH[3]}
		fi
		TSTR=l${LC}r${RC}_"${TIMBL// /}"
		printf -v S "%s %s %s %s %s %s %s %s %s %s %s %s %s" ${ID} ${LINES} ${cg} ${cd} ${ic} ${pplx} ${pplx1} ${mrr_cd} ${mrr_cg} ${mrr_gd} ${adc} ${ads} ${TSTR}
		echo ${S} >> ${PLOT}
	    done
	done
    done
done

