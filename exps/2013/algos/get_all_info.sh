#!/bin/bash
#
# Print info on DATA.___.plot files
#
for FILE in $(ls DATA*plot); 
do
	echo Processing: ${FILE} #>/dev/stderr
	bash get_info.sh ${FILE}
done
