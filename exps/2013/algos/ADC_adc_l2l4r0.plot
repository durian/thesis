# autogen
# 2014-05-27 11:24:37 fixes for fonts etc
# 2015-05-19 new dash style
#
set terminal dumb 
#set terminal postscript eps enhanced dashed rounded lw 2 'Helvetica' 18
#
#set title "Average distribution size, contex l2r0 and l4r0" #in caption now
#
set encoding utf8
#
set style line 1 lt -1 lw 0.5 pi -1 pt 7 ps 0.4
set style line 2 lt -1 lw 0.5 pi -1 pt 6
set style line 3 lt -1 lw 0.5 pi -1 pt 7
set style line 4 dt 2 lw 0.5 pi -1 pt 7 ps 0.4
set style line 5 dt 3 lw 0.5 pi -1 pt 6
set style line 6 dt 4 lw 0.5 pi -1 pt 7
set style line 7 lt 3 lw 0.5 pi -1 pt 7 ps 0.4
set style line 8 lt 3 lw 0.5 pi -1 pt 6
set style line 9 lt 3 lw 0.5 pi -1 pt 7
#
set xlabel "Lines of data"
set key left top
set logscale x
set border 3
set xtics out nomirror
set ytics out nomirror
set mytics 2
set ylabel "Average distribution size"
#set grid
set xtics ( "10^3" 1000, "10^4" 10000, "10^5" 100000, "10^6" 1000000, "10^7" 10000000   )
set xtics add ( "5{\267}10^3" 5000, "5{\267}10^4" 50000, "5{\267}10^5" 500000, "5{\267}10^5" 500000, "5{\267}10^6" 5000000 )
#
plot [900:][0:8000] "ADC_adc_l2r0_-a0+D.data" using 2:11 w lp ls 1 t "l2r0 IB1","ADC_adc_l2r0_-a1+D.data" using 2:11 w lp ls 2 t "l2r0 IGTree","ADC_adc_l2r0_-a4+D.data" using 2:11 w lp ls 3 t "l2r0 Tribl2",\
"ADC_adc_l4r0_-a0+D.data" using 2:11 w lp ls 4 t "l4r0 IB1","ADC_adc_l4r0_-a1+D.data" using 2:11 w lp ls 5 t "l4r0 IGTree","ADC_adc_l4r0_-a4+D.data" using 2:11 w lp ls 6 t "l4r0 Tribl2"
set terminal push
#set terminal postscript eps enhanced dashed rounded lw 2 'Helvetica' 10
set terminal postscript eps enhanced dashed rounded lw 2 'Helvetica' 18
set out 'ADC_adc_l2l4r0.ps'
replot
!epstopdf 'ADC_adc_l2l4r0.ps'
set term pop


