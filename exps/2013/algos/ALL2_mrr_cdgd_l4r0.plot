# hand combined
# 2014-05-27 12:50:26 fixes
#
set style line 1 lt -1 lw 0.5 pi -1 pt 7 ps 0.4
set style line 2 lt -1 lw 0.5 pi -1 pt 6
set style line 3 lt -1 lw 0.5 pi -1 pt 7

set style line 4 lt 2 lw 0.5 pi -1 pt 7 ps 0.4
set style line 5 lt 2 lw 0.5 pi -1 pt 6
set style line 6 lt 2 lw 0.5 pi -1 pt 7

#set title "MRR, context l4r0" #in caption now
set xlabel "Lines of data"
set key bottom
set logscale x
set ylabel "MRR correct distribution and classification"
#set grid
set border 3
set xtics out nomirror
set ytics out nomirror
set xtics ( "10^3" 1000, "10^4" 10000, "10^5" 100000, "10^6" 1000000, "10^7" 10000000   )
set xtics add ( "5{\267}10^3" 5000, "5{\267}10^4" 50000, "5{\267}10^5" 500000, "5{\267}10^5" 500000, "5{\267}10^6" 5000000 )
set mytics 2
plot [900:][0:0.8] "ALL2_mrr_cd_l4r0_-a0+D.data" using 2:8 w lp ls 1 t "ib1","ALL2_mrr_cd_l4r0_-a1+D.data" using 2:8 w lp ls 2 t "igtree","ALL2_mrr_cd_l4r0_-a4+D.data" using 2:8 w lp ls 3 t "tribl2",\
"ALL2_mrr_gd_l4r0_-a0+D.data" using 2:10 w lp ls 4 t "ib1","ALL2_mrr_gd_l4r0_-a1+D.data" using 2:10 w lp ls 5 t "igtree","ALL2_mrr_gd_l4r0_-a4+D.data" using 2:10 w lp ls 6 t "tribl2"
#
#set arrow from 100000,0.8 to 100000,0.599
#
set terminal push
#set terminal postscript eps enhanced color solid rounded lw 2 'Helvetica' 10
set terminal postscript eps enhanced dashed rounded lw 2 'Helvetica' 18
#set terminal postscript eps enhanced color solid rounded lw 2 'Helvetica' 18
set out 'ALL2_mrr_cdgd_l4r0.ps'
replot
!epstopdf 'ALL2_mrr_cdgd_l4r0.ps'
set term pop
