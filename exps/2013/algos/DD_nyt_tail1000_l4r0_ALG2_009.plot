set style line 1 lc rgb '#1047a9' lt 1 lw 5 pt 7 pi -1 ps 1.5
set style line 2 lc rgb '#4577d4' lt 1 lw 5 pt 7 pi -1 ps 1.5
set style line 3 lc rgb '#6b90d4' lt 1 lw 5 pt 7 pi -1 ps 1.5
set title "Distribution Size Distribution, l4r0, 100000 lines"
set xlabel "Size of distribution"
set size 1.5,1
set xrange [0:50]
set xtics out
set xtics rotate by 270
set ytics out
set yrange [0:7000]
set ylabel "frequency of size"
plot 'nyt.tail1000.l4r0_ALG20009.px.ds.data' using  ($1-0.3):2 with impulses ls 1 title "igtree",\
'nyt.tail1000.l4r0_ALG21009.px.ds.data' using ($1):2 with impulses ls 2 title "tribl2",\
'nyt.tail1000.l4r0_ALG22009.px.ds.data' using ($1+0.3):2 with impulses ls 3 title "ib1"
set terminal push
set terminal postscript eps enhanced monochrome rounded lw 2 'Helvetica' 20
set out 'DD_nyt_tail1000_l4r0_ALG2_009.ps'
replot
!epstopdf 'DD_nyt_tail1000_l4r0_ALG2_009.ps'
set term pop

