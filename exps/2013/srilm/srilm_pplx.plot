# hand combined
#set style line 8 lt 8 pt 2 ps 1.2 lw 2
#set style line 1 lc rgb '#FFFFFF' # white
#set style line 2 lc rgb '#F0F0F0' # 
#set style line 3 lc rgb '#D9D9D9' # 
#set style line 4 lc rgb '#BDBDBD' # light grey
#set style line 5 lc rgb '#969696' # 
#set style line 6 lc rgb '#737373' # medium grey
#set style line 7 lc rgb '#525252' #
#set style line 8 lc rgb '#252525' # dark grey
# lw 0.5 to get one with lw 2 in terminal def
set style line 1 lt -1 lw 0.5 pi -1 pt 7 ps 0.4
set style line 2 lt -1 lw 0.5 pi -1 pt 6
set style line 3 lt -1 lw 0.5 pi -1 pt 7
set title "SRILM"
set xlabel "lines of data"
set key bottom
set logscale x
set xtics out nomirror
set xtics ("1,000" 1000,"10,000" 10000,"100,000" 100000,"1,000,000" 1000000,"10,000,000" 10000000)
set ytics out nomirror
set ylabel "perplexity"
#set grid
# to fit the 10000000 label:
set rmargin at screen 0.95
set border 3
plot [1000:][0:550] "srilm_pplx.data" using 11:9 w lp ls 2 t "pplx",\
"srilm_pplx.data" using 11:10 w lp ls 3 t "pplx1"
set terminal push
#                                 solid  color
set terminal postscript eps enhanced dashed rounded lw 2 'Helvetica' 10
#set terminal pdfcairo dashed rounded lw 2 
set out 'srilm_pplx.ps'
#set out 'PPLX_3algos_l2r0.pdf'
replot
!epstopdf 'srilm_pplx.ps'
set term pop
