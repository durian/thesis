#!/bin/sh
#
TRAINBASE="/exp2/pberck/utexas_iit.pages_ALL.txt.ucto.cl"
ORIGTESTFILE="utexas_iit.tail1000"
TESTFILE="utexas_iit.tail1000sc"
#
WOPR="/exp2/pberck/wopr/wopr"
SCRIPT="do_spelcorr.wopr_script"
SCSCRIPT="/exp/pberck/wopr/etc/check_spelerr.pl"
#
CYCLE=52000 #Like 51000 with sc:0,  and max_tf:0
LOG="README.SC.${CYCLE}.txt"
PLOT="DATA.SC.${CYCLE}.plot"
#
# Do spelling correction
#
if [ ! -e "${ORIGTESTFILE}" ]
then
    echo "${ORIGTESTFILE} nor found, aborting."
    exit 1
fi
if [ ! -e "${TESTFILE}" ]
then
    echo "${TESTFILE} nor found, aborting."
    exit 1
fi
#
date "+#%Y-%m-%d %H:%M:%S" >> ${LOG}
#
for LINES in  100000 1000000 2000000
do
    #Make the datasets here.
    TRAINFILE=${TRAINBASE}.${LINES}
    if [ -e "${TRAINFILE}" ]
    then
	echo "Data file ${TRAINFILE} exists."
    else
	echo "Creating data file ${TRAINFILE}."
	head -n ${LINES} ${TRAINBASE} > ${TRAINFILE}
    fi
    for TIMBL in "-a1 +D" "-a4 +D"  
    do
	for LC in 2 3
	do
	    for RC in 2 3
	    do
		FREE=`df -k . | tail -1 | awk '{print $3}'`
		# 25 577 264 = 25GB
		while [ $FREE -lt 10000000 ]
		do  
		    echo "Space critical, pausing at ${CYCLE}."
		    sleep 60
		    FREE=`df -k . | tail -1 | awk '{print $3}'`
		done
		# original windowed test set
		${WOPR} -r window_lr -p filename:${ORIGTESTFILE},lc:${LC},rc:${RC}
		CYCLESTR=`printf "%05d" ${CYCLE}`
		ID=SC${CYCLESTR}
		echo ${ID}
		# if exists PXFILE=${TESTFILE}.l${LC}r${RC}_${ID}.sc, skip?
		SCFILE=${TESTFILE}.l${LC}r${RC}_${ID}.sc
		OUT=output.${ID}
		if [ -e "${SCFILE}" ]
		then
		    echo "${SCFILE} exists. Skipping."
		else
		    echo "#${ID},${TRAINFILE},timbl:'${TIMBL}',lc:${LC},rc:${RC}" >> ${LOG}
		    echo ${WOPR} -s ${SCRIPT} -p trainfile:${TRAINFILE},rc:${RC},lc:${LC},id:${ID},timbl:${TIMBL},testfile:${TESTFILE},cs:0,max_tf:0
		    ${WOPR} -s ${SCRIPT} -p trainset:${TRAINFILE},rc:${RC},lc:${LC},id:${ID},timbl:"${TIMBL}",testset:${TESTFILE},cs:0,max_tf:0 >>${OUT}
		fi
		CYCLE=$(( $CYCLE + 1 ))
		#
		#reuters.martin.tok.1000.l3r2_ALG023.sc
		echo "perl ${SCSCRIPT} -s ${SCFILE} -o ${ORIGTESTFILE}.l${LC}r${RC}" >> ${OUT}
		BLOB=`perl ${SCSCRIPT} -s ${SCFILE} -o ${ORIGTESTFILE}.l${LC}r${RC}`
		#http://wiki.bash-hackers.org/commands/builtin/printf
		#printf -v S "%s %s" ${ID} ${BLOB}
		TSTR=l${LC}r${RC}_"${TIMBL// /}"
		#blob: lines errors good_sugg bad_sugg wrong_sugg no_sugg
		echo "${ID} ${LINES} ${BLOB} ${TSTR}" >> ${PLOT}
	    done
	done
    done
done

