#!/bin/sh
#
# BASELINE spelling corrections, parameters maxed out
# to try everything in the distribution.
# As 12300, different data
#
TRAINBASE="/exp2/pberck/DUTCH-TWENTE-ILK.tok"
ORIGTESTFILE="DTI.tok.t1000"
TESTFILE="DTI.tok.t1000.vkerr1"
#
WOPR="/exp2/pberck/wopr-1.36.8/wopr"
SCRIPT="do_spelcorr_hpx.wopr_script"
SCSCRIPT="/exp2/pberck/wopr/etc/check_spelerr.pl"
#
CYCLE=13300
LOG="README.TYP.${CYCLE}.txt"
PLOT="DATA.TYP.${CYCLE}.plot"
#
# Do spelling correction
#
if [ ! -e "${ORIGTESTFILE}" ]
then
    echo "${ORIGTESTFILE} nor found, aborting."
    exit 1
fi
if [ ! -e "${TESTFILE}" ]
then
    echo "${TESTFILE} nor found, aborting."
    exit 1
fi
#
date "+#%Y-%m-%d %H:%M:%S" >> ${LOG}
#
# check afterwards with examine_sc.py !
PARAMS="mwl:0,mld:1,max_ent:20,max_distr:1000000,min_ratio:0,max_tf:1000000"
#
HPX=1
#
for LINES in 10000 20000 50000 100000 200000 500000 1000000 2000000
do
    #Make the datasets here.
    TRAINFILE=${TRAINBASE}.${LINES}
    if [ -e "${TRAINFILE}" ]
    then
	echo "Data file ${TRAINFILE} exists."
    else
	echo "Creating data file ${TRAINFILE}."
	head -n ${LINES} ${TRAINBASE} > ${TRAINFILE}
    fi
    for TIMBL in "-a1 +D" "-a4 +D" 
    do
	for LC in 2 4
	do
	    for RC in 0 2
	    do
		FREE=`df -k . | tail -1 | awk '{print $3}'`
		# 25 577 264 = 25GB
		while [ $FREE -lt 10000000 ]
		do  
		    echo "Space critical, pausing at ${CYCLE}."
		    sleep 60
		    FREE=`df -k . | tail -1 | awk '{print $3}'`
		done
		# original windowed test set
		${WOPR} -r window_lr -p filename:${ORIGTESTFILE},lc:${LC},rc:${RC}
		CYCLESTR=`printf "%05d" ${CYCLE}`
		ID=TPO${CYCLESTR}
		echo ${ID}
		# if exists PXFILE=${TESTFILE}.l${LC}r${RC}_${ID}.sc, skip?
		#SCFILE=${TESTFILE}.l${LC}r${RC}_${ID}.sc
		SCFILE=${TESTFILE}_${ID}.sc
		OUT=output.${ID}
		if [ -e "${SCFILE}" ]
		then
		    echo "${SCFILE} exists. Skipping."
		else
		    echo "#${ID},${TRAINFILE},timbl:'${TIMBL}',lc:${LC},rc:${RC},hpx:${HPX},${PARAMS}" >> ${LOG}
		    echo ${WOPR} -s ${SCRIPT} -p trainfile:${TRAINFILE},rc:${RC},lc:${LC},id:${ID},timbl:${TIMBL},testfile:${TESTFILE},${PARAMS},hpx:${HPX}
		    ${WOPR} -s ${SCRIPT} -p trainset:${TRAINFILE},rc:${RC},lc:${LC},id:${ID},timbl:"${TIMBL}",testset:${TESTFILE},${PARAMS},hpx:${HPX} >>${OUT}
		fi
		CYCLE=$(( $CYCLE + 1 ))
		#
		#reuters.martin.tok.1000.l3r2_ALG023.sc
		echo "perl ${SCSCRIPT} -s ${SCFILE} -o ${ORIGTESTFILE}.l${LC}r${RC}" >> ${OUT}
		BLOB=`perl ${SCSCRIPT} -s ${SCFILE} -o ${ORIGTESTFILE}.l${LC}r${RC}`
		#http://wiki.bash-hackers.org/commands/builtin/printf
		#printf -v S "%s %s" ${ID} ${BLOB}
		TSTR=l${LC}r${RC}_"${TIMBL// /}"
		#
		#md5sum testfile
		MD5=`md5sum ${TESTFILE}.l${LC}r${RC} | awk {'print $1'}`
		#blob: lines errors good_sugg bad_sugg wrong_sugg no_sugg
		echo "${ID} ${LINES} ${BLOB} ${TSTR} ${HPX} ${MD5}" >> ${PLOT}
		#
		# Once more, now in topres_only mode
		#
		echo "perl ${SCSCRIPT} -s ${SCFILE} -o ${ORIGTESTFILE}.l${LC}r${RC} -t" >> ${OUT}
		BLOB=`perl ${SCSCRIPT} -s ${SCFILE} -o ${ORIGTESTFILE}.l${LC}r${RC} -t`
		echo "${ID} ${LINES} ${BLOB} ${TSTR} ${HPX} ${MD5} TOP1" >> ${PLOT}
	    done
	    echo "RM ${TESTFILE}.l${LC}r${RC}"  #and ORIGTESTFILE?
	done
    done
    echo "RM ${TRAINFILE}"
done

