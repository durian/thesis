#
# plot learning curve over max_distr, igtree
# the data is combined from:
#   perl ~/uvt/wopr/etc/plot_typostats_to_latex.pl -n 'CFNE.31600'  -b -c -C -t
# where the max_entropy is added by hand as last column, taken from the READMECF
# files.
#
set style line 1 lt -1 lw 0.5 pi -1 pt 7 ps 0.4
set style line 2 lt -1 lw 0.5 pi -1 pt 6
set style line 3 lt -1 lw 0.5 pi -1 pt 7
set style line 4 lt 2 lw 0.5 pi -1 pt 7 ps 0.4
set title ""
set xlabel "maximum entropy"
set key bottom
set ylabel "F-score"
#set grid
set border 3
set xtics out nomirror
set ytics out nomirror
plot [][0:18] "CFNE.31600_a4.gnuplot_data" using 14:10 with lp ls 2 t "triblt2"
set terminal push
#set terminal postscript eps enhanced color solid rounded lw 2 'Helvetica' 10
set terminal postscript eps enhanced dashed rounded lw 2 'Helvetica' 10
set out 'CFNE_31600_a4.ps'
replot
!epstopdf 'CFNE_31600_a4.ps'
set term pop