set title "NYT, different contexts, igtree, correct guess %"
set xlabel "lines of data"
set key bottom
set logscale x
set ylabel "correct guess %"
set grid
#
plot [1000:][10:20]\
"<grep 'l2r0_-a1+D' DATA.BTS.20000.plot" using 2:3 w lp t "l2r0",\
"<grep 'l4r0_-a1+D' DATA.BTS.20000.plot" using 2:3 w lp t "l4r0",\
"<grep 'l8r0_-a1+D' DATA.BTS.20000.plot" using 2:3 w lp t "l8r0"
#
set terminal push
set terminal postscript eps enhanced color solid rounded lw 2 'Helvetica' 10
set out 'bts_CTX_igt_cg.ps'
replot
!epstopdf 'bts_CTX_igt_cg.ps'
set term pop
