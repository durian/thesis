#!/bin/sh
#
# Print info on DATA.___.plot files
#
PLOT=$1
#TYP=`cut -c1-2 ${PLOT}`
TYP=${PLOT:5:2} #NB no directory paths here
#
if [ "$TYP" == "AL" -o "$TYP" == "" -o "$TYP" == "MR" ]
then
    ALGOS=`cut -d' ' -f 13 ${PLOT}  | cut -c6- | sort -u`
    LCS=`cut -d' ' -f 13 ${PLOT}  | cut -c2 | sort -u`
    RCS=`cut -d' ' -f 13 ${PLOT}  | cut -c4 | sort -u`
	LINES=`cut -d' ' -f 2 ${PLOT}  | sort -n -u`
elif [ "$TYP" == "HP" ]
then
    ALGOS=`cut -d' ' -f 13 ${PLOT}  | cut -c6- | sort -u`
    LCS=`cut -d' ' -f 13 ${PLOT}  | cut -c2 | sort -u`
    RCS=`cut -d' ' -f 13 ${PLOT}  | cut -c4 | sort -u`
    HPX=`cut -d' ' -f 14 ${PLOT}  | sort -n -u`
	LINES=`cut -d' ' -f 2 ${PLOT}  | sort -n -u`
else #SC or DT
    ALGOS=`cut -d' ' -f 13 ${PLOT}  | cut -c6- | sort -u`
    LCS=`cut -d' ' -f 13 ${PLOT}  | cut -c2 | sort -u`
    RCS=`cut -d' ' -f 13 ${PLOT}  | cut -c4 | sort -u`
	LINES=`cut -d' ' -f 2 ${PLOT}  | sort -n -u`
fi
#
# Determine algos used from file.
# Determine the LC and RC used.
# Format:
# ALG1000 1000 9.67 42.99 47.34 188.67 188.67 0.188 1.000 0.337 12 22 l1r0_-a1+D
# ALG50004 1000 10.93 29.60 59.47 214.49 214.49 0.203 1.000 0.418 1269.02 6118.77 l3r0_-a1+D_hpx0
# SC10024 5000 22829 1785 21 12 1 1763 1.18 0.67 0.06 98.77 l2r0_-a1+D
#
#
echo "LINES" ${LINES}
echo "TIMBL" ${ALGOS}
echo "LCS  " ${LCS}
echo "RCS  " ${RCS}
echo "HPX  " ${HPX}
