hapaxing experiments on ceto, march/april 2013

grep 000000.l2r0 ibases.size.txt | cut -c33-42,56-
for l in 1000 10000; do echo $l;grep $l.l2r0 ibases.size.txt | cut -c33-42,56- ;done

for l in 1000 5000 10000 50000 100000 500000 1000000; do echo $l;grep $l.l2r0 ibases.size.txt | grep a1 | grep hpx1 | cut -c33-42,56- ;done

vi hapax.hpx(...).data files, concatenate lines with J

gnuplot> plot "hapax.hpx1.data" using 1:2 w lp,"hapax.hpx5.data" using 1:2 w lp,"hapax.hpx10.data" using 1:2 w lp

10000: no hapaxing (-a1 +D)
20000: hpx  1 (-a1 +D)
30000: hpx  1 (-a4 +D)
40000: hpx 10 (-a1 +D)
50000: hpx 50 (-a1 +D)

for l in 1000 5000 10000 50000 100000 500000 1000000; do echo $l;grep $l.l2r0 ibases.size.txt | grep a1 | grep hpx10_ | cut -c33-42,56- ;done >hapax.hpx10.data

for l in 1000 5000 10000 50000 100000 500000 1000000; do echo $l;grep $l.l2r0 ibases.size.txt | grep a1 | grep hpx5_ | cut -c33-42,56- ;done >hapax.hpx5.data

for l in 1000 5000 10000 50000 100000 500000 1000000; do echo $l;grep $l.l2r0 ibases.size.txt | grep a1 | grep hpx1_ | cut -c33-42,56- ;done >hapax.hpx1.data

// ---- 2013-04-25 09:33:36 Thu -----------------------------------------------

This turned out te be no good. New ones 66000 and 76000, with hapaxed test set,
1000000 lines of nyt (66000) and OpenSub (76000), both l2r0 l2r2 and igtree and
tribl2

See below

HPX66036 1000000 22.91 49.95 27.14 175.08 175.08 0.190 1.000 0.445 8000.55 364409.19 l2r0_-a1+D 0
HPX66037 1000000 36.59 27.36 36.06 96.20 96.20 0.260 1.000 0.683 3342.40 246106.35 l2r2_-a1+D 0
HPX66038 1000000 22.94 43.57 33.49 160.04 160.04 0.225 1.000 0.492 2677.20 59919.57 l2r0_-a4+D 0
HPX66039 1000000 38.28 16.92 44.80 94.09 94.09 0.337 1.000 0.797 86.67 854.82 l2r2_-a4+D 0

HPX66000 1000000 23.06 49.67 27.27 167.05 167.05 0.192 1.000 0.448 5489.72 125338.07 l2r0_-a1+D 1
HPX66001 1000000 37.09 26.06 36.85 90.87 90.87 0.272 1.000 0.699 799.32 25041.37 l2r2_-a1+D 1
HPX66002 1000000 23.06 43.17 33.77 154.03 154.03 0.228 1.000 0.497 2348.42 49319.65 l2r0_-a4+D 1
HPX66003 1000000 38.18 16.52 45.30 92.66 92.66 0.343 1.000 0.801 67.30 482.62 l2r2_-a4+D 1

HPX66004 1000000 23.02 50.24 26.73 155.88 155.88 0.189 1.000 0.444 5695.57 118077.68 l2r0_-a1+D 5
HPX66005 1000000 37.34 25.84 36.82 82.62 82.62 0.272 1.000 0.702 657.84 19369.97 l2r2_-a1+D 5
HPX66006 1000000 23.02 43.68 33.30 142.45 142.45 0.225 1.000 0.493 2366.38 48958.12 l2r0_-a4+D 5
HPX66007 1000000 38.26 16.60 45.14 84.57 84.57 0.343 1.000 0.801 67.57 456.38 l2r2_-a4+D 5

HPX66008 1000000 22.92 50.69 26.39 146.91 146.91 0.188 1.000 0.441 5949.54 120370.30 l2r0_-a1+D 10
HPX66009 1000000 37.49 25.55 36.95 76.93 76.93 0.274 1.000 0.706 571.36 16205.10 l2r2_-a1+D 10
HPX66010 1000000 22.92 43.94 33.13 133.71 133.71 0.224 1.000 0.490 2403.21 49012.03 l2r0_-a4+D 10
HPX66011 1000000 38.36 16.56 45.08 78.71 78.71 0.341 1.000 0.801 68.73 451.18 l2r2_-a4+D 10

HPX66012 1000000 22.50 52.44 25.07 130.45 130.45 0.182 1.000 0.428 7070.16 142326.70 l2r0_-a1+D 50
HPX66013 1000000 37.35 25.13 37.52 65.87 65.87 0.281 1.000 0.711 461.13 14022.32 l2r2_-a1+D 50
HPX66014 1000000 22.50 45.53 31.98 117.47 117.47 0.219 1.000 0.477 2615.17 50547.09 l2r0_-a4+D 50
HPX66015 1000000 38.05 16.84 45.12 66.94 66.94 0.347 1.000 0.800 68.61 442.05 l2r2_-a4+D 50

HPX66016 1000000 22.16 54.00 23.84 119.15 119.15 0.177 1.000 0.416 8238.77 175680.96 l2r0_-a1+D 100
HPX66017 1000000 37.18 24.77 38.06 58.35 58.35 0.282 1.000 0.713 414.61 12482.68 l2r2_-a1+D 100
HPX66018 1000000 22.16 47.02 30.82 105.93 105.93 0.212 1.000 0.464 2895.03 53118.44 l2r0_-a4+D 100
HPX66019 1000000 37.74 16.96 45.29 59.13 59.13 0.343 1.000 0.796 68.87 456.30 l2r2_-a4+D 100

HPX66020 1000000 20.15 60.30 19.55 95.64 95.64 0.156 1.000 0.368 14021.96 411767.39 l2r0_-a1+D 500
HPX66021 1000000 35.81 25.07 39.12 40.70 40.70 0.277 1.000 0.702 424.31 10789.81 l2r2_-a1+D 500
HPX66022 1000000 20.15 53.82 26.03 83.45 83.45 0.185 1.000 0.407 4800.49 80595.02 l2r0_-a4+D 500
HPX66023 1000000 36.17 18.32 45.52 40.73 40.73 0.324 1.000 0.773 103.05 756.90 l2r2_-a4+D 500

HPX66024 1000000 18.86 64.50 16.64 82.85 82.85 0.144 1.000 0.337 19491.08 692611.47 l2r0_-a1+D 1000
HPX66025 1000000 34.71 26.37 38.91 31.74 31.74 0.266 1.000 0.683 570.88 12988.52 l2r2_-a1+D 1000
HPX66026 1000000 18.86 58.68 22.46 71.37 71.37 0.168 1.000 0.370 6949.71 125356.97 l2r0_-a4+D 1000
HPX66027 1000000 34.95 20.73 44.32 31.16 31.16 0.301 1.000 0.740 174.57 1514.89 l2r2_-a4+D 1000

HPX66028 1000000 14.83 76.68 8.48 52.51 52.51 0.110 1.000 0.254 44123.96 2449195.76 l2r0_-a1+D 5000
HPX66029 1000000 27.82 42.60 29.58 16.47 16.47 0.189 1.000 0.510 2782.57 63452.66 l2r2_-a1+D 5000
HPX66030 1000000 14.83 73.52 11.65 47.13 47.13 0.120 1.000 0.268 20563.33 618694.81 l2r0_-a4+D 5000
HPX66031 1000000 27.84 38.39 33.77 15.23 15.23 0.208 1.000 0.541 1346.82 19857.84 l2r2_-a4+D 5000

HPX66032 1000000 13.27 80.53 6.21 41.27 41.27 0.100 1.000 0.228 58324.32 3685256.98 l2r0_-a1+D 10000
HPX66033 1000000 24.61 52.44 22.95 13.61 13.61 0.150 1.000 0.421 6066.59 161556.43 l2r2_-a1+D 10000
HPX66034 1000000 13.27 78.13 8.60 37.89 37.89 0.108 1.000 0.237 30039.95 1116944.70 l2r0_-a4+D 10000
HPX66035 1000000 24.61 48.61 26.77 12.45 12.45 0.167 1.000 0.447 3074.93 56184.92 l2r2_-a4+D 10000

and

HPX76036 1000000 22.99 52.41 24.61 110.20 110.20 0.166 1.000 0.421 4441.35 234430.59 l2r0_-a1+D 0
HPX76037 1000000 36.78 29.34 33.88 57.08 57.08 0.278 1.000 0.680 1843.24 139315.43 l2r2_-a1+D 0
HPX76038 1000000 22.94 47.52 29.54 105.50 105.50 0.183 1.000 0.449 2332.32 95059.38 l2r0_-a4+D 0
HPX76039 1000000 37.03 21.73 41.24 57.54 57.54 0.321 1.000 0.749 127.48 3068.11 l2r2_-a4+D 0

HPX76000 1000000 23.01 51.89 25.10 105.73 105.73 0.168 1.000 0.423 3432.49 157851.51 l2r0_-a1+D 1
HPX76001 1000000 37.06 27.75 35.19 53.97 53.97 0.290 1.000 0.696 460.95 25928.14 l2r2_-a1+D 1
HPX76002 1000000 23.01 46.99 30.00 101.33 101.33 0.186 1.000 0.453 2120.98 84619.20 l2r0_-a4+D 1
HPX76003 1000000 37.08 21.14 41.77 55.31 55.31 0.330 1.000 0.757 85.59 1652.44 l2r2_-a4+D 1

HPX76004 1000000 22.95 52.22 24.83 91.49 91.49 0.168 1.000 0.422 2987.73 116248.56 l2r0_-a1+D 5
HPX76005 1000000 37.16 27.45 35.39 45.74 45.74 0.293 1.000 0.700 409.26 21384.52 l2r2_-a1+D 5
HPX76006 1000000 22.95 47.76 29.29 87.49 87.49 0.187 1.000 0.451 2120.07 84126.15 l2r0_-a4+D 5
HPX76007 1000000 37.16 20.88 41.96 47.10 47.10 0.333 1.000 0.760 84.09 1556.84 l2r2_-a4+D 5

HPX76008 1000000 22.91 52.67 24.42 84.52 84.52 0.168 1.000 0.420 3004.42 108351.69 l2r0_-a1+D 10
HPX76009 1000000 37.26 27.60 35.14 41.61 41.61 0.293 1.000 0.699 373.44 17724.77 l2r2_-a1+D 10
HPX76010 1000000 22.91 48.14 28.95 80.59 80.59 0.187 1.000 0.449 2125.51 83877.85 l2r0_-a4+D 10
HPX76011 1000000 37.17 21.13 41.70 42.79 42.79 0.333 1.000 0.758 83.47 1513.33 l2r2_-a4+D 10

HPX76012 1000000 23.00 54.49 22.51 67.00 67.00 0.165 1.000 0.413 3554.45 116921.41 l2r0_-a1+D 50
HPX76013 1000000 37.51 27.42 35.06 31.49 31.49 0.294 1.000 0.702 293.09 11623.93 l2r2_-a1+D 50
HPX76014 1000000 23.00 49.70 27.30 63.44 63.44 0.183 1.000 0.442 2195.17 84330.87 l2r0_-a4+D 50
HPX76015 1000000 37.29 21.51 41.20 32.35 32.35 0.333 1.000 0.756 84.59 1540.06 l2r2_-a4+D 50

HPX76016 1000000 23.33 55.05 21.62 60.37 60.37 0.161 1.000 0.411 3744.20 120903.42 l2r0_-a1+D 100
HPX76017 1000000 37.58 27.65 34.77 27.65 27.65 0.293 1.000 0.700 295.89 11934.87 l2r2_-a1+D 100
HPX76018 1000000 23.33 50.18 26.49 56.72 56.72 0.181 1.000 0.441 2244.20 84711.50 l2r0_-a4+D 100
HPX76019 1000000 37.39 21.98 40.63 28.27 28.27 0.329 1.000 0.751 90.08 1616.48 l2r2_-a4+D 100

HPX76020 1000000 23.10 59.30 17.60 43.14 43.14 0.149 1.000 0.387 5453.87 181425.14 l2r0_-a1+D 500
HPX76021 1000000 36.86 29.65 33.50 17.36 17.36 0.287 1.000 0.682 436.87 21141.36 l2r2_-a1+D 500
HPX76022 1000000 23.10 54.10 22.80 39.58 39.58 0.168 1.000 0.417 2658.87 90993.82 l2r0_-a4+D 500
HPX76023 1000000 36.78 24.43 38.79 17.46 17.46 0.315 1.000 0.727 142.56 2559.36 l2r2_-a4+D 500

HPX76024 1000000 22.58 61.63 15.79 38.12 38.12 0.143 1.000 0.373 6481.96 227391.23 l2r0_-a1+D 1000
HPX76025 1000000 36.33 31.01 32.66 14.69 14.69 0.273 1.000 0.665 576.76 29185.41 l2r2_-a1+D 1000
HPX76026 1000000 22.58 56.73 20.69 34.71 34.71 0.161 1.000 0.400 2953.90 96548.73 l2r0_-a4+D 1000
HPX76027 1000000 36.29 26.13 37.58 14.64 14.64 0.297 1.000 0.706 183.04 3425.48 l2r2_-a4+D 1000

HPX76028 1000000 21.22 68.40 10.38 28.20 28.20 0.121 1.000 0.329 10784.11 468769.21 l2r0_-a1+D 5000
HPX76029 1000000 32.66 40.75 26.59 10.24 10.24 0.220 1.000 0.567 1303.40 67695.03 l2r2_-a1+D 5000
HPX76030 1000000 21.22 64.62 14.16 25.86 25.86 0.134 1.000 0.348 4604.86 139532.64 l2r0_-a4+D 5000
HPX76031 1000000 32.73 36.69 30.58 9.85 9.85 0.237 1.000 0.597 477.17 10963.14 l2r2_-a4+D 5000

HPX76032 1000000 19.94 72.16 7.89 29.01 29.01 0.106 1.000 0.300 17708.29 944282.58 l2r0_-a1+D 10000
HPX76033 1000000 30.10 48.26 21.64 9.37 9.37 0.191 1.000 0.502 2208.19 99709.69 l2r2_-a1+D 10000
HPX76034 1000000 19.94 69.14 10.91 23.17 23.17 0.119 1.000 0.316 6561.06 205665.11 l2r0_-a4+D 10000
HPX76035 1000000 30.09 45.09 24.82 8.79 8.79 0.209 1.000 0.525 876.45 20302.41 l2r2_-a4+D 10000

TIME AND SIZE:

egrep "(Learning took|Nodes)" output.HPX660*

output.HPX66000:Size of InstanceBase = 2455986 Nodes, (98239440 bytes), 73.15 % compression
output.HPX66000:Learning took 396 seconds, 444 milliseconds and 606 microseconds
output.HPX66001:Size of InstanceBase = 10124597 Nodes, (404983880 bytes), 84.98 % compression
output.HPX66001:Learning took 555 seconds, 408 milliseconds and 701 microseconds
output.HPX66002:Size of InstanceBase = 6249890 Nodes, (249995600 bytes), 31.66 % compression
output.HPX66002:Learning took 351 seconds, 345 milliseconds and 60 microseconds
output.HPX66002:Size of InstanceBase = 6249890 Nodes, (249995600 bytes), 31.66 % compression
output.HPX66003:Size of InstanceBase = 41449278 Nodes, (1657971120 bytes), 38.52 % compression
output.HPX66003:Learning took 581 seconds, 844 milliseconds and 741 microseconds
output.HPX66003:Size of InstanceBase = 41449278 Nodes, (1657971120 bytes), 38.52 % compression
output.HPX66004:Size of InstanceBase = 2242328 Nodes, (89693120 bytes), 73.29 % compression
output.HPX66004:Learning took 348 seconds, 204 milliseconds and 896 microseconds
output.HPX66005:Size of InstanceBase = 10028689 Nodes, (401147560 bytes), 84.98 % compression
output.HPX66005:Learning took 546 seconds, 254 milliseconds and 149 microseconds
output.HPX66006:Size of InstanceBase = 5673969 Nodes, (226958760 bytes), 32.40 % compression
output.HPX66006:Learning took 338 seconds, 74 milliseconds and 74 microseconds
output.HPX66006:Size of InstanceBase = 5673969 Nodes, (226958760 bytes), 32.40 % compression
output.HPX66007:Size of InstanceBase = 40622972 Nodes, (1624918880 bytes), 39.15 % compression
output.HPX66007:Learning took 538 seconds, 772 milliseconds and 739 microseconds
output.HPX66007:Size of InstanceBase = 40622972 Nodes, (1624918880 bytes), 39.15 % compression
output.HPX66008:Size of InstanceBase = 2102362 Nodes, (84094480 bytes), 73.35 % compression
output.HPX66008:Learning took 355 seconds, 48 milliseconds and 956 microseconds
output.HPX66009:Size of InstanceBase = 9955503 Nodes, (398220120 bytes), 84.96 % compression
output.HPX66009:Learning took 535 seconds, 787 milliseconds and 501 microseconds
output.HPX66010:Size of InstanceBase = 5314963 Nodes, (212598520 bytes), 32.64 % compression
output.HPX66010:Learning took 336 seconds, 998 milliseconds and 307 microseconds
output.HPX66010:Size of InstanceBase = 5314963 Nodes, (212598520 bytes), 32.64 % compression
output.HPX66011:Size of InstanceBase = 40000891 Nodes, (1600035640 bytes), 39.58 % compression
output.HPX66011:Learning took 516 seconds, 995 milliseconds and 192 microseconds
output.HPX66011:Size of InstanceBase = 40000891 Nodes, (1600035640 bytes), 39.58 % compression
output.HPX66012:Size of InstanceBase = 1614243 Nodes, (64569720 bytes), 73.52 % compression
output.HPX66012:Learning took 333 seconds, 544 milliseconds and 884 microseconds
output.HPX66013:Size of InstanceBase = 9576656 Nodes, (383066240 bytes), 84.90 % compression
output.HPX66013:Learning took 525 seconds, 333 milliseconds and 54 microseconds
output.HPX66014:Size of InstanceBase = 4085301 Nodes, (163412040 bytes), 32.98 % compression
output.HPX66014:Learning took 346 seconds, 443 milliseconds and 452 microseconds
output.HPX66014:Size of InstanceBase = 4085301 Nodes, (163412040 bytes), 32.98 % compression
output.HPX66015:Size of InstanceBase = 37218810 Nodes, (1488752400 bytes), 41.32 % compression
output.HPX66015:Learning took 509 seconds, 368 milliseconds and 251 microseconds
output.HPX66015:Size of InstanceBase = 37218810 Nodes, (1488752400 bytes), 41.32 % compression
output.HPX66016:Size of InstanceBase = 1341694 Nodes, (53667760 bytes), 73.57 % compression
output.HPX66016:Learning took 344 seconds, 665 milliseconds and 432 microseconds
output.HPX66017:Size of InstanceBase = 9233175 Nodes, (369327000 bytes), 84.86 % compression
output.HPX66017:Learning took 533 seconds, 989 milliseconds and 771 microseconds
output.HPX66018:Size of InstanceBase = 3398244 Nodes, (135929760 bytes), 33.07 % compression
output.HPX66018:Learning took 338 seconds, 651 milliseconds and 476 microseconds
output.HPX66018:Size of InstanceBase = 3398244 Nodes, (135929760 bytes), 33.07 % compression
output.HPX66019:Size of InstanceBase = 35079451 Nodes, (1403178040 bytes), 42.48 % compression
output.HPX66019:Learning took 478 seconds, 590 milliseconds and 494 microseconds
output.HPX66019:Size of InstanceBase = 35079451 Nodes, (1403178040 bytes), 42.48 % compression
output.HPX66020:Size of InstanceBase = 674629 Nodes, (26985160 bytes), 73.70 % compression
output.HPX66020:Learning took 287 seconds, 682 milliseconds and 67 microseconds
output.HPX66021:Size of InstanceBase = 7539042 Nodes, (301561680 bytes), 84.71 % compression
output.HPX66021:Learning took 441 seconds, 437 milliseconds and 773 microseconds
output.HPX66022:Size of InstanceBase = 1714439 Nodes, (68577560 bytes), 33.17 % compression
output.HPX66022:Learning took 282 seconds, 97 milliseconds and 724 microseconds
output.HPX66022:Size of InstanceBase = 1714439 Nodes, (68577560 bytes), 33.17 % compression
output.HPX66023:Size of InstanceBase = 26614765 Nodes, (1064590600 bytes), 46.01 % compression
output.HPX66023:Learning took 412 seconds, 336 milliseconds and 7 microseconds
output.HPX66023:Size of InstanceBase = 26614765 Nodes, (1064590600 bytes), 46.01 % compression
output.HPX66024:Size of InstanceBase = 413007 Nodes, (16520280 bytes), 73.83 % compression
output.HPX66024:Learning took 272 seconds, 429 milliseconds and 192 microseconds
output.HPX66025:Size of InstanceBase = 6201875 Nodes, (248075000 bytes), 84.62 % compression
output.HPX66025:Learning took 411 seconds, 749 milliseconds and 718 microseconds
output.HPX66026:Size of InstanceBase = 1054364 Nodes, (42174560 bytes), 33.19 % compression
output.HPX66026:Learning took 271 seconds, 702 milliseconds and 189 microseconds
output.HPX66026:Size of InstanceBase = 1054364 Nodes, (42174560 bytes), 33.19 % compression
output.HPX66027:Size of InstanceBase = 21036061 Nodes, (841442440 bytes), 47.84 % compression
output.HPX66027:Learning took 386 seconds, 798 milliseconds and 639 microseconds
output.HPX66027:Size of InstanceBase = 21036061 Nodes, (841442440 bytes), 47.84 % compression
output.HPX66028:Size of InstanceBase = 56029 Nodes, (2241160 bytes), 74.59 % compression
output.HPX66028:Learning took 243 seconds, 763 milliseconds and 617 microseconds
output.HPX66029:Size of InstanceBase = 2228869 Nodes, (89154760 bytes), 84.20 % compression
output.HPX66029:Learning took 323 seconds, 588 milliseconds and 466 microseconds
output.HPX66030:Size of InstanceBase = 147440 Nodes, (5897600 bytes), 33.14 % compression
output.HPX66030:Learning took 241 seconds, 488 milliseconds and 371 microseconds
output.HPX66030:Size of InstanceBase = 147440 Nodes, (5897600 bytes), 33.14 % compression
output.HPX66031:Size of InstanceBase = 6716536 Nodes, (268661440 bytes), 52.38 % compression
output.HPX66031:Learning took 311 seconds, 655 milliseconds and 40 microseconds
output.HPX66031:Size of InstanceBase = 6716536 Nodes, (268661440 bytes), 52.38 % compression
output.HPX66032:Size of InstanceBase = 16238 Nodes, (649520 bytes), 75.25 % compression
output.HPX66032:Learning took 229 seconds, 106 milliseconds and 572 microseconds
output.HPX66033:Size of InstanceBase = 1058973 Nodes, (42358920 bytes), 83.86 % compression
output.HPX66033:Learning took 291 seconds, 278 milliseconds and 362 microseconds
output.HPX66034:Size of InstanceBase = 43934 Nodes, (1757360 bytes), 33.04 % compression
output.HPX66034:Learning took 227 seconds, 406 milliseconds and 422 microseconds
output.HPX66034:Size of InstanceBase = 43934 Nodes, (1757360 bytes), 33.04 % compression
output.HPX66035:Size of InstanceBase = 3020675 Nodes, (120827000 bytes), 53.96 % compression
output.HPX66035:Learning took 283 seconds, 452 milliseconds and 63 microseconds
output.HPX66035:Size of InstanceBase = 3020675 Nodes, (120827000 bytes), 53.96 % compression
output.HPX66037:Size of InstanceBase = 10168765 Nodes, (406750600 bytes), 84.98 % compression
output.HPX66037:Learning took 643 seconds, 743 milliseconds and 629 microseconds
output.HPX66038:Size of InstanceBase = 6631435 Nodes, (265257400 bytes), 30.78 % compression
output.HPX66038:Learning took 371 seconds, 694 milliseconds and 396 microseconds
output.HPX66038:Size of InstanceBase = 6631435 Nodes, (265257400 bytes), 30.78 % compression
output.HPX66039:Size of InstanceBase = 41881743 Nodes, (1675269720 bytes), 38.12 % compression
output.HPX66039:Learning took 607 seconds, 498 milliseconds and 463 microseconds
output.HPX66039:Size of InstanceBase = 41881743 Nodes, (1675269720 bytes), 38.12 % compression

and

output.HPX76000:Size of InstanceBase = 322730 Nodes, (12909200 bytes), 74.46 % compression
output.HPX76000:Learning took 47 seconds, 706 milliseconds and 875 microseconds
output.HPX76001:Size of InstanceBase = 1950052 Nodes, (78002080 bytes), 85.40 % compression
output.HPX76001:Learning took 118 seconds, 408 milliseconds and 87 microseconds
output.HPX76002:Size of InstanceBase = 886186 Nodes, (35447440 bytes), 29.86 % compression
output.HPX76002:Learning took 45 seconds, 67 milliseconds and 174 microseconds
output.HPX76002:Size of InstanceBase = 886186 Nodes, (35447440 bytes), 29.86 % compression
output.HPX76003:Size of InstanceBase = 8129214 Nodes, (325168560 bytes), 39.13 % compression
output.HPX76003:Learning took 116 seconds, 199 milliseconds and 383 microseconds
output.HPX76003:Size of InstanceBase = 8129214 Nodes, (325168560 bytes), 39.13 % compression
output.HPX76004:Size of InstanceBase = 288399 Nodes, (11535960 bytes), 74.64 % compression
output.HPX76004:Learning took 44 seconds, 185 milliseconds and 74 microseconds
output.HPX76005:Size of InstanceBase = 1911737 Nodes, (76469480 bytes), 85.38 % compression
output.HPX76005:Learning took 111 seconds, 963 milliseconds and 850 microseconds
output.HPX76006:Size of InstanceBase = 783458 Nodes, (31338320 bytes), 31.11 % compression
output.HPX76006:Learning took 42 seconds, 548 milliseconds and 319 microseconds
output.HPX76006:Size of InstanceBase = 783458 Nodes, (31338320 bytes), 31.11 % compression
output.HPX76007:Size of InstanceBase = 7837877 Nodes, (313515080 bytes), 40.07 % compression
output.HPX76007:Learning took 112 seconds, 71 milliseconds and 954 microseconds
output.HPX76007:Size of InstanceBase = 7837877 Nodes, (313515080 bytes), 40.07 % compression
output.HPX76008:Size of InstanceBase = 264087 Nodes, (10563480 bytes), 74.78 % compression
output.HPX76008:Learning took 42 seconds, 424 milliseconds and 22 microseconds
output.HPX76009:Size of InstanceBase = 1879330 Nodes, (75173200 bytes), 85.36 % compression
output.HPX76009:Learning took 109 seconds, 371 milliseconds and 38 microseconds
output.HPX76010:Size of InstanceBase = 715562 Nodes, (28622480 bytes), 31.66 % compression
output.HPX76010:Learning took 42 seconds, 430 milliseconds and 835 microseconds
output.HPX76010:Size of InstanceBase = 715562 Nodes, (28622480 bytes), 31.66 % compression
output.HPX76011:Size of InstanceBase = 7616367 Nodes, (304654680 bytes), 40.68 % compression
output.HPX76011:Learning took 103 seconds, 491 milliseconds and 608 microseconds
output.HPX76011:Size of InstanceBase = 7616367 Nodes, (304654680 bytes), 40.68 % compression
output.HPX76012:Size of InstanceBase = 188068 Nodes, (7522720 bytes), 75.18 % compression
output.HPX76012:Learning took 41 seconds, 0 milliseconds and 916 microseconds
output.HPX76013:Size of InstanceBase = 1734786 Nodes, (69391440 bytes), 85.28 % compression
output.HPX76013:Learning took 100 seconds, 252 milliseconds and 780 microseconds
output.HPX76014:Size of InstanceBase = 511264 Nodes, (20450560 bytes), 32.51 % compression
output.HPX76014:Learning took 40 seconds, 303 milliseconds and 637 microseconds
output.HPX76014:Size of InstanceBase = 511264 Nodes, (20450560 bytes), 32.51 % compression
output.HPX76015:Size of InstanceBase = 6752840 Nodes, (270113600 bytes), 42.70 % compression
output.HPX76015:Learning took 98 seconds, 761 milliseconds and 723 microseconds
output.HPX76015:Size of InstanceBase = 6752840 Nodes, (270113600 bytes), 42.70 % compression
output.HPX76016:Size of InstanceBase = 149424 Nodes, (5976960 bytes), 75.32 % compression
output.HPX76016:Learning took 40 seconds, 339 milliseconds and 969 microseconds
output.HPX76017:Size of InstanceBase = 1625092 Nodes, (65003680 bytes), 85.23 % compression
output.HPX76017:Learning took 95 seconds, 968 milliseconds and 421 microseconds
output.HPX76018:Size of InstanceBase = 407284 Nodes, (16291360 bytes), 32.72 % compression
output.HPX76018:Learning took 39 seconds, 385 milliseconds and 96 microseconds
output.HPX76018:Size of InstanceBase = 407284 Nodes, (16291360 bytes), 32.72 % compression
output.HPX76019:Size of InstanceBase = 6170965 Nodes, (246838600 bytes), 43.91 % compression
output.HPX76019:Learning took 98 seconds, 67 milliseconds and 500 microseconds
output.HPX76019:Size of InstanceBase = 6170965 Nodes, (246838600 bytes), 43.91 % compression
output.HPX76020:Size of InstanceBase = 69532 Nodes, (2781280 bytes), 75.33 % compression
output.HPX76020:Learning took 38 seconds, 564 milliseconds and 458 microseconds
output.HPX76021:Size of InstanceBase = 1240179 Nodes, (49607160 bytes), 85.04 % compression
output.HPX76021:Learning took 95 seconds, 113 milliseconds and 536 microseconds
output.HPX76022:Size of InstanceBase = 188944 Nodes, (7557760 bytes), 32.96 % compression
output.HPX76022:Learning took 37 seconds, 477 milliseconds and 596 microseconds
output.HPX76022:Size of InstanceBase = 188944 Nodes, (7557760 bytes), 32.96 % compression
output.HPX76023:Size of InstanceBase = 4374653 Nodes, (174986120 bytes), 47.24 % compression
output.HPX76023:Learning took 87 seconds, 23 milliseconds and 897 microseconds
output.HPX76023:Size of InstanceBase = 4374653 Nodes, (174986120 bytes), 47.24 % compression
output.HPX76024:Size of InstanceBase = 45619 Nodes, (1824760 bytes), 75.29 % compression
output.HPX76024:Learning took 36 seconds, 744 milliseconds and 987 microseconds
output.HPX76025:Size of InstanceBase = 1044655 Nodes, (41786200 bytes), 84.93 % compression
output.HPX76025:Learning took 88 seconds, 198 milliseconds and 733 microseconds
output.HPX76026:Size of InstanceBase = 123716 Nodes, (4948640 bytes), 32.98 % compression
output.HPX76026:Learning took 35 seconds, 726 milliseconds and 864 microseconds
output.HPX76026:Size of InstanceBase = 123716 Nodes, (4948640 bytes), 32.98 % compression
output.HPX76027:Size of InstanceBase = 3554295 Nodes, (142171800 bytes), 48.71 % compression
output.HPX76027:Learning took 85 seconds, 234 milliseconds and 763 microseconds
output.HPX76027:Size of InstanceBase = 3554295 Nodes, (142171800 bytes), 48.71 % compression
output.HPX76028:Size of InstanceBase = 9780 Nodes, (391200 bytes), 75.06 % compression
output.HPX76028:Learning took 34 seconds, 733 milliseconds and 513 microseconds
output.HPX76029:Size of InstanceBase = 464074 Nodes, (18562960 bytes), 84.42 % compression
output.HPX76029:Learning took 80 seconds, 622 milliseconds and 241 microseconds
output.HPX76030:Size of InstanceBase = 26316 Nodes, (1052640 bytes), 32.88 % compression
output.HPX76030:Learning took 33 seconds, 618 milliseconds and 608 microseconds
output.HPX76030:Size of InstanceBase = 26316 Nodes, (1052640 bytes), 32.88 % compression
output.HPX76031:Size of InstanceBase = 1414918 Nodes, (56596720 bytes), 52.49 % compression
output.HPX76031:Learning took 70 seconds, 649 milliseconds and 871 microseconds
output.HPX76031:Size of InstanceBase = 1414918 Nodes, (56596720 bytes), 52.49 % compression
output.HPX76032:Size of InstanceBase = 3807 Nodes, (152280 bytes), 76.23 % compression
output.HPX76032:Learning took 32 seconds, 423 milliseconds and 842 microseconds
output.HPX76033:Size of InstanceBase = 240711 Nodes, (9628440 bytes), 84.17 % compression
output.HPX76033:Learning took 68 seconds, 363 milliseconds and 636 microseconds
output.HPX76034:Size of InstanceBase = 10775 Nodes, (431000 bytes), 32.73 % compression
output.HPX76034:Learning took 30 seconds, 716 milliseconds and 242 microseconds
output.HPX76034:Size of InstanceBase = 10775 Nodes, (431000 bytes), 32.73 % compression
output.HPX76035:Size of InstanceBase = 704640 Nodes, (28185600 bytes), 53.67 % compression
output.HPX76035:Learning took 66 seconds, 29 milliseconds and 52 microseconds
output.HPX76035:Size of InstanceBase = 704640 Nodes, (28185600 bytes), 53.67 % compression
output.HPX76036:Size of InstanceBase = 343047 Nodes, (13721880 bytes), 74.29 % compression
output.HPX76036:Learning took 50 seconds, 276 milliseconds and 804 microseconds
output.HPX76037:Size of InstanceBase = 1965994 Nodes, (78639760 bytes), 85.39 % compression
output.HPX76037:Learning took 132 seconds, 436 milliseconds and 374 microseconds
output.HPX76038:Size of InstanceBase = 950284 Nodes, (38011360 bytes), 28.78 % compression
output.HPX76038:Learning took 49 seconds, 428 milliseconds and 164 microseconds
output.HPX76038:Size of InstanceBase = 950284 Nodes, (38011360 bytes), 28.78 % compression
output.HPX76039:Size of InstanceBase = 8269771 Nodes, (330790840 bytes), 38.54 % compression
output.HPX76039:Learning took 122 seconds, 746 milliseconds and 533 microseconds
output.HPX76039:Size of InstanceBase = 8269771 Nodes, (330790840 bytes), 38.54 % compression

// ---- 2013-06-13 14:43:45 Thu -----------------------------------------------

I wanted more data points, re-ran 66000 with 100000 and 1000000 so the
references to filenames above will be off. Old fiels were deleted.


pberck@ceto:/exp2/pberck/2013/hapax$ grep "l2r0_-a4+D 10$" DATA.HPX.66500.plot
HPX66526 100000 15.12 32.75 52.13 262.16 262.16 0.241 1.000 0.481 823.29 6543.82 l2r0_-a4+D 10
HPX66532 1000000 22.92 43.94 33.13 133.71 133.71 0.224 1.000 0.490 2403.21 49012.03 l2r0_-a4+D 10

etc
