#!/bin/sh
#
# 2013, LC on OS data
#
TRAINBASE="" #Make on the fly
TESTFILE=""  #Make on the fly
#
WOPR="wopr"
SCRIPT="do_osa.wopr_script"
PXSCRIPT="/scratch/pberck/wopr/etc/pplx_px.pl"
SCATTERSCRIPT="/scratch/pberck/wopr/etc/create_cg_f_scatterplot.pl"
#
CYCLE=20000
LOG="README.OSA.${CYCLE}.txt"
PLOT="DATA.OSA.${CYCLE}.plot"
#
BLOB=`${WOPR} | head -n1` #09:30:51.01: Starting wopr 1.35.5
#http://www.arachnoid.com/linux/shell_programming.html
STR=${BLOB#*wopr }
RX='([0123456789.]*)\.([0123456789.]*)'
if [[ "$STR" =~ $RX ]]
then
    WV=${BASH_REMATCH[1]}
    #echo ${WV}
    if [[ "${WV}" < "1.35" ]]
    then
	echo "NO UPTODATE WOPR VERSION."
	exit 1
    fi
fi
#
# Start experiment!
#
date "+#%Y-%m-%d %H:%M:%S" >> ${LOG}
#
for LNG in "en-tr.tr.tok" "en-sv.sv.tok" "en-fi.fi.tok" "en-ru.ru.tok" "de-en.de.tok" "en-nl.en.tok" "en-nl.nl.tok"
do
#
    TRAINBASE="/scratch/pberck/2013/data/OpenSubtitles2011.${LNG}"
    if [ ! -e "${TRAINBASE}" ]
    then
	echo "NO ${TRAINBASE} FOUND."
	exit 1
    fi
TLINES=1000
for LINES in 1000 5000 10000 50000 100000 500000 1000000 5000000 10000000
do
    #Make the dataset here, clean up afterwards?
    TRAINFILE=${TRAINBASE}.${LINES}
    if [ -e "${TRAINFILE}" ]
    then
	echo "Data file ${TRAINFILE} exists."
    else
	echo "Creating data file ${TRAINFILE}."
	head -n ${LINES} ${TRAINBASE} > ${TRAINFILE}
    fi
    # check nr lines
    BLOB=`wc -l ${TRAINFILE}` #85 output.OSA10015
    STR=${BLOB#*wopr }
    RX='([0123456789]*)'
    if [[ "$STR" =~ $RX ]]
    then
	RLINES=${BASH_REMATCH[1]}
	if test $RLINES -ne $LINES
	then
	    echo "********"
            echo "Real number of lines is: ${RLINES}/${LINES}"
	    NEWLINES=$(echo "scale=0; $RLINES-$TLINES" | bc)
	    echo "Making dataset with ${NEWLINES} lines."
	    echo "NB: name is not changed."
	    head -n ${NEWLINES} ${TRAINBASE} > ${TRAINFILE}
	    echo "********"
	fi
    fi
    # Test file.
    TESTFILE=${LNG}.t${TLINES}
    if [ -e "${TESTFILE}" ]
    then
	echo "Data file ${TESTFILE} exists."
    else
	echo "Creating data file ${TESTFILE}."
	tail -n ${TLINES} ${TRAINBASE} > ${TESTFILE}
    fi
    #     
    for TIMBL in "-a4 +D" 
    do
	for LC in 1 2 3 4
	do
	    for RC in 0 1 2 3
	    do
		FREE=`df -k . | tail -1 | awk '{print $3}'`
		# 25 577 264 = 25GB
		while [ $FREE -lt 100000 ]
		do  
		    echo "Space critical, pausing at ${CYCLE}."
		    sleep 60
		    FREE=`df -k . | tail -1 | awk '{print $3}'`
		done
		CYCLESTR=`printf "%05d" ${CYCLE}`
		ID=OSA${CYCLESTR}
		echo ${ID}
		# if exists PXFILE=${TESTFILE}.l${LC}r${RC}_${ID}.px, skip?
		PXFILE=${TESTFILE}.l${LC}r${RC}_${ID}.px
		if [ -e "${PXFILE}" ]
		then
		    echo "${PXFILE} exists. Skipping."
		else
		    echo "#${ID},${TRAINFILE},timbl:'${TIMBL}',lc:${LC},rc:${RC}" >> ${LOG}
		    echo ${WOPR} -s ${SCRIPT} -p trainfile:${TRAINFILE},rc:${RC},lc:${LC},id:${ID},timbl:${TIMBL},testfile:${TESTFILE}
		    ${WOPR} -s ${SCRIPT} -p trainset:${TRAINFILE},rc:${RC},lc:${LC},id:${ID},timbl:"${TIMBL}",testset:${TESTFILE} > output.${ID}
		fi
		CYCLE=$(( $CYCLE + 1 ))
		#
		#reuters.martin.tok.1000.l3r2_ALG023.px
		PXFILE=${TESTFILE}.l${LC}r${RC}_${ID}.px
		WSFILE=${PXFILE}.ws
		LEXFILE=${TRAINFILE}.lex
		perl ${PXSCRIPT} -f ${PXFILE} -l ${LC} -r ${RC} -w > ${WSFILE}
		perl ${SCATTERSCRIPT} -w ${WSFILE} -l ${LEXFILE}
		#http://wiki.bash-hackers.org/commands/builtin/printf
		#printf -v S "%s %s" ${ID} ${PXFILE}
		#echo $S
		adc=0
		ads=0
		cg=0
		cd=0
		ic=0
		mrr_cd=0
		mrr_cg=0
		mrr_gd=0
		pplx=0
		pplx1=0
		#change to incorporate average over dist.
		# 0.96698110  -0.0484  -0.0146  4 1 [00110] .
		# 00110:  5137 ( 22.50%)
		#
		BLOB=`perl ${PXSCRIPT} -f ${PXFILE} -l ${LC} -r ${RC} | tail -n18`
		#http://www.arachnoid.com/linux/shell_programming.html

		# dist_freq sum: 2266012, ave: ( 99.26)
		# dist_sum sum: 44968874, ave: (1969.81)
		STR=${BLOB#*dist_freq sum}
		RX=': ([0123456789]*)\, ave: \(([0123456789. ]*)\)'
		if [[ "$STR" =~ $RX ]]
		then
		    adc=${BASH_REMATCH[2]}
		fi
		#
		STR=${BLOB#*dist_sum sum}
		RX=': (.*)\, ave: \(([0123456789. ]*)\)'
		if [[ "$STR" =~ $RX ]]
		then
		    ads=${BASH_REMATCH[2]}
		fi
		#
		STR=${BLOB#*Column:  2}
		RX='.* \((.*)%\).*3.* \((.*)%\).*4.* \((.*)%\).*'
		if [[ "$STR" =~ $RX ]]
		then
		    cg=${BASH_REMATCH[1]}
		    cd=${BASH_REMATCH[2]}
		    ic=${BASH_REMATCH[3]}
		fi
		#echo ${BLOB}
		STR=${BLOB#*Wopr ppl}
		RX=': (.*) Wopr ppl1: (.*) \(.*'
		#
		STR=${BLOB#*Wopr ppl}
		RX=': (.*) Wopr ppl1: (.*) \(.*'
		if [[ "${STR}" =~ $RX ]]
		then
		    pplx=${BASH_REMATCH[1]}
		    pplx1=${BASH_REMATCH[2]}
		fi
		#
		STR=${BLOB#*RR(cd)}
		#echo $STR
		RX='.* MRR: (.*).*RR\(cg\).* MRR: (.*).*RR\(gd\).* MRR: (.*)'
		if [[ "${STR}" =~ $RX ]]
		then
		    mrr_cd=${BASH_REMATCH[1]}
		    mrr_cg=${BASH_REMATCH[2]}
		    mrr_gd=${BASH_REMATCH[3]}
		fi
		TSTR=l${LC}r${RC}_"${TIMBL// /}"
		printf -v S "%s %s %s %s %s %s %s %s %s %s %s %s %s %s" ${ID} ${RLINES} ${cg} ${cd} ${ic} ${pplx} ${pplx1} ${mrr_cd} ${mrr_cg} ${mrr_gd} ${adc} ${ads} ${TSTR} ${LNG}
		echo ${S} >> ${PLOT}
	    done
	done
    done
done
done

