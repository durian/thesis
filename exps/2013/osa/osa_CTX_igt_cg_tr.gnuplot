set title "Turkish OpenSubtitles, different contexts, igtree, correct guess %"
set xlabel "lines of data"
set key bottom
set logscale x
set ylabel "correct guess %"
set grid
#
plot [1000:][10:40]\
"<grep 'l2r0' DATA.OSA.10000.plot|grep 'en-tr'" using 2:3 w lp t "l2r0",\
"<grep 'l3r0' DATA.OSA.10000.plot|grep 'en-tr'" using 2:3 w lp t "l3r0",\
"<grep 'l4r0' DATA.OSA.10000.plot|grep 'en-tr'" using 2:3 w lp t "l4r0"
#
set terminal push
set terminal postscript eps enhanced color solid rounded lw 2 'Helvetica' 10
set out 'osa_CTX_igt_cg_tr.ps'
replot
!epstopdf 'osa_CTX_igt_cg_tr.ps'
set term pop
