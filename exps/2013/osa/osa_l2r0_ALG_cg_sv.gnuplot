set title "Different Algorithms OpenSubtitles Swedish, l2r0, correct classification %"
set xlabel "lines of data"
set key bottom
set logscale x
set ylabel "correct classification %"
set grid
#
plot [1000:][10:30]\
"<grep 'l2r0' DATA.OSA.10000.plot|grep 'en-sv.sv'" using 2:3 w lp t "l2 igtree",\
"<grep 'l2r0' DATA.OSA.20000.plot|grep 'en-sv.sv'" using 2:3 w lp t "l2 tribl2",\
"<grep 'l2r0' DATA.OSA.30000.plot|grep 'en-sv.sv'" using 2:3 w lp t "l2 ib1"
#
set terminal push
set terminal postscript eps enhanced color solid rounded lw 2 'Helvetica' 10
set out 'osa_l2r0_ALG_cg_sv.ps'
replot
!epstopdf 'osa_l2r0_ALG_cg_sv.ps'
set term pop
