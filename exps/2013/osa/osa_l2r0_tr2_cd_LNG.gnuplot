set title "Different Languages OpenSubtitles, l2r0, tribl2, correct distribution %"
set xlabel "lines of data"
set key bottom
set logscale x
set ylabel "correct distribution %"
set grid
#
plot [1000:][10:60]\
"<grep 'l2r0' DATA.OSA.20000.plot|grep 'en-tr.tr'" using 2:4 w lp t "Turkish",\
"<grep 'l2r0' DATA.OSA.20000.plot|grep 'en-sv.sv'" using 2:4 w lp t "Swedish",\
"<grep 'l2r0' DATA.OSA.20000.plot|grep 'en-fi.fi'" using 2:4 w lp t "Finnish",\
"<grep 'l2r0' DATA.OSA.20000.plot|grep 'en-ru.ru'" using 2:4 w lp t "Russian",\
"<grep 'l2r0' DATA.OSA.20000.plot|grep 'en-nl.en'" using 2:4 w lp t "English",\
"<grep 'l2r0' DATA.OSA.20000.plot|grep 'de-en.de'" using 2:4 w lp t "German",\
"<grep 'l2r0' DATA.OSA.20000.plot|grep 'en-nl.nl'" using 2:4 w lp t "Dutch"
#
set terminal push
set terminal postscript eps enhanced color solid rounded lw 2 'Helvetica' 10
set out 'osa_l2r0_tr2_cd_LNG.ps'
replot
!epstopdf 'osa_l2r0_tr2_cd_LNG.ps'
set term pop
