#!/usr/bin/env bash
#
if test $# -lt 1
then
  echo "Supply SHOW or RM"
  exit
fi
#
ACTION=$1 
FG="*.kvs"
if test $# -eq 2
then
    FG=$2
fi

#
for file in ${FG};
do
    echo $file
    IN=`cat ${file}`
    arr=$(echo $IN | tr " " "^" | tr "," "\n")

    for x in $arr
    do
	#echo $x
	k=`echo ${x} | awk -F: '{print $1}'`
	v=`echo ${x} | awk -F: '{print $2}'`
	if [ "$k" = "ibasefile" ]
	then
	    #echo $k
	    ls -s $v*
	    if [ "$ACTION" = "RM" ]
	    then
		rm $v*
	    fi
	    #ibasefile:utexas.1000000.cf02.100000.l2t1r2_-a1+D.ibase
	    #p0=`echo ${v} | awk -F_ '{print $1}'` #first part 
	    #ls -s $p0
	    #rm $p0
	fi
	if [ "$k" = "OFF_trainset" ]
	then
	    #echo $k
	    ls -s $v
	    rm $v
	fi
	if [ "$k" = "OFF_testset" ]
	then
	    #echo $k
	    ls -s $v
	    rm $v
	fi
	if [ "$k" = "OFF_filename" ]
	then
	    #echo $k
	    ls -s $v
	    rm $v
	fi
    done
done