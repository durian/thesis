#!/usr/bin/env bash
#

for file in "*.kvs";
do
    IN=`cat ${file}`
    arr=$(echo $IN | tr " " "^" | tr "," "\n")

    for x in $arr
    do
	k=`echo ${x} | awk -F: '{print $1}'`
	v=`echo ${x} | awk -F: '{print $2}'`
	if [ "$k" = "ibasefile" ]
	then
	    #echo $k
	    ls -s $v
	    echo rm $v | tr "^" " "
	fi
    done
done