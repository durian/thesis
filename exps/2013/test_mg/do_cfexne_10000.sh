#!/bin/sh
#
TRAINBASE="utexas.1000000.cf03"   #with errors
ORIGBASE="utexas.1000000"         #w/o errors
ORIGTESTFILE="utexas_iit.tail1000"
TESTFILE="utexas_iit.tail1000.cf03"
#
WOPR="/exp2/pberck/wopr/wopr"
SCRIPT="do_spcerr.wopr_script"
SCSCRIPT="/exp2/pberck/wopr/etc/check_spelerr.pl"
#
ME="$(basename "$(test -L "$0" && readlink "$0" || echo "$0")")"
CYCLE=`echo ${ME} | tr -dc "0-9"`
I=CFEXNE #confusibles, expert, no-errors
#
LOG="README.${I}.${CYCLE}.txt"
PLOT="DATA.${I}.${CYCLE}.plot"
STAT="STAT.${I}.${CYCLE}.data"
#
# Do spelling correction
#
if [ ! -e "${ORIGTESTFILE}" ]
then
    echo "${ORIGTESTFILE} nor found, aborting."
    exit 1
fi
if [ ! -e "${TESTFILE}" ]
then
    echo "${TESTFILE} nor found, aborting."
    exit 1
fi
#
date "+#%Y-%m-%d %H:%M:%S" >> ${LOG}
#
# it:1 is hardcoded everywhere
PARAMS="mwl:1,mld:10,max_ent:5,max_distr:100,min_ratio:0,max_tf:1000000"
#
for LINES in 100000 200000 500000 654000 1000000 #1565000
do
    #Make the datasets here.
    TRAINFILE=${TRAINBASE}.${LINES}
    if [ -e "${TRAINFILE}" ]
    then
	echo "Data file ${TRAINFILE} exists."
    else
	echo "Creating data file ${TRAINFILE}."
	head -n ${LINES} ${TRAINBASE} > ${TRAINFILE}
    fi
    ORIGFILE=${ORIGBASE}.${LINES}
    if [ -e "${ORIGFILE}" ]
    then
	echo "Data file ${ORIGFILE} exists."
    else
	echo "Creating data file ${ORIGFILE}."
	head -n ${LINES} ${ORIGBASE} > ${ORIGFILE}
    fi
    for TIMBL in "-a1 +D" #"-a4 +D" 
    do
	for LC in 2 #4
	do
	    for RC in 0 #2 4
	    do
		FREE=`df -k . | tail -1 | awk '{print $3}'`
		# 25 577 264 = 25GB
		while [ $FREE -lt 10000000 ]
		do  
		    echo "Space critical, pausing at ${CYCLE}."
		    sleep 60
		    FREE=`df -k . | tail -1 | awk '{print $3}'`
		done
		#
		CYCLESTR=`printf "%05d" ${CYCLE}`
		ID=${I}${CYCLESTR}
		echo ${ID}
		KVS="${ID}.kvs"
		OUT=output.${ID}
		#
		# Window train and testset(s)
		${WOPR} -l -r window_lr -p filename:${ORIGTESTFILE},lc:${LC},rc:${RC},it:1
		${WOPR} -l -r window_lr -p filename:${TESTFILE},lc:${LC},rc:${RC},it:1  	
		${WOPR} -l -r window_lr -p filename:${TRAINFILE},lc:${LC},rc:${RC},it:1,targetfile:${ORIGFILE}
		#
		# if exists, skip? Note the extra t1
		SCFILE=${TESTFILE}.l${LC}t1r${RC}_${ID}.sc
		if [ -e "${SCFILE}" ]
		then
		    echo "${SCFILE} exists. Skipping."
		else
		    python /exp2/pberck/wopr/etc/split_data_confusibles.py -f ${TRAINFILE}.l${LC}t1r${RC} -c goldingroth.txt -t "${TIMBL}" -i ${ID} >> ${OUT}
		    
		    #utexas.1000000.cf03.100000.l2t1r0_EXP01.cs0
		    bash make_ibases_${ID}.sh >> ${OUT}

		    ${WOPR} -l -r mcorrect -p configfile:configfile_${ID}.txt,filename:${TESTFILE}.l${LC}t1r${RC},timbl:"${TIMBL}",mld:10,id:${ID},${PARAMS},kvsfile:${KVS} >> ${OUT}
		    
		    #perl /exp2/pberck/wopr/etc/check_spelerr.pl -s utexas_iit.tail1000.cf03.l2r0_EXP01.sc -o utexas_iit.tail1000.l2r0 -t -a
		fi
		CYCLE=$(( $CYCLE + 1 ))
		#
		#reuters.martin.tok.1000.l3t1r2_ALG023.sc
		echo "perl ${SCSCRIPT} -s ${SCFILE} -o ${ORIGTESTFILE}.l${LC}t1r${RC}" >> ${OUT}
		BLOB=`perl ${SCSCRIPT} -s ${SCFILE} -o ${ORIGTESTFILE}.l${LC}t1r${RC}`
		#http://wiki.bash-hackers.org/commands/builtin/printf
		#printf -v S "%s %s" ${ID} ${BLOB}
		TSTR=l${LC}t1r${RC}_"${TIMBL// /}"
		#
		#md5sum testfile
		MD5=${TESTFILE}.l${LC}t1r${RC}
		#blob: lines errors good_sugg bad_sugg wrong_sugg no_sugg
		echo "${ID} ${LINES} ${BLOB} ${TSTR} 0,i1:1,${PARAMS} ${MD5}" >> ${PLOT}
		#
		# Precision, recall, accuracy
		echo "perl ${SCSCRIPT} -s ${SCFILE} -o ${ORIGTESTFILE}.l${LC}t1r${RC} -a" >> ${OUT}
                BLOB=`perl ${SCSCRIPT} -s ${SCFILE} -o ${ORIGTESTFILE}.l${LC}t1r${RC} -a`
		echo "${ID} ${LINES} ${TSTR} ${BLOB}" >> ${STAT}
		# Once more, now in topres_only mode
		#
		echo "perl ${SCSCRIPT} -s ${SCFILE} -o ${ORIGTESTFILE}.l${LC}t1r${RC} -t" >> ${OUT}
		BLOB=`perl ${SCSCRIPT} -s ${SCFILE} -o ${ORIGTESTFILE}.l${LC}t1r${RC} -t`
		echo "${ID} ${LINES} ${BLOB} ${TSTR} 0,it:1,${PARAMS} ${MD5} TOP1" >> ${PLOT}
		# Precision, recall, accuracy
		echo "perl ${SCSCRIPT} -s ${SCFILE} -o ${ORIGTESTFILE}.l${LC}t1r${RC} -t -a" >> ${OUT}
                BLOB=`perl ${SCSCRIPT} -s ${SCFILE} -o ${ORIGTESTFILE}.l${LC}t1r${RC} -t -a`
		echo "${ID} ${LINES} ${TSTR} ${BLOB} TOP1" >> ${STAT}
		
	    done
	    #rm ${TESTFILE}.l${LC}r${RC}  #and ORIGTESTFILE?
	done
    done
    #rm ${TRAINFILE}
done

