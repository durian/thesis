set title "scatter"
set xlabel "frequency"
set logscale x
set key bottom
set ylabel "cg"
set grid
plot [][] "nyt.tail100000.l2r0_ALG1022.px.ws.data" using 5:2
set terminal push
set terminal postscript eps enhanced color solid rounded lw 2 'Helvetica' 10
set out 'nyt.tail100000.l2r0_ALG1022.px.ws.ps'
replot
!epstopdf 'nyt.tail100000.l2r0_ALG1022.px.ws.ps'
set term pop
